export default class WebWorkerEnabler {
  constructor (worker) {
    // console.log(worker)
    const code = worker.toString()
    // code = code.substring(code.indexOf('{') + 1, code.lastIndexOf('}'))
    const blob = new Blob(['(' + code + ')()'])
    return new Worker(URL.createObjectURL(blob))

    // const blob = new Blob([code], { type: 'application/javascript' })
    // return new Worker(URL.createObjectURL(blob))
  }
}
