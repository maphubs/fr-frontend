# Forest Report

Forest Report is a tool for monitoring forests and creating reports, learn more at https://www.forest.report/

## Forest Report - Front End

The is the front end web application for Forest Report.


### Installation


#### Dependencies

* Forest Report API and jobs service (https://github.com/forestreport/fr-api)
* MapHubs Raster Upload API (https://github.com/maphubs/raster-upload)
* MapHubs Asset Upload API (https://github.com/maphubs/asset-upload)
* AWS Lambda Raster Stats API (https://github.com/forestreport/)
* AWS Lambda Raster Tiles API (https://github.com/forestreport/)
* FR Fire API (https://github.com/forestreport/)
* FR GLAD Updates (https://github.com/forestreport/)
* Sentinel Tiler (https://github.com/mapbox/sentinel-tiler)
* Forest Report App (https://github.com/forestreport/forest-report-app)

From 3rd parties:

* an Auth0 account (required)
* a Mapbox account (required)
* an Amazon AWS account (required)
* Bing Maps key (optional)
* Google Earth Engine key (optional)


#### Configuration

See `config.js` for a list of environment variables FR front will expect to find. 

For development these can be placed in a `.env` file. 


#### Running the Front End

FR is normally run using Docker, you can find builds at https://quay.io/repository/maphubs/fr-frontend?tab=tags

[![Docker Repository on Quay](https://quay.io/repository/maphubs/fr-frontend/status "Docker Repository on Quay")](https://quay.io/repository/maphubs/fr-frontend)



### Development

```
npm install
docker-compose up -d redis
npm run dev
```


## License

Copyright (C) 2020 MapHubs, Incorportated

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

see License.txt for full text.