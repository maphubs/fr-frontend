import en from './en.json'
import fr from './fr.json'
import es from './es.json'
import pt from './pt.json'
import id from './id.json'

const supportedLangs = {
  en,
  fr,
  es,
  pt,
  id
}

export default {
  en,
  fr,
  es,
  pt,
  id,
  getLocaleString: (locale, text) => {
    if (supportedLangs[locale]) {
      if (supportedLangs[locale][text]) {
        return supportedLangs[locale][text]
      } else {
        console.log(`missing ${locale} translation for: ${text}`)
      }
    }
    return text
  }
}
