// @flow
import * as React from 'react'
import { Query } from 'urql'

type Props = {
  query: any,
  children: any,
  onComplete?: Function,
  onError?: Function
}
export default class extends React.Component<Props, void> {
  render () {
    const { query, children, onComplete, onError } = this.props
    return (
      <Query query={query}>
        {({fetching, error, data}) => {
          if (error) {
            if (onError) {
              onError(error)
            }
            let message = 'Server Error'
            if (error.graphQLErrors && error.graphQLErrors.length > 0) {
              message = error.graphQLErrors[0].message
            } else if (error.message) {
              message = error.message
            }
            console.error(message)
            return (
              <div style={styles.error}>
                <div>
                  <style dangerouslySetInnerHTML={{ __html: 'body { margin: 0 }' }} />
                  <h1 style={styles.h1}>Error</h1>
                  <div style={styles.desc}>
                    <h2 style={styles.h2}>{message}</h2>
                  </div>
                </div>
              </div>
            )
          } else {
            if (onComplete) {
              onComplete(data)
            }
            return children({ fetching, error, data })
          }
        }}
      </Query>
    )
  }
}

const styles = {
  error: {
    color: '#000',
    background: '#fff',
    fontFamily:
      '-apple-system, BlinkMacSystemFont, Roboto, "Segoe UI", "Fira Sans", Avenir, "Helvetica Neue", "Lucida Grande", sans-serif',
    height: '100%',
    textAlign: 'center',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center'
  },

  desc: {
    display: 'inline-block',
    textAlign: 'left',
    lineHeight: '49px',
    height: '49px',
    verticalAlign: 'middle'
  },

  h1: {
    display: 'inline-block',
    borderRight: '1px solid rgba(0, 0, 0,.3)',
    margin: 0,
    marginRight: '20px',
    padding: '10px 23px 10px 0',
    fontSize: '24px',
    fontWeight: 500,
    verticalAlign: 'top'
  },

  h2: {
    fontSize: '14px',
    fontWeight: 'normal',
    lineHeight: 'inherit',
    margin: 0,
    padding: 0
  }
}
