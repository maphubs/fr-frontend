// @flow
import getRemainingLayer from './layer-remaining'
import { Container } from 'unstated'

import type {AOIDashboardState} from '../../../containers/AOIDashboardContainer'
type State = {
  threshold: number
}

export default class RemainingContainer extends Container<State> {
  init (config: Object, aoiDashboardState: AOIDashboardState, toggled: boolean) {
    this.state = {
      threshold: config.threshold
    }
  }

  update (aoiDashboardState: AOIDashboardState) { }

  getLayer (toggled: boolean) {
    const { threshold } = this.state
    return getRemainingLayer(toggled, threshold)
  }
}
