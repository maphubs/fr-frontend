//  @flow
import * as React from 'react'
import { Card, Button, Switch } from 'antd'
import PctChart from '../../Charts/PctChart'
import { subscribe } from '../../../lib/unstated-props'
import AOIDashboardContainer from '../../../containers/AOIDashboardContainer'

type Props = {
  height?: string,
  t: Function,
  containers: {aoiDashboard: any},
  cardHeadStyle: Object,
  cardConfig?: Object,
  report?: boolean,
  onReady?: Function
}

class Remaining extends React.Component<Props, void> {
  static defaultProps = {
    height: '40px'
  }

  componentDidMount () {
    if (this.props.onReady) this.props.onReady()
  }

  render () {
    const { t, height, containers, cardHeadStyle, cardConfig, report } = this.props
    const threshold = cardConfig.threshold || 80
    const { aoiDashboard } = containers
    const { results, area, toggles, locale, loading, showToggle, resultIDFilter } = aoiDashboard.state
    let toggled
    let hectares = 0
    let percent = 0
    let modID
    if (!loading) {
      if (cardConfig && cardConfig.moduleID) {
        modID = cardConfig.moduleID
      } else {
        modID = `tree-cover-density-${threshold}`
      }
      toggled = toggles['tree-cover-stat']

      const result = results[modID] ? results[modID][resultIDFilter] : null
      if (result) {
        hectares = result.area
        percent = (hectares / area) * 100
      }
    }

    const cardStyle = { maxWidth: report ? '300px' : '100%', margin: 'auto' }
    if (report) cardStyle.border = 'none'

    return (
      <Card
        title={t(`2018 Tree Cover ${threshold}%`)}
        style={cardStyle}
        bodyStyle={{ padding: '2%', zoom: report ? 2 : 1 }}
        headStyle={cardHeadStyle}
        loading={loading} hoverable={false} bordered>
        {!report &&
          <Button
            style={{
              position: 'absolute', bottom: '0px', right: '0px', border: 'none', boxShadow: 'none'
            }}
            onClick={() => {
              aoiDashboard.showInfo((
                <div>
                  <p>Remaining tree canopy at {threshold}% density calculated by subtracting annual loss totals from year 2000 baseline.</p>
                  <p>{t('Source')}: Hansen/UMD/Google/USGS/NASA</p>
                  <p>{t('License')}:<a href='http://creativecommons.org/licenses/by/4.0/' target='_blank' rel='noopener noreferrer'> CC-BY </a></p>
                  <p>
                    <a href='http://earthenginepartners.appspot.com/science-2013-global-forest/download_v1.4.html' target='_blank' rel='noopener noreferrer'>{t('More Info')}</a>
                  </p>
                </div>
              ))
            }}
            shape='circle'
            icon='info-circle-o'
            type='primary'
            ghost
          />
        }
        {(showToggle && !report) &&
          <Switch style={{
            position: 'absolute', top: '13px', right: '10px', border: 'none'
          }} size='small' defaultChecked={toggled} onChange={(checked) => {
            aoiDashboard.toggle(modID, checked)
          }} />
        }
        <div style={{ height, textAlign: 'center' }}>
          {!loading &&
            <PctChart type='large' language={locale} percent={percent} hectares={hectares} />
          }
        </div>
      </Card>
    )
  }
}
export default subscribe(Remaining, {'aoiDashboard': AOIDashboardContainer})
