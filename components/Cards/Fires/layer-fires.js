// @flow

function getFiresLayer (modID: string, firesGeoJSON: Object, active: boolean) {
  const firesColor = '#F96034'
  const layers = [
    {
      id: 'omh-data-point-fires-geojson',
      type: 'circle',
      metadata: {
        'maphubs:interactive': false,
        'maphubs:showBehindBaseMapLabels': false
      },
      source: 'fr-fires-geojson',
      minzoom: 10,
      paint: {
        'circle-color': firesColor,
        'circle-opacity': 1,
        'circle-radius': 3
      },
      layout: {
        visibility: active ? 'visible' : 'none'
      }
    },
    {
      id: 'omh-data-point-fires-heatmap',
      type: 'heatmap',
      metadata: {
        'maphubs:interactive': false,
        'maphubs:showBehindBaseMapLabels': false
      },
      source: 'fr-fires-geojson',
      paint: {
        'heatmap-weight': 0.5,
        'heatmap-intensity': [
          'interpolate',
          [
            'linear'
          ],
          [
            'zoom'
          ],
          0,
          0.1,
          7,
          10
        ],
        'heatmap-color': [
          'interpolate',
          [
            'linear'
          ],
          [
            'heatmap-density'
          ],
          0,
          'rgba(33,102,172,0)',
          0.2,
          'rgb(103,169,207)',
          0.4,
          'rgb(209,229,240)',
          0.6,
          'rgb(253,219,199)',
          0.8,
          'rgb(239,138,98)',
          1,
          '#F96034'
          // 'rgb(178,24,43)'
        ],
        'heatmap-radius': [
          'interpolate',
          [
            'linear'
          ],
          [
            'zoom'
          ],
          0,
          0.3,
          7,
          9
        ],
        'heatmap-opacity': [
          'interpolate',
          [
            'linear'
          ],
          [
            'zoom'
          ],
          10,
          1,
          12,
          0
        ]
      }
    }
  ]

  return {
    layer_id: modID,
    short_id: 'fr-fires-geojson',
    name: {
      en: 'Fires'
    },
    source: {
      en: 'NASA/MODIS'
    },
    style: {
      version: 8,
      sources: {
        'fr-fires-geojson': {
          type: 'geojson',
          data: firesGeoJSON
        }
      },
      layers,
      metadata: {
        'maphubs:active': active,
        'maphubs:priority': 1
      }
    },
    legend_html: `
    <div class="omh-legend">
    <div class="block" style="background-color: ${firesColor}">
    </div>
    <h3>{NAME}</h3>
    </div>`,
    is_external: true
  }
}

export default getFiresLayer
export {
  getFiresLayer
}
