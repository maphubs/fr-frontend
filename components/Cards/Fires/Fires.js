//  @flow
import * as React from 'react'
import { Card, Button, Tooltip, Row, Col, Slider, Spin, Switch } from 'antd'

import download from 'downloadjs'
import FiresChart from '../../Charts/FiresChart'
import { subscribe } from '../../../lib/unstated-props'
import AOIDashboardContainer from '../../../containers/AOIDashboardContainer'

type Props = {
  t: Function,
  containers: {aoiDashboard: any},
  cardHeadStyle: Object,
  cardConfig?: Object,
  report?: boolean,
  onReady?: Function
}

type FireResults = {
  count: number,
  features: Array<Object>,
  day_details: Object
}

type State = {
  stats?: FireResults,
  selectedValue?: Object,
  geojsonStr?: string,
  success?: boolean,
  year: string,
  density?: number,
  updating?: boolean,
  resultIDFilter?: string,
  showInfo?: boolean
}

const defaultYear = '2020'

class Fires extends React.Component<Props, State> {
  constructor (props: Props) {
    super(props)
    this.state = {
      year: defaultYear
    }
  }

  componentWillMount () {
    const { aoiDashboard } = this.props.containers
    const { results, geom } = aoiDashboard.state
    this.loadData(results, geom, '2020', undefined)
  }

  componentDidMount () {
    if (this.props.onReady) this.props.onReady()
  }

  async componentWillReceiveProps (nextProps: Props) {
    const currentFilter = this.state.resultIDFilter
    const nextFilter = nextProps.containers.aoiDashboard.state.resultIDFilter
    if (currentFilter !== nextFilter) {
      console.log(`changing from ${currentFilter} to ${nextFilter}`)
      const { results, geom } = nextProps.containers.aoiDashboard.state
      await this.loadData(results, geom, '2020', undefined)
    }
  }

  loadData = async (results: Object, geom: Object, year: string, density?: number) => {
    const { aoiDashboard } = this.props.containers
    let modId = 'fire-alerts-{YEAR}'
    if (this.props.cardConfig && this.props.cardConfig.moduleID) {
      modId = this.props.cardConfig.moduleID
    }
    modId = modId.replace('{YEAR}', year)
    if (!density || density === 0) {
      if (results[modId]) {
        const result = results[modId][aoiDashboard.state.resultIDFilter]
        console.log(`using precalucated fire result for year ${year}`)
        console.log(result)

        const fireGeoJSON = {
          type: 'FeatureCollection',
          features: result.features
        }
        this.setState({
          stats: result,
          geojsonStr: JSON.stringify(fireGeoJSON),
          success: true,
          updating: false,
          resultIDFilter: aoiDashboard.state.resultIDFilter
        })
        aoiDashboard.changeFires(result)
      }
    }
  }

  selectYear = async (results: Object, geom: Object, value: number) => {
    const { density } = this.state
    let year = defaultYear
    if (value === 0) {
      year = '2015'
    } else if (value === 20) {
      year = '2016'
    } else if (value === 40) {
      year = '2017'
    } else if (value === 60) {
      year = '2018'
    } else if (value === 80) {
      year = '2019'
    } else if (value === 100) {
      year = '2020'
    }
    this.setState({ year, updating: true })
    return this.loadData(results, geom, year, density)
  }

  onClick = (value: Object) => {
    this.setState({ selectedValue: value })
    this.props.containers.aoiDashboard.onFireClick(value)
  }

  clearSelected = () => {
    this.setState({ selectedValue: null })
    this.props.containers.aoiDashboard.onFireClearSelected()
  }

  showInfo = () => {
    this.setState({ showInfo: true })
  }

  onDownloadClick = () => {
    const { geojsonStr, selectedValue } = this.state
    // if a value is selected use that for the download instead
    let geojsonDownloadData = geojsonStr
    if (selectedValue) {
      geojsonDownloadData = JSON.stringify({
        type: 'FeatureCollection',
        features: selectedValue.features
      })
    }
    download(`data:text/json;charset=utf-8,${geojsonDownloadData}`, 'Fires_Export.geojson', 'text/json')
  }

  render () {
    const { stats, success, year, updating } = this.state
    const { t, containers, cardHeadStyle, report } = this.props
    const { aoiDashboard } = containers
    const { results, toggles, loading, showToggle, geom, area } = aoiDashboard.state
    let modId = 'fire-alerts-{YEAR}'
    if (this.props.cardConfig && this.props.cardConfig.moduleID) {
      modId = this.props.cardConfig.moduleID
    }
    modId = modId.replace('{YEAR}', year)
    const toggled = toggles[modId]

    let max = 0
    const dayValues = Object.keys(stats.day_details).map(day => {
      const dayValue = stats.day_details[day]
      if (dayValue.count > max) max = dayValue.count
      return dayValue
    })

    const dayStats = {
      values: dayValues,
      max
    }

    let content
    if (stats && success) {
      content = (
        <Spin spinning={loading || updating} delay={50}>
          <div>
            <Row style={{ textAlign: 'center', marginBottom: '10px' }} type='flex' justify='center'>
              <Col span={8}>
                <b style={{color: '#323333'}}>{t('Last Month')} </b>
                {stats.last_30}
              </Col>
              <Col span={8}>
                <b style={{color: '#323333'}}>{year}: </b>
                {stats.count}
              </Col>
            </Row>
            <Row>
              <p style={{marginBottom: '10px', color: '#323333'}}><b>Detection Date</b></p>
              <FiresChart
                stats={dayStats}
                onClick={(val) => {
                  this.onClick(val)
                  aoiDashboard.onFireClick(val)
                }}
                onClearSelected={this.clearSelected}
                t={t}
                startDate={`${year}-01-01`}
                endDate={`${year}-12-31`}
              />
            </Row>
            {!report &&
              <Row style={{ width: '100%', height: '35px', textAlign: 'left' }}>
                <Tooltip title={t('Download GeoJSON')} placement='left'>
                  <Button
                    onClick={this.onDownloadClick}
                    shape='circle'
                    icon='download'
                    type='primary'
                    size='small'
                    ghost
                  />
                </Tooltip>
              </Row>}
          </div>
        </Spin>
      )
    } else {
      content = (
        <div style={{ textAlign: 'center', minHeight: '100px' }}>
          <p style={{ paddingTop: '50px', fontSize: '28px' }}>{t('Data Not Available')}</p>
        </div>
      )
    }

    return (
      <Card
        title={t('Fires')}
        headStyle={cardHeadStyle}
        bodyStyle={{
          paddingLeft: '2%', paddingRight: '2%', paddingTop: '5px', paddingBottom: '5px'
        }}
        loading={loading}
        hoverable={false}
        bordered={!report}
      >
        {!report &&
          <Button
            style={{
              position: 'absolute', bottom: '0px', right: '0px', border: 'none', zIndex: 999, boxShadow: 'none'
            }}
            onClick={() => {
              aoiDashboard.showInfo((
                <div>
                  <p>Fire hot spots</p>
                  <p>{t('Source')}: NASA MODIS</p>
                  <p>
                    <a href='https://earthdata.nasa.gov/earth-observation-data/near-real-time/firms/active-fire-data' target='_blank' rel='noopener noreferrer'>{t('More Info')}</a>
                  </p>
                </div>
              ))
            }}
            shape='circle'
            icon='info-circle-o'
            type='primary'
            ghost
          />}
        {(showToggle && !report) &&
          <Switch
            style={{
              position: 'absolute', top: '13px', right: '10px', border: 'none'
            }} size='small' defaultChecked={toggled} onChange={(checked) => {
              aoiDashboard.toggle(modId, checked)
            }}
          />}
        {!report &&
          <Row>
            <Col span={12}>
              <Slider
                style={{ width: '225px', float: 'left' }}
                tipFormatter={null}
                disabled={updating}
                marks={{
                  0: '2015',
                  20: '2016',
                  40: '2017',
                  60: '2018',
                  80: '2019',
                  100: '2020'
                }}
                step={null}
                included={false}
                defaultValue={100}
                max={100}
                onChange={val => this.selectYear(results, geom, val)}
              />
            </Col>
          </Row>}
        {content}
      </Card>
    )
  }
}
export default subscribe(Fires, {aoiDashboard: AOIDashboardContainer})
