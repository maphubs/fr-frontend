// @flow
import getFiresLayer from './layer-fires'
import { Container } from 'unstated'

import type {AOIDashboardState} from '../../../containers/AOIDashboardContainer'
type State = {
  year: string,
  geoJSON: Object,
  modID: string
}

export default class FiresContainer extends Container<State> {
  init (config: Object, layerConfig: Object, aoiDashboardState: AOIDashboardState) {
    const { results, resultIDFilter, moduleID } = aoiDashboardState
    const year = config.year || '2020'
    let modID = 'fire-alerts-modis-{YEAR}'
    if (moduleID) {
      modID = moduleID
    }
    modID = modID.replace('{YEAR}', year)
    const result = results[modID][resultIDFilter]
    const geoJSON = {
      type: 'FeatureCollection',
      features: result.features
    }
    this.state = {year, geoJSON, modID}
  }

  async update (aoiDashboardState: AOIDashboardState) {
    await this.init({year: this.state.year}, aoiDashboardState)
    const mapboxGL = this.state.mapState.state.map.map
    const geoJSONData = mapboxGL.getSource('fr-fires-geojson')
    geoJSONData.setData(this.state.geoJSON)
  }

  getLayer (toggled: boolean) {
    const { modID } = this.state
    return getFiresLayer(modID, this.state.geoJSON, toggled)
  }
}
