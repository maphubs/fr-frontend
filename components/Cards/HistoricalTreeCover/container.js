// @flow
import { Container } from 'unstated'

import type {AOIDashboardState} from '../../../containers/AOIDashboardContainer'
type State = {}

export default class HistoricalTreeCoverContainer extends Container<State> {
  init (config: Object, layerConfig: Object, aoiDashboardState: AOIDashboardState) { }

  update (aoiDashboardState: AOIDashboardState) { }

  getLayer (toggled: boolean) {}
}
