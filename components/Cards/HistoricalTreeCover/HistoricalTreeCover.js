//  @flow
import * as React from 'react'
import { Card, Button } from 'antd'

import ButtonSelect from '../../ButtonSelect'
import TreeCoverTable from '../../Charts/TreeCoverTable'
import TreeCoverChart from '../../Charts/TreeCoverChart'
import { subscribe } from '../../../lib/unstated-props'
import AOIDashboardContainer from '../../../containers/AOIDashboardContainer'

type Props = {
  height: string,
  t: Function,
  containers: {aoiDashboard: any},
  cardHeadStyle: Object
}

class HistoricalTreeCover extends React.Component<Props, void> {
  static defaultProps = {
    height: '33vh'
  }

  render () {
    const { t, height, containers, cardHeadStyle } = this.props
    const { aoiDashboard } = containers
    const { results, loading, resultIDFilter } = aoiDashboard.state
    const result = results[`tree-cover-historical`][resultIDFilter]
    const stats = result ? result.stats : {}
    return (
      <Card
        title={t('Tree Cover (2001-2018)')}
        headStyle={cardHeadStyle}
        bodyStyle={{ padding: 0 }}
        loading={loading} hoverable={false} bordered>
        <Button
          style={{
            position: 'absolute', bottom: '0px', right: '0px', border: 'none', boxShadow: 'none'
          }}
          onClick={() => {
            aoiDashboard.showInfo((
              <div>
                <p>Remaining tree canopy at 30% density calculated by subtracting annual loss totals from year 2000 baseline.</p>
                <p>{t('Source')}: Hansen/UMD/Google/USGS/NASA</p>
                <p>{t('License')}:<a href='http://creativecommons.org/licenses/by/4.0/' target='_blank' rel='noopener noreferrer'> CC-BY </a></p>
                <p>
                  <a href='http://earthenginepartners.appspot.com/science-2013-global-forest/download_v1.4.html' target='_blank' rel='noopener noreferrer'>{t('More Info')}</a>
                </p>
              </div>
            ))
          }}
          shape='circle'
          icon='info-circle-o'
          type='primary'
          ghost
        />
        <ButtonSelect defaultIcon='bar-chart' toggleIcon='bars' style={{ margin: '10px' }} >
          <TreeCoverChart stats={stats} height={height} t={t} />
          <TreeCoverTable stats={stats} height={height} t={t} />
        </ButtonSelect>
      </Card>
    )
  }
}
export default subscribe(HistoricalTreeCover, {'aoiDashboard': AOIDashboardContainer})
