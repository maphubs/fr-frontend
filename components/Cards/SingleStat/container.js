// @flow
import { Container } from 'unstated'

import type {AOIDashboardState} from '../../../containers/AOIDashboardContainer'
type State = {
  config: Object,
  layerConfig: Object
}

export default class CardContainer extends Container<State> {
  init (config: Object, layerConfig: Object, aoiDashboardState: AOIDashboardState, toggled: boolean) {
    this.state = {
      config,
      layerConfig
    }
  }

  update (aoiDashboardState: AOIDashboardState) { }

  getLayer (toggled: boolean) {
    const { layerConfig } = this.state
    if (layerConfig && layerConfig.style) {
      if (!layerConfig.style.metadata) layerConfig.style.metadata = {}
      layerConfig.style.metadata['maphubs:active'] = toggled
      layerConfig.style.layers.forEach(layer => {
        if (!layer.layout) layer.layout = {}
        layer.layout.visibility = toggled ? 'visible' : 'none'
      })
    }
    return layerConfig
  }
}
