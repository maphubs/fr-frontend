//  @flow
import * as React from 'react'
import { Card, Button, Switch } from 'antd'
import PctChart from '../../Charts/PctChart'
import { subscribe } from '../../../lib/unstated-props'
import AOIDashboardContainer from '../../../containers/AOIDashboardContainer'
import getColVal from '../../../utils/get-table-col-value'

type Props = {
  height?: string,
  t: Function,
  containers: {aoiDashboard: any},
  cardHeadStyle: Object,
  cardConfig?: Object,
  report?: boolean,
  onReady?: Function
}

class SingleStat extends React.Component<Props, void> {
  static defaultProps = {
    height: '40px',
    threshold: 80
  }

  componentDidMount () {
    if (this.props.onReady) this.props.onReady()
  }

  render () {
    const { t, height, containers, cardHeadStyle, cardConfig, report } = this.props
    const { aoiDashboard } = containers
    const { results, area, toggles, locale, loading, showToggle, resultIDFilter } = aoiDashboard.state
    const modID = cardConfig.moduleID
    const toggled = toggles[modID]
    const dataIndex = cardConfig.dataIndex || 'area'
    let hectares = 0
    let percent = 0
    if (!loading) {
      const result = results[modID][resultIDFilter]
      if (result) {
        hectares = getColVal(result, dataIndex)
        percent = (hectares / area) * 100
      }
    }

    const cardStyle = { maxWidth: report ? '300px' : '100%', margin: 'auto' }

    return (
      <Card
        title={cardConfig.title ? t(cardConfig.title) : modID}
        style={cardStyle}
        bodyStyle={{ padding: '2%', zoom: report ? 2 : 1 }}
        headStyle={cardHeadStyle}
        loading={loading} hoverable={false} bordered={!report}
      >
        {!report &&
          <Button
            style={{
              position: 'absolute', bottom: '0px', right: '0px', border: 'none', boxShadow: 'none'
            }}
            onClick={() => {
              aoiDashboard.showInfo((
                <>
                  {cardConfig.attribution &&
                    <div dangerouslySetInnerHTML={{__html: t(cardConfig.attribution)}} />}
                  {!cardConfig.attribution &&
                    <p>No information available</p>}
                </>
              ))
            }}
            shape='circle'
            icon='info-circle-o'
            type='primary'
            ghost
          />}
        {(showToggle && !report) &&
          <Switch
            style={{
              position: 'absolute', top: '13px', right: '10px', border: 'none'
            }} size='small' defaultChecked={toggled} onChange={(checked) => {
              aoiDashboard.toggle(modID, checked)
            }}
          />}
        <div style={{ height, textAlign: 'center' }}>
          {!loading &&
            <PctChart type={cardConfig.chartType || 'large'} language={locale} percent={percent} hectares={hectares} />}
        </div>
      </Card>
    )
  }
}
export default subscribe(SingleStat, {aoiDashboard: AOIDashboardContainer})
