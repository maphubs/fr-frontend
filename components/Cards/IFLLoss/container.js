// @flow
import getIFLLossLayer from './layer-ifl-loss'
import { Container } from 'unstated'

import type {AOIDashboardState} from '../../../containers/AOIDashboardContainer'
type State = {
  iflLoss0013Data: Object,
  iflLoss1316Data: Object
}

export default class IFLLossContainer extends Container<State> {
  init (config: Object, aoiDashboardState: AOIDashboardState) {
    const {results, resultIDFilter} = aoiDashboardState
    if (results && results['ifl-loss-0013']) {
      const result0013 = results['ifl-loss-0013'][resultIDFilter]
      const result1316 = results['ifl-loss-1316'][resultIDFilter]
      if (result0013 && result0013.features &&
          result1316 && result1316.features) {
        let iflLoss0013Data = {type: 'FeatureCollection', features: result0013.features}
        let iflLoss1316Data = {type: 'FeatureCollection', features: result1316.features}
        this.state = {iflLoss0013Data, iflLoss1316Data}
      }
    }
  }

  async update (aoiDashboardState: AOIDashboardState) {
    await this.init(null, aoiDashboardState)
    // update data in map layer
    if (this.state && this.state.iflLoss0013Data) {
      const mapboxGL = this.state.mapState.state.map.map
      const geoJSONData0013 = mapboxGL.getSource('fr-iflloss0013-geojson')
      geoJSONData0013.setData(this.state.iflLoss0013Data)
      const geoJSONData1316 = mapboxGL.getSource('fr-iflloss1316-geojson')
      geoJSONData1316.setData(this.state.iflLoss1316Data)
    }
  }

  getLayer (toggled: boolean) {
    if (this.state && this.state.iflLoss0013Data) {
      const { iflLoss0013Data, iflLoss1316Data } = this.state
      return getIFLLossLayer(iflLoss0013Data, iflLoss1316Data, toggled)
    }
  }
}
