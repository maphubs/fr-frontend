import Remaining from './Remaining/container'
import IFLLoss from './IFLLoss/container'
import IFLRemaining from './IFLRemaining/container'
import HistoricalTreeCover from './HistoricalTreeCover/container'
import HistoricalTreeCoverLoss from './HistoricalTreeCoverLoss/container'
import GLAD from './GLAD/container'
import Fires from './Fires/container'
import IDNPlantation from './IDN_Plantation/container'
import IDNPrimary from './IDN_Primary/container'
import SingleStat from './SingleStat/container'
import LandClass from './LandClass/container'

export default {
  'tree-cover-stat': Remaining,
  ifl: IFLRemaining,
  'ifl-loss': IFLLoss,
  glad: GLAD,
  fires: Fires,
  'tree-cover-chart': HistoricalTreeCover,
  'loss-chart': HistoricalTreeCoverLoss,
  'idn-plantation': IDNPlantation,
  'idn-primary': IDNPrimary,
  'single-stat': SingleStat,
  'land-class': LandClass
}
