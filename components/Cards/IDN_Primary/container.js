// @flow
import getLayer from './layer-idn-primary'
import { Container } from 'unstated'

import type {AOIDashboardState} from '../../../containers/AOIDashboardContainer'
type State = {}

export default class IDNPrimaryContainer extends Container<State> {
  init (config: Object, layerConfig: Object, aoiDashboardState: AOIDashboardState) { }

  update (aoiDashboardState: AOIDashboardState) { }

  getLayer (toggled: boolean) {
    return getLayer(toggled)
  }
}
