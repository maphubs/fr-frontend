// @flow

function getLayer (active: boolean) {
  return {
    layer_id: 'id-primary-forest-2017',
    short_id: 'id-primary-forest-2017',
    name: {
      en: 'Primary Dry Forest 2017 Indonesia'
    },
    source: {
      en: 'Ministry of Forestry and Environment'
    },
    style: {
      version: 8,
      sources: {
        'fr-idn-primary-2017': {
          type: 'vector',
          url: 'https://mapforenvironment.org/api/lyr/rJNVaT9cM/tile.json',
          metadata: {
            'maphubs:presets': [
              {
                'tag': 'Name',
                'label': 'Name',
                'type': 'text',
                'isRequired': false,
                'showOnMap': true,
                'mapTo': 'Name',
                'id': 1
              }
            ]
          }
        }
      },
      layers: [
        {
          'id': 'fr-idn-primary-2017-fill',
          'type': 'fill',
          'metadata': {
            'maphubs:layer_id': 99999921,
            'maphubs:globalid': 'fr-idn-primary-2017',
            'maphubs:interactive': false,
            'maphubs:showBehindBaseMapLabels': false
          },
          'source': 'fr-idn-primary-2017',
          'source-layer': 'data',
          'filter': [
            'in',
            '$type',
            'Polygon'
          ],
          'paint': {
            'fill-color': 'rgba(51,105,30,0.65)',
            'fill-outline-color': 'rgba(51,105,30,0.65)',
            'fill-opacity': 1
          },
          'layout': {
            'visibility': active ? 'visible' : 'none'
          }
        }
      ],
      metadata: {
        'maphubs:active': active,
        'maphubs:priority': 2
      }
    },
    legend_html: `
    <div class="omh-legend">
<div class="block double-stroke" style="background-color: rgba(51,105,30,0.65)">
 </div>
 <h3>{NAME}</h3>
</div>`
  }
}

export default getLayer
export {
  getLayer
}
