//  @flow
import * as React from 'react'
import { Card, Button, Switch, Row, Col } from 'antd'
import PctChart from '../../Charts/PctChart'
import { subscribe } from '../../../lib/unstated-props'
import AOIDashboardContainer from '../../../containers/AOIDashboardContainer'

type Props = {
  height?: string,
  t: Function,
  containers: {aoiDashboard: any}
}

class IFLRemaining extends React.Component<Props, void> {
  static defaultProps = {
    height: '135px'
  }

  render () {
    const { t, height, containers } = this.props
    const { aoiDashboard } = containers
    const { results, area, toggles, locale, loading, showToggle } = aoiDashboard.state
    const toggled = toggles['ifl']
    const result = results['ifl2016']
    const hectares16 = result ? result.area : NaN
    const percentIFLRemaining16 = (hectares16 / area) * 100

    return (
      <Card
        title={t('Intact Forest Landscape (IFL)')}
        headStyle={{fontSize: '14px', lineHeight: '14px', minHeight: '20px'}}
        bodyStyle={{ padding: '2%' }} loading={loading} hoverable={false} bordered>
        <Button
          style={{
            position: 'absolute', bottom: '0px', right: '0px', border: 'none', boxShadow: 'none'
          }}
          onClick={() => {
            aoiDashboard.showInfo((
              <div style={{overflow: 'auto', maxHeight: '400px'}}>
                <p>The world's IFL map is a spatial database (scale 1:1,000,000) that shows the extent of the intact forest landscapes (IFL) http://www.intactforests.org/data.ifl.html Credit: Potapov P., Yaroshenko A., Turubanova S., Dubinin M., Laestadius L., Thies C., Aksenov D., Egorov A., Yesipova Y., Glushkov I., Karpachevskiy M., Kostikova A., Manisha A., Tsybikova E., Zhuravleva I. 2008. Mapping the World's Intact Forest Landscapes by Remote Sensing. Ecology and Society, 13 (2)</p>
                <p>{t('Source')}: Greenpeace/UMD/TransparentWorld/WRI/WWF-Russia</p>
                <p>{t('License')}: Provided IFL data are available for use for valid scientific, conservation, and educational purposes as long as proper citations are used.</p>
                <p>
                  <a href='http://www.intactforests.org/data.ifl.html' target='_blank' rel='noopener noreferrer'>{t('More Info')}</a>
                </p>
              </div>
            ))
          }}
          shape='circle'
          icon='info-circle-o'
          type='primary'
          ghost
        />
        {showToggle &&
          <Switch style={{
            position: 'absolute', top: '16px', right: '10px', border: 'none'
          }} size='small' defaultChecked={toggled} onChange={(checked) => {
            aoiDashboard.toggle('ifl', checked)
          }} />
        }
        <div style={{ height, textAlign: 'center' }}>
          <Row>
            <Col span={24}>
              <p>2016</p>
              <PctChart language={locale} percent={percentIFLRemaining16} hectares={hectares16} color='#6AC164' />
            </Col>
          </Row>
        </div>
      </Card>
    )
  }
}
export default subscribe(IFLRemaining, {'aoiDashboard': AOIDashboardContainer})
