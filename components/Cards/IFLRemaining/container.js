// @flow
import getIFLLayer from './layer-ifl'
import { Container } from 'unstated'

import type {AOIDashboardState} from '../../../containers/AOIDashboardContainer'
type State = {
  data: Object
}

export default class IFLRemaingContainer extends Container<State> {
  init (config: Object, aoiDashboardState: AOIDashboardState) {
    const {results, resultIDFilter} = aoiDashboardState
    if (results && results['ifl2016']) {
      const result = results['ifl2016'][resultIDFilter]
      if (result && result.features) {
        const data = {type: 'FeatureCollection', features: config.ifl2016.features}
        this.state = {data}
      } else {
        console.error('IFL result not found')
      }
    }
  }

  async update (aoiDashboardState: AOIDashboardState) {
    await this.init(null, aoiDashboardState)
    // update data in map layer
    if (this.state && this.state.data) {
      const mapboxGL = this.state.mapState.state.map.map
      const geoJSONData = mapboxGL.getSource('fr-ifl2016-geojson')
      geoJSONData.setData(this.state.data)
    }
  }

  getLayer (toggled: boolean) {
    if (this.state && this.state.data) {
      const { data } = this.state
      return getIFLLayer(data, toggled)
    }
  }
}
