//  @flow
import * as React from 'react'
import { Card, Button, Tooltip, Row, Col, Slider, Spin, Switch } from 'antd'

import download from 'downloadjs'

import GLADChart from '../../Charts/GLADChart'
import PctChart from '../../Charts/PctChart'
import forestReportAPI from '../../../utils/forest-report-api'
import gladRegion from '../../../utils/glad-region'
import { subscribe } from '../../../lib/unstated-props'
import AOIDashboardContainer from '../../../containers/AOIDashboardContainer'

type Props = {
  t: Function,
  containers: {aoiDashboard: any},
  cardHeadStyle: Object,
  cardConfig?: Object,
  report?: boolean,
  onReady?: Function
}

type State = {
  stats?: { values: Array<Object>, max: number},
  selectedValue?: Object,
  geojsonStr?: string,
  success?: boolean,
  year: string,
  density?: number,
  updating?: boolean,
  resultIDFilter?: string
}

const defaultYear = '2020'

class GLAD extends React.Component<Props, State> {
  constructor (props: Props) {
    super(props)
    this.state = {
      year: defaultYear
    }
  }

  componentWillMount () {
    const { aoiDashboard } = this.props.containers
    const { results, geom } = aoiDashboard.state
    this.loadData(results, geom, '2020', undefined)
  }

  componentDidMount () {
    if (this.props.onReady) this.props.onReady()
  }

  async componentWillReceiveProps (nextProps: Props) {
    const currentFilter = this.state.resultIDFilter
    const nextFilter = nextProps.containers.aoiDashboard.state.resultIDFilter
    if (currentFilter !== nextFilter) {
      console.log(`changing from ${currentFilter || 'unknown'} to ${nextFilter}`)
      const { results, geom } = nextProps.containers.aoiDashboard.state
      await this.loadData(results, geom, '2020', undefined)
    }
  }

  loadData = async (results: Object, geom: Object, year: string, density?: number) => {
    const { aoiDashboard } = this.props.containers
    let modId = 'glad-alerts-{YEAR}'
    if (this.props.cardConfig && this.props.cardConfig.moduleID) {
      modId = this.props.cardConfig.moduleID
    }
    modId = modId.replace('{YEAR}', year)
    if (!density || density === 0) {
      if (results[modId]) {
        const precalcResult = results[modId][aoiDashboard.state.resultIDFilter]
        console.log(`using precalucated GLAD result for year ${year}`)
        console.log(precalcResult)
        const gladStats = forestReportAPI.processGLADResult(precalcResult, year)
        gladStats.lastMonth = precalcResult.last_month
        const gladGeoJSON = {
          type: 'FeatureCollection',
          features: precalcResult.geoms
        }
        this.setState({
          stats: gladStats,
          geojsonStr: JSON.stringify(gladGeoJSON),
          success: true,
          updating: false,
          resultIDFilter: aoiDashboard.state.resultIDFilter
        })
        aoiDashboard.changeGLAD(precalcResult)
        return
      }
    }

    let region
    if (geom) {
      try {
        region = gladRegion(geom)
        let url = 'https://stats-api.forest.report/glad'
        if (density && density > 0) {
          url += '/density'
        }
        // eslint-disable-next-line no-undef
        const response = await fetch(url, {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json'
          },
          body: JSON.stringify({
            geom,
            include_geom: 'True',
            region,
            year,
            density
          })
        })
        const gladResult = await response.json()
        let gladStats
        if (gladResult) {
          gladStats = forestReportAPI.processGLADResult(gladResult, year)
          gladStats.lastMonth = gladResult.last_month

          const gladGeoJSON = {
            type: 'FeatureCollection',
            features: gladResult.geoms
          }
          this.setState({
            stats: gladStats,
            geojsonStr: JSON.stringify(gladGeoJSON),
            success: true,
            updating: false,
            resultIDFilter: aoiDashboard.state.resultIDFilter
          })
          aoiDashboard.changeGLAD(gladResult)
        } else {
          this.setState({
            success: false,
            updating: false
          })
        }
      } catch (err) {
        this.setState({
          success: false,
          updating: false
        })
      }
    } else {
      console.error('geom not found')
    }
  }

  selectYear = async (results: Object, geom: Object, value: number) => {
    const { density } = this.state
    let year = defaultYear
    if (value === 0) {
      year = '2015'
    } else if (value === 20) {
      year = '2016'
    } else if (value === 40) {
      year = '2017'
    } else if (value === 60) {
      year = '2018'
    } else if (value === 80) {
      year = '2019'
    } else if (value === 100) {
      year = '2020'
    }
    this.setState({ year, updating: true })
    return this.loadData(results, geom, year, density)
  }

  selectDensity = async (results: Object, geom: Object, density: number) => {
    const { year } = this.state
    this.setState({ density, updating: true })
    return this.loadData(results, geom, year, density)
  }

  onClick = (value: Object) => {
    this.setState({ selectedValue: value })
    this.props.containers.aoiDashboard.onGLADClick(value)
  }

  clearSelected = () => {
    this.setState({ selectedValue: null })
    this.props.containers.aoiDashboard.onGLADClearSelected()
  }

  showInfo = () => {
    this.setState({ showInfo: true })
  }

  onDownloadClick = () => {
    const { geojsonStr, selectedValue } = this.state
    // if a value is selected use that for the download instead
    let geojsonDownloadData = geojsonStr
    if (selectedValue) {
      geojsonDownloadData = JSON.stringify({
        type: 'FeatureCollection',
        features: selectedValue.features
      })
    }
    download(`data:text/json;charset=utf-8,${geojsonDownloadData}`, 'GLAD_Export.geojson', 'text/json')
  }

  render () {
    const { stats, success, year, updating } = this.state
    const { t, containers, cardHeadStyle, report } = this.props
    const { aoiDashboard } = containers
    const { results, toggles, loading, showToggle, geom, area } = aoiDashboard.state
    let modId = 'glad-alerts-{YEAR}'
    if (this.props.cardConfig && this.props.cardConfig.moduleID) {
      modId = this.props.cardConfig.moduleID
    }
    modId = modId.replace('{YEAR}', year)
    const toggled = toggles[modId]

    let total = 0
    let percent = 0
    let percentLastMonth = 0

    if (stats && stats.values) {
      stats.values.forEach((value) => {
        total += value.count
      })
      percent = (total / area) * 100
      if (stats.lastMonth) {
        percentLastMonth = (stats.lastMonth / area) * 100
      }
    }

    let content
    if (stats && success) {
      content = (
        <Spin spinning={loading || updating} delay={50}>
          <div>
            <Row style={{ textAlign: 'center', marginBottom: '10px' }} type='flex' justify='center'>
              <Col span={8}>
                <b style={{color: '#323333'}}>{t('Last Month')}</b>
                <PctChart hectares={stats.lastMonth} percent={percentLastMonth} />
              </Col>
              <Col span={8}>
                <b style={{color: '#323333'}}>{year}</b>
                <PctChart hectares={total} percent={percent} />
              </Col>
            </Row>
            <Row>
              <p style={{marginBottom: '10px', color: '#323333'}}><b>{t('Detection Date')}</b></p>
              <GLADChart
                stats={stats}
                onClick={(val) => {
                  this.onClick(val)
                  aoiDashboard.onGLADClick(val)
                }}
                onClearSelected={this.clearSelected}
                t={t}
                startDate={`${year}-01-01`}
                endDate={`${year}-12-31`}
              />
            </Row>
            {!report &&
              <Row style={{ width: '100%', height: '35px', textAlign: 'left' }}>
                <Tooltip title={t('Download GeoJSON')} placement='left'>
                  <Button
                    onClick={this.onDownloadClick}
                    shape='circle'
                    icon='download'
                    type='primary'
                    size='small'
                    ghost
                  />
                </Tooltip>
              </Row>}
          </div>
        </Spin>
      )
    } else {
      content = (
        <div style={{ textAlign: 'center', minHeight: '100px' }}>
          <p style={{ paddingTop: '50px', fontSize: '28px' }}>{t('Data Not Available')}</p>
        </div>
      )
    }

    return (
      <Card
        title={t('Forest Alerts')}
        headStyle={cardHeadStyle}
        bodyStyle={{
          paddingLeft: '2%', paddingRight: '2%', paddingTop: '5px', paddingBottom: '5px'
        }}
        loading={loading}
        hoverable={false}
        bordered={!report}
      >
        {!report &&
          <Button
            style={{
              position: 'absolute', bottom: '0px', right: '0px', border: 'none', zIndex: 999, boxShadow: 'none'
            }}
            onClick={() => {
              aoiDashboard.showInfo((
                <div>
                  <p>GLAD forest alerts by detection day</p>
                  <p>Cautions: While Landsat 7 and 8 satellites together have a revisit period of 8 days, cloud cover can majorly limit the availability of imagery, particularly in the wet season. Alert dates represent the instance of detection, though tree cover loss could have taken place earlier, possibly weeks or months earlier, due to persistent cloud cover.</p>
                  <p>{t('Source')}: GLAD/UMD accessed through Global Forest Watch</p>
                  <p>{t('License')}:<a href='http://creativecommons.org/licenses/by/4.0/' target='_blank' rel='noopener noreferrer'> CC-BY </a></p>
                  <p>
                    <a href='http://iopscience.iop.org/article/10.1088/1748-9326/11/3/034008' target='_blank' rel='noopener noreferrer'>{t('More Info')}</a>
                  </p>
                </div>
              ))
            }}
            shape='circle'
            icon='info-circle-o'
            type='primary'
            ghost
          />}
        {(showToggle && !report) &&
          <Switch
            style={{
              position: 'absolute', top: '13px', right: '10px', border: 'none'
            }} size='small' defaultChecked={toggled} onChange={(checked) => {
              aoiDashboard.toggle(modId, checked)
            }}
          />}
        {!report &&
          <Row>
            <Col span={12}>
              <Slider
                style={{ width: '225px', float: 'left' }}
                tipFormatter={null}
                disabled={updating}
                marks={{
                  0: '2015',
                  20: '2016',
                  40: '2017',
                  60: '2018',
                  80: '2019',
                  100: '2020'
                }}
                step={null}
                included={false}
                defaultValue={100}
                max={100}
                onChange={val => this.selectYear(results, geom, val)}
              />
            </Col>
            <Col span={12}>
              <Tooltip title={t('Canopy Density Threshold')} placement='left'>
                <div style={{ width: '250px', float: 'right' }}>
                  <Slider
                    tipFormatter={null}
                    disabled={updating}
                    marks={{
                      0: 'Off',
                      10: '10%',
                      20: '',
                      30: '30%',
                      40: '',
                      50: '50%',
                      60: '',
                      70: '70%',
                      80: '',
                      90: '90%'
                    }}
                    step={null}
                    included={false}
                    defaultValue={0}
                    max={90}
                    onChange={val => this.selectDensity(results, geom, val)}
                  />
                </div>
              </Tooltip>
            </Col>
          </Row>}
        {content}
      </Card>
    )
  }
}
export default subscribe(GLAD, {aoiDashboard: AOIDashboardContainer})
