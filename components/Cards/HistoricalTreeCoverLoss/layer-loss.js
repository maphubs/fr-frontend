// @flow

function getLossLayer (active: boolean) {
  return {
    layer_id: 'tree-cover-loss',
    short_id: 'fr-tree-cover-loss',
    name: {
      en: 'Tree Cover Loss 2000 - 2018'
    },
    source: {
      en: 'Hansen/UMD/Google/USGS/NASA'
    },
    style: {
      version: 8,
      sources: {
        'fr-tree-cover-loss': {
          type: 'raster',
          tiles: [
            'https://qzxvv33134iutzy.belugacdn.link/lossramp2018/1/{z}/{x}/{y}'
          ],
          tileSize: 256
        }
      },
      layers: [
        {
          id: 'fr-tree-cover-loss',
          type: 'raster',
          source: 'fr-tree-cover-loss',
          minzoom: 0,
          maxzoom: 18,
          paint: {
            'raster-opacity': 1
          },
          layout: {
            visibility: active ? 'visible' : 'none'
          }
        }
      ],
      metadata: {
        'maphubs:active': active,
        'maphubs:priority': 3
      }
    },
    legend_html: `
<div class="omh-legend">
<style scoped>
    .waterLegendGradient {
  height: 10px;
  background-image: -webkit-linear-gradient(left, #fff5f0, #ffebe2 11.76%, #ffe1d3 17.64%, #fed0bd 23.52%, #fdbea5 29.40%, #fcab8e 35.28%, #fc9777 41.16%, #fc8363 47.04%, #fc7050 52.92%, #f85a40 58.80%, #f24431 64.68%, #e53127 70.56%, #d42020 76.44%, #c1151a 82.32%, #ad1016 88.20%, #8e0912 94.08%, #67000d 100%);
  background-image: -o-linear-gradient(left, #fff5f0, #ffebe2 11.76%, #ffe1d3 17.64%, #fed0bd 23.52%, #fdbea5 29.40%, #fcab8e 35.28%, #fc9777 41.16%, #fc8363 47.04%, #fc7050 52.92%, #f85a40 58.80%, #f24431 64.68%, #e53127 70.56%, #d42020 76.44%, #c1151a 82.32%, #ad1016 88.20%, #8e0912 94.08%, #67000d 100%);
  background-image: -moz-linear-gradient(left, #fff5f0, #ffebe2 11.76%, #ffe1d3 17.64%, #fed0bd 23.52%, #fdbea5 29.40%, #fcab8e 35.28%, #fc9777 41.16%, #fc8363 47.04%, #fc7050 52.92%, #f85a40 58.80%, #f24431 64.68%, #e53127 70.56%, #d42020 76.44%, #c1151a 82.32%, #ad1016 88.20%, #8e0912 94.08%, #67000d 100%);
  background-image: -ms-linear-gradient(left, #fff5f0, #ffebe2 11.76%, #ffe1d3 17.64%, #fed0bd 23.52%, #fdbea5 29.40%, #fcab8e 35.28%, #fc9777 41.16%, #fc8363 47.04%, #fc7050 52.92%, #f85a40 58.80%, #f24431 64.68%, #e53127 70.56%, #d42020 76.44%, #c1151a 82.32%, #ad1016 88.20%, #8e0912 94.08%, #67000d 100%);
  background-image: linear-gradient(left, #fff5f0, #ffebe2 11.76%, #ffe1d3 17.64%, #fed0bd 23.52%, #fdbea5 29.40%, #fcab8e 35.28%, #fc9777 41.16%, #fc8363 47.04%, #fc7050 52.92%, #f85a40 58.80%, #f24431 64.68%, #e53127 70.56%, #d42020 76.44%, #c1151a 82.32%, #ad1016 88.20%, #8e0912 94.08%, #67000d 100%);
}
</style>
<h3>{NAME}</h3>
<table style="width: 100%; padding-bottom: 22px">
    <tbody>    
      <tr>
        <td colspan="2">
          <div class="waterLegendGradient" style="clear: both;"></div>
          <div style="color: #656565; font-size: 8px; float: left">2000</div>
          <div style="color: #656565; font-size: 8px; float: right">2018</div>
        </td>
      </tr>
    </tbody>
  </table>
</div>`,
    is_external: true,
    external_layer_type: 'raster',
    external_layer_config: {
      type: 'raster',
      url: 'https://qzxvv33134iutzy.belugacdn.link/lossramp2018/1/{z}/{x}/{y}'
    }
  }
}

export default getLossLayer
export {
  getLossLayer
}
