//  @flow
import * as React from 'react'
import { Card, Button, Row, Col, Switch, Icon } from 'antd'

import LossTable from '../../Charts/LossTable'
import LossBarChart from '../../Charts/LossBarChart'
import PctChart from '../../Charts/PctChart'
import { subscribe } from '../../../lib/unstated-props'
import AOIDashboardContainer from '../../../containers/AOIDashboardContainer'

type Props = {
  height: string,
  t: Function,
  containers: {aoiDashboard: any},
  cardHeadStyle: Object,
  report?: boolean,
  onReady?: Function
}

type State = {
  showTable?: boolean
}

class HistoricalTreeCoverLoss extends React.Component<Props, State> {
  static defaultProps = {
    height: '400px'
  }

  state = {}

  onShowChart = () => this.setState({ showTable: false })
  onShowTable = () => this.setState({ showTable: true })

  componentDidMount () {
    if (this.props.onReady) this.props.onReady()
  }

  render () {
    const { onShowChart, onShowTable } = this
    const { t, height, containers, cardHeadStyle, report } = this.props
    const { showTable } = this.state
    const { aoiDashboard } = containers
    const { results, loading, resultIDFilter, area, showToggle, toggles } = aoiDashboard.state
    const modID = 'tree-cover-loss'
    const toggled = toggles[modID]
    const color = '#d9d9d9'

    let total = 0
    let percent = 0
    let latest = 0
    let latestPct = 0
    let stats = {}
    if (!loading) {
      const result = results[modID][resultIDFilter]
      if (result && result.stats) {
        stats = result.stats
        Object.keys(stats).forEach((key) => {
          total += stats[key]
        })
        latest = stats[18]
      }
      percent = (total / area) * 100
      latestPct = (latest / area) * 100
    }
    return (
      <Card
        title={t('Tree Cover Loss (2001-2018)')}
        headStyle={cardHeadStyle}
        style={{height}}
        bodyStyle={{ padding: 0, height: '100%' }}
        hoverable={false}
        bordered={!report}
      >
        {(showToggle && !report) &&
          <Switch
            style={{
              position: 'absolute', top: '13px', right: '10px', border: 'none'
            }}
            checkedChildren={<Icon type='eye' />}
            unCheckedChildren={<Icon type='eye-invisible' />}
            size='small' defaultChecked={toggled} onChange={(checked) => {
              aoiDashboard.toggle(modID, checked)
            }} />
        }
        <Row style={{ textAlign: 'center' }} type='flex' justify='center'>
          <Col span={8}>
            <b>Total</b>
            <PctChart hectares={total} percent={percent} color={color} />
          </Col>
          <Col span={8}>
            <b>2018</b>
            <PctChart hectares={latest} percent={latestPct} color={color} />
          </Col>
        </Row>

        {!report &&
          <Row style={{ height: 'calc(100% - 141px)' }} >
            {showTable &&
              <>
                <Button shape='circle' icon='bar-chart' type='primary' ghost onClick={onShowChart} size='small'
                  style={{ position: 'absolute', top: '-55px', right: '5px' }}
                />
                <LossTable stats={stats} height={height} t={t} />
              </>
            }
            {!showTable &&
              <>
                <Button shape='circle' icon='table' type='primary' ghost onClick={onShowTable} size='small'
                  style={{ position: 'absolute', top: '-55px', right: '5px' }}
                />
                <LossBarChart stats={stats} height={height} t={t} color={color} />
              </>
            }
          </Row>
        }
        {report &&
          <Row style={{ height: 'calc(100% - 61px)' }} >
            <LossBarChart stats={stats} height={height} t={t} color={color} />
          </Row>
        }
        {!report &&
          <Button
            style={{
              position: 'absolute', bottom: '0px', right: '0px', border: 'none', boxShadow: 'none'
            }}
            onClick={() => {
              aoiDashboard.showInfo((
                <div>
                  <p>Tree canopy loss at 30% density.</p>
                  <p>{t('Source')}: Hansen/UMD/Google/USGS/NASA</p>
                  <p>{t('License')}:<a href='http://creativecommons.org/licenses/by/4.0/' target='_blank' rel='noopener noreferrer'> CC-BY </a></p>
                  <p>
                    <a href='http://earthenginepartners.appspot.com/science-2013-global-forest/download_v1.4.html' target='_blank' rel='noopener noreferrer'>{t('More Info')}</a>
                  </p>
                </div>
              ))
            }}
            shape='circle'
            icon='info-circle-o'
            type='primary'
            ghost
          />
        }
      </Card>
    )
  }
}
export default subscribe(HistoricalTreeCoverLoss, {'aoiDashboard': AOIDashboardContainer})
