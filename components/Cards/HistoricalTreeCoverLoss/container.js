// @flow
import { Container } from 'unstated'
import getLossLayer from './layer-loss'

import type {AOIDashboardState} from '../../../containers/AOIDashboardContainer'
type State = {}

export default class HistoricalTreeCoverLossContainer extends Container<State> {
  init (config: Object, layerConfig: Object, aoiDashboardState: AOIDashboardState) { }

  update (aoiDashboardState: AOIDashboardState) { }

  getLayer (toggled: boolean) {
    return getLossLayer(toggled)
  }
}
