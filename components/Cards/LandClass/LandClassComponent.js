//  @flow
import * as React from 'react'
import { Card, Button, Switch, Row, Col, Statistic, Progress } from 'antd'
import { PieChart, Pie, Cell, Tooltip, Legend } from 'recharts'
import numeral from 'numeral'
import AutoSizer from 'react-virtualized-auto-sizer'
import { subscribe } from '../../../lib/unstated-props'
import AOIDashboardContainer from '../../../containers/AOIDashboardContainer'
import getColVal from '../../../utils/get-table-col-value'

type Props = {
  t: Function,
  containers: {aoiDashboard: any},
  cardHeadStyle: Object,
  cardConfig?: Object,
  report?: boolean,
  onReady?: Function
}

class LandClassComponent extends React.Component<Props, void> {
  static defaultProps = {
    threshold: 80
  }

  componentDidMount () {
    if (this.props.onReady) this.props.onReady()
  }

  render () {
    const { t, containers, cardHeadStyle, cardConfig, report } = this.props
    const { aoiDashboard } = containers
    const { results, area, toggles, locale, loading, showToggle, resultIDFilter } = aoiDashboard.state
    const modID = cardConfig.moduleID
    const toggled = toggles[modID]
    // in this case dataIndex should be list of objects defining the code value, color, and label of each land class
    const dataIndex = cardConfig.dataIndex
    if (!dataIndex || !Array.isArray(dataIndex) || dataIndex.length === 0) {
      return (<p>Invalid card configuration</p>)
    }

    let data = []
    const dataUnsorted = []
    if (!loading) {
      const result = results[modID][resultIDFilter]
      if (result) {
        dataIndex.map(dataIndexItem => {
          const hectares = getColVal(result, dataIndexItem.index)
          const percent = (hectares / area) * 100
          if (hectares) {
            dataUnsorted.push({
              value: hectares,
              name: dataIndexItem.label,
              color: dataIndexItem.color,
              percent
            })
          }
        })
        data = dataUnsorted.sort((a, b) => {
          let comparison = 0
          if (a.value < b.value) {
            comparison = 1
          } else if (a.value > b.value) {
            comparison = -1
          }
          return comparison
        })
      }
    }

    const cardStyle = { maxWidth: '100%', margin: 'auto' }

    return (
      <Card
        title={cardConfig.title ? t(cardConfig.title) : modID}
        style={cardStyle}
        bodyStyle={{ padding: '2%' }}
        headStyle={cardHeadStyle}
        loading={loading} hoverable={false} bordered={!report}
      >
        {!report &&
          <Button
            style={{
              position: 'absolute', bottom: '0px', right: '0px', border: 'none', boxShadow: 'none'
            }}
            onClick={() => {
              aoiDashboard.showInfo((
                <>
                  {cardConfig.attribution &&
                    <div dangerouslySetInnerHTML={{__html: t(cardConfig.attribution)}} />}
                  {!cardConfig.attribution &&
                    <p>No information available</p>}
                </>
              ))
            }}
            shape='circle'
            icon='info-circle-o'
            type='primary'
            ghost
          />}
        {(showToggle && !report) &&
          <Switch
            style={{
              position: 'absolute', top: '13px', right: '10px', border: 'none'
            }} size='small' defaultChecked={toggled} onChange={(checked) => {
              aoiDashboard.toggle(modID, checked)
            }}
          />}
        <div style={{ height: report ? '480px' : '250px', textAlign: 'center' }}>
          {!loading &&
            <Row style={{height: '100%', width: '100%'}}>
              <Col style={{height: '100%'}} span={16}>
                <AutoSizer>
                  {({ height, width }) => (
                    <PieChart height={height} width={width}>
                      <Pie
                        data={data}
                        outerRadius={(width / 2) - 10}
                        label={false}
                        isAnimationActive={false}
                        dataKey='value'
                      >
                        {
                          data.map((entry, index) => <Cell fill={entry.color} />)
                        }
                      </Pie>
                      <Tooltip
                        formatter={(val) => { return `${numeral(val).format('0,0')} ha` }}
                      />
                    </PieChart>
                  )}
                </AutoSizer>
              </Col>
              <Col style={{height: '100%', overflowY: 'auto'}} span={8}>
                <div style={{width: '100%', position: 'absolute', top: '50%', transform: 'translateY(-50%)', overflowY: 'auto'}}>
                  {
                    data.map((entry, index) => {
                      const percent = Math.round(entry.percent)
                      if (index < 5) {
                        return (
                          <Row key={`item-${index}`} style={{marginBottom: '10px'}}>
                            <Col span={8} style={{padding: '0px'}}>
                              <Progress style={{color: '#323333'}} type='circle' percent={percent} width={report ? 50 : 25} strokeColor='#21C6D8' strokeWidth={report ? 16 : 8} />
                            </Col>
                            <Col span={16} className='land-class-stat'>
                              <Row>
                                <Col span={4}><div style={{width: report ? '20px' : '10px', height: report ? '20px' : '10px', backgroundColor: entry.color}} /></Col>
                                <Col span={20}><p style={{fontSize: report ? '20px' : '10px', lineHeight: report ? '20px' : '10px', textAlign: 'left', fontWeight: 600, margin: 0}}>{entry.name}</p></Col>
                              </Row>
                              <Row>
                                <Statistic
                                  value={entry.value}
                                  precision={2}
                                  formatter={(val) => numeral(val).format('0,0.00')}
                                  valueStyle={{ color: '#323333', fontSize: report ? '24px' : '12px', textAlign: 'left' }}
                                  suffix='ha'
                                />
                                <style jsx global>{`
                                  .land-class-stat .ant-statistic-content-value-decimal {
                                    font-size: ${report ? '20px' : '10px'};
                                  }
                                  .land-class-stat .ant-statistic-content-suffix {
                                    font-size: ${report ? '20px' : '10px'};
                                  }
                                `}
                                </style>
                              </Row>
                            </Col>
                          </Row>
                        )
                      } else {
                        return ''
                      }
                    })
                  }
                </div>
              </Col>
            </Row>}
        </div>
      </Card>
    )
  }
}
export default subscribe(LandClassComponent, {aoiDashboard: AOIDashboardContainer})
