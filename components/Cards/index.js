import Remaining from './Remaining'
import IFLLoss from './IFLLoss'
import IFLRemaining from './IFLRemaining'
import HistoricalTreeCover from './HistoricalTreeCover'
import HistoricalTreeCoverLoss from './HistoricalTreeCoverLoss'
import GLAD from './GLAD'
import Fires from './Fires'
import IDNPlantation from './IDN_Plantation'
import IDNPrimary from './IDN_Primary'
import SingleStat from './SingleStat'
import LandClass from './LandClass'

export default {
  'tree-cover-stat': Remaining,
  ifl: IFLRemaining,
  'ifl-loss': IFLLoss,
  glad: GLAD,
  fires: Fires,
  'tree-cover-chart': HistoricalTreeCover,
  'loss-chart': HistoricalTreeCoverLoss,
  'idn-plantation': IDNPlantation,
  'idn-primary': IDNPrimary,
  'single-stat': SingleStat,
  'land-class': LandClass
}
