//  @flow
import * as React from 'react'
import { Card, Button, Switch } from 'antd'
import PctChart from '../../Charts/PctChart'
import { subscribe } from '../../../lib/unstated-props'
import AOIDashboardContainer from '../../../containers/AOIDashboardContainer'

type Props = {
  height?: string,
  t: Function,
  containers: {aoiDashboard: any},
  cardHeadStyle: Object
}

class IDNPlantation extends React.Component<Props, void> {
  static defaultProps = {
    height: '40px'
  }

  render () {
    const { t, height, containers, cardHeadStyle } = this.props
    const { aoiDashboard } = containers
    const { results, area, toggles, showToggle, locale, loading, resultIDFilter } = aoiDashboard.state
    const modID = 'id-plantations-2017'
    const toggled = toggles[modID]
    let hectares = 0
    let percent = 0
    if (!loading) {
      const result = results[modID][resultIDFilter]
      if (result) {
        hectares = result.area
        percent = (hectares / area) * 100
      }
    }
    return (
      <Card
        title={t('2017 Plantations Indonesia')}
        headStyle={cardHeadStyle}
        bodyStyle={{ padding: '2%' }} loading={loading} hoverable={false} bordered>
        <Button
          style={{
            position: 'absolute', bottom: '0px', right: '0px', border: 'none', boxShadow: 'none'
          }}
          onClick={() => {
            aoiDashboard.showInfo((
              <div>
                <p>Indonesia land cover data from <a href='http://webgis.menlhk.go.id:8080/kemenhut/index.php/en/'>Ministry of Forestry and Environment</a></p>
              </div>
            ))
          }}
          shape='circle'
          icon='info-circle-o'
          type='primary'
          ghost
        />
        {showToggle &&
          <Switch style={{
            position: 'absolute', top: '13px', right: '10px', border: 'none'
          }} size='small' defaultChecked={toggled} onChange={(checked) => {
            aoiDashboard.toggle(modID, checked)
          }} />
        }
        <div style={{ height, textAlign: 'center' }}>
          {!loading &&
            <PctChart language={locale} percent={percent} hectares={hectares} color='rgba(244,67,54,0.65)' />
          }
        </div>
      </Card>
    )
  }
}
export default subscribe(IDNPlantation, {'aoiDashboard': AOIDashboardContainer})
