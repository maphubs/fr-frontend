import * as React from 'react'

const tree = (n) => {
  return (<div className='trunk'>{branches(n)}</div>)
}

const branches = (n) => {
  if (n > 0) {
    if (n < 3) {
      return (
        <>
          <div className='leaves'>{branches(n - 1)}</div>
          <div className='leaves'>{branches(n - 1)}</div>
        </>
      )
    } else {
      return (
        <>
          <div>{branches(n - 1)}</div>
          <div>{branches(n - 1)}</div>
        </>
      )
    }
  } else {
    return ''
  }
}

const trunkColor = '#323333'
const leafColor = '#42B532'

export default class Spinner extends React.Component {
  shouldComponentUpdate () { return false }
  render () {
    return (
      <div style={{position: 'absolute', right: '50%', bottom: 'calc(50% - 30px)'}}>
        {tree(9)}
        <style jsx global>{`
          .leaves {
            background: ${leafColor} !important;
          }
          .trunk,
.trunk div {
  background: ${trunkColor};
  will-change: transform;
  width: 10vmin;
  height: 1vmin;
  position: absolute;
  margin-left: -1vmin;
  -webkit-animation-name: rot;
          animation-name: rot;
  -webkit-animation-duration: 4.53124s;
          animation-duration: 4.53124s;
  -webkit-animation-iteration-count: infinite;
          animation-iteration-count: infinite;
  -webkit-animation-direction: alternate;
          animation-direction: alternate;
  -webkit-transform-origin: 1vmin 1vmin;
          transform-origin: 1vmin 1vmin;
  -webkit-animation-timing-function: ease-in-out;
          animation-timing-function: ease-in-out;
}
.trunk,
.trunk div {
  background: ${trunkColor};
  will-change: transform;
  width: 30px;
  height: 5px;
  position: absolute;
  margin-left: -5px;
  -webkit-animation-name: rot;
          animation-name: rot;
  -webkit-animation-duration: 2.26562s;
          animation-duration: 2.26562s;
  -webkit-animation-iteration-count: infinite;
          animation-iteration-count: infinite;
  -webkit-animation-direction: alternate;
          animation-direction: alternate;
  -webkit-transform-origin: 2.5px 2.5px;
          transform-origin: 2.5px 2.5px;
  -webkit-animation-timing-function: ease-in-out;
          animation-timing-function: ease-in-out;
}
.trunk.trunk,
.trunk div.trunk {
  bottom: 0;
  left: 50%;
  -webkit-animation-name: rot-root;
          animation-name: rot-root;
  -webkit-animation-duration: 2.46428s;
          animation-duration: 2.46428s;
}
.trunk >div,
.trunk div >div {
  top: 0;
  left: 30px;
  -webkit-animation-duration: calc((inherit / 2));
          animation-duration: calc((inherit / 2));
}
.trunk >div:nth-child(2),
.trunk div >div:nth-child(2) {
  -webkit-animation-name: rot-inv;
          animation-name: rot-inv;
  -webkit-animation-duration: 1.777351213486541s;
          animation-duration: 1.777351213486541s;
}
@-webkit-keyframes rot {
  from {
    -webkit-transform: rotate(15deg) scale(0.72);
            transform: rotate(15deg) scale(0.72);
  }
  to {
    -webkit-transform: rotate(45deg) scale(0.72);
            transform: rotate(45deg) scale(0.72);
  }
}
@keyframes rot {
  from {
    -webkit-transform: rotate(15deg) scale(0.72);
            transform: rotate(15deg) scale(0.72);
  }
  to {
    -webkit-transform: rotate(45deg) scale(0.72);
            transform: rotate(45deg) scale(0.72);
  }
}
@-webkit-keyframes rot-inv {
  from {
    -webkit-transform: rotate(-45deg) scale(0.72);
            transform: rotate(-45deg) scale(0.72);
  }
  to {
    -webkit-transform: rotate(-15deg) scale(0.72);
            transform: rotate(-15deg) scale(0.72);
  }
}
@keyframes rot-inv {
  from {
    -webkit-transform: rotate(-45deg) scale(0.72);
            transform: rotate(-45deg) scale(0.72);
  }
  to {
    -webkit-transform: rotate(-15deg) scale(0.72);
            transform: rotate(-15deg) scale(0.72);
  }
}
@-webkit-keyframes rot-root {
  from {
    -webkit-transform: rotate(-95deg);
            transform: rotate(-95deg);
  }
  to {
    -webkit-transform: rotate(-85deg);
            transform: rotate(-85deg);
  }
}
@keyframes rot-root {
  from {
    -webkit-transform: rotate(-95deg);
            transform: rotate(-95deg);
  }
  to {
    -webkit-transform: rotate(-85deg);
            transform: rotate(-85deg);
  }
}


        `}</style>
      </div>
    )
  }
}
