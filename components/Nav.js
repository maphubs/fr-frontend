import * as React from 'react'
import Link from 'next/link'

type Props = {
  t: any
}

const NavComponent = ({t}: Props) => (
  <nav>
    <li>
      <Link href='/'>
        <a href='#'>{t('test')}</a>
      </Link>
    </li>
    <li>
      <Link href='/about'>
        <a href='#'>test</a>
      </Link>
    </li>

    <style jsx>{`
      nav {
        display: flex;
      }
      li {
        list-style: none;
        margin-right: 1rem;
      }
    `}
    </style>
  </nav>
)

export default NavComponent
