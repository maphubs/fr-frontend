// @flow
import * as React from 'react'
import { Button, Divider, Icon, Input, Popconfirm, Table, Tooltip } from 'antd'
import Mutation from '../Mutation'
import gql from 'graphql-tag'
import AutoSizer from 'react-virtualized-auto-sizer'
import Highlighter from 'react-highlight-words'
import slugify from 'slugify'
import Link from 'next/link'
import RerunAOIScreenshots from './components/RerunAOIScreenshots'
import { DndProvider, DragSource, DropTarget } from 'react-dnd'
import HTML5Backend from 'react-dnd-html5-backend'
import update from 'immutability-helper'

let dragingIndex = -1

type BodyRowProps = {
  isOver: boolean,
  connectDragSource: any,
  connectDropTarget: any,
  moveRow: any,
  restProps: {style: Object, className: string, index: number}
}

class BodyRow extends React.Component<BodyRowProps, void> {
  render () {
    const {
      isOver,
      connectDragSource,
      connectDropTarget,
      ...restProps
    } = this.props
    const style = { ...restProps.style, cursor: 'move' }

    let className = restProps.className
    if (isOver) {
      if (restProps.index > dragingIndex) {
        className += ' drop-over-downward'
      }
      if (restProps.index < dragingIndex) {
        className += ' drop-over-upward'
      }
    }

    return connectDragSource(
      connectDropTarget(
        <tr
          {...restProps}
          className={className}
          style={style}
        />
      )
    )
  }
}

const rowSource = {
  beginDrag (props) {
    dragingIndex = props.index
    return {
      index: props.index
    }
  }
}

const rowTarget = {
  drop (props, monitor) {
    const dragIndex = monitor.getItem().index
    const hoverIndex = props.index

    // Don't replace items with themselves
    if (dragIndex === hoverIndex) {
      return
    }

    // Time to actually perform the action
    props.moveRow(dragIndex, hoverIndex)

    // Note: we're mutating the monitor item here!
    // Generally it's better to avoid mutations,
    // but it's good here for the sake of performance
    // to avoid expensive index searches.
    monitor.getItem().index = hoverIndex
  }
}

const DragableBodyRow = DropTarget(
  'row',
  rowTarget,
  (connect, monitor) => ({
    connectDropTarget: connect.dropTarget(),
    isOver: monitor.isOver()
  })
)(
  DragSource(
    'row',
    rowSource,
    (connect) => ({
      connectDragSource: connect.dragSource()
    })
  )(BodyRow)
)

type Props = {
  report: Object,
  aois: Array<Object>,
  t: Function
}

type State = {
  aoiSearchText: string,
  data: Object
}

export default class ReportAOIs extends React.Component<Props, State> {
  constructor (props: Props) {
    super(props)
    const data = props.aois.map(aoi => {
      aoi.key = aoi?.aoi?.id
      return aoi
    })
    this.state = {
      data,
      aoiSearchText: ''
    }
  }

  components = {
    body: {
      row: DragableBodyRow
    }
  }
  /*
  componentWillReceiveProps (nextProps: Props) {
    if (nextProps.aois && nextProps.aois.length !== this.state.data.length) {
      const data = nextProps.aois.map(aoi => {
        aoi.key = aoi?.aoi?.id
        return aoi
      })
      this.setState({
        data
      })
    }
  }
*/

  moveRow = (dragIndex: number, hoverIndex: number) => {
    const { data } = this.state
    const dragRow = data[dragIndex]

    this.setState(
      update(this.state, {
        data: {
          $splice: [[dragIndex, 1], [hoverIndex, 0, dragRow]]
        }
      })
    )
  }

  handleSearch = (selectedKeys: Array<string>, confirm: Function) => {
    confirm()
    this.setState({ aoiSearchText: selectedKeys[0] })
  }

  handleReset = (clearFilters: Function) => {
    clearFilters()
    this.setState({ aoiSearchText: '' })
  }

  aoiSearchInput: any
  render () {
    const { report, t } = this.props
    const aoiColumns = [{
      title: t('ID'),
      dataIndex: 'aoi.id',
      width: 150,
      key: 'id',
      sorter: (a, b) => {
        let aVal = a.aoi.id
        let bVal = b.aoi.id
        if (typeof aVal !== 'string') aVal = ''
        if (typeof bVal !== 'string') bVal = ''
        const aValLower = aVal.toLowerCase()
        const bValLower = bVal.toLowerCase()
        return aValLower.localeCompare(bValLower)
      }
    }, {
      title: t('Name'),
      dataIndex: 'aoi.properties.name',
      key: 'name',
      sorter: (a, b) => {
        let aVal = a.aoi.properties && a.aoi.properties.name
        let bVal = b.aoi.properties && b.aoi.properties.name
        if (typeof aVal !== 'string') aVal = ''
        if (typeof bVal !== 'string') bVal = ''
        const aValLower = aVal.toLowerCase()
        const bValLower = bVal.toLowerCase()
        return aValLower.localeCompare(bValLower)
      },
      filterDropdown: ({
        setSelectedKeys, selectedKeys, confirm, clearFilters
      }) => (
        <div style={{ padding: 8 }}>
          <Input
            ref={node => { this.aoiSearchInput = node }}
            placeholder={t('Search')}
            value={selectedKeys[0]}
            onChange={e => setSelectedKeys(e.target.value ? [e.target.value] : [])}
            onPressEnter={() => this.handleSearch(selectedKeys, confirm)}
            style={{ width: 188, marginBottom: 8, display: 'block' }}
          />
          <Button
            type='primary'
            onClick={() => this.handleSearch(selectedKeys, confirm)}
            icon='search'
            size='small'
            style={{ width: 90, marginRight: 8 }}
          >
            {t('Search')}
          </Button>
          <Button
            onClick={() => this.handleReset(clearFilters)}
            size='small'
            style={{ width: 90 }}
          >
            {t('Reset')}
          </Button>
        </div>
      ),
      filterIcon: filtered => <Icon type='search' style={{ color: filtered ? '#1890ff' : undefined }} />,
      onFilter: (value, record) => {
        if (record.aoi.properties && record.aoi.properties.name && typeof record.aoi.properties.name === 'string') {
          return record.aoi.properties.name.toLowerCase().includes(value.toLowerCase())
        }
      },
      onFilterDropdownVisibleChange: (visible) => {
        if (visible) {
          setTimeout(() => this.aoiSearchInput.select())
        }
      },
      render: (text) => (
        <Highlighter
          highlightStyle={{ backgroundColor: '#ffc069', padding: 0 }}
          searchWords={[this.state.aoiSearchText]}
          autoEscape
          textToHighlight={text}
        />
      )
    }, {
      title: t('List'),
      dataIndex: 'aoi.list.name',
      key: 'list',
      width: 300,
      sorter: (a, b) => {
        let aVal = t(a?.list?.name)
        let bVal = t(b?.list?.name)
        if (typeof aVal !== 'string') aVal = ''
        if (typeof bVal !== 'string') bVal = ''
        const aValLower = aVal.toLowerCase()
        const bValLower = bVal.toLowerCase()
        return aValLower.localeCompare(bValLower)
      },
      render: (text, record) => (
        <span>{t(record.aoi.list.name)}</span>
      )
    }, {
      title: t('Action'),
      width: 150,
      key: 'action',
      render: (text, record) => (
        <span>
          <Link
            href={{
              pathname: '/dashboard',
              query: {
                aoiID: record.aoi.id,
                listID: record.aoi.list.id,
                listName: t(record.aoi.list.name),
                name: record.aoi.properties.name
              }
            }}
            as={`/dashboard/${record.aoi.list.id}/${record.aoi.id}/${slugify(t(record.aoi.list.name))}/${slugify(record.aoi.properties.name)}#reportID=${report.id}`}
          >
            <Tooltip placement='bottom' title={t('Edit')}>
              <Button icon='edit' shape='circle' size='small' />
            </Tooltip>
          </Link>
          <Divider type='vertical' />
          <RerunAOIScreenshots report_id={report.id} aoi_id={record.aoi.id} t={t} />
          <Divider type='vertical' />
          <Mutation
            query={gql`
              mutation deleteReportAOI($id: String!, $aoi_id: String!) {
                deleteReportAOI(id: $id, aoi_id: $aoi_id) {
                  id
                  aois {
                    aoi {
                      id
                      properties
                    }
                  }
                }
              }
            `}
            successMessage={t('Area Removed!')}
          >
            {(deleteReportAOI) => (
              <Popconfirm
                title={t('Remove this Area?')}
                onConfirm={() => {
                  deleteReportAOI({
                    id: report.id,
                    aoi_id: record.aoi.id
                  })
                }}
                okText={t('Yes')}
                cancelText={t('No')}
              >
                <Tooltip placement='bottom' title={t('Remove Area')}>
                  <Button icon='delete' shape='circle' size='small' type='danger' />
                </Tooltip>
              </Popconfirm>
            )}
          </Mutation>
        </span>
      )
    }]
    return (
      <AutoSizer disableWidth>
        {({ height }) => (
          <Mutation
            query={gql`
              mutation updateReportAOIPriorities($id: String!, $aois: [JSON]!) {
                updateReportAOIPriorities(id: $id, aois: $aois)
              }
            `}
            successMessage={t('Area Order Updated')}
          >
            {(updateReportAOIPriorities) => (
              <DndProvider backend={HTML5Backend}>
                <Table
                  dataSource={this.state.data}
                  components={this.components}
                  columns={aoiColumns}
                  onRow={(record, index) => ({
                    index,
                    moveRow: async (dragIndex, hoverIndex) => {
                      this.moveRow(dragIndex, hoverIndex)
                      const aois = this.state.data.map((record, i) => {
                        return {
                          aoi_id: record.aoi.id,
                          priority: i + 1
                        }
                      })
                      updateReportAOIPriorities({id: report.id, aois})
                    }
                  })}
                  size='small'
                  scroll={{ y: height - 150 }}
                  pagination={{
                    pageSize: 100,
                    showSizeChanger: true,
                    pageSizeOptions: ['25', '50', '100']
                  }}
                />
              </DndProvider>
            )}
          </Mutation>
        )}
      </AutoSizer>
    )
  }
}
