// @flow
import * as React from 'react'
import { Button, Popconfirm, Table, Tooltip, Modal } from 'antd'
import Mutation from '../Mutation'
import gql from 'graphql-tag'
import AutoSizer from 'react-virtualized-auto-sizer'

import type { Observation, Photo } from '../../types/observation'

type Props = {
  report: Object,
  observations: Array<Observation>,
  t: Function
}

type State = {
  viewPhoto?: Photo
}

export default class ReportObservations extends React.Component<Props, State> {
  constructor (props: Props) {
    super()
    this.state = {}
  }

  showPhoto = (photo: Photo) => {
    console.log('showing photo')
    console.log(photo)
    this.setState({viewPhoto: photo})
  }

  closePhoto = () => {
    this.setState({viewPhoto: undefined})
  }

  render () {
    const { report, observations, t } = this.props
    const { viewPhoto } = this.state
    const observationColumns = [{
      title: t('Area'),
      dataIndex: 'aoi_id',
      width: 150,
      key: 'aoi_id',
      sorter: (a, b) => {
        let aVal = a.aoi.id
        let bVal = b.aoi.id
        if (typeof aVal !== 'string') aVal = ''
        if (typeof bVal !== 'string') bVal = ''
        const aValLower = aVal.toLowerCase()
        const bValLower = bVal.toLowerCase()
        return aValLower.localeCompare(bValLower)
      }
    }, {
      title: t('Time'),
      width: 150,
      dataIndex: 'photo.exif.DateTime',
      key: 'time',
      sorter: (a, b) => {
        let aVal = a.aoi.properties && a.aoi.properties.name
        let bVal = b.aoi.properties && b.aoi.properties.name
        if (typeof aVal !== 'string') aVal = ''
        if (typeof bVal !== 'string') bVal = ''
        const aValLower = aVal.toLowerCase()
        const bValLower = bVal.toLowerCase()
        return aValLower.localeCompare(bValLower)
      }
    }, {
      title: t('Latitude'),
      width: 150,
      dataIndex: 'location.coords.latitude',
      key: 'latitude',
      sorter: (a, b) => {
        let aVal = a.aoi.properties && a.aoi.properties.name
        let bVal = b.aoi.properties && b.aoi.properties.name
        if (typeof aVal !== 'string') aVal = ''
        if (typeof bVal !== 'string') bVal = ''
        const aValLower = aVal.toLowerCase()
        const bValLower = bVal.toLowerCase()
        return aValLower.localeCompare(bValLower)
      }
    }, {
      title: t('Longitude'),
      dataIndex: 'location.coords.longitude',
      width: 150,
      key: 'longitude',
      sorter: (a, b) => {
        let aVal = a.aoi.properties && a.aoi.properties.name
        let bVal = b.aoi.properties && b.aoi.properties.name
        if (typeof aVal !== 'string') aVal = ''
        if (typeof bVal !== 'string') bVal = ''
        const aValLower = aVal.toLowerCase()
        const bValLower = bVal.toLowerCase()
        return aValLower.localeCompare(bValLower)
      }
    }, {
      title: t('Notes'),
      dataIndex: 'notes',
      key: 'notes'
    }, {
      title: t('Action'),
      width: 150,
      key: 'action',
      render: (text, record) => (
        <span>
          <Tooltip placement='bottom' title={t('View Photo')}>
            <Button
              icon='camera' shape='circle' size='small'
              style={{marginRight: '20px'}}
              onClick={() => {
                this.showPhoto(record.photo)
              }}
            />
          </Tooltip>
          <Mutation
            query={gql`
              mutation deleteReportObservation($id: String!, $aoi_id: String!) {
                deleteReportObservation(id: $id, aoi_id: $aoi_id) {
                  id
                  observations {
                    aoi {
                      id
                      properties
                    }
                  }
                }
              }
            `}
            successMessage='Observation Removed!'
          >
            {(deleteReportObservation) => (
              <Popconfirm
                title='Remove this Observation?'
                onConfirm={() => {
                  deleteReportObservation({
                    id: report.id,
                    report_id: record.report_id,
                    aoi_id: record.aoi_id
                  })
                }}
                okText='Yes'
                cancelText='No'
              >
                <Tooltip placement='bottom' title={t('Remove Observation')}>
                  <Button icon='delete' shape='circle' size='small' type='danger' />
                </Tooltip>
              </Popconfirm>
            )}
          </Mutation>
        </span>
      )
    }]
    return (
      <>
        <AutoSizer disableWidth>
          {({ height }) => (
            <Table
              dataSource={observations}
              columns={observationColumns}
              size='small'
              scroll={{ y: height - 150 }}
              pagination={{
                pageSize: 100,
                showSizeChanger: true,
                pageSizeOptions: ['25', '50', '100']
              }}
            />
          )}
        </AutoSizer>
        <Modal
          visible={viewPhoto}
          onCancel={this.closePhoto}
          footer={false}
          width='90vw'
          bodyStyle={{height: '90vh', textAlign: 'center'}}
          style={{ top: 20 }}
        >
          <img src={viewPhoto?.base64} style={{height: '100%', width: 'auto'}} />
        </Modal>
      </>
    )
  }
}
