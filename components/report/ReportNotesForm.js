// @flow
import * as React from 'react'
import { Row, Button } from 'antd'
import Mutation from '../Mutation'
import gql from 'graphql-tag'
import $ from 'jquery'
import AutoSizer from 'react-virtualized-auto-sizer'
import dynamic from 'next/dynamic'
const CKEditorReport = dynamic(() => import('../form/CKEditorReport.js'), {
  ssr: false
})

type Props = {
  id: string,
  notes: Object,
  t: Function
}

type State = {
  modified?: boolean,
  notes: Object
}

export default class ReportNotesForm extends React.Component<Props, State> {
  constructor (props: Props) {
    super(props)
    this.state = {
      notes: props.notes
    }
  }

  editor: any

  onNotesChange = (lang: string, update: string) => {
    const notes = this.state.notes || {}
    notes[lang] = update
    this.setState({notes, modified: true})
  }

  getFirstLine = () => {
    const firstLine = $('.document-editor__editable').find('p')
      .filter(function () {
        return ($.trim($(this).text()).length)
      }).first().text()
    return firstLine
  }

  getFirstImage = () => {
    let firstImg = null
    const firstEmbed = $('.document-editor__editable').find('img').first()
    firstImg = firstEmbed.attr('src')
    return firstImg
  }

  render () {
    const { onNotesChange, getFirstLine, getFirstImage } = this
    const { id, t } = this.props
    const { notes, modified } = this.state
    return (
      <Mutation
        query={gql`
          mutation updateReport($id: String!, $body: JSON, $first_image: String, $first_line: String) {
            updateReport(id: $id, body: $body, first_image: $first_image, first_line: $first_line) {
              id
              body
              first_image
              first_line
            }
          }
        `}
        successMessage='Saved'
        onComplete={() => {
          this.setState({modified: false})
        }}
      >
        {(updateReport, { fetching }) => (
          <Row style={{height: '100%'}}>
            <div style={{position: 'absolute', right: '20px'}}>
              {
                modified &&
                  <span style={{marginRight: '10px'}}>{t('Not Saved')}</span>
              }
              <Button
                type='primary'
                disabled={!modified}
                loading={fetching}
                style={{zIndex: 99}}
                onClick={() => {
                  updateReport({
                    id,
                    body: notes,
                    first_image: getFirstImage(),
                    first_line: getFirstLine()
                  })
                }}
              >{t('Save')}
              </Button>
            </div>
            <AutoSizer disableWidth>
              {({ height }) => (
                <Row style={{height: `${height - 16}px`}}>
                  <CKEditorReport
                    reportID={id}
                    getMap={() => { alert(t('Not supported for PDF reports, please insert a image/screenshot of the map.')) }}
                    cropImage={(image) => { return image }}
                    initialData={notes}
                    onChange={onNotesChange}
                  />
                </Row>
              )}
            </AutoSizer>
          </Row>
        )}
      </Mutation>
    )
  }
}
