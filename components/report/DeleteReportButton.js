// @flow
import * as React from 'react'
import { Button, Popconfirm } from 'antd'
import Mutation from '../Mutation'
import gql from 'graphql-tag'

type Props = {
  id: string,
  t: Function
}

export default class DeleteReportButton extends React.Component<Props, void> {
  render () {
    const { id, t } = this.props
    return (
      <Mutation
        query={gql`
          mutation deleteReport($id: String!) {
            deleteReport(id: $id)
          }
        `}
        successMessage={t('Report Deleted')}
      >
        {(deleteReport) => (
          <Popconfirm
            title={t('Delete this report and all data?')}
            okText={t('Yes')} cancelText={t('Cancel')}
            onConfirm={() => {
              deleteReport({id})
            }}
          >
            <Button icon='delete' shape='circle' type='danger' size='small' />
          </Popconfirm>
        )}
      </Mutation>
    )
  }
}
