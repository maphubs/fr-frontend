// @flow
import * as React from 'react'
import { Row, Col, Typography } from 'antd'
import ModuleRow from '../components/ModuleRow'
import PageLayout from '../components/PageLayout'
import turfCentroid from '@turf/centroid'
import DmsCoordinates from 'dms-conversion'
import wgs84Util from 'wgs84-util'
import numeral from 'numeral'
import getPlaceName from '../../../utils/getPlaceName'

const { Paragraph } = Typography

type Props = {|
  aoi: Object,
  modules: Array<Object>,
  config?: Object,
  pageNumber: number,
  logo: string,
  accentColor: string,
  copyright: string,
  t: Function
|}

type State = {
  placeName: string,
  lon: number,
  lat: number,
  centroid: any
}

export default class AOIOverviewPage extends React.Component<Props, State> {
  state = {}

  async componentWillMount () {
    const { aoi } = this.props
    const feature = aoi.aoi.feature
    let centroid

    if (feature.geometry.type === 'Point') {
      centroid = feature
    } else {
      centroid = turfCentroid(feature)
    }

    const lon = centroid.geometry.coordinates[0]
    const lat = centroid.geometry.coordinates[1]
    this.setState({lon, lat, centroid})
    const placeName = await getPlaceName(lon, lat)
    this.setState({placeName})
  }

  render () {
    const { aoi, modules, config, t } = this.props
    const { lat, lon, placeName, centroid } = this.state
    const mod1 = modules[0]
    const mod2 = modules[1]

    const utm = wgs84Util.LLtoUTM(centroid.geometry)
    const dmsCoords = new DmsCoordinates(lat, lon)
    const lonC = dmsCoords.dmsArrays.longitude
    const latC = dmsCoords.dmsArrays.latitude
    const lonCoordString = `${lonC[0]}°${lonC[1]}′${numeral(lonC[2]).format('0,0.0000')}″ ${lonC[3]}`
    const latCoordString = `${latC[0]}°${latC[1]}′${numeral(latC[2]).format('0,0.0000')}″ ${latC[3]}`

    let properties = [{
      label: t('Latitude'),
      value: `${latCoordString} (${numeral(lat).format('0.000000')})`
    }, {
      label: t('Longitude'),
      value: `${lonCoordString} (${numeral(lon).format('0.000000')})`
    }, {
      label: t('UTM'),
      value: `${utm.properties.zoneNumber}${utm.properties.zoneLetter} ${numeral(utm.geometry.coordinates[0]).format('0.0')} ${numeral(utm.geometry.coordinates[1]).format('0.00')}`
    }, {
      label: t('Place Name'),
      value: placeName
    }

    ]
    if (config && config.attributes) {
      config.attributes.forEach(attr => {
        Object.keys(aoi.aoi.feature.properties).forEach(propKey => {
          if (attr.key === propKey) {
            properties.push({
              label: attr.label,
              value: aoi.aoi.feature.properties[propKey]
            })
          }
        })
      })
    } else {
      properties = properties.concat(Object.keys(aoi.aoi.feature.properties).map(key => {
        return {
          label: key,
          value: aoi.aoi.feature.properties[key]
        }
      }))
    }
    return (
      <PageLayout title={aoi.aoi.properties.name} {...this.props}>
        <Row style={{ paddingTop: '10px' }}>
          <Col span={10}>
            {aoi.overview_screenshot &&
              <img src={aoi.overview_screenshot.url} style={{height: '250px', width: 'auto'}} />}
          </Col>
          <Col span={14}>
            <Row>
              {properties.map(prop => {
                return (
                  <Col key={prop.label} span={8} sm={8} xs={8} style={{padding: '0px 4px 4px 4px', marginBottom: '5px'}}>
                    <Paragraph ellipsis style={{fontSize: '12px', fontWeight: '700', marginBottom: '0px'}}>{prop.label}</Paragraph>
                    <Paragraph ellipsis style={{fontSize: '12px', margin: 0}}>{prop.value}</Paragraph>
                  </Col>
                )
              })}
            </Row>
          </Col>
        </Row>
        <hr style={{color: '#d9d9d9', marginBottom: '0px'}} />
        {mod1 &&
          <>
            <ModuleRow module={mod1} t={t} />
            <hr style={{color: '#d9d9d9', marginBottom: '0px'}} />
          </>}
        {mod2 &&
          <ModuleRow module={mod2} t={t} />}
      </PageLayout>
    )
  }
}
