// @flow
import * as React from 'react'
import PageLayout from '../components/PageLayout'
import { Row } from 'antd'

type Props = {|
  modules: Array<Object>,
  pageNumber: number,
  logo: string,
  accentColor: string,
  copyright: string,
  t: Function
|}

export default class AttributionsPage extends React.Component<Props, void> {
  shouldComponentUpdate () {
    return false
  }

  render () {
    const { modules, t } = this.props
    return (
      <PageLayout title={t('Attributions')} {...this.props}>
        <div style={{padding: '50px'}}>
          {modules.map(mod => {
            if (mod) {
              return (
                <Row key={mod.id} style={{marginBottom: '10px'}}>
                  <h2>{t(mod.name)}</h2>
                  {mod.attribution &&
                    <div className='report-notes-wrapper'>
                      <div dangerouslySetInnerHTML={{__html: mod.attribution}} />
                    </div>}
                  {!mod.attribution &&
                    <p>{t('No attribution provided.')}</p>}
                </Row>
              )
            } else {
              return ''
            }
          })}
        </div>
      </PageLayout>
    )
  }
}
