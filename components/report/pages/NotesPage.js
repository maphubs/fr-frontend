// @flow
import * as React from 'react'
import PageLayout from '../components/PageLayout'

type Props = {|
  notes: string,
  pageNumber: number,
  logo: string,
  accentColor: string,
  copyright: string,
  t: Function
|}

export default class NotesPage extends React.Component<Props, void> {
  shouldComponentUpdate () {
    return false
  }

  render () {
    const { notes, t } = this.props
    return (
      <PageLayout title={t('Summary')} {...this.props}>
        <div className='report-notes-wrapper' style={{padding: '50px'}}>
          <div dangerouslySetInnerHTML={{__html: t(notes)}} />
        </div>
      </PageLayout>
    )
  }
}
