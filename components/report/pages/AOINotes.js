// @flow
import * as React from 'react'
import PageLayout from '../components/PageLayout'

type Props = {|
  aoi: Object,
  notes: string,
  pageNumber: number,
  logo: string,
  accentColor: string,
  copyright: string,
  t: Function
|}

export default class AOINotesPage extends React.Component<Props, void> {
  shouldComponentUpdate () {
    return false
  }

  render () {
    const { aoi, notes, t } = this.props
    return (
      <PageLayout title={aoi.aoi.properties.name} {...this.props}>
        <div style={{padding: '50px'}}>
          {(aoi && notes) &&
            <div className='report-notes-wrapper'>
              <div dangerouslySetInnerHTML={{__html: notes}} />
            </div>}
        </div>
      </PageLayout>
    )
  }
}
