// @flow
import * as React from 'react'
import { Row } from 'antd'
import PageLayout from '../components/PageLayout'

type Props = {|
  aois: Array<Object>,
  notesPages: number,
  pageNumber: number,
  logo: string,
  accentColor: string,
  copyright: string,
  t: Function
|}

export default class TOCPage extends React.Component<Props, void> {
  shouldComponentUpdate () {
    return false
  }

  computeAOIPages = (aoi: Object) => {
    let pageCount = 0
    if (aoi.modules && aoi.modules.length > 0) {
      // 2 modules on first page
      // 3 modules on subsequent pages
      const length = aoi.modules.length
      if (length <= 2) {
        pageCount++ // overview page
      } else if (length > 2) {
        pageCount++ // overview page
        const remaining = length - 2
        pageCount += Math.ceil(remaining / 3)
      }
    }
    if (aoi.drawings && aoi.drawings.drawings && aoi.drawings.drawings.length > 0) {
      // 2 drawings per page
      pageCount += Math.ceil(aoi.drawings.drawings.length / 2)
    }
    if (aoi.notes && aoi.notes.notes) pageCount++ // 1 page for AOI notes if provided
    return pageCount
  }

  computeTOC = (aois: Array<Object>, notesPages: number) => {
    let currentPage = 4 + notesPages
    return aois.map(aoi => {
      const page = currentPage
      currentPage += this.computeAOIPages(aoi)
      return {
        name: aoi.aoi.properties.name,
        page,
        pageAfter: currentPage
      }
    })
  }

  render () {
    const { aois, notesPages, t } = this.props
    const toc = this.computeTOC(aois, notesPages)
    const pageAfterLastAOI = toc[toc.length - 1].pageAfter
    return (
      <PageLayout title={t('Table of Contents')} {...this.props}>
        <Row style={{padding: '50px'}}>
          <table style={{fontSize: '16px'}}>
            {toc.map((item, i) => <tr key={i}><td>{i + 1}.&nbsp;&nbsp;</td><td>{item.name}</td>...........<td>{item.page}</td></tr>)}
            <tr><td>&nbsp;&nbsp;&nbsp;&nbsp;</td><td>{t('Attributions')}</td>...........<td>{pageAfterLastAOI}</td></tr>
          </table>
        </Row>
      </PageLayout>
    )
  }
}
