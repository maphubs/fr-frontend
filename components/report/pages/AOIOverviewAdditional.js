// @flow
import * as React from 'react'
import ModuleRow from '../components/ModuleRow'
import PageLayout from '../components/PageLayout'

type Props = {|
  aoi: Object,
  mod1?: Object,
  mod2?: Object,
  mod3?: Object,
  pageNumber: number,
  logo: string,
  accentColor: string,
  copyright: string,
  t: Function
|}

export default class AOIOverviewAdditionalPage extends React.Component<Props, void> {
  shouldComponentUpdate () {
    return false
  }

  render () {
    const { aoi, mod1, mod2, mod3, t } = this.props

    return (
      <PageLayout title={aoi.aoi.properties.name} {...this.props}>
        {mod1 &&
          <>
            <ModuleRow module={mod1} t={t} />
            <hr style={{color: '#d9d9d9', marginBottom: '0px'}} />
          </>}
        {mod2 &&
          <>
            <ModuleRow module={mod2} t={t} />
            <hr style={{color: '#d9d9d9', marginBottom: '0px'}} />
          </>}
        {mod3 &&
          <ModuleRow module={mod3} t={t} />}
      </PageLayout>
    )
  }
}
