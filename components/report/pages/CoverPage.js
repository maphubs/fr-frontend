// @flow
import * as React from 'react'

type Props = {
  title: Object,
  subtitle: Object,
  author: Object,
  monitoring_period: Object,
  image?: string,
  logo?: string,
  t: Function
}

export default class CoverPage extends React.Component<Props, void> {
  shouldComponentUpdate () {
    return false
  }

  render () {
    const { title, subtitle, monitoring_period, author, t } = this.props
    let { image, logo } = this.props
    image = image || '/static/report_cover.jpg'
    logo = logo || '/static/FR_LOGO_JUNE2019.png'

    return (
      <>
        <div className='report-cover-page'>
          <img src={image} className='report-cover-image' alt='report cover background image' />
          <div className='report-cover-title'>
            <img src={logo} className='report-fr-logo' alt='Forest Report Logo' />
            <div className='verical-divider' />
            <div>
              <h1 className='report-cover-title-text'>{title ? t(title) : ''}</h1>
              <h1 className='report-cover-subtitle-text'>{subtitle ? t(subtitle) : ''}</h1>
            </div>
          </div>
          <div className='report-cover-info'>
            <h2 style={{color: '#32333', fontSize: '1.8em', marginBottom: '.25em'}}>{t('Compiled by')}</h2>
            <h2 style={{color: '#323333', fontSize: '1.8em', marginBottom: '.25em', fontWeight: '300'}}>{author ? t(author) : ''}</h2>
            <h2 style={{color: '#32333', fontSize: '1.8em', marginBottom: '.25em'}}>{t('Monitoring Period')}</h2>
            <h2 style={{color: '#323333', fontSize: '1.8em', marginBottom: '.25em', fontWeight: '300'}}>{monitoring_period ? t(monitoring_period) : ''}</h2>
          </div>
        </div>
        <style jsx>{`
          .report-cover-page {
            position: relative;
            height: 1100px;
            width: 850px;
            margin: auto;
            page-break-after: always;
          }
          .report-cover-image {
            height: 100%;
            width:  100%;
          }
          .report-cover-title {
            position: absolute;
            top: 5%;
            left: 0;
            width: 80%;
            height: 100px;
            background-color: white;
            padding: 10px;
          }
          
          .report-cover-title-text {
            margin: 0;
            font-size: 2em;
            color: #323333;
          }
          .report-cover-subtitle-text {
            font-weight: 300;
            font-size: 2em;
            color: #323333;
          }

          .report-cover-info {
            position: absolute;
            top: 66%;
            right: 0;
            width: 60%;
            height: 200px;
            background-color: white;
            padding: 10px 25px;
          }
          .verical-divider {
            width: 2px;
            height: 100%;
            background-color: #323333;
            border-radius: 5px;
            margin-left: 10px;
            margin-right: 10px;
            margin-top: 2px;
            margin-bottom: 2px;
            float: left;
          }
          .report-fr-logo {
            height: 100%;
            width: auto;
            float: left;
          }
        `}
        </style>
      </>
    )
  }
}
