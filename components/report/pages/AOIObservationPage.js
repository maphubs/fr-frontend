// @flow
import * as React from 'react'
import { Row, Col, Typography } from 'antd'
import PageLayout from '../components/PageLayout'
import DmsCoordinates from 'dms-conversion'
import wgs84Util from 'wgs84-util'
import numeral from 'numeral'
import { QRCode } from 'react-qr-svg'

import type { Observation } from '../../../types/observation'

const { Paragraph } = Typography

type Props = {|
  aoi: Object,
  observation1: Observation,
  observation2?: Observation,
  observation3?:Observation,
  pageNumber: number,
  logo: string,
  accentColor: string,
  copyright: string,
  t: Function
|}

export default class AOIImageryPage extends React.Component<Props, void> {
  shouldComponentUpdate () {
    return false
  }

  render () {
    const { aoi, observation1, observation2, observation3, t } = this.props
    const observations = []
    if (observation1) observations.push(observation1)
    if (observation2) observations.push(observation2)
    if (observation3) observations.push(observation3)
    return (
      <PageLayout title={aoi.aoi.properties.name} {...this.props}>
        <h2>{t('Observations')}</h2>

        <Row>
          {observations.map((obs) => {
            const lon = obs.location.coords.longitude
            const lat = obs.location.coords.latitude
            const utm = wgs84Util.LLtoUTM({coordinates: [lon, lat]})
            const dmsCoords = new DmsCoordinates(lat, lon)
            const lonC = dmsCoords.dmsArrays.longitude
            const latC = dmsCoords.dmsArrays.latitude
            const lonCoordString = `${lonC[0]}°${lonC[1]}′${numeral(lonC[2]).format('0,0.0000')}″ ${lonC[3]}`
            const latCoordString = `${latC[0]}°${latC[1]}′${numeral(latC[2]).format('0,0.0000')}″ ${latC[3]}`

            const properties = [
              {
                label: t('Date'),
                value: obs.photo.exif.DateTime
              },
              {
                label: t('Camera'),
                value: obs.photo.exif.Model
              },
              {
                label: t('Latitude'),
                value: `${latCoordString} (${numeral(lat).format('0.000000')})`
              },
              {
                label: t('Longitude'),
                value: `${lonCoordString} (${numeral(lon).format('0.000000')})`
              },
              {
                label: t('UTM'),
                value: `${utm.properties.zoneNumber}${utm.properties.zoneLetter} ${numeral(utm.geometry.coordinates[0]).format('0.0')} ${numeral(utm.geometry.coordinates[1]).format('0.00')}`
              }
            ]

            return (
              <Row key={obs.id} style={{borderBottom: '1px solid #ddd', marginBottom: '20px'}}>
                <Col span={9} style={{textAlign: 'center'}}>
                  <img src={obs.photo.base64} style={{height: '275px', width: 'auto', margin: '10px'}} />
                </Col>
                <Col span={15}>
                  <Row style={{marginBottom: '20px'}}>
                    <p>{obs.notes}</p>
                  </Row>
                  <Row>
                    {properties.map(prop => {
                      return (
                        <Col key={prop.label} span={10} sm={8} xs={8} style={{padding: '0px 2px 2px 2px', marginBottom: '5px'}}>
                          <Paragraph ellipsis style={{fontSize: '12px', fontWeight: '700', marginBottom: '0px'}}>{prop.label}</Paragraph>
                          <Paragraph ellipsis style={{fontSize: '12px', margin: 0}}>{prop.value}</Paragraph>
                        </Col>
                      )
                    })}
                    <Col span={4} sm={4} xs={4}>
                      <QRCode
                        bgColor='#FFFFFF'
                        fgColor='#000000'
                        level='L'
                        style={{ width: 64 }}
                        value={`geo:${numeral(lat).format('0.000000')},${numeral(lon).format('0.000000')}`}
                      />
                    </Col>
                  </Row>
                </Col>
              </Row>
            )
          })}
        </Row>
      </PageLayout>
    )
  }
}
