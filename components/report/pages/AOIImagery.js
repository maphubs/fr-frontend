// @flow
import * as React from 'react'
import { Row, Col, Typography } from 'antd'
import PageLayout from '../components/PageLayout'
import turfCentroid from '@turf/centroid'
import DmsCoordinates from 'dms-conversion'
import wgs84Util from 'wgs84-util'
import numeral from 'numeral'
import { QRCode } from 'react-qr-svg'

import type { Drawing } from '../../../types/drawing'

const { Paragraph } = Typography

type Props = {|
  aoi: Object,
  drawing1: Drawing,
  drawing2: Drawing,
  pageNumber: number,
  logo: string,
  accentColor: string,
  copyright: string,
  t: Function
|}

function getScreenshot (screenshots, id, type) {
  let result
  screenshots.forEach(screenshot => {
    if (screenshot.drawing_id === id && screenshot.type === type) {
      result = screenshot
    }
  })
  return result
}

export default class AOIImageryPage extends React.Component<Props, void> {
  shouldComponentUpdate () {
    return false
  }

  render () {
    const { aoi, drawing1, drawing2, t } = this.props
    const drawings = []
    if (drawing1) drawings.push(drawing1)
    if (drawing2) drawings.push(drawing2)
    return (
      <PageLayout title={aoi.aoi.properties.name} {...this.props}>
        <h2>{t('Satellite Imagery')}</h2>

        <Row>
          {drawings.map((drawing) => {
            let beforeScreenshot
            let afterScreenshot
            if (aoi.drawing_screenshots) {
              beforeScreenshot = getScreenshot(aoi.drawing_screenshots, drawing.id, 'before')
              afterScreenshot = getScreenshot(aoi.drawing_screenshots, drawing.id, 'after')
            }
            const centroid = turfCentroid({type: 'FeatureCollection', features: drawing.features})
            const utm = wgs84Util.LLtoUTM(centroid.geometry)
            const lon = centroid.geometry.coordinates[0]
            const lat = centroid.geometry.coordinates[1]
            const dmsCoords = new DmsCoordinates(lat, lon)
            const lonC = dmsCoords.dmsArrays.longitude
            const latC = dmsCoords.dmsArrays.latitude
            const lonCoordString = `${lonC[0]}°${lonC[1]}′${numeral(lonC[2]).format('0,0.0000')}″ ${lonC[3]}`
            const latCoordString = `${latC[0]}°${latC[1]}′${numeral(latC[2]).format('0,0.0000')}″ ${latC[3]}`

            const properties = [{
              label: t('Latitude'),
              value: `${latCoordString} (${numeral(lat).format('0.000000')})`
            }, {
              label: t('Longitude'),
              value: `${lonCoordString} (${numeral(lon).format('0.000000')})`
            }, {
              label: t('UTM'),
              value: `${utm.properties.zoneNumber}${utm.properties.zoneLetter} ${numeral(utm.geometry.coordinates[0]).format('0.0')} ${numeral(utm.geometry.coordinates[1]).format('0.00')}`
            }]

            return (
              <Row key={drawing.id}>
                <h2>{t(drawing.name)}</h2>
                {beforeScreenshot &&
                  <Col span={12} style={{textAlign: 'center'}}>
                    <img src={beforeScreenshot.image.url} style={{height: '300px', width: 'auto', margin: '10px'}} />
                  </Col>}
                {afterScreenshot &&
                  <Col span={12} style={{textAlign: 'center'}}>
                    <img src={afterScreenshot.image.url} style={{height: '300px', width: 'auto', margin: '10px'}} />
                  </Col>}
                <Row>
                  {properties.map(prop => {
                    return (
                      <Col key={prop.label} span={6} sm={6} xs={6} style={{padding: '0px 4px 4px 4px', marginBottom: '5px'}}>
                        <Paragraph ellipsis style={{fontSize: '12px', fontWeight: '700', marginBottom: '0px'}}>{prop.label}</Paragraph>
                        <Paragraph ellipsis style={{fontSize: '12px', margin: 0}}>{prop.value}</Paragraph>
                      </Col>
                    )
                  })}
                  <Col span={6} sm={6} xs={6}>
                    <QRCode
                      bgColor='#FFFFFF'
                      fgColor='#000000'
                      level='L'
                      style={{ width: 64 }}
                      value={`geo:${numeral(lat).format('0.000000')},${numeral(lon).format('0.000000')}`}
                    />
                  </Col>
                </Row>
              </Row>
            )
          })}
        </Row>
      </PageLayout>
    )
  }
}
