// @flow
import * as React from 'react'
import PageLayout from '../components/PageLayout'

type Props = {|
  body: Object,
  pageNumber: number,
  logo: string,
  accentColor: string,
  copyright: string,
  t: Function
|}

export default class LicensePage extends React.Component<Props, void> {
  shouldComponentUpdate () {
    return false
  }

  render () {
    const { body, t } = this.props
    return (
      <PageLayout title={t('Copyright and Disclaimer')} {...this.props}>
        <div className='report-notes-wrapper' style={{padding: '50px'}}>
          <div dangerouslySetInnerHTML={{__html: t(body) || ''}} />
        </div>
      </PageLayout>
    )
  }
}
