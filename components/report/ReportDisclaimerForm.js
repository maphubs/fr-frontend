// @flow
import * as React from 'react'
import { Row, Button, Input } from 'antd'
import Mutation from '../Mutation'
import gql from 'graphql-tag'
import dynamic from 'next/dynamic'
const CKEditorNote = dynamic(() => import('../form/CKEditorNote.js'), {
  ssr: false
})

type Props = {
  id: string,
  disclaimer: Object,
  copyright: string,
  t: Function
}

type State = {
  modified?: boolean,
  disclaimer: Object,
  copyright: string
}

export default class ReportNotesForm extends React.Component<Props, State> {
  constructor (props: Props) {
    super(props)
    this.state = {
      disclaimer: props.disclaimer || {en: ''},
      copyright: props.copyright || ''
    }
  }

  editor: any

  setModified = (modified: boolean) => {
    this.setState({modified})
  }

  disclaimerChange = (disclaimer: Object) => {
    this.setState({disclaimer, modified: true})
  }

  copyrightChange = (copyright: string) => {
    this.setState({copyright, modified: true})
  }

  render () {
    const { setModified, disclaimerChange, copyrightChange } = this
    const { id, t } = this.props
    const { disclaimer, copyright, modified } = this.state
    return (
      <Mutation
        query={gql`
          mutation updateReport($id: String!, $disclaimer: JSON, $copyright: String) {
            updateReport(id: $id, disclaimer: $disclaimer, copyright: $copyright) {
              id
              disclaimer
              copyright
            }
          }
        `}
        successMessage='Saved'
        onComplete={() => {
          setModified(false)
        }}
      >
        {(updateReport, {fetching}) => (
          <Row style={{height: '100%'}}>
            <Row>
              <div style={{position: 'absolute', right: '20px'}}>
                {
                  modified &&
                    <span style={{marginRight: '10px'}}>{t('Not Saved')}</span>
                }
                <Button
                  type='primary'
                  disabled={!modified}
                  loading={fetching}
                  style={{zIndex: 99}}
                  onClick={() => {
                    updateReport({
                      id,
                      disclaimer,
                      copyright
                    })
                  }}
                >{t('Save')}
                </Button>
              </div>
            </Row>
            <Row style={{marginBottom: '10px'}}>
              <p><b>{t('Copyright')}</b></p>
              <Input
                type='text' value={copyright}
                placeholder={t('Copyright')}
                onChange={(e) => copyrightChange(e.target.value)}
              />
            </Row>
            <Row style={{height: 'calc(100% - 200px)'}}>
              <CKEditorNote
                initialData={disclaimer.en}
                onChange={(val) => {
                  disclaimerChange({en: val})
                }}
              />
            </Row>
          </Row>
        )}
      </Mutation>
    )
  }
}
