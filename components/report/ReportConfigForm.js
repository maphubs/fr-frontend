// @flow
import * as React from 'react'
import { Row } from 'antd'
import Mutation from '../Mutation'
import gql from 'graphql-tag'
import CodeEditor from '../CodeEditor'

type Props = {
  id: string,
  config: Object,
  t: Function
}

type State = {
  configModified?: boolean,
  config: Object
}

export default class ReportConfigForm extends React.Component<Props, State> {
  constructor (props: Props) {
    super(props)
    this.state = {
      config: props.config
    }
  }

  setConfigModified = (configModified: boolean) => {
    this.setState({configModified})
  }

  render () {
    const { setConfigModified } = this
    const { id, t } = this.props
    const { configModified, config } = this.state

    return (
      <Mutation
        query={gql`
          mutation updateReport($id: String!, $config: JSON) {
            updateReport(id: $id, config: $config) {
              id
              config
            }
          }
        `}
        successMessage='Saved'
        onComplete={() => {
          setConfigModified(false)
        }}
      >
        {(updateReport) => (
          <Row>
            <div style={{height: '400px', width: '600px'}}>
              <CodeEditor
                title={t('Config')}
                onSave={code => {
                  updateReport({ id, config: code })
                }}
                onChange={code => {
                  if (!configModified) {
                    setConfigModified(true)
                  }
                }}
                code={JSON.stringify(config, undefined, 2)}
                t={t}
                showOnMount
              />
              {
                configModified &&
                  <span style={{marginLeft: '10px'}}>{t('Not Saved')}</span>
              }
            </div>
          </Row>
        )}
      </Mutation>
    )
  }
}
