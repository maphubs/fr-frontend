// @flow
import * as React from 'react'
import { Button, Col, DatePicker, Input, Row, Switch } from 'antd'
import Mutation from '../Mutation'
import gql from 'graphql-tag'
import moment from 'moment'
import { SketchPicker } from 'react-color'
import LocalizedInput from '../form/LocalizedInput'

type Props = {
  id: string,
  status: string,
  published_at: string,
  title: Object,
  subtitle: Object,
  author: Object,
  monitoring_period: Object,
  accent_color: string,
  cover_image: string,
  logo: string,
  t: Function
}

type State = {
  modified?: boolean,
  published?: boolean,
  title: Object,
  subtitle: Object,
  author: Object,
  monitoring_period: Object,
  publishedDate?: string,
  accent_color: string,
  cover_image: string,
  logo: string
}

export default class ReportInfoForm extends React.Component<Props, State> {
  constructor (props: Props) {
    super(props)
    this.state = {
      title: props.title,
      subtitle: props.subtitle,
      author: props.author,
      monitoring_period: props.monitoring_period,
      accent_color: props.accent_color || '#21C6D8',
      cover_image: props.cover_image,
      logo: props.logo
    }
  }

  setModified = (modified: boolean) => {
    this.setState({modified})
  }

  publishDateChange = (date: Object) => {
    this.setState({publishedDate: date.format('YYYY-MM-DD'), modified: true})
  }

  togglePublished = (published: boolean) => {
    this.setState({published, modified: true})
  }

  titleChange = (title: Object) => {
    this.setState({title, modified: true})
  }

  subtitleChange = (subtitle: Object) => {
    this.setState({subtitle, modified: true})
  }

  authorChange = (author: Object) => {
    this.setState({author, modified: true})
  }

  monitoringPeriodChange = (monitoring_period: Object) => {
    this.setState({monitoring_period, modified: true})
  }

  accentColorChange = (accent_color: string) => {
    this.setState({accent_color, modified: true})
  }

  logoChange = (logo: string) => {
    this.setState({logo, modified: true})
  }

  coverImageChange = (cover_image: string) => {
    this.setState({cover_image, modified: true})
  }

  render () {
    const { setModified, titleChange, togglePublished, publishDateChange, monitoringPeriodChange, authorChange, subtitleChange, accentColorChange, logoChange, coverImageChange } = this
    const { id, status, published_at, t } = this.props
    const { modified, title, subtitle, author, monitoring_period, cover_image, logo, accent_color } = this.state

    let published
    if (typeof this.state.published !== 'undefined') {
      published = this.state.published
    } else if (status === 'published') {
      published = true
    }

    const publishedDate = this.state.publishedDate || published_at

    return (
      <Mutation
        query={gql`
          mutation updateReport($id: String!, $title: JSON, $subtitle: JSON, $author: JSON, $monitoring_period: JSON, $status: String, $published_at: String, $cover_image: String, $logo: String, $accent_color: String) {
            updateReport(id: $id, title: $title, subtitle: $subtitle, author: $author, monitoring_period: $monitoring_period, status: $status, published_at: $published_at, cover_image: $cover_image, logo: $logo, accent_color: $accent_color) {
              id
              title
              subtitle
              author
              monitoring_period
              status
              published_at
              cover_image
              logo
              accent_color
            }
          }
        `}
        successMessage='Saved'
        onComplete={() => {
          setModified(false)
        }}
      >
        {(updateReport, { fetching }) => (
          <>
            <Row type='flex' justify='start'>
              <Col span={4} style={{textAlign: 'center', lineHeight: '32px'}}>
                <span style={{marginRight: '5px', fontWeight: 'bold', fontSize: '12px'}}>{t('Draft')}</span>
                <Switch defaultChecked={published} onChange={togglePublished} />
                <span style={{marginLeft: '5px', fontWeight: 'bold', fontSize: '12px'}}>{t('Published')}</span>
              </Col>
              <Col span={4}>
                <DatePicker onChange={publishDateChange} defaultValue={publishedDate && moment(publishedDate)} format='YYYY-MM-DD' />
              </Col>
              <Col span={4}>
                {
                  modified &&
                    <span style={{marginRight: '10px'}}>{t('Not Saved')}</span>
                }
                <Button
                  type='primary'
                  disabled={!modified}
                  loading={fetching}
                  onClick={() => {
                    const status = published ? 'published' : 'draft'
                    updateReport({
                      id,
                      title,
                      subtitle,
                      author,
                      monitoring_period,
                      status,
                      published_at: moment(publishedDate).format('YYYY-MM-DD'),
                      cover_image,
                      logo,
                      accent_color
                    })
                  }}
                >{t('Save')}
                </Button>
              </Col>
            </Row>
            <Row>
              <Col span={12} style={{padding: '20px'}}>
                <Row style={{marginBottom: '10px'}}>
                  <p><b>{t('Title')}</b></p>
                  <LocalizedInput value={title} placeholder={t('Title')} onChange={titleChange} />
                </Row>
                <Row style={{marginBottom: '10px'}}>
                  <p><b>{t('Subtitle')}</b></p>
                  <LocalizedInput value={subtitle} placeholder={t('Subtitle')} onChange={subtitleChange} />
                </Row>
                <Row style={{marginBottom: '10px'}}>
                  <p><b>{t('Cover Image')}</b></p>
                  <Input
                    type='text' value={cover_image}
                    placeholder={t('Cover Image URL')}
                    onChange={(e) => coverImageChange(e.target.value)}
                  />
                </Row>
                <Row style={{marginBottom: '10px'}}>
                  <p><b>{t('Logo')}</b></p>
                  <Input
                    type='text' value={logo}
                    placeholder={t('Logo')}
                    onChange={(e) => logoChange(e.target.value)}
                  />
                </Row>
              </Col>
              <Col span={12} style={{padding: '20px'}}>
                <Row style={{marginBottom: '10px'}}>
                  <p><b>{t('Author')}</b></p>
                  <LocalizedInput value={author} placeholder={t('Author')} onChange={authorChange} />
                </Row>
                <Row style={{marginBottom: '10px'}}>
                  <p><b>{t('Monitoring Period')}</b></p>
                  <LocalizedInput value={monitoring_period} placeholder={t('Monitoring Period')} onChange={monitoringPeriodChange} />
                </Row>
                <Row>
                  <p><b>{t('Accent Color')}</b></p>
                  <SketchPicker
                    color={accent_color}
                    onChangeComplete={(data) => { accentColorChange(data.hex) }}
                  />
                </Row>
              </Col>
            </Row>
          </>
        )}
      </Mutation>
    )
  }
}
