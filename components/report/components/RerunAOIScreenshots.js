// @flow
import * as React from 'react'
import { Popconfirm, Button, Tooltip } from 'antd'
import Mutation from '../../Mutation'
import gql from 'graphql-tag'

type Props = {
  report_id: string,
  aoi_id: string,
  t: Function
}

export default class RerunAOIScreenshots extends React.Component<Props, void> {
  render () {
    const { report_id, aoi_id, t } = this.props
    return (
      <Mutation
        query={gql`
          mutation createReportAOIScreenshots($id: String!, $aoi_id: String!) {
            createReportAOIScreenshots(id: $id, aoi_id: $aoi_id) {
              id
              timestamp
              processedOn
              finishedOn
              progress
              data
            }
          }
        `}
        successMessage={t('Jobs Created')}
      >
        {(createJobsForModule) => (
          <Popconfirm
            title={t('Rerun screenshots for this Area?')}
            okText={t('Yes')} cancelText={t('Cancel')}
            onConfirm={() => {
              createJobsForModule({ id: report_id, aoi_id, force: true })
            }}
          >
            <Tooltip placement='bottom' title={t('Rerun Screenshots')}>
              <Button icon='reload' shape='circle' size='small' />
            </Tooltip>
          </Popconfirm>
        )}
      </Mutation>
    )
  }
}
