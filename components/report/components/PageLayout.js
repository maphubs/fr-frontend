// @flow
import * as React from 'react'

type Props = {
  title?: string,
  pageNumber?: number,
  logo?: string,
  accentColor?: string,
  copyright?: string,
  children: any
}

export default class PageLayout extends React.Component<Props, void> {
  render () {
    const { title, pageNumber, children } = this.props
    let { logo, accentColor, copyright } = this.props
    logo = logo || '/static/FR_LOGO_JUNE2019.png'
    accentColor = accentColor || '#21C6D8'
    copyright = copyright || 'MapHubs Incorporated All Rights Reserved'
    return (
      <>
        <div className='report-page'>
          <header>
            <h1 className='report-page-title'>{title}</h1>
            <div className='report-divider' />
          </header>
          <main className='report-page-contents'>
            {children}
          </main>
          <footer className='report-footer'>
            <div className='report-divider' />
            <img src={logo} className='report-footer-fr-logo' alt='Forest Report Logo' />
            <div className='report-footer-copyright'>
              <p>&copy; {(new Date()).getFullYear()} {copyright}</p>
            </div>
            <p className='report-footer-page-number'>{pageNumber}</p>
          </footer>
        </div>
        <style jsx>{`
          .report-page {
            position: relative;
            
            height: 1100px;
            width: 850px;
            margin: 20px auto;
            border: 2px ridge #aaa;
            page-break-after: always;
          }
          .report-page-title {
            color: ${accentColor};
            margin-left: 10px;
            margin-bottom: 5px;
          }
          .report-page-contents {
            padding-left: 10px;
            padding-right: 10px;
            padding-top: 0px;
            padding-bottom: 10px;
          }
          .report-divider {
            width: 100%;
            height: 3px;
            background-color: ${accentColor};
          }
          .report-footer {
            width: 100%;
            position: absolute;
            bottom: 0px;
          }
          .report-footer-fr-logo {
            height: 50px;
            padding: 2px;
            margin-left: 10px;
            width: auto;
            float: left;
          }
          .report-footer-copyright {
            margin-top: 20px;
            text-align: center;
            margin-right: 66px;
          }
          .report-footer-copyright p {
            color: #d9d9d9;
          }
          .report-footer-page-number {
            position: absolute;
            bottom: 0px;
            right: 10px;
            color: #323333;
          }
          @media print {
            .report-page {
              margin: auto;
              border: none;
            }
          }
        `}
        </style>
      </>
    )
  }
}
