// @flow
import * as React from 'react'
import { Col, Row } from 'antd'

type Props = {
  module: Object,
  t: Function
}

export default class ModuleRow extends React.Component<Props, void> {
  shouldComponentUpdate () {
    return false
  }

  render () {
    const { module, t } = this.props
    if (!module.mapScreenshot && !module.cardScreenshot) {
      return ''
    }
    return (
      <Row>
        {module.name &&
          <h2 style={{margin: 0, fontSize: '1.2em'}}>{t(module.name)}</h2>}
        {!module.name &&
          <h2 style={{margin: 0, fontSize: '1.2em'}}>{module.id}</h2>}
        <Col span={12}>
          {module.mapScreenshot &&
            <img src={module.mapScreenshot.url} style={{height: '300px', width: 'auto'}} />}
        </Col>
        <Col span={12}>
          {module.cardScreenshot &&
            <img src={module.cardScreenshot.url} style={{height: '300px', width: 'auto'}} />}
        </Col>
      </Row>
    )
  }
}
