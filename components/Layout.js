import * as React from 'react'
import Head from 'next/head'
import enUS from 'antd/lib/locale-provider/en_US'
import enGB from 'antd/lib/locale-provider/en_GB'
import frFR from 'antd/lib/locale-provider/fr_FR'
import esES from 'antd/lib/locale-provider/es_ES'
import deDE from 'antd/lib/locale-provider/de_DE'
import itIT from 'antd/lib/locale-provider/it_IT'
import ptBR from 'antd/lib/locale-provider/pt_BR'
import ptPT from 'antd/lib/locale-provider/pt_PT'
import { Avatar, Breadcrumb, Icon, Layout, LocaleProvider, Menu, message } from 'antd'
import Link from 'next/link'
import { subscribe } from '../lib/unstated-props'
import UserContainer from '../containers/UserContainer'
import LocaleChooser from './LocaleChooser'
import Spinner from './Spinner'
import getConfig from 'next/config'
const MAPHUBS_CONFIG = getConfig().publicRuntimeConfig

Icon.setTwoToneColor(MAPHUBS_CONFIG.primaryColor)

const {Content, Sider} = Layout
const SubMenu = Menu.SubMenu

const getAntLocale = (lang) => {
  // console.log(`lang: ${lang}`)
  let antLocale
  if (lang === 'en' || lang === 'en-US') {
    antLocale = enUS
  } else if (lang === 'de' || lang === 'de-DE') {
    antLocale = deDE
  } else if (lang === 'es' || lang === 'es-ES') {
    antLocale = esES
  } else if (lang === 'fr' || lang === 'fr-FR') {
    antLocale = frFR
  } else if (lang === 'it' || lang === 'it-IT') {
    antLocale = itIT
  } else if (lang === 'pt' || lang === 'pt-BR') {
    antLocale = ptBR
  } else if (lang === 'pt-PT') {
    antLocale = ptPT
  } else if (lang === 'en-GB') {
    antLocale = enGB
  } else {
    antLocale = enUS
  }
  return antLocale
}

type Props = {
  t: any,
  initialLanguage?: string,
  changeLanguage: Function,
  title: string,
  crumbs: Array<{href: string | {pathname: string, query: Object}, as?: string, label: string}>,
  padding: boolean,
  children: any,
  collapsed?: boolean,
  containers: {user: UserContainer}
}

type State = {
  collapsed: boolean
}

class AppLayout extends React.Component<Props, State> {
  constructor (props: Props) {
    super(props)
    this.state = {
      collapsed: props.collapsed
    }
  }

  static defaultProps = {
    crumbs: [],
    padding: 12,
    collapsed: true,
    initialLanguage: 'en'
  }

  onCollapse = (collapsed) => {
    this.setState({collapsed})
  }

  render () {
    const {t, initialLanguage, title, children, crumbs, padding} = this.props
    const { user } = this.props.containers
    const { collapsed } = this.state
    const {profile} = user.state

    if (!profile) {
      return (
        <div>
          <Head>
            <title>{title}</title>
          </Head>
          <div>
            <style jsx>{`
              .wrapper {
                display: flex;
                align-items: center;
                justify-content: center;
                height: 100vh;
              }
            `}
            </style>
            <div className='wrapper'>
              <Spinner />
              <img src='/static/FR_LOGO_JUNE2019.png' style={{position: 'absolute', top: 'calc(50% - 200px)'}} height={94} width={128} alt='Forest Report Logo' />
            </div>
          </div>
        </div>
      )
    }

    return (
      <div>
        <Head>
          <title>{title}</title>
        </Head>
        <LocaleProvider locale={getAntLocale(initialLanguage)}>
          <Layout style={{ height: '100vh' }} hasSider>
            <style global jsx>{`
              .logo {
                height: ${collapsed ? '32px' : '128px'};
                background: rgba(255,255,255,.2);
                margin: 10px;
                text-align: center;
              }
              .ant-layout-sider-trigger {
                background: #323333 !important;
              }
               body {
                font-family: 'Roboto', sans-serif !important;
              }
            `}
            </style>
            <Sider
              collapsible
              collapsed={collapsed}
              onCollapse={this.onCollapse}
              style={{background: 'white'}}
            >
              <div className='logo'>
                <img style={{height: '100%', width: 'auto'}} src='/static/FR_LOGO_JUNE2019.png' alt='Forest Report Logo' />
              </div>
              <Menu theme='light' defaultSelectedKeys={['1']} mode='inline'>
                <Menu.Item key='home'>
                  <Link href='/'>
                    <Icon type='home' />
                  </Link>
                  <span>
                    <Link href='/'>
                      <a>{t('Lists')}</a>
                    </Link>
                  </span>
                </Menu.Item>
                <Menu.Item key='reports'>
                  <Link href='/reports'>
                    <Icon type='read' />
                  </Link>
                  <span>
                    <Link href='/reports'>
                      <a>{t('Reports')}</a>
                    </Link>
                  </span>
                </Menu.Item>
                {/*
                <Menu.Item key='mylists'>
                  <Link href='/mylists'>
                    <Icon type='compass' />
                  </Link>
                  <span>
                    <Link href='/mylists'>
                      <a>My Lists</a>
                    </Link>
                  </span>
                </Menu.Item>

                <Menu.Item key='organization'>
                  <Link href='/organization'>
                    <Icon type='team' />
                  </Link>
                  <span>
                    <Link href='/organization'>
                      <a>My Organization</a>
                    </Link>
                  </span>
                </Menu.Item>
                */}
                {
                  (profile && profile.role === 1) &&
                    <SubMenu
                      key='sub2'
                      title={<span><Icon type='setting' /><span>{t('Admin')}</span></span>}
                    >
                      <Menu.Item key='admin-lists'>
                        <Link href='/admin/lists'>
                          <a>{t('Lists')}</a>
                        </Link>
                      </Menu.Item>
                      <Menu.Item key='admin-modules'>
                        <Link href='/admin/modules'>
                          <a>Modules</a>
                        </Link>
                      </Menu.Item>
                      <Menu.Item key='admin-drivers'>
                        <Link href='/admin/drivers'>
                          <a>Drivers</a>
                        </Link>
                      </Menu.Item>
                      <Menu.Item key='admin-orgs'>
                        <Link href='/admin/organizations'>
                          <a>Organizations</a>
                        </Link>
                      </Menu.Item>
                      <Menu.Item key='admin-users'>
                        <Link href='/admin/users'>
                          <a>Users</a>
                        </Link>
                      </Menu.Item>
                    </SubMenu>
                }
              </Menu>
              {profile &&
                <div style={{
                  position: 'absolute',
                  bottom: '50px',
                  width: '100%',
                  cursor: 'pointer',
                  textAlign: 'right'
                }}
                >
                  <Menu theme='light'>
                    <Menu.Item key='locale' title={t('Language')}>
                      <LocaleChooser />
                    </Menu.Item>
                    <Menu.Item key='account' title={t('Account')}>
                      <Avatar style={{marginLeft: '-10px', float: 'left', marginRight: '10px'}} shape='square' size='large' src={profile.picture} />
                      <Link href='/account'>
                        <a style={{display: collapsed ? 'none' : 'inherit'}}>{t('Account')}</a>
                      </Link>
                    </Menu.Item>
                    <Menu.Item key='logout' title={t('Logout')}>
                      <Link href='/logout'>
                        <a href='#'>
                          <Icon type='logout' style={{float: 'left', marginRight: '10px', lineHeight: 'inherit'}} />
                          <span style={{display: collapsed ? 'none' : 'inherit'}}>{t('Logout')}</span>
                        </a>
                      </Link>
                    </Menu.Item>
                    <Menu.Item key='feedback' title={t('Questions/Feedback')}>
                      <a
                        href='#'
                        onClick={() => {
                          // eslint-disable-next-line no-undef
                          const UserbackInstance = Userback
                          if (UserbackInstance) {
                            UserbackInstance.open()
                          } else {
                            message.info('Feedback tool not enabled, please contact support@maphubs.com')
                          }
                        }}
                      >
                        <Icon type='question-circle' style={{float: 'left', marginRight: '10px', lineHeight: 'inherit'}} />
                        <span style={{display: collapsed ? 'none' : 'inherit'}}>{t('Questions/Feedback')}</span>
                      </a>
                    </Menu.Item>
                  </Menu>
                </div>}
            </Sider>
            <Layout>
              <Content style={{height: '100%', background: 'white', border: 'solid 1px #ddd'}}>
                {(crumbs && crumbs.length > 0) &&
                  <Breadcrumb style={{margin: '10px 5px', color: '#323333'}} separator='>'>
                    {
                      crumbs.map((crumb) => {
                        let linkAs = crumb.as
                        if (!linkAs && typeof crumb.href === 'string') {
                          linkAs = crumb.href
                        }
                        if (crumb.href) {
                          return (
                            <Breadcrumb.Item key={linkAs || crumb.label}>
                              <Link href={crumb.href} as={linkAs}>
                                <a style={{fontWeight: 'bold', color: '#323333'}}>{crumb.label}</a>
                              </Link>
                            </Breadcrumb.Item>
                          )
                        } else {
                          return (
                            <Breadcrumb.Item key={crumb.label}>
                              <span style={{fontWeight: 'bold', color: '#323333'}}>{crumb.label}</span>
                            </Breadcrumb.Item>
                          )
                        }
                      })
                    }
                  </Breadcrumb>}
                <div style={{padding, background: '#fff', minHeight: 360, height: crumbs && crumbs.length > 0 ? 'calc(100% - 40px)' : '100%'}}>
                  {children}
                </div>
              </Content>
            </Layout>
          </Layout>
        </LocaleProvider>
      </div>
    )
  }
}

export default subscribe(AppLayout, {user: UserContainer})
