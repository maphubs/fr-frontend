// @flow
import * as React from 'react'
import Mutation from '../Mutation'
import gql from 'graphql-tag'
import { Button, Row } from 'antd'

import dynamic from 'next/dynamic'
const CKEditorNote = dynamic(() => import('../form/CKEditorNote.js'), {
  ssr: false
})

type Props = {
  module_id: string,
  attribution?: string
}

type State = {
  hasChanges: boolean,
  attribution?: string
}

export default class ModuleAttributionForm extends React.Component<Props, State> {
  constructor (props: Props) {
    super(props)
    this.state = {
      hasChanges: false,
      attribution: props.attribution
    }
  }

  setAttribution = (attribution: string) => {
    this.setState({ attribution, hasChanges: true })
  }

  render () {
    const { module_id } = this.props
    const { attribution, hasChanges } = this.state
    return (
      <Mutation
        query={gql`
          mutation updateModuleAttribution($id: String!, $attribution: String!) {
            updateModuleAttribution(id: $id, attribution: $attribution) {
              id
              attribution
            }
          }
        `}
        successMessage='Attribution Saved'
        onComplete={() => {
          this.setState({hasChanges: false})
        }}
      >
        {(updateModuleAttribution) => (
          <div>
            <Row style={{height: '400px', width: '600px'}}>
              <CKEditorNote
                initialData={attribution}
                onChange={(val) => {
                  this.setAttribution(val)
                }}
              />
            </Row>
            <Row>
              <Button
                type='primary'
                disabled={!hasChanges}
                onClick={() => {
                  updateModuleAttribution({ id: module_id, attribution })
                }}
              >Save
              </Button>
              {
                hasChanges &&
                  <p>Not Saved</p>
              }
            </Row>
          </div>
        )}
      </Mutation>
    )
  }
}
