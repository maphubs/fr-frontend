// @flow
import * as React from 'react'
import { Button, Popconfirm } from 'antd'
import Mutation from '../Mutation'
import gql from 'graphql-tag'

type Props = {
  module_id: string,
  t: Function
}

export default class DeleteModuleButton extends React.Component<Props, void> {
  render () {
    const { module_id, t } = this.props
    return (
      <Mutation
        query={gql`
          mutation deleteModule($id: String!) {
            deleteModule(id: $id)
          }
        `}
        successMessage='Module Deleted'
      >
        {(deleteModule) => (
          <Popconfirm
            title={t('Delete this module and all data?')}
            okText={t('Yes')} cancelText={t('Cancel')}
            onConfirm={() => {
              deleteModule({id: module_id})
            }}
          >
            <Button icon='delete' shape='circle' type='danger' size='small' />
          </Popconfirm>
        )}
      </Mutation>
    )
  }
}
