// @flow
import * as React from 'react'
import { Form, Button, Input, Select, notification, Row } from 'antd'
import { Query } from 'urql'
import Mutation from '../Mutation'
import gql from 'graphql-tag'
import Router from 'next/router'
import LocalizedInput from '../form/LocalizedInput'
import Spinner from '../Spinner'
const FormItem = Form.Item
const { Option } = Select

type Props = {
  form: any,
  t: Function
}

function hasErrors (fieldsError) {
  return Object.keys(fieldsError).some(field => fieldsError[field])
}

class CreateModuleForm extends React.Component<Props, void> {
  componentDidMount () {
    // To disabled submit button at the beginning.
    this.props.form.validateFields()
  }

  render () {
    const {props} = this
    const { t } = this.props
    const { getFieldDecorator, getFieldsError, getFieldError, isFieldTouched } = this.props.form
    const nameError = isFieldTouched('name') && getFieldError('name')
    const idError = isFieldTouched('id') && getFieldError('id')
    return (
      <Query
        query={gql`
          {
            drivers {
              id
              version
            }
          }
        `}
      >
        {({ fetching, error, data }) => {
          if (fetching) return <Spinner />
          if (error) {
            notification.error({
              message: 'Error',
              description: error.message,
              duration: 0
            })
            return ''
          }

          return (
            <Mutation
              query={gql`
                mutation createModule($id: String!, $name: JSON!, $driverID: String!) {
                  createModule(id: $id, name: $name, driverID: $driverID) {
                    id
                  }
                }
              `}
              onComplete={(data) => {
                Router.push(`/admin/module/view/${data.createModule.id}`)
              }}
            >
              {(createModule) => (
                <Row style={{padding: '20px', maxWidth: '600px'}}>
                  <Form onSubmit={(e) => {
                    e.preventDefault()
                    props.form.validateFields((err, values) => {
                      if (!err) {
                        createModule({ id: values.id, name: values.name, driverID: values.driver_id })
                      }
                    })
                  }}
                  >
                    <FormItem
                      validateStatus={idError ? 'error' : ''}
                      help={idError || ''}
                    >
                      {getFieldDecorator('id', {
                        initialValue: '',
                        rules: [{ required: true, message: t('Please input an id') }]
                      })(
                        <Input placeholder={t('Module ID')} />
                      )}
                    </FormItem>
                    <FormItem
                      validateStatus={nameError ? 'error' : ''}
                      help={nameError || ''}
                    >
                      {getFieldDecorator('name', {
                        initialValue: {},
                        rules: [{ required: true, message: t('Please input a name') }]
                      })(
                        <LocalizedInput placeholder={t('Name')} />
                      )}
                    </FormItem>
                    <FormItem
                      validateStatus={nameError ? 'error' : ''}
                      help={nameError || ''}
                    >
                      {getFieldDecorator('driver_id', {
                        initialValue: '',
                        rules: [{ required: true, message: t('Please select a driver id') }]
                      })(
                        <Select placeholder={t('Driver ID')}>
                          {data.drivers.map(driver => {
                            const { id } = driver
                            return (
                              <Option key={id} value={id}>{id}</Option>
                            )
                          })}
                        </Select>
                      )}
                    </FormItem>
                    <FormItem>
                      <Button
                        type='primary'
                        htmlType='submit'
                        disabled={hasErrors(getFieldsError())}
                      >
                        {t('Save')}
                      </Button>
                    </FormItem>
                  </Form>
                </Row>
              )}
            </Mutation>
          )
        }}
      </Query>
    )
  }
}

export default Form.create()(CreateModuleForm)
