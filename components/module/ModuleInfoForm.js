// @flow
import * as React from 'react'
import { Form, Button } from 'antd'
import Mutation from '../Mutation'
import gql from 'graphql-tag'
import LocalizedInput from '../form/LocalizedInput'
const FormItem = Form.Item

type Props = {
  form: any,
  id: string,
  name: Object
}

type State = {

}

function hasErrors (fieldsError) {
  return Object.keys(fieldsError).some(field => fieldsError[field])
}

class ModuleInfoForm extends React.Component<Props, State> {
  componentDidMount () {
    // To disabled submit button at the beginning.
    this.props.form.validateFields()
  }

  render () {
    const {props} = this
    const { getFieldDecorator, getFieldsError, getFieldError, isFieldTouched } = this.props.form
    const nameError = isFieldTouched('name') && getFieldError('name')
    const { id, name } = this.props

    return (
      <Mutation
        query={gql`
          mutation updateModuleInfo($id: String!, $name: JSON!) {
            updateModuleInfo(id: $id, name: $name) {
              id
              name
            }
          }
        `}
        successMessage='Saved'
      >
        {(updateModuleInfo) => (
          <Form onSubmit={(e) => {
            e.preventDefault()
            props.form.validateFields((err, values) => {
              if (!err) {
                updateModuleInfo({
                  id,
                  name: values.name
                })
                console.log(`Received values of form: ${values}`)
              }
            })
          }}
          >
            <FormItem
              validateStatus={nameError ? 'error' : ''}
              help={nameError || ''}
            >
              {getFieldDecorator('name', {
                initialValue: name || {},
                rules: [{ required: true, message: 'Please input a name' }]
              })(
                <LocalizedInput />
              )}
            </FormItem>
            <FormItem>
              <Button
                type='primary'
                htmlType='submit'
                disabled={hasErrors(getFieldsError())}
              >
                {'Save'}
              </Button>
            </FormItem>
          </Form>
        )}
      </Mutation>
    )
  }
}

export default Form.create()(ModuleInfoForm)
