// @flow
import * as React from 'react'
import { Row, Col, Statistic, Card, Typography, Drawer, Icon, Button } from 'antd'
import slugify from 'slugify'
import Link from 'next/link'
import numeral from 'numeral'
import chroma from 'chroma-js'
import ListDashboardContainer from '../../containers/ListDashboardContainer'
import { subscribe } from '../../lib/unstated-props'
import { Histogram, BarSeries, withParentSize, XAxis, YAxis } from '@data-ui/histogram'

const ResponsiveHistogram = withParentSize(({ parentWidth, parentHeight, ...rest }) => (
  <Histogram
    width={parentWidth}
    height={parentHeight}
    {...rest}
  />
))

const { Paragraph } = Typography

type Props = {
  containers: {listState: Object},
  list_id: string,
  list_name: string,
  t: Function
}

type State = {
  info?: string
}

const cardBodyStyle = {
  padding: '5px',
  height: 'calc(100% - 25px)'
}

const statTitleStyle = {
  fontSize: '10px',
  fontWeight: '700'
}

class ListSummaryStats extends React.Component<Props, State> {
  constructor (props: Props) {
    super(props)
    this.state = {}
  }

  setInfo = (info: string) => {
    this.setState({info})
  }

  hideInfo = () => {
    this.setState({info: undefined})
  }

  render () {
    const { setInfo, hideInfo } = this
    const { containers, list_id, list_name, t } = this.props
    const { listState } = containers
    const { modules, selectedColumn, selectedColumnValues, selectedColumnStats, selectedAOI } = listState.state
    const { info } = this.state
    const modMap = {}
    modules.map(mod => {
      modMap[mod.id] = mod
    })
    const mod = modMap[selectedColumn.module_id]

    if (!selectedColumnValues || selectedColumnValues.length === 0) {
      return (
        <p>No Data</p>
      )
    }

    const {min, mean, max, sum, ckmeans} = selectedColumnStats

    let barChartData
    let statFontSize = '20px'

    if (ckmeans) {
      const colors = chroma
        .scale(['#fff', 'red'])
        .mode('lch').colors(5)

      barChartData = ckmeans.map((data, i) => {
        const bin0 = Math.round(data.min) || 0
        let bin1 = Math.round(data.max) || 0
        if (bin1 === bin0) bin1 = bin1 + 1
        return {
          bin0,
          bin1,
          count: data.count || 0,
          color: colors[i]
        }
      })
      // console.log(barChartData)

      if (sum > 1000000) {
        statFontSize = '16px'
      }
    }

    if (selectedAOI) {
      return (
        <Row style={{height: '100%', padding: '12px', backgroundColor: '#fff'}}>
          <Button
            type='danger' shape='circle' icon='close'
            style={{position: 'absolute', top: '5px', right: '5px', zIndex: 999}}
            onClick={() => {
              listState.deselectAOI()
            }}
          />
          <Row style={{textAlign: 'center', height: '100%', paddingTop: 'calc(25% - 25px)'}}>
            <h3>{selectedAOI.properties.name}</h3>
            <Link
              href={{
                pathname: '/dashboard',
                query: {
                  aoiID: selectedAOI.id,
                  listID: list_id,
                  listName: list_name,
                  name: selectedAOI.properties.name
                }
              }}
              as={`/dashboard/${list_id}/${selectedAOI.id}/${slugify(list_name)}/${slugify(selectedAOI.properties.name)}`}
            >
              <Button type='primary' size='large'>
                <a>
                  <Icon type='dashboard' /> {t('Open Dashboard')}
                </a>
              </Button>
            </Link>
          </Row>
        </Row>
      )
    }

    if (selectedColumn.key === 'name') {
      return (
        <Row style={{height: '100%', padding: '12px', backgroundColor: '#fff'}}>
          <h3>{t('Sorting by name')}</h3>
        </Row>
      )
    }
    if (selectedColumn.key === 'rating') {
      return (
        <Row style={{height: '100%', padding: '12px', backgroundColor: '#fff'}}>
          <h3>{t('Sorting by rating')}</h3>
        </Row>
      )
    }

    return (
      <Row style={{height: '100%', position: 'relative'}}>
        <div style={{position: 'absolute', top: '0px', right: '0px', width: '20px', height: '20px', zIndex: 999}}>
          <Icon
            type='info-circle'
            style={{fontSize: '12px', cursor: 'pointer'}}
            onClick={() => {
              setInfo(mod.attribution)
            }}
          />
        </div>
        <style jsx global>{`
          .ant-card-head {
            font-size: 12px;
            padding: 0 6px;
            min-height: 20px;
          }
          .ant-card-head-title {
            padding: 0;
          }
        `}
        </style>
        <Card
          bodyStyle={cardBodyStyle}
          title={selectedColumn.title}
          style={{ height: '100%' }}
        >
          <Row style={{height: '100%'}}>
            <Col span={6} style={{height: '100%', overflowY: 'scroll'}}>
              <Row style={{marginBottom: '10px'}}>
                <Statistic
                  title={<Paragraph ellipsis style={statTitleStyle}>{t('Total')}</Paragraph>}
                  value={sum}
                  valueStyle={{fontSize: statFontSize}}
                  formatter={(val) => numeral(val).format('0,0.0')}
                  precision={2}
                  suffix={selectedColumn.unit}
                />
              </Row>
              <Row style={{marginBottom: '10px'}}>
                <Statistic
                  title={<Paragraph ellipsis style={statTitleStyle}>{t('Average')}</Paragraph>}
                  value={mean}
                  valueStyle={{fontSize: statFontSize}}
                  formatter={(val) => numeral(val).format('0,0.0')}
                  precision={2}
                  suffix={selectedColumn.unit}
                />
              </Row>
              <Row style={{marginBottom: '10px'}}>
                <Statistic
                  title={<Paragraph ellipsis style={statTitleStyle}>{t('Min')}</Paragraph>}
                  value={min}
                  formatter={(val) => numeral(val).format('0,0.0')}
                  valueStyle={{fontSize: statFontSize}}
                  precision={2}
                  suffix={selectedColumn.unit}
                />
              </Row>
              <Row>
                <Statistic
                  title={<Paragraph ellipsis style={statTitleStyle}>{t('Max')}</Paragraph>}
                  value={max}
                  formatter={(val) => numeral(val).format('0,0.0')}
                  valueStyle={{fontSize: statFontSize}}
                  precision={2}
                  suffix={selectedColumn.unit}
                />
              </Row>
            </Col>
            <Col span={18} style={{height: '100%'}}>
              {(barChartData && barChartData.length > 0) &&
                <ResponsiveHistogram
                  ariaLabel='Histogram'
                  cumulative={false}
                  normalized={false}
                  binCount={5}
                  valueAccessor={datum => datum}
                  binType='numeric'
                  margin={{top: 10, right: 10, bottom: 32, left: 60}}
                  renderTooltip={({ event, datum, data, color }) => (
                    <div>
                      <strong style={{ color }}>{datum.bin0} to {datum.bin1}</strong>
                      <div><strong>count </strong>{datum.count}</div>
                    </div>
                  )}
                >
                  <BarSeries
                    animated={false}
                    stroke={(data) => {
                      if (data.count < 2) {
                        return data.color
                      }
                      return '#212121'
                    }}
                    strokeWidth={(data) => {
                      if (data.count < 2) {
                        return 7
                      }
                      return 1
                    }}
                    fillOpacity={1}
                    fill={(data) => {
                      return data.color
                    }}
                    binnedData={barChartData}
                  />
                  <XAxis numTicks={5} />
                  <YAxis />
                </ResponsiveHistogram>}
            </Col>
          </Row>
        </Card>
        <Drawer
          title={t('Information')}
          placement='bottom'
          height={300}
          closable
          onClose={hideInfo}
          visible={info}
        >
          <div dangerouslySetInnerHTML={{__html: info}} />
        </Drawer>
      </Row>
    )
  }
}
export default subscribe(ListSummaryStats, {listState: ListDashboardContainer})

/*
 <BarSeries
    animated={false}
    stroke='#212121'
    fillOpacity={1}
    fill={(data) => {
      return data.color
    }}
    binnedData={barChartData}
  />
  */
