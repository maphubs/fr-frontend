// @flow
import * as React from 'react'
import { Form, Input, Button, Switch } from 'antd'
import Mutation from '../Mutation'
import gql from 'graphql-tag'
import LocalizedInput from '../form/LocalizedInput'
const FormItem = Form.Item

type Props = {
  form: any,
  id: string,
  name: Object,
  logo: string,
  status: string,
  t: Function
}

type State = {

}

function hasErrors (fieldsError) {
  return Object.keys(fieldsError).some(field => fieldsError[field])
}

class ListInfoForm extends React.Component<Props, State> {
  componentDidMount () {
    // To disabled submit button at the beginning.
    this.props.form.validateFields()
  }

  render () {
    const {props} = this
    const { t } = this.props
    const { getFieldDecorator, getFieldsError, getFieldError, isFieldTouched } = this.props.form
    const nameError = isFieldTouched('name') && getFieldError('name')
    const {id, name, logo, status} = this.props
    let published
    if (status === 'published') published = true
    return (
      <Mutation
        query={gql`
          mutation updateListInfo($id: String!, $name: JSON, $logo: String, $status: String) {
            updateListInfo(id: $id, name: $name, logo: $logo, status: $status) {
              id
              name
              logo
              status
            }
          }
        `}
        successMessage={t('Saved')}
      >
        {(updateListInfo) => (
          <Form onSubmit={(e) => {
            e.preventDefault()
            props.form.validateFields((err, values) => {
              if (!err) {
                updateListInfo({
                  id,
                  name: values.name,
                  logo: values.logo,
                  status: values.status ? 'published' : 'draft'
                })
                // console.log(`Received values of form: ${values}`)
              }
            })
          }}
          >
            <FormItem>
              <span style={{marginRight: '5px', fontWeight: 'bold', fontSize: '12px'}}>{t('Draft')}</span>
              {getFieldDecorator('status', { valuePropName: 'checked', initialValue: published })(
                <Switch />
              )}
              <span style={{marginLeft: '5px', fontWeight: 'bold', fontSize: '12px'}}>{t('Published')}</span>
            </FormItem>
            <FormItem
              validateStatus={nameError ? 'error' : ''}
              help={nameError || ''}
            >
              {getFieldDecorator('name', {
                initialValue: name || {},
                rules: [{ required: true, message: t('Please input a name') }]
              })(
                <LocalizedInput />
              )}
            </FormItem>
            <FormItem>
              {getFieldDecorator('logo', {
                initialValue: logo,
                rules: [{ required: false, message: t('Set a logo') }]
              })(
                <Input placeholder={t('Logo URL')} />
              )}
            </FormItem>
            <FormItem>
              <Button
                type='primary'
                htmlType='submit'
                disabled={hasErrors(getFieldsError())}
              >
                {t('Save')}
              </Button>
            </FormItem>
          </Form>
        )}
      </Mutation>
    )
  }
}

export default Form.create()(ListInfoForm)
