// @flow
import * as React from 'react'
import { Button, Popconfirm } from 'antd'
import Mutation from '../../Mutation'
import gql from 'graphql-tag'

type Props = {
  list_id: string,
  t: Function
}

export default class CalcSummaryStatsButton extends React.Component<Props, void> {
  render () {
    const { list_id, t } = this.props
    return (
      <Mutation
        query={gql`
          mutation calcSummaryStats($id: String!) {
            calcSummaryStats(id: $id)
          }
        `}
        successMessage='Calculation Complete'
      >
        {(calcSummaryStats) => (
          <Popconfirm
            title={t('Calculate summary stats for this list?')}
            okText={t('Yes')} cancelText={t('Cancel')}
            onConfirm={() => {
              calcSummaryStats({id: list_id})
            }}
          >
            <Button icon='pie-chart' size='small'>{t('Calculate Summary Stats')}</Button>
          </Popconfirm>
        )}
      </Mutation>
    )
  }
}
