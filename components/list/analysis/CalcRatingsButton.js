// @flow
import * as React from 'react'
import { Button, Popconfirm } from 'antd'
import Mutation from '../../Mutation'
import gql from 'graphql-tag'

type Props = {
  list_id: string,
  t: Function
}

export default class CalcRatingsButton extends React.Component<Props, void> {
  render () {
    const { list_id, t } = this.props
    return (
      <Mutation
        query={gql`
          mutation calcRatings($id: String!) {
            calcRatings(id: $id)
          }
        `}
        successMessage='Calculation Complete'
      >
        {(calcRatings) => {
          return (
            <Popconfirm
              title={t('Calculate ratings for this list?')}
              okText={t('Yes')} cancelText={t('Cancel')}
              onConfirm={() => {
                calcRatings({id: list_id})
              }}
            >
              <Button icon='rise' size='small'>{t('Calculate Ratings')}</Button>
            </Popconfirm>
          )
        }}
      </Mutation>
    )
  }
}
