// @flow
import * as React from 'react'
import { Button, Popconfirm } from 'antd'
import Mutation from '../../Mutation'
import gql from 'graphql-tag'

type Props = {
  list_id: string,
  t: Function
}

export default class CalcAggregatesButton extends React.Component<Props, void> {
  render () {
    const { list_id, t } = this.props
    return (
      <Mutation
        query={gql`
          mutation calcAggregates($id: String!) {
            calcAggregates(id: $id)
          }
        `}
        successMessage='Calculation Complete'
      >
        {(calcAggregates) => (
          <Popconfirm
            title={t('Calculate aggregate stats for this list?')}
            okText={t('Yes')} cancelText={t('Cancel')}
            onConfirm={() => {
              calcAggregates({id: list_id})
            }}
          >
            <Button icon='pie-chart' size='small'>{t('Calculate Aggregate Stats')}</Button>
          </Popconfirm>
        )}
      </Mutation>
    )
  }
}
