// @flow
import * as React from 'react'
import { Col, Row } from 'antd'
import CalcSummaryStatsButton from './CalcSummaryStatsButton'
import CalcAggregatesButton from './CalcAggregatesButton'
import CalcRatingsButton from './CalcRatingsButton'

type Props = {
  list_id: string,
  t: Function
}

export default class Analysis extends React.Component<Props, void> {
  shouldComponentUpdate () {
    return false
  }

  render () {
    return (
      <Row>
        <Col span={8}>
          <CalcSummaryStatsButton {...this.props} />
        </Col>
        <Col span={8}>
          <CalcAggregatesButton {...this.props} />
        </Col>
        <Col span={8}>
          <CalcRatingsButton {...this.props} />
        </Col>
      </Row>
    )
  }
}
