// @flow
import * as React from 'react'
import { Query } from 'urql'
import gql from 'graphql-tag'
import AutoSizer from 'react-virtualized-auto-sizer'
import Link from 'next/link'
import slugify from 'slugify'
import Highlighter from 'react-highlight-words'
import Spinner from '../Spinner'
import { Row, Popconfirm, Input, Table, Tooltip, Divider, Button, notification, message, Icon } from 'antd'

const getQuery = (id: string) => {
  return gql`
    {
      listAOIs(id: "${id}") {
        id
        properties
      }
    }
  `
}

type Props = {
  list_id: string,
  t: Function
}

type State = {
  aoiSearchText: string
}

export default class ListAOIsAdmin extends React.Component<Props, State> {
  constructor (props: Props) {
    super(props)
    this.state = {
      aoiSearchText: ''
    }
  }

  handleSearch = (selectedKeys: Array<string>, confirm: Function) => {
    confirm()
    this.setState({ aoiSearchText: selectedKeys[0] })
  }

  handleReset = (clearFilters: Function) => {
    clearFilters()
    this.setState({ aoiSearchText: '' })
  }

  aoiSearchInput: any

  render () {
    const { list_id, t } = this.props
    const query = getQuery(list_id)

    const aoiColumns = [{
      title: t('ID'),
      dataIndex: 'id',
      width: 150,
      key: 'id',
      sorter: (a, b) => {
        let aVal = a.id
        let bVal = b.id
        if (typeof aVal !== 'string') aVal = ''
        if (typeof bVal !== 'string') bVal = ''
        const aValLower = aVal.toLowerCase()
        const bValLower = bVal.toLowerCase()
        return aValLower.localeCompare(bValLower)
      }
    }, {
      title: t('Name'),
      dataIndex: 'properties.name',
      key: 'name',
      sorter: (a, b) => {
        let aVal = a.properties && a.properties.name
        let bVal = b.properties && b.properties.name
        if (typeof aVal !== 'string') aVal = ''
        if (typeof bVal !== 'string') bVal = ''
        const aValLower = aVal.toLowerCase()
        const bValLower = bVal.toLowerCase()
        return aValLower.localeCompare(bValLower)
      },
      filterDropdown: ({
        setSelectedKeys, selectedKeys, confirm, clearFilters
      }) => (
        <div style={{ padding: 8 }}>
          <Input
            ref={node => { this.aoiSearchInput = node }}
            placeholder={t('Search')}
            value={selectedKeys[0]}
            onChange={e => setSelectedKeys(e.target.value ? [e.target.value] : [])}
            onPressEnter={() => this.handleSearch(selectedKeys, confirm)}
            style={{ width: 188, marginBottom: 8, display: 'block' }}
          />
          <Button
            type='primary'
            onClick={() => this.handleSearch(selectedKeys, confirm)}
            icon='search'
            size='small'
            style={{ width: 90, marginRight: 8 }}
          >
            Search
          </Button>
          <Button
            onClick={() => this.handleReset(clearFilters)}
            size='small'
            style={{ width: 90 }}
          >
            Reset
          </Button>
        </div>
      ),
      filterIcon: filtered => <Icon type='search' style={{ color: filtered ? '#1890ff' : undefined }} />,
      onFilter: (value, record) => {
        if (record.properties && record.properties.name && typeof record.properties.name === 'string') {
          return record.properties.name.toLowerCase().includes(value.toLowerCase())
        }
      },
      onFilterDropdownVisibleChange: (visible) => {
        if (visible) {
          setTimeout(() => this.aoiSearchInput.select())
        }
      },
      render: (text) => (
        <Highlighter
          highlightStyle={{ backgroundColor: '#ffc069', padding: 0 }}
          searchWords={[this.state.aoiSearchText]}
          autoEscape
          textToHighlight={text}
        />
      )
    }, {
      title: t('Action'),
      width: 200,
      key: 'action',
      render: (text, record) => (
        <span>
          <Link
            href={{
              pathname: '/admin/aoi/view',
              query: {
                id: record.id,
                name: record.properties ? record.properties.name : 'Unknown'
              }
            }}
            as={`/admin/aoi/view/${record.id}/${record.properties ? slugify(record.properties.name) : 'Unknown'}`}
          >
            <Tooltip placement='bottom' title='Edit'>
              <Button icon='edit' shape='circle' size='small' />
            </Tooltip>
          </Link>
          <Divider type='vertical' />
          <Popconfirm
            title='Remove this AOI?'
            onConfirm={() => {
              message.warn('Coming Soon!')
            }}
            okText='Yes'
            cancelText='No'
          >
            <Tooltip placement='bottom' title='Remove AOI'>
              <Button icon='delete' shape='circle' size='small' type='danger' />
            </Tooltip>
          </Popconfirm>

        </span>
      )
    }]

    return (
      <Query query={query}>
        {({ fetching, error, data, executeQuery }) => {
          if (fetching) return <Spinner />

          if (error) {
            notification.error({
              message: 'Error',
              description: error.message,
              duration: 0
            })
            return ''
          }

          const {listAOIs} = data

          return (
            <AutoSizer disableWidth>
              {({ height }) => (
                <>
                  <Table
                    dataSource={listAOIs}
                    columns={aoiColumns}
                    size='small'
                    scroll={{ y: height - 200 }}
                    pagination={{
                      pageSize: 25,
                      showSizeChanger: true,
                      pageSizeOptions: ['10', '25', '50']
                    }}
                  />
                  <Row style={{marginTop: '10px'}}>
                    <a
                    // needs to be a direct link so the page fully reloads, possibly an issue with user container state
                      href={`/admin/list/addaois/${list_id}`}
                    >
                      <Button type='primary'>Add AOIs</Button>
                    </a>
                  </Row>
                </>
              )}
            </AutoSizer>
          )
        }}
      </Query>
    )
  }
}
