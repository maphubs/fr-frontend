// @flow
import * as React from 'react'
import { Card, Icon, Typography, Statistic } from 'antd'
import slugify from 'slugify'
import numeral from 'numeral'
import Link from 'next/link'

const { Paragraph } = Typography

type Props = {
  id: string,
  name: Object,
  description: Object,
  logo?: string,
  aoiCount?: number,
  style: Object,
  t: Function
}

export default class ListCard extends React.Component<Props, void> {
  shouldComponentUpdate () {
    return false
  }

  render () {
    const {id, name, description, logo, style, aoiCount, t} = this.props

    const linkHref = {
      pathname: '/list',
      query: {
        id,
        name: t(name)
      }
    }

    const linkAs = `/list/${id}/${slugify(t(name) || '')}`

    let cover = ''
    if (logo) {
      cover = (
        <Link href={linkHref} as={linkAs}>
          <div style={{borderBottom: '1px solid #DDD', cursor: 'pointer', display: 'flex'}}>
            <img style={{height: '150px', width: '198px'}} src={logo} />
          </div>
        </Link>
      )
    } else {
      cover = (
        <Link href={linkHref} as={linkAs}>
          <div style={{borderBottom: '1px solid #DDD', cursor: 'pointer', textAlign: 'center'}}>
            <Icon style={{height: '150px', width: '150px', fontSize: '75px', padding: '35px'}} type='file-text' theme='twoTone' />
          </div>
        </Link>
      )
    }

    return (
      <Card
        style={{ width: '200px', height: '300px', marginTop: '20px' }}
        bodyStyle={{padding: '10px'}}
        cover={cover}
        hoverable
      >
        <style jsx>{`
          .list-title {
            font-size: 14px;
            font-weight: 600;
            color: #323333;
          }
          .list-description {
            height: 50px;
            overflow-y: hidden;
          }
        `}
        </style>
        <Link href={linkHref} as={linkAs}>
          <div className='list-title'>
            <Paragraph ellipsis={{rows: 2}}>{t(name)}</Paragraph>
          </div>
        </Link>
        <Link href={linkHref} as={linkAs}>
          <div className='list-description' dangerouslySetInnerHTML={{__html: description ? t(description) : ''}} />
        </Link>
        <div style={{position: 'absolute', bottom: '5px', right: '5px'}}>
          <Statistic
            valueStyle={{ fontSize: '12px' }}
            value={aoiCount} prefix={<Icon type='compass' />}
            formatter={(val) => numeral(val).format('0,0')}
          />
        </div>
      </Card>
    )
  }
}
