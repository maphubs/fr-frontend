// @flow
import * as React from 'react'
import {Icon} from 'antd'

export default {
  title: 'Rating',
  width: 85,
  key: 'rating',
  dataIndex: 'rank',
  module_id: 'rating',
  sorter: (a: Object, b: Object) => {
    let aVal = a?.aoi?.rating?.rank ?? 99
    let bVal = b?.aoi?.rating?.rank ?? 99
    if (typeof aVal !== 'number') aVal = 99
    if (typeof bVal !== 'number') bVal = 99
    return aVal - bVal
  },
  render: (text: string, record: Object) => {
    if (record?.aoi?.rating) {
      const {name, color} = record.aoi.rating
      const style = {color, fontSize: '20px', marginLeft: 'calc(50% - 10px)'}
      let type
      if (name === 'high') {
        type = 'exclamation-circle'
      } else if (name === 'medium') {
        type = 'warning'
      } else if (name === 'low') {
        type = 'check-circle'
      }
      return <Icon type={type} theme='filled' style={style} />
    }
    return ''
  }
}
