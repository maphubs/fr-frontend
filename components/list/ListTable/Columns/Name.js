// @flow
import * as React from 'react'
import {Input, Button, Icon} from 'antd'
import Highlighter from 'react-highlight-words'

// see https://stackoverflow.com/questions/14677060/400x-sorting-speedup-by-switching-a-localecompareb-to-ab-1ab10
// const collator = new Intl.Collator('en', { numeric: true, sensitivity: 'base' })

export default ({
  title, state, handleSearch, searchInput, handleReset
}: {
  title: string,
  state: Object,
  handleSearch: Function,
  searchInput: any,
  handleReset: Function
}) => {
  return {
    title,
    dataIndex: 'properties.name',
    key: 'name',
    module_id: 'name',
    filterDropdown: ({
      setSelectedKeys, selectedKeys, confirm, clearFilters
    }: {
      setSelectedKeys: Function,
      selectedKeys: Array<string>,
      confirm: Function,
      clearFilters: Function
    }) => (
      <div style={{ padding: 8 }}>
        <Input
          ref={node => { searchInput = node }}
          placeholder={title}
          value={selectedKeys[0]}
          onChange={e => setSelectedKeys(e.target.value ? [e.target.value] : [])}
          onPressEnter={() => handleSearch(selectedKeys, confirm)}
          style={{ width: 188, marginBottom: 8, display: 'block' }}
        />
        <Button
          type='primary'
          onClick={() => handleSearch(selectedKeys, confirm)}
          icon='search'
          size='small'
          style={{ width: 90, marginRight: 8 }}
        >
          Search
        </Button>
        <Button
          onClick={() => handleReset(clearFilters)}
          size='small'
          style={{ width: 90 }}
        >
          Reset
        </Button>
      </div>
    ),
    filterIcon: (filtered: boolean) => <Icon type='search' style={{ color: filtered ? '#1890ff' : undefined }} />,
    onFilter: (value: string, record: Object) => {
      if (record?.properties?.name && typeof record.properties.name === 'string') {
        return record.properties.name.toLowerCase().includes(value.toLowerCase())
      }
    },
    onFilterDropdownVisibleChange: (visible: boolean) => {
      if (visible) {
        setTimeout(() => searchInput.select())
      }
    },
    render: (text: string) => (
      <Highlighter
        highlightStyle={{ backgroundColor: '#ffc069', padding: 0 }}
        searchWords={[state.searchText]}
        autoEscape
        textToHighlight={text}
      />
    )
  }
}
