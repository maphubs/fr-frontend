// @flow
import * as React from 'react'
import { Table } from 'antd'
import AutoSizer from 'react-virtualized-auto-sizer'
import ListDashboardContainer from '../../../containers/ListDashboardContainer'
import { subscribe } from '../../../lib/unstated-props'

type Props = { containers: any }

class ListTable extends React.Component<Props, void> {
  render () {
    const { listState } = this.props.containers
    const { resultColumns, tableData, tableTotalWidth, selectedRowKeys, sorting } = listState.state

    const rowSelection = {
      type: 'radio',
      selectedRowKeys,
      onChange: (selectedRowKeys, selectedRows) => {
        const selectedAOI = selectedRows[0].aoi
        listState.selectAOI(selectedAOI.id, selectedRowKeys)
      },
      getCheckboxProps: record => ({
        name: record.name
      })
    }

    return (
      <>
        <style jsx global>{`
          .ant-table-thead {
            font-size: 12px;
            line-height: 1;
          }
          .ant-table-tbody {
            font-size: 12px;
            line-height: 0.8;
          }
          .ant-table-column-has-actions {
            padding: 4px 8px 4px 8px !important;
          }
          .ant-table-selection-column {
            padding: 4px 8px 4px 8px !important;
          }
        `}
        </style>
        <AutoSizer disableWidth>
          {({ height }) => (
            <Table
              dataSource={tableData}
              loading={sorting}
              rowKey={record => record.key}
              columns={resultColumns}
              scroll={{ y: height - 142, x: tableTotalWidth }}
              size='small'
              rowSelection={rowSelection}
              bordered
              pagination={{
                position: 'bottom',
                pageSize: 100,
                showSizeChanger: true,
                pageSizeOptions: ['50', '100', '200']
              }}
              onRow={(record) => ({
                onClick: () => {
                  listState.selectAOI(record.aoi.id)
                }
              })}
            />
          )}
        </AutoSizer>
      </>
    )
  }
}
export default subscribe(ListTable, {listState: ListDashboardContainer})
