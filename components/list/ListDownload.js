// @flow
import * as React from 'react'
import { Button, message, Dropdown, Menu, Icon } from 'antd'
import download from 'downloadjs'
import converter from 'json-2-csv'
import getColVal from '../../utils/get-table-col-value'
import ListDashboardContainer from '../../containers/ListDashboardContainer'
import { subscribe } from '../../lib/unstated-props'

type Props = {
  name: string,
  containers: any
}

class ListDownload extends React.Component<Props, void> {
  createGeoJSON = () => {
    const { name, containers } = this.props
    const { tableData, resultColumns } = containers.listState.state
    const flatCols = this.flattenColumns(resultColumns)
    const features = tableData.map(row => {
      const feature = row.aoi.centroid
      const results = this.processResult(row, flatCols)
      Object.assign(feature.properties, results)
      return feature
    })
    const geoJSON = {
      type: 'FeatureCollection',
      features
    }
    download(`data:text/json;charset=utf-8,${JSON.stringify(geoJSON)}`, `${name}.geojson`, 'text/json')
    message.success('GeoJSON Download Started')
  }

  createCSV = async () => {
    const { name, containers } = this.props
    const { tableData, resultColumns } = containers.listState.state

    const flatCols = this.flattenColumns(resultColumns)
    const cleanData = tableData.map(row => {
      const rowResult = this.processResult(row, flatCols)
      if (row.properties) {
        Object.assign(rowResult, row.properties)
      }
      return rowResult
    })
    const csvData = await converter.json2csvAsync(cleanData, {})
    download(csvData, `${name}.csv`, 'text/csv')
    message.success('CSV Download Started')
  }

  processResult = (result: Object, columns: Array<Object>) => {
    const output = {}
    output.id = result?.aoi?.id || result.id || result.properties.name
    columns.forEach(col => {
      if (col.dataIndex) {
        output[col.title] = getColVal(result, `${col.module_id}.${col.dataIndex}`) || getColVal(result, col.dataIndex) || ''
      }
    })
    return output
  }

  flattenColumns = (columns: Array<Object>) => {
    let flatCols = []
    columns.forEach(col => {
      if (col.children) {
        flatCols = flatCols.concat(col.children)
      } else {
        flatCols.push(col)
      }
    })
    return flatCols
  }

  render () {
    const menu = (
      <Menu>
        <Menu.Item key='1' onClick={this.createCSV}>
          <Icon type='download' /> CSV
        </Menu.Item>
        <Menu.Item key='2' onClick={this.createGeoJSON}>
          <Icon type='download' /> GeoJSON
        </Menu.Item>
      </Menu>
    )
    return (
      <Dropdown overlay={menu}>
        <Button shape='circle' icon='download' />
      </Dropdown>
    )
  }
}
export default subscribe(ListDownload, {listState: ListDashboardContainer})
