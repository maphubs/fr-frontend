// @flow
import * as React from 'react'
import turfbbox from '@turf/bbox'
import chroma from 'chroma-js'
import turfBuffer from '../../../utils/patched-turf-buffer'
import ListDashboardContainer from '../../../containers/ListDashboardContainer'
import { subscribe } from '../../../lib/unstated-props'

import 'mapbox-gl-dual-scale-control/dist/mapbox-gl-dual-scale-control.css'
import '../../../node_modules/mapbox-gl/dist/mapbox-gl.css'

import getConfig from 'next/config'
const MAPHUBS_CONFIG = getConfig().publicRuntimeConfig

let mapboxgl = {}
let ScalePositionControl
if (typeof window !== 'undefined') {
  // eslint-disable-next-line security/detect-non-literal-require
  mapboxgl = require('mapbox-gl')
  // eslint-disable-next-line security/detect-non-literal-require
  ScalePositionControl = require('mapbox-gl-dual-scale-control')
}

type Props = {
  id: string,
  list_id: string,
  geoJSON?: Object,
  mapStyle: Object,
  interactive: boolean,
  dragRotate: boolean,
  touchZoomRotate: boolean,
  hash: boolean,
  attributionControl: boolean,
  showScale: boolean,
  fullScreen: boolean,
  bbox: Array<number>,
  children?: any,
  width?: number,
  height?: number,
  containers: any
}

type State = {
}

class ListMap extends React.Component<Props, State> {
  static defaultProps = {
    id: 'map',
    interactive: true,
    dragRotate: false,
    touchZoomRotate: false,
    hash: false,
    attributionControl: true,
    showScale: false,
    fullScreen: false
  }

  componentDidMount () {
    this.createMap()
  }

  shouldComponentUpdate () {
    return false
  }

  getMap = () => this.map

  map: Object

  zoomToData = (data: any, animate: boolean = true) => {
    let bbox: Array<number>
    if (data.bbox && Array.isArray(data.bbox) && data.bbox.length > 0) {
      bbox = (data.bbox: Array<number>)
    } else {
      bbox = turfbbox(data)
    }
    if (bbox) {
      this.map.fitBounds(bbox, {padding: 25, curve: 3, speed: 0.6, maxZoom: 10, animate})
    }
  }

  zoomMapToDefault = (animate: boolean) => {
    console.log('zoom to default')
    const { bbox } = this.props
    if (bbox) {
      return new Promise((resolve) => {
        const toDefaultMoveEnd = () => {
          this.map.off('render', toDefaultMoveEnd)
          console.log('zoom to default complete')
          resolve()
        }
        this.map.on('render', toDefaultMoveEnd)
        this.map.fitBounds(bbox, {padding: 25, curve: 3, speed: 0.6, maxZoom: 10, animate})
      })
    }
  }

  createMap = () => {
    const {
      id, mapStyle, // required
      interactive, dragRotate, touchZoomRotate, hash, // config flags
      attributionControl, showScale, fullScreen, // UI control flags
      bbox,
      list_id,
      containers
    } = this.props

    const { selectedColumn, selectedColumnStats, result_id } = containers.listState.state
    const { dataIndex, module_id } = selectedColumn
    const { min, max, ckmeans, equalIntervalBreaks } = selectedColumnStats

    mapboxgl.accessToken = MAPHUBS_CONFIG.MAPBOX_ACCESS_TOKEN
    const map = new mapboxgl.Map({
      container: id,
      style: mapStyle,
      interactive,
      dragRotate,
      touchZoomRotate,
      hash,
      attributionControl: false,
      bounds: bbox,
      fitBoundsOptions: {padding: 25, curve: 3, speed: 0.6, maxZoom: 12}
    })
    this.map = map
    containers.listState.registerMapComponent(this)

    if (interactive) {
      map.addControl(new mapboxgl.NavigationControl(), 'top-right')
    }

    if (fullScreen) {
      map.addControl(new mapboxgl.FullscreenControl())
    }

    if (attributionControl) {
      map.addControl(new mapboxgl.AttributionControl(), 'bottom-right')
    }

    if (showScale) {
      map.addControl(new ScalePositionControl({
        maxWidth: 175
      }), 'bottom-left')
    }

    // catch generic errors so 404 tile errors etc don't cause unexpected issues
    map.on('error', (err) => {
      console.error(err.error)
    })

    map.on('load', () => {
      console.log('MAP LOADED')

      this.addMapLayers({
        list_id,
        module_id,
        result_id,
        dataIndex,
        min,
        max,
        ckmeans
      })

      map.on('click', 'aois', (e: any) => {
        const feature = e.features[0]
        // console.log(feature)
        containers.listState.selectAOI(feature.properties.id)
      })
      map.on('click', 'points', (e: any) => {
        const feature = e.features[0]
        // console.log(feature)
        containers.listState.selectAOI(feature.properties.id)
      })
    })

    // Change the cursor to a pointer when the mouse is over the places layer.
    map.on('mouseenter', 'aois', function () {
      map.getCanvas().style.cursor = 'pointer'
    })
    map.on('mouseenter', 'points', function () {
      map.getCanvas().style.cursor = 'pointer'
    })

    // Change it back to a pointer when it leaves.
    map.on('mouseleave', 'aois', function () {
      map.getCanvas().style.cursor = ''
    })
    map.on('mouseleave', 'points', function () {
      map.getCanvas().style.cursor = ''
    })

    map.on('style.load', () => {
      console.log('style.load')
    })
  }

  zoomToAOI = (aoi_id: string, result_id: string) => {
    let features = this.map.querySourceFeatures('aois', {
      sourceLayer: 'data',
      filter: ['==', 'id', aoi_id]
    })

    let usingPoint
    if (!features || features.length === 0) {
      // check the points layer
      features = this.map.querySourceFeatures('points', {
        sourceLayer: 'data',
        filter: ['==', 'id', aoi_id]
      })
      usingPoint = true
    }

    console.log(features)

    if (features && features.length > 0) {
      let feature = features[0]
      if (usingPoint) {
        if (result_id.startsWith('buffer')) {
          const parts = result_id.split('-')
          const buffer = parseInt(parts[1], 10)
          feature = turfBuffer(feature, buffer, {steps: 128})
        }
      }
      const bbox = turfbbox(feature)
      if (bbox) {
        this.map.fitBounds(bbox, {padding: 25, curve: 3, speed: 0.6, maxZoom: 10, animate: true})
      }
    }
  }

  selectSource = ({module_id, result_id, dataIndex, min, max, ckmeans}: {
    module_id: string,
    result_id: string,
    dataIndex: string,
    min: number,
    max: number,
    ckmeans: Array<{min: number}>
  }) => {
    const map = this.map
    // remove layers
    map.removeLayer('aois-outline')
    map.removeLayer('aois')
    map.removeLayer('points')
    // remove source
    map.removeSource('points')
    map.removeSource('aois')

    const dataIndexParts = dataIndex.split('.')
    const dataFieldLast = dataIndexParts[dataIndexParts.length - 1]
    // add layers
    this.addMapLayers({list_id: this.props.list_id, module_id, result_id, dataIndex: dataFieldLast, min, max, ckmeans})
  }

  addMapLayers = ({list_id, module_id, result_id, dataIndex, min, max, ckmeans}: {
    list_id: string,
    module_id: string,
    result_id: string,
    dataIndex: string,
    min: number,
    max: number,
    ckmeans: Array<number>
  }) => {
    const map = this.map

    const layers = map.getStyle().layers
    // Find the index of the first symbol layer in the map style
    let firstSymbolId
    for (const element of layers) {
      if (element.type === 'symbol') {
        firstSymbolId = element.id
        break
      }
    }

    let colorExpression: Array<any> | string
    if (module_id === 'rating') {
      colorExpression = [
        'match',
        ['get', 'rank'],
        1, 'red',
        2, 'orange',
        3, 'green',
        'grey'
      ]
    } else if (dataIndex === 'name') {
      colorExpression = '#FFF'
    } else if (ckmeans) {
      // assume we are interpolating a numeric value
      const ckMeanscolorExpression = [
        'interpolate',
        ['linear'],
        ['get', dataIndex]
      ]
      const colors = chroma
        .scale(['#fff', 'red'])
        .mode('lch').colors(5)
      ckmeans.map(({min}, i) => {
        ckMeanscolorExpression.push(min)
        ckMeanscolorExpression.push(colors[i])
      })
      colorExpression = ckMeanscolorExpression
    } else {
      colorExpression = 'rgba(255,255,255,0.5)'
    }
    map.addSource('points', {
      type: 'vector',
      tiles: [
        `${MAPHUBS_CONFIG.API_URL}/api/list/${list_id}/${module_id}/${result_id}/mvt-centroid/{z}/{x}/{y}.pbf`
      ],
      attribution: 'Forest Report'
    })
    map.addSource('aois', {
      type: 'vector',
      tiles: [
        `${MAPHUBS_CONFIG.API_URL}/api/list/${list_id}/${module_id}/${result_id}/mvt/{z}/{x}/{y}.pbf`
      ]
    })

    map.addLayer({
      id: 'aois',
      type: 'fill',
      minzoom: 6,
      source: 'aois',
      'source-layer': 'data',
      filter: [
        'in',
        '$type',
        'Polygon'
      ],
      paint: {
        'fill-color': colorExpression,
        'fill-opacity': 0.75
      }
    }, firstSymbolId)

    map.addLayer({
      id: 'aois-outline',
      type: 'line',
      minzoom: 6,
      source: 'aois',
      'source-layer': 'data',
      filter: [
        'in',
        '$type',
        'Polygon'
      ],
      paint: {
        'line-color': 'black',
        'line-opacity': 0.9,
        'line-width': 1
      }
    }, firstSymbolId)

    map.addLayer({
      id: 'points',
      type: 'circle',
      source: 'points',
      'source-layer': 'data',
      maxzoom: 6,
      layout: {
      },
      paint: {
        'circle-radius': 5,
        'circle-color': colorExpression
      }
    })
  }

  render () {
    const { id, children } = this.props
    let { width, height } = this.props
    width = width || '100%'
    height = height || '100%'

    return (
      <div style={{width, height}}>
        <style jsx global>{`
          .mapboxgl-canvas {

          }

          .maphubs-ctrl-scale {
            left: 5px;
          }
        
          .map-position {
            left: 0;
          }
        
          .metric-scale {
            left: 0;
          }
        
          .imperial-scale {
            left: 0;
          }        
          `}
        </style>
        <div id={id} style={{ width, height }}>
          {children}
        </div>
      </div>
    )
  }
}
export default subscribe(ListMap, {listState: ListDashboardContainer})
