// @flow
import * as React from 'react'
import { Button, Dropdown, Menu } from 'antd'
import Link from 'next/link'

type Props = {
  id: string,
  name: string,
  aggFields: Array<string>,
  t: Function
}

export default class ListSummaryButton extends React.Component<Props, void> {
  render () {
    const { aggFields, id, name, t } = this.props
    const menu = (
      <Menu>
        {
          aggFields.map(aggField => {
            return (
              <Menu.Item key={aggField}>
                <Link
                  href={{
                    pathname: '/listsummary',
                    query: {id, aggField, name}
                  }}
                  as={`/listsummary/${id}/${aggField}/${name}`}
                >
                  <a>{aggField}</a>
                </Link>
              </Menu.Item>
            )
          })
        }
      </Menu>
    )
    return (
      <Dropdown overlay={menu}>
        <Button icon='rise' shape='round'>{t('Summary')}</Button>
      </Dropdown>
    )
  }
}
