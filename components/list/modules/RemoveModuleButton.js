// @flow
import * as React from 'react'
import { Button, Popconfirm, Tooltip } from 'antd'
import Mutation from '../../Mutation'
import gql from 'graphql-tag'

type Props = {
  id: string,
  module_id: string,
  t: Function
}

export default class RemoveModuleButton extends React.Component<Props, void> {
  render () {
    const { id, module_id, t } = this.props
    return (
      <Mutation
        query={gql`
          mutation removeListModule($id: String!, $moduleID: String!) {
            removeListModule(id: $id, moduleID: $moduleID)
          }
        `}
        successMessage='Module Deleted'
      >
        {(removeListModule) => (
          <Popconfirm
            title={t('Remove this module and delete data?')}
            okText={t('Yes')} cancelText={t('Cancel')}
            onConfirm={() => {
              removeListModule({id, moduleID: module_id})
            }}
          >
            <Tooltip placement='bottom' title='Remove'>
              <Button icon='delete' shape='circle' type='danger' size='small' />
            </Tooltip>
          </Popconfirm>
        )}
      </Mutation>
    )
  }
}
