// @flow
import * as React from 'react'
import { Row, Button, Divider, Table, message, notification } from 'antd'
import AutoSizer from 'react-virtualized-auto-sizer'
import { DndProvider, DragSource, DropTarget } from 'react-dnd'
import HTML5Backend from 'react-dnd-html5-backend'
import update from 'immutability-helper'
import Mutation from '../../Mutation'
import gql from 'graphql-tag'
import Link from 'next/link'
import RerunModuleJobs from '../../list/jobs/RerunModuleJobs'
import RemoveModuleButton from './RemoveModuleButton'

let dragingIndex = -1

type BodyRowProps = {
  isOver: boolean,
  connectDragSource: any,
  connectDropTarget: any,
  moveRow: any,
  restProps: {style: Object, className: string, index: number}
}

class BodyRow extends React.Component<BodyRowProps, void> {
  render () {
    const {
      isOver,
      connectDragSource,
      connectDropTarget,
      moveRow,
      ...restProps
    } = this.props
    const style = { ...restProps.style, cursor: 'move' }

    let className = restProps.className
    if (isOver) {
      if (restProps.index > dragingIndex) {
        className += ' drop-over-downward'
      }
      if (restProps.index < dragingIndex) {
        className += ' drop-over-upward'
      }
    }

    return connectDragSource(
      connectDropTarget(
        <tr
          {...restProps}
          className={className}
          style={style}
        />
      )
    )
  }
}

const rowSource = {
  beginDrag (props) {
    dragingIndex = props.index
    return {
      index: props.index
    }
  }
}

const rowTarget = {
  drop (props, monitor) {
    const dragIndex = monitor.getItem().index
    const hoverIndex = props.index

    // Don't replace items with themselves
    if (dragIndex === hoverIndex) {
      return
    }

    // Time to actually perform the action
    props.moveRow(dragIndex, hoverIndex)

    // Note: we're mutating the monitor item here!
    // Generally it's better to avoid mutations,
    // but it's good here for the sake of performance
    // to avoid expensive index searches.
    monitor.getItem().index = hoverIndex
  }
}

const DragableBodyRow = DropTarget(
  'row',
  rowTarget,
  (connect, monitor) => ({
    connectDropTarget: connect.dropTarget(),
    isOver: monitor.isOver()
  })
)(
  DragSource(
    'row',
    rowSource,
    (connect) => ({
      connectDragSource: connect.dragSource()
    })
  )(BodyRow)
)

type Props = {
  list_id: string,
  modules: Array<Object>,
  query: Object,
  t: Function
}

type State = {
  data: Array<Object>
}

export default class ListModules extends React.Component<Props, State> {
  constructor (props: Props) {
    super(props)
    this.state = {
      data: props.modules
    }
  }

  components = {
    body: {
      row: DragableBodyRow
    }
  }

  moveRow = (dragIndex: number, hoverIndex: number) => {
    const { data } = this.state
    const dragRow = data[dragIndex]

    this.setState(
      update(this.state, {
        data: {
          $splice: [[dragIndex, 1], [hoverIndex, 0, dragRow]]
        }
      })
    )
  }

  render () {
    const { list_id, query, t } = this.props

    const moduleColumns = [{
      title: t('ID'),
      dataIndex: 'id',
      width: 150,
      key: 'id',
      sorter: (a, b) => {
        let aVal = a.id
        let bVal = b.id
        if (typeof aVal !== 'string') aVal = ''
        if (typeof bVal !== 'string') bVal = ''
        const aValLower = aVal.toLowerCase()
        const bValLower = bVal.toLowerCase()
        return aValLower.localeCompare(bValLower)
      }
    }, {
      title: t('Name'),
      dataIndex: 'name',
      key: 'name',
      sorter: (a, b) => {
        let aVal = t(a.name)
        let bVal = t(b.name)
        if (typeof aVal !== 'string') aVal = ''
        if (typeof bVal !== 'string') bVal = ''
        const aValLower = aVal.toLowerCase()
        const bValLower = bVal.toLowerCase()
        return aValLower.localeCompare(bValLower)
      },
      render: (text, record) => (
        <span>
          {t(record.name)}
        </span>
      )
    }, {
      title: t('Driver'),
      dataIndex: 'driver.id',
      width: 150,
      key: 'driver',
      sorter: (a, b) => {
        let aVal = a.driver.id
        let bVal = b.driver.id
        if (typeof aVal !== 'string') aVal = ''
        if (typeof bVal !== 'string') bVal = ''
        const aValLower = aVal.toLowerCase()
        const bValLower = bVal.toLowerCase()
        return aValLower.localeCompare(bValLower)
      },
      render: (text, record) => (
        <Link
          href={{
            pathname: '/admin/driver',
            query: {id: record.driver.id}
          }}
          as={`/admin/driver/${record.driver.id}`}
        >
          <a>{record.driver.id}</a>
        </Link>
      )
    }, {
      title: t('Action'),
      width: 100,
      key: 'action',
      render: (text, record) => (
        <span>
          <RerunModuleJobs
            list_id={list_id} module_id={record.id} t={t}
            onUpdate={(cache, data) => {
              const {jobsForList} = cache.readQuery({ query })
              const combinedJobs = jobsForList.concat(data.jobs)
              cache.writeQuery({
                query,
                data: {jobsForList: combinedJobs}
              })
            }}
          />
          <Divider type='vertical' />
          <RemoveModuleButton id={list_id} module_id={record.id} t={t} />
        </span>
      )
    }]
    return (
      <>
        <AutoSizer disableWidth>
          {({ height }) => (
            <Mutation
              query={gql`
                mutation updateListModulePriorities($id: String!, $modules: [JSON]!) {
                  updateListModulePriorities(id: $id, modules: $modules)
                }
              `}
              successMessage='Module Priority Updated'
            >
              {(updateListModulePriorities) => {
                return (
                  <DndProvider backend={HTML5Backend}>
                    <Table
                      dataSource={this.state.data}
                      components={this.components}
                      columns={moduleColumns}
                      onRow={(record, index) => ({
                        index,
                        moveRow: async (dragIndex, hoverIndex) => {
                          this.moveRow(dragIndex, hoverIndex)
                          const modules = this.state.data.map((mod, i) => {
                            return {
                              id: mod.id,
                              priority: i + 1
                            }
                          })
                          updateListModulePriorities({id: list_id, modules})
                        }
                      })}
                      size='small'
                      scroll={{ y: height - 100 }}
                      pagination={{
                        pageSize: 25,
                        showSizeChanger: true,
                        pageSizeOptions: ['10', '25', '50']
                      }}
                    />

                  </DndProvider>
                )
              }}
            </Mutation>
          )}
        </AutoSizer>
        <div style={{position: 'absolute', top: '-37px', right: '10px'}}>
          <Link
            href={{
              pathname: '/admin/list/addmodules',
              query: { id: list_id }
            }}
            as={`/admin/list/addmodules/${list_id}`}
          >
            <Button type='primary'>{t('Add Modules')}</Button>
          </Link>
        </div>
      </>
    )
  }
}
