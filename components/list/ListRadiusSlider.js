// @flow
import * as React from 'react'
import ListDashboardContainer from '../../containers/ListDashboardContainer'
import { subscribe } from '../../lib/unstated-props'
import RadiusSlider from '../RadiusSlider'

type Props = {
  containers: {listState: Object}
}

class ListRadiusSlider extends React.Component<Props, void> {
  render () {
    const { listState } = this.props.containers
    const { isBuffered, radiusSelection } = listState.state
    if (isBuffered) {
      return (
        <RadiusSlider defaultValue={radiusSelection} onSelect={listState.selectRadius} />
      )
    } else {
      return ''
    }
  }
}
export default subscribe(ListRadiusSlider, {listState: ListDashboardContainer})
