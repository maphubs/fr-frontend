// @flow
import * as React from 'react'
import { Drawer, Row, Col, Button, Select, Popconfirm } from 'antd'
import Mutation from '../Mutation'
import gql from 'graphql-tag'
import moment from 'moment'

const Option = Select.Option

type Props = {
  t: Function,
  onClose: Function,
  reports: Array<Object>,
  data: Array<Object>,
  containers: any
}

type State = {
  module?: Object,
  notes?: string
}

class SaveReportSummaryDrawer extends React.Component<Props, State> {
  constructor (props: Props) {
    super(props)
    const { aoiReport } = this.props.containers
    this.state = {
      notes: aoiReport.state.notes
    }
  }

  // don't use container for notes since it causes too many re-renders
  setNotes = (notes: string) => {
    this.setState({notes})
  }

  render () {
    const { reports, containers, onClose, t } = this.props
    const { aoiReport, aoiDashboard, aoiDrawings } = containers
    const { notes } = this.state
    return (
      <Drawer
        title={t('Save to Report')}
        placement='bottom'
        height='calc(100vh - 50px)'
        closable
        destroyOnClose
        bodyStyle={{ height: 'calc(100vh - 105px)', padding: 0 }}
        onClose={onClose}
        visible
      >
        <Row style={{height: '100%'}}>

          <Col span={16}>
            <Row style={{height: '140px', maxWidth: '500px', textAlign: 'center', paddingTop: '50px'}}>
              <Col span={16}>
                <p>Select Report</p>
                <Select
                  style={{ width: 250 }}
                  labelInValue
                  defaultValue={{key: aoiReport.state.report_id}}
                  onChange={(value) => {
                    aoiReport.selectReport(value.key)
                  }}
                >
                  {reports.map((report) => {
                    return (
                      <Option value={report.id} key={report.id}>{report.title && t(report.title)}</Option>
                    )
                  })}
                </Select>
              </Col>
              {aoiReport.state.saved && <p>Saved!</p>}
            </Row>
            <Row style={{textAlign: 'center'}}>
              <Mutation
                query={gql`
                mutation addReportAOI($id: String!, $aoi_id: String!, $imagery: JSON, $drawings: JSON, $notes: JSON, $config: JSON, $results: JSON, $updated_at: String, $updated_by: String) {
                  addReportAOI(id: $id, aoi_id: $aoi_id, imagery: $imagery, drawings: $drawings, notes: $notes, config: $config, results: $results, updated_at: $updated_at, updated_by: $updated_by) {
                    id
                  }
                }
              `}
                successMessage='Report Saved!'
                onComplete={() => {
                  aoiReport.setSaved()
                }}
              >
                {(addReportAOI) => (
                  <Mutation
                    query={gql`
                      mutation updateReportAOI($id: String!, $aoi_id: String!, $imagery: JSON, $drawings: JSON, $notes: JSON, $config: JSON, $results: JSON, $updated_at: String, $updated_by: String) {
                        updateReportAOI(id: $id, aoi_id: $aoi_id, imagery: $imagery, drawings: $drawings, notes: $notes, config: $config, results: $results, updated_at: $updated_at, updated_by: $updated_by) {
                          id
                        }
                      }
                    `}
                    successMessage='Report Saved!'
                    onComplete={() => {
                      aoiReport.setSaved()
                    }}
                  >
                    {(updateReportAOI) => (
                      <>
                        {
                          !aoiReport.state.saved &&
                            <Button
                              disabled={!aoiReport.state.report_id} type='primary'
                              onClick={() => {
                                addReportAOI({
                                  id: aoiReport.state.report_id,
                                  aoi_id: aoiReport.state.aoi.id,
                                  imagery: {},
                                  drawings: {drawings: aoiDrawings.state.drawings},
                                  notes: {notes},
                                  config: {},
                                  results: {
                                    result_id: aoiDashboard.state.resultIDFilter
                                  },
                                  updated_at: moment().format(),
                                  updated_by: 'MapHubs'
                                })
                              }}
                            >Add to Report
                            </Button>
                        }
                        {aoiReport.state.saved &&
                          <Popconfirm
                            title='Update this AOI in the report?'
                            onConfirm={() => {
                              updateReportAOI({
                                id: aoiReport.state.report_id,
                                aoi_id: aoiReport.state.aoi.id,
                                imagery: {},
                                drawings: {drawings: aoiDrawings.state.drawings},
                                notes: {notes},
                                config: {},
                                results: {
                                  result_id: aoiDashboard.state.resultIDFilter
                                },
                                updated_at: moment().format(),
                                updated_by: 'MapHubs'
                              })
                            }}
                            okText='Yes'
                            cancelText='No'
                          >
                            <Button disabled={!aoiReport.state.report_id} type='primary'>Update Report</Button>
                          </Popconfirm>}
                      </>
                    )}
                  </Mutation>
                )}
              </Mutation>
            </Row>
          </Col>
        </Row>
      </Drawer>
    )
  }
}
export default SaveReportSummaryDrawer
