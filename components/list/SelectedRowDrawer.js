// @flow
import * as React from 'react'
import { Drawer, Row, Button, Icon, notification, Spin, Tooltip } from 'antd'
import {Query} from 'urql'
import Mutation from '../Mutation'
import gql from 'graphql-tag'
import Router from 'next/router'
import Link from 'next/link'
import slugify from 'slugify'
import turfBuffer from '../../utils/patched-turf-buffer'
import turfbbox from '@turf/bbox'
import getConfig from 'next/config'
const MAPHUBS_CONFIG = getConfig().publicRuntimeConfig

let Map
if (typeof window !== 'undefined') {
  Map = require('@bit/kriscarle.maphubs-components.components.map').default
}

type Props = {
  list_id: string,
  list_name: string,
  aoi_id: string,
  title: string,
  onClose: Function,
  t: Function,
  showReportButton?: boolean
}

export default function SelectedRowDrawer ({ list_id, list_name, aoi_id, title, onClose, t, showReportButton }: Props) {
  if (!aoi_id) return ''

  return (
    <Drawer
      title={title}
      placement='right'
      width={400}
      closable
      onClose={onClose}
      visible
      bodyStyle={{padding: 0}}
    >
      <Query
        query={gql`
          {
            aoi(id: "${aoi_id}") {
              id
              feature
              properties
            }
          }
        `}
      >
        {({ fetching, error, data }) => {
          if (fetching) return <Spin><div style={{height: '100%'}} /></Spin>
          if (error) {
            notification.error({
              message: 'Error',
              description: error.message,
              duration: 0
            })
            return ''
          }

          const aoi = data.aoi
          let feature = aoi.feature
          const geom = feature.geometry
          if (geom.type === 'Point' || geom.type === 'LineString' || geom.type === 'MultiLineString') {
            feature = turfBuffer(geom, 25, {steps: 128}) // TODO: set buffer based on user setting
          }
          const geoJSON = {type: 'FeatureCollection', features: [feature]}
          const bbox = turfbbox(feature)
          return (
            <>
              <Row style={{height: '300px'}}>
                <Map
                  id={`map-selected-aoi-${aoi.id}`}
                  style={{width: '100%', height: '100%'}}
                  disableScrollZoom
                  attributionControl
                  fitBounds={bbox}
                  fitBoundsOptions={{padding: 50, maxZoom: 4}}
                  data={geoJSON}
                  t={t}
                  insetMap={false}
                  interactive
                  showScale={false}
                  showLogo={false}
                  showPlayButton={false}
                  primaryColor={MAPHUBS_CONFIG.primaryColor}
                  logoSmall={MAPHUBS_CONFIG.logoSmall}
                  logoSmallHeight={MAPHUBS_CONFIG.logoSmallHeight}
                  logoSmallWidth={MAPHUBS_CONFIG.logoSmallWidth}
                />
              </Row>
              <Row style={{marginTop: '50px', textAlign: 'center'}}>
                <Link
                  prefetch
                  href={{
                    pathname: '/dashboard',
                    query: {
                      aoiID: aoi.id,
                      listID: list_id,
                      listName: list_name,
                      name: aoi.properties.name
                    }
                  }}
                  as={`/dashboard/${list_id}/${aoi.id}/${slugify(list_name)}/${slugify(aoi.properties.name)}`}
                >
                  <Button type='primary'>
                    <a>
                      <Icon type='dashboard' /> {t('Open Dashboard')}
                    </a>
                  </Button>
                </Link>
              </Row>
              {
                showReportButton &&
                  <Mutation
                    query={gql`
                      mutation createReport($aoi_id: String!, $organization_id: String!) {
                        createReport(aoi_id: $aoi_id, organization_id: $organization_id) {
                          id
                        }
                      }
                    `}
                    onComplete={(data) => {
                      console.log(data)
                      Router.push(`/report/edit/${data.createReport.id}`)
                    }}
                  >
                    {(createReport, { loading, error }) => (
                      <Spin spinning={loading} delay={10}>
                        <Tooltip title={t('New Report')}>
                          <a onClick={() => {
                            createReport({aoi_id: aoi.id, organization_id: 'DEFAULT'}) // TODO: use org from user info, require user to select org if needed
                          }}
                          >
                            <Icon type='file-add' />
                          </a>
                        </Tooltip>
                      </Spin>
                    )}
                  </Mutation>
              }
            </>
          )
        }}
      </Query>
    </Drawer>
  )
}
