// @flow
import * as React from 'react'
import { Button, Popconfirm } from 'antd'
import Mutation from '../../Mutation'
import gql from 'graphql-tag'

type Props = {
  list_id: string,
  t: Function
}

export default class PauseJobsButton extends React.Component<Props, void> {
  render () {
    const { list_id, t } = this.props
    return (
      <Mutation
        query={gql`
          mutation pauseJobs($list_id: String!) {
            pauseJobs(list_id: $list_id)
          }
        `}
        successMessage='Jobs Paused'
      >
        {(pauseJobs) => (
          <Popconfirm
            title={t('Pause all pending jobs for this list?')}
            okText={t('Yes')} cancelText={t('Cancel')}
            onConfirm={() => {
              pauseJobs({list_id})
            }}
          >
            <Button icon='pause-circle' size='small'>{t('Pause')}</Button>
          </Popconfirm>
        )}
      </Mutation>
    )
  }
}
