// @flow
import * as React from 'react'
import { Button, Popconfirm } from 'antd'
import Mutation from '../../Mutation'
import gql from 'graphql-tag'

type Props = {
  id: string,
  list_id: string,
  t: Function
}

export default class DeleteJobButton extends React.Component<Props, void> {
  render () {
    const { id, list_id, t } = this.props
    return (
      <Mutation
        query={gql`
          mutation deleteJob($list_id: String!, $id: String!) {
            deleteJob(list_id: $list_id, id: $id)
          }
        `}
        successMessage='Job Deleted'
      >
        {(deleteJob) => (
          <Popconfirm
            title={t('Delete this Job?')}
            okText={t('Yes')} cancelText={t('Cancel')}
            onConfirm={() => {
              deleteJob({list_id, id})
            }}
          >
            <Button icon='delete' shape='circle' type='danger' size='small' />
          </Popconfirm>
        )}
      </Mutation>
    )
  }
}
