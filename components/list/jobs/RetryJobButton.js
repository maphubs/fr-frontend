// @flow
import * as React from 'react'
import { Button, Popconfirm } from 'antd'
import Mutation from '../../Mutation'
import gql from 'graphql-tag'

type Props = {
  id: string,
  list_id: string,
  t: Function
}

export default class RetryJobButton extends React.Component<Props, void> {
  render () {
    const { id, list_id, t } = this.props
    return (
      <Mutation
        query={gql`
          mutation retryJob($list_id: String!, $id: String!) {
            retryJob(list_id: $list_id, id: $id)
          }
        `}
        successMessage='Job Queued'
      >
        {(retryJob) => (
          <Popconfirm
            title={t('Retry this Job?')}
            okText={t('Yes')} cancelText={t('Cancel')}
            onConfirm={() => {
              retryJob({list_id, id})
            }}
          >
            <Button icon='reload' shape='circle' size='small' />
          </Popconfirm>
        )}
      </Mutation>
    )
  }
}
