// @flow
import * as React from 'react'
import { Popconfirm, Tooltip, Button } from 'antd'
import Mutation from '../../Mutation'
import gql from 'graphql-tag'
import JobsContainer from '../../../containers/JobsContainer'
import { subscribe } from '../../../lib/unstated-props'

type Props = {
  list_id: string,
  module_id: string,
  t: Function,
  containers: {jobsState: any}
}

class RerunModuleJobs extends React.Component<Props, void> {
  render () {
    const { list_id, module_id, t, containers } = this.props
    const { jobsState } = containers
    return (
      <Mutation
        query={gql`
          mutation createJobsForModule($list_id: String!, $module_id: String!, $force: Boolean) {
            createJobsForModule(list_id: $list_id, module_id: $module_id, force: $force) {
              id
              timestamp
              processedOn
              finishedOn
              progress
              data
            }
          }
        `}
        successMessage='Jobs Created'
        onComplete={(data) => {
          const { jobsCounts, jobsForList } = jobsState.state
          const prevList = jobsForList || []
          const combinedJobs = prevList.concat(data.createJobsForModule)
          jobsState.setJobs(jobsCounts, combinedJobs)
        }}
      >
        {(createJobsForModule) => (
          <Popconfirm
            title={t('Rerun jobs for this module?')}
            okText={t('Yes')} cancelText={t('Cancel')}
            onConfirm={() => {
              createJobsForModule({ list_id, module_id, force: true })
            }}
          >
            <Tooltip placement='bottom' title='Rerun Jobs'>
              <Button icon='reload' shape='circle' size='small' />
            </Tooltip>
          </Popconfirm>
        )}
      </Mutation>
    )
  }
}
export default subscribe(RerunModuleJobs, {jobsState: JobsContainer})
