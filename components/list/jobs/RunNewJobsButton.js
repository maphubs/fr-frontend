// @flow
import * as React from 'react'
import { Button, Popconfirm } from 'antd'
import Mutation from '../../Mutation'
import gql from 'graphql-tag'
import JobsContainer from '../../../containers/JobsContainer'
import { subscribe } from '../../../lib/unstated-props'

type Props = {
  list_id: string,
  t: Function,
  containers: {jobsState: any}
}

class RunNewJobsButton extends React.Component<Props, void> {
  render () {
    const { list_id, t, containers } = this.props
    const { jobsState } = containers
    return (
      <Mutation
        query={gql`
          mutation createJobs($list_id: String!) {
            createJobs(list_id: $list_id) {
              id
              timestamp
              processedOn
              finishedOn
              progress
              data
            }
          }
        `}
        successMessage='Jobs Created'
        onComplete={(data) => {
          const { jobsCount, jobsForList } = jobsState.state
          const combinedJobs = jobsForList.concat(data.jobs)
          jobsState.setJobs(jobsCount, combinedJobs)
        }}
      >
        {(createJobs) => (
          <Popconfirm
            title={t('Run new jobs for all modules in this list?')}
            okText={t('Yes')} cancelText={t('Cancel')}
            onConfirm={() => {
              createJobs({list_id})
            }}
          >
            <Button icon='play-circle' size='small'>{t('Run New Jobs')}</Button>
          </Popconfirm>
        )}
      </Mutation>
    )
  }
}
export default subscribe(RunNewJobsButton, {jobsState: JobsContainer})
