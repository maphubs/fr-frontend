// @flow
import * as React from 'react'
import { Button, Popconfirm } from 'antd'
import Mutation from '../../Mutation'
import gql from 'graphql-tag'

type Props = {
  list_id: string,
  t: Function
}

export default class ResumeJobsButton extends React.Component<Props, void> {
  render () {
    const { list_id, t } = this.props
    return (
      <Mutation
        query={gql`
          mutation resumeJobs($list_id: String!) {
            resumeJobs(list_id: $list_id)
          }
        `}
        successMessage='Jobs Resumed'
      >
        {(resumeJobs) => (
          <Popconfirm
            title={t('Resume all pending jobs for this list?')}
            okText={t('Yes')} cancelText={t('Cancel')}
            onConfirm={() => {
              resumeJobs({list_id})
            }}
          >
            <Button icon='play-circle' size='small'>{t('Resume')}</Button>
          </Popconfirm>
        )}
      </Mutation>
    )
  }
}
