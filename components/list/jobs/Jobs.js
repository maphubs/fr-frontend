// @flow
import * as React from 'react'
import { Query } from 'urql'
import gql from 'graphql-tag'
import Link from 'next/link'
import { Row, Col, Table, Button, Statistic, notification, Icon } from 'antd'
import AutoSizer from 'react-virtualized-auto-sizer'
import moment from 'moment'
import numeral from 'numeral'
import Spinner from '../../Spinner'
import JobsContainer from '../../../containers/JobsContainer'
import { subscribe } from '../../../lib/unstated-props'
import RunNewJobsButton from './RunNewJobsButton'
import PauseJobsButton from './PauseJobsButton'
import ResumeJobsButton from './ResumeJobsButton'
import RetryJobButton from './RetryJobButton'
import DeleteJobButton from './DeleteJobButton'
import ClearJobsButton from './ClearJobsButton'

const getQuery = (id: string) => {
  return gql`
    {
      jobCounts(list_id: "${id}"){
        waiting
        active
        completed
        failed
        delayed
        paused
      }

      jobsForList(list_id: "${id}") {
        id
        timestamp
        processedOn
        finishedOn
        progress
        data
        attemptsMade
      }
    }
  `
}

type Props = {
  list_id: string,
  t: Function,
  containers: {jobs: any}
}

class Jobs extends React.Component<Props, void> {
  render () {
    const { list_id, t, containers } = this.props
    const { jobs } = containers
    const query = getQuery(list_id)

    const jobsColumns = [{
      title: t('Status'),
      width: 75,
      key: 'status',
      render: (text, record) => {
        if (record.attemptsMade > 0) {
          return <Icon type='exclamation-circle' theme='filled' style={{color: 'red', fontSize: '24px', marginLeft: '15px'}} />
        } else {
          return <Icon type='clock-circle' theme='filled' style={{fontSize: '24px', marginLeft: '15px'}} />
        }
      }
    }, {
      title: t('ID'),
      width: 175,
      dataIndex: 'id',
      key: 'id',
      render: (text, record) => (
        <span style={{fontSize: '12px'}}>
          {record.id}
        </span>
      )
    }, {
      title: t('Created'),
      width: 175,
      dataIndex: 'timestamp',
      key: 'timestamp',
      render: (text, record) => (
        <span style={{fontSize: '12px'}}>
          {moment(record.timestamp).format('YYYY-MM-DD HH:MM:SS')}
        </span>
      )
    }, {
      title: t('Started'),
      width: 175,
      dataIndex: 'processedOn',
      key: 'processedOn',
      render: (text, record) => (
        <span style={{fontSize: '12px'}}>
          {moment(record.processedOn).format('YYYY-MM-DD HH:MM:SS')}
        </span>
      )
    }, {
      title: t('Finished'),
      width: 175,
      dataIndex: 'finishedOn',
      key: 'finishedOn',
      render: (text, record) => (
        <span style={{fontSize: '12px'}}>
          {moment(record.finishedOn).format('YYYY-MM-DD HH:MM:SS')}
        </span>
      )
    }, {
      title: t('Attempts'),
      width: 75,
      dataIndex: 'attemptsMade',
      key: 'attemptsMade'
    }, {
      title: t('Module'),
      width: 150,
      dataIndex: 'data.module_id',
      key: 'module',
      render: (text, record) => (
        <Link
          href={{
            pathname: '/admin/module/view',
            query: { id: record.data.module_id }
          }}
          as={`/admin/module/view/${record.data.module_id}`}
        >
          <a>{record.data.module_id}</a>
        </Link>
      )
    },
    {
      title: t('Action'),
      key: 'action',
      render: (text, record) => (
        <Row>
          <Col span={8}>
            <Link
              href={{
                pathname: '/admin/list/job',
                query: {
                  list_id,
                  job_id: record.id
                }
              }}
              as={`/admin/list/job/${list_id}/${record.id}`}
            >
              <Button icon='exception' shape='circle' size='small' />
            </Link>
          </Col>
          <Col span={8}>
            <RetryJobButton id={record.id} list_id={list_id} t={t} />
          </Col>
          <Col span={8}>
            <DeleteJobButton id={record.id} list_id={list_id} t={t} />
          </Col>
        </Row>
      )
    }]

    return (
      <Query query={query}>
        {({ fetching, error, data, executeQuery }) => {
          if (fetching) return <Spinner />

          const {jobCounts, jobsForList} = jobs.state
          if (error) {
            notification.error({
              message: 'Error',
              description: error.message,
              duration: 0
            })
            return ''
          } else {
            if (!jobCounts || !jobsForList) {
              console.log('setting jobs data')
              jobs.setJobs(data.jobCounts, data.jobsForList)
            }
          }
          if (!jobCounts || !jobsForList) return <p>No Data</p>

          return (
            <>
              <Row style={{height: '80px'}}>
                <Col span={12} style={{padding: '10px'}}>
                  <Row>
                    <Col span={12}>
                      <Statistic
                        title={t('Pending')}
                        value={jobCounts.waiting + jobCounts.active}
                        formatter={(val) => numeral(val).format('0,0')}
                        prefix={<Icon type='clock-circle' />}
                      />
                    </Col>
                    <Col span={12}>
                      <Statistic
                        title={t('Errors')}
                        value={jobCounts.failed}
                        valueStyle={{ color: '#cf1322' }}
                        prefix={<Icon type='exclamation-circle' style={{color: '#cf1322'}} />}
                        formatter={(val) => numeral(val).format('0,0')}
                      />
                    </Col>
                  </Row>
                </Col>
                <Col span={12} style={{padding: '20px'}}>
                  <Row>
                    <Col span={8}>
                      <RunNewJobsButton list_id={list_id} t={t} />
                    </Col>
                    <Col span={8}>
                      <PauseJobsButton list_id={list_id} t={t} />
                    </Col>
                    <Col span={8}>
                      <ResumeJobsButton list_id={list_id} t={t} />
                    </Col>
                  </Row>
                  <Row style={{marginTop: '5px'}}>
                    <Col span={8}>
                      <Button
                        icon='reload' size='small' onClick={async () => {
                          await jobs.setJobs(undefined, undefined)
                          await executeQuery({
                            requestPolicy: 'network-only'
                          })
                        }}
                      >{t('Refresh List')}
                      </Button>
                    </Col>
                    <Col span={8}>
                      <ClearJobsButton list_id={list_id} t={t} />
                    </Col>
                  </Row>
                </Col>
              </Row>
              <Row style={{height: 'calc(100% - 100px)'}}>
                <AutoSizer disableWidth>
                  {({ height }) => (
                    <>
                      <Table dataSource={jobsForList} columns={jobsColumns} size='small' bordered scroll={{ y: height - 200, x: 1000 }} />
                    </>
                  )}
                </AutoSizer>
              </Row>
            </>
          )
        }}
      </Query>
    )
  }
}
export default subscribe(Jobs, {jobs: JobsContainer})
