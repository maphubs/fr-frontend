// @flow
import * as React from 'react'
import { Button, Popconfirm } from 'antd'
import Mutation from '../../Mutation'
import gql from 'graphql-tag'

type Props = {
  list_id: string,
  t: Function
}

export default class ClearJobsButton extends React.Component<Props, void> {
  render () {
    const { list_id, t } = this.props
    return (
      <Mutation
        query={gql`
          mutation clearFailedJobs($list_id: String!) {
            clearFailedJobs(list_id: $list_id)
          }
        `}
        successMessage='Jobs Cleared'
      >
        {(clearFailedJobs) => (
          <Popconfirm
            title={t('Clear all failed jobs for this list?')}
            okText={t('Yes')} cancelText={t('Cancel')}
            onConfirm={() => {
              clearFailedJobs({list_id})
            }}
          >
            <Button icon='delete' size='small'>{t('Clear Failed')}</Button>
          </Popconfirm>
        )}
      </Mutation>
    )
  }
}
