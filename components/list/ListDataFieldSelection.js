// @flow
import * as React from 'react'
import { Select, Icon, Button } from 'antd'
import ListDashboardContainer from '../../containers/ListDashboardContainer'
import { subscribe } from '../../lib/unstated-props'

const { Option } = Select
const ButtonGroup = Button.Group

type Props = {
 containers: {listState: Object}
}

class ListDataFieldSelection extends React.Component<Props, void> {
  render () {
    const { listState } = this.props.containers
    const { selectedColumn, selectionResultColumns, sortDirectionAscending } = listState.state
    return (
      <>
        <span style={{float: 'left'}}><Icon type='bar-chart' style={{ color: '#42B532', fontSize: '32px' }} /></span>
        <Select
          defaultValue={selectedColumn?.key} style={{ width: 250, marginRight: '10px', marginLeft: '10px' }} onChange={(val) => {
            console.log(val)
            selectionResultColumns.forEach(col => {
              if (col.key === val) {
                listState.selectColumn(col)
              }
            })
          }}
        >
          {selectionResultColumns.map(col => {
            return (
              <Option key={col.key} value={col.key}>{col.title}</Option>
            )
          })}
        </Select>
        <ButtonGroup>
          <Button
            type={sortDirectionAscending ? 'primary' : undefined} icon='sort-ascending'
            onClick={() => {
              if (!sortDirectionAscending) {
                listState.setSortDirection(true)
              }
            }}
          />
          <Button
            type={!sortDirectionAscending ? 'primary' : undefined} icon='sort-descending'
            onClick={() => {
              if (sortDirectionAscending) {
                listState.setSortDirection(false)
              }
            }}
          />
        </ButtonGroup>
      </>
    )
  }
}
export default subscribe(ListDataFieldSelection, {listState: ListDashboardContainer})
