// @flow
import * as React from 'react'
import AutoSizer from 'react-virtualized-auto-sizer'
import { Empty } from 'antd'
import { FixedSizeList as List, areEqual } from 'react-window'
import ListCard from './ListCard'

type Props = {
  lists: Array<Object>,
  t: Function
}

export default class ListCarousel extends React.Component<Props, void> {
  render () {
    const { lists, t } = this.props
    if (!lists || lists.length === 0) return <Empty />
    const Card = React.memo(
      props => {
        const { index, style } = props
        return <div style={style}><ListCard style={style} {...lists[index]} t={t} /></div>
      },
      areEqual
    )

    return (
      <div style={{ height: '330px' }}>
        <AutoSizer disableHeight>
          {({ width }) => (
            <>
              {((220 * lists.length) > width) &&
                <style jsx global>{`

                  div.listscarousel::-webkit-scrollbar {
                      width:9px;
                      height: 10px;
                  }

                  div.listscarousel::-webkit-scrollbar-track {
                      -webkit-border-radius:5px;
                      border-radius:5px;
                      background:rgba(0,0,0,0.1);
                  }

                  div.listscarousel::-webkit-scrollbar-thumb {
                      -webkit-border-radius:5px;
                      border-radius:5px;
                      background:rgba(0,0,0,0.2);
                  }

                  div.listscarousel::-webkit-scrollbar-thumb:hover {
                      background:rgba(0,0,0,0.4);
                  }

                  div.listscarousel::-webkit-scrollbar-thumb:window-inactive {
                      background:rgba(0,0,0,0.05);
                  }
                `}
                </style>}
              <List
                className='listscarousel'
                layout='horizontal'
                height={345}
                itemCount={lists.length}
                itemSize={220}
                width={width}
              >
                {Card}
              </List>
            </>
          )}
        </AutoSizer>
      </div>
    )
  }
}
