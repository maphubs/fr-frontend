// @flow
import * as React from 'react'
import { Mutation } from 'urql'
import { message, notification } from 'antd'

type Props = {
  query: any,
  successMessage?: string,
  children: any,
  onComplete?: Function,
  onError?: Function,
  disableErrorMessage?: boolean,
  disableSuccessMessage?: boolean
}
export default class extends React.Component<Props, void> {
  static defaultProps = {
    sucessMessage: 'Saved'
  }

  render () {
    const { query, successMessage, children, onComplete, onError, disableErrorMessage, disableSuccessMessage } = this.props
    return (
      <Mutation query={query}>
        {({fetching, error, data, executeMutation}) => {
          const wrappedExecute = async (variables: Object) => {
            try {
              const result = await executeMutation(variables)
              const {error} = result
              if (error) {
                if (onError) {
                  onError(error)
                }
                if (!disableErrorMessage) {
                  notification.error({
                    message: 'Error',
                    description: (error && error.message) ? error.message : 'Server Error',
                    duration: 0
                  })
                }
              } else {
                if (onComplete) {
                  onComplete(result.data)
                }
                if (!disableSuccessMessage) {
                  message.success(successMessage)
                }
              }
            } catch (err) {
              notification.error({
                message: 'Error',
                description: (err && err.message) ? err.message : 'Server Error',
                duration: 0
              })
            }
          }
          return children(wrappedExecute, { fetching, error, data })
        }}
      </Mutation>
    )
  }
}
