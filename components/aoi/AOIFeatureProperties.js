// @flow
import * as React from 'react'

import { Row, Col, Card, Typography } from 'antd'
const { Paragraph } = Typography

type Props = {
  properties: Object,
  report?: boolean
}

export default class AOIFeatureProperties extends React.Component<Props, void> {
  shouldComponentUpdate () {
    return false
  }

  render () {
    const { properties, report } = this.props
    return (
      <Row>
        {Object.keys(properties).map(key => {
          return (
            <Col key={key} span={6} sm={6} xs={6} style={{padding: '0px 4px 4px 4px'}}>
              <Card
                bordered={!report}
                bodyStyle={{ padding: '2px', height: '40px' }}>
                <Paragraph ellipsis style={{fontSize: '10px', fontWeight: '700', marginBottom: '5px'}}>{key}</Paragraph>
                <Paragraph ellipsis style={{fontSize: '10px', margin: 0}}>{properties[key]}</Paragraph>
              </Card>
            </Col>
          )
        })}
      </Row>
    )
  }
}
