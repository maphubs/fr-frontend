// @flow
import * as React from 'react'
import { Row, Col, Switch, Tooltip, Button, Table, Empty, Popconfirm } from 'antd'
import { subscribe } from '../../lib/unstated-props'
import uuidv4 from 'uuid/v4'
import download from 'downloadjs'
import tokml from '@maphubs/tokml'
import turfbbox from '@turf/bbox'
import AOIDashboardContainer from '../../containers/AOIDashboardContainer'
import AOIReportContainer from './AOIReportContainer'
import AOIDrawingContainer from './AOIDrawingContainer'
import AOIDrawingDrawer from './AOIDrawingDrawer'
import AOIDrawingRowContent from './AOIDrawingRowContent'
import UploadDrawer from '../Imagery/UploadDrawer'
import SelectionDrawer from '../Imagery/SelectionDrawer'
import ImagerySelectionContainer from '../Imagery/ImagerySelectionContainer'
import satAPI from '../../utils/sat-api'
import {getLayerFRActive} from './map/layer-feature'

import type { Drawing } from '../../types/drawing'
// import type { AOIDrawingContainer } from './AOIDrawingContainer'

type Props = {
  t: Function,
  containers: {
    s2Selection: any,
    aoiDashboard: AOIDashboardContainer,
    aoiDrawings: AOIDrawingContainer
  }
}

class AOIDrawingPanel extends React.Component<Props, void> {
  showS2Selection = (id: string, bbox: Array<number>, type: string) => {
    const { s2Selection, aoiDrawings } = this.props.containers
    aoiDrawings.startImageSelection(id, type)
    s2Selection.showSelection(bbox)
  }

  addAOIBoundaryAsDrawing = () => {
    const { aoiDrawings, aoiDashboard } = this.props.containers
    const { geom, aoi, feature } = aoiDashboard.state
    const name = aoi.properties.name

    if (geom) {
      const bbox = turfbbox({
        type: 'FeatureCollection',
        features: [{
          type: 'Feature',
          properties: feature.properties,
          geometry: geom
        }]
      })
      const id = uuidv4()
      aoiDrawings.addDrawing({
        id,
        usingAOIBoundary: true,
        toggled: true,
        name: {en: name},
        type: 'area',
        features: [{
          type: 'Feature',
          properties: feature.properties,
          geometry: geom
        }],
        bbox,
        layer: getLayerFRActive({layer_id: id, short_id: id, name: {en: name}}, feature)
      })
    }
  }

  onDownloadClick = (record) => {
    const name = this.props.t(record.name)
    const geoJSON = {
      type: 'FeatureCollection',
      features: record.features
    }
    const kml = tokml(geoJSON)
    download(`data:text/xml;charset=utf-8,${kml}`, `${name}.kml`, 'text/xml')
  }

  render () {
    const { t, containers } = this.props
    const { aoiDrawings } = containers
    const { showDrawingDrawer, showImageryUploadDrawer, drawings, activeDrawing } = aoiDrawings.state

    const columns = [{
      title: t('Name'),
      dataIndex: 'name',
      key: 'name',
      sorter: (a, b) => {
        let aVal = t(a.name)
        let bVal = t(b.name)
        if (typeof aVal !== 'string') aVal = ''
        if (typeof bVal !== 'string') bVal = ''
        const aValLower = aVal.toLowerCase()
        const bValLower = bVal.toLowerCase()
        return aValLower.localeCompare(bValLower)
      },
      render: (text, record) => (
        <span>
          {t(record.name)}
        </span>
      )
    }, {
      title: t('Action'),
      key: 'action',
      width: 150,
      render: (text: string, record: Drawing) => (
        <Row>
          <Col span={6}>
            <Tooltip placement='top' title={t('Download KML')}>
              <Button
                onClick={() => { this.onDownloadClick(record) }}
                icon='download'
                type='primary'
                shape='circle' size='small'
              />
            </Tooltip>
          </Col>
          <Col span={6}>
            <Tooltip placement='top' title={t('Edit')}>
              <Button
                icon='edit' disabled={record.usingAOIBoundary} shape='circle' size='small' onClick={() => {
                  aoiDrawings.startDrawing(record)
                }}
              />
            </Tooltip>
          </Col>
          <Col span={6}>
            <Tooltip placement='top' title={t('Remove')}>
              <Popconfirm
                title={t('Remove this drawing?')}
                okText={t('Yes')} cancelText={t('Cancel')}
                onConfirm={() => {
                  aoiDrawings.removeDrawing(record.id)
                }}
              >
                <Button icon='delete' shape='circle' size='small' />
              </Popconfirm>
            </Tooltip>
          </Col>
          <Col span={6}>
            <Switch
              style={{
                border: 'none'
              }} size='small' checked={record.toggled} defaultChecked={record.toggled} onChange={(checked) => {
                record.toggled = checked
                aoiDrawings.toggleDrawing(record.id, checked)
              }}
            />
          </Col>
        </Row>
      )
    }]

    return (
      <Row style={{height: '100%'}}>
        <Row style={{position: 'relative', height: '30px', marginBottom: '5px'}}>
          <div style={{position: 'absolute', bottom: '0px', left: '0px'}}>
            <Button
              style={{marginRight: '20px'}}
              onClick={() => { aoiDrawings.startDrawing() }}
              icon='plus'
              type='primary'
              size='small'
              disabled={activeDrawing}
            >{t('New Drawing')}
            </Button>
          </div>
          <div style={{position: 'absolute', bottom: '0px', right: '0px'}}>
            <Button
              onClick={this.addAOIBoundaryAsDrawing}
              icon='plus'
              type='primary'
              size='small'
              ghost
              disabled={activeDrawing}
            >{t('Use AOI Boundary')}
            </Button>
          </div>
        </Row>
        {drawings.length > 0 &&
          <Table
            rowKey='id'
            dataSource={drawings}
            columns={columns}
            showHeader={false}
            size='small'
            pagination={false}
            defaultExpandAllRows
            expandedRowRender={record => <AOIDrawingRowContent {...record} showS2Selection={this.showS2Selection} t={t} />}
            scroll={{ y: 100 }}
          />}
        {drawings.length === 0 &&
          <Empty
            image={Empty.PRESENTED_IMAGE_SIMPLE}
            description={
              <span>
                {t('Add a Drawing')}
              </span>
            }
          />}
        {showDrawingDrawer &&
          <AOIDrawingDrawer onClose={aoiDrawings.cancelDrawing} onComplete={aoiDrawings.saveInfo} t={t} />}
        {showImageryUploadDrawer &&
          <UploadDrawer
            onClose={() => {
              aoiDrawings.cancelImageryUpload()
            }}
            onComplete={(layer) => {
              layer.toggled = true
              // save it to the report
              aoiDrawings.addImage(layer)
            }} t={t}
          />}
        <SelectionDrawer
          onSelect={(selectedResult => {
            // get the map layer
            const layer = satAPI.getMapHubsLayer(selectedResult)
            layer.toggled = true
            // save it to the report
            aoiDrawings.addImage(layer)
            this.props.containers.s2Selection.hideSelection()
          })}
          t={t}
        />
      </Row>
    )
  }
}
export default subscribe(AOIDrawingPanel, {
  aoiDashboard: AOIDashboardContainer,
  aoiDrawings: AOIDrawingContainer,
  s2Selection: ImagerySelectionContainer,
  aoiReport: AOIReportContainer
})
