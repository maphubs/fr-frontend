// @flow
import * as React from 'react'
import { Row, Col, Switch, Button, Table, Empty, Popconfirm } from 'antd'
import { subscribe } from '../../lib/unstated-props'
import AOIDashboardContainer from '../../containers/AOIDashboardContainer'
import SelectionDrawer from '../Imagery/SelectionDrawer'
import ImagerySelectionContainer from '../Imagery/ImagerySelectionContainer'
import satAPI from '../../utils/sat-api'
import download from 'downloadjs'
import tokml from '@maphubs/tokml'
import AOIReportContainer from './AOIReportContainer'
import UploadDrawer from '../Imagery/UploadDrawer'

type Props = {
  t: Function,
  containers: any
}

type State = {
  layer?: Object,
  showUploadDrawer?: boolean
}

/**
  Wrap imagery selection
  When a scene is selected add it to the map provide a toggle for the layer
 */
class AOIImageryPanel extends React.Component<Props, State> {
  constructor (props: Props) {
    super(props)
    this.state = {}
  }

  onDownloadClick = () => {
    const { aoiDashboard } = this.props.containers
    const { geom, aoi, feature } = aoiDashboard.state
    const name = aoi.properties.name
    if (geom) {
      const geoJSON = {
        type: 'FeatureCollection',
        features: [{
          type: 'Feature',
          properties: feature.properties,
          geometry: geom
        }]
      }
      const kml = tokml(geoJSON)
      download(`data:text/xml;charset=utf-8,${kml}`, `${name}.kml`, 'text/xml')
    }
  }

  startUpload = () => {
    this.setState({ showUploadDrawer: true })
  }

  showS2Selection = () => {
    this.props.containers.s2Selection.showSelection()
  }

  render () {
    const { t, containers } = this.props
    const { showUploadDrawer } = this.state
    const { aoiDashboard, aoiReport } = containers
    const { mapComponent, bbox } = aoiDashboard.state
    const { imagery } = aoiReport.state

    const columns = [{
      title: t('Name'),
      dataIndex: 'name',
      key: 'name',
      sorter: (a, b) => {
        let aVal = t(a.name)
        let bVal = t(b.name)
        if (typeof aVal !== 'string') aVal = ''
        if (typeof bVal !== 'string') bVal = ''
        const aValLower = aVal.toLowerCase()
        const bValLower = bVal.toLowerCase()
        return aValLower.localeCompare(bValLower)
      },
      render: (text, record) => (
        <span>
          {t(record.name)}
        </span>
      )
    }, {
      title: t('Action'),
      key: 'action',
      width: 150,
      render: (text, record) => (
        <Row>
          <Col span={12}>
            <Switch
              style={{
                border: 'none'
              }} size='small' checked={record.toggled} defaultChecked={record.toggled} onChange={(checked) => {
                record.toggled = checked
                aoiDashboard.toggle(record.layer_id, checked)
              }}
            />
          </Col>
          <Col span={12}>
            <Popconfirm
              title={t('Remove this imagery layer?')}
              okText={t('Yes')} cancelText={t('Cancel')}
              onConfirm={() => {
                aoiDashboard.removeLayer(record.layer_id)
                aoiReport.removeImagery(record.layer_id)
              }}
            >
              <Button icon='delete' shape='circle' size='small' />
            </Popconfirm>
          </Col>
        </Row>
      )
    }]
    return (
      <Row style={{height: '100%'}}>
        {showUploadDrawer &&
          <UploadDrawer
            onClose={() => {
              this.setState({showUploadDrawer: false})
            }}
            onComplete={(layer) => {
              // toggle off any previous layers in the map
              imagery.forEach(imageLayer => {
                if (imageLayer.toggled) {
                  aoiDashboard.toggle(imageLayer.layer_id, false)
                  imageLayer.toggled = false
                }
              })
              layer.toggled = true
              // add the new layer to the map
              aoiDashboard.addLayer(layer)
              // save it to the report
              aoiReport.addImagery(layer)
              this.setState({showUploadDrawer: false})
            }} t={t}
          />}
        <SelectionDrawer
          bbox={bbox}
          onSelect={(selectedResult => {
            // get the map layer
            const layer = satAPI.getMapHubsLayer(selectedResult)
            // add to map
            imagery.forEach(imageLayer => {
              if (imageLayer.toggled) {
                aoiDashboard.toggle(imageLayer.layer_id, false)
                imageLayer.toggled = false
              }
            })
            layer.toggled = true
            // add the new layer to the map
            aoiDashboard.addLayer(layer)
            // save it to the report
            aoiReport.addImagery(layer)
          })}
          t={t}
        />
        <Row style={{position: 'relative', height: '30px', marginBottom: '5px'}}>
          <div style={{position: 'absolute', bottom: '0px', left: '0px'}}>
            <Button
              style={{marginRight: '20px'}}
              onClick={this.startUpload}
              icon='upload'
              type='primary'
              size='small'
            >{t('Upload Imagery')}
            </Button>
            <Button
              onClick={this.showS2Selection}
              icon='cloud'
              type='primary'
              size='small'
            >{t('Add Sentinel-2')}
            </Button>
          </div>
          <div style={{position: 'absolute', bottom: '0px', right: '0px'}}>
            <Button
              onClick={this.onDownloadClick}
              icon='download'
              type='primary'
              size='small'
              ghost
            >{t('KML')}
            </Button>
          </div>
        </Row>
        {imagery.length > 0 &&
          <Table dataSource={imagery} columns={columns} showHeader={false} size='small' pagination={false} scroll={{ y: 100 }} />}
        {imagery.length === 0 &&
          <Empty
            image={Empty.PRESENTED_IMAGE_SIMPLE}
            description={
              <span>
                No Imagery
              </span>
            }
          />}
      </Row>
    )
  }
}

export default subscribe(AOIImageryPanel, {aoiDashboard: AOIDashboardContainer, s2Selection: ImagerySelectionContainer, aoiReport: AOIReportContainer})
