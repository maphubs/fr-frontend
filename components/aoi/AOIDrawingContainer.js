// @flow
import { Container } from 'unstated'
import { message } from 'antd'
import turfArea from '@turf/area'
import uuidv4 from 'uuid/v4'
import DrawRectangle from 'mapbox-gl-draw-rectangle-mode'
import DrawControlBar from './map/DrawControlBar'
import turfbbox from '@turf/bbox'
import getDrawingLayer from './map/layer-drawing'
import $ from 'jquery'
import { cloneStyle } from '../../utils/safe-style-clone'

import type { Drawing } from '../../types/drawing'
import type { LocalizedString } from '../../types/localizedString'

import getConfig from 'next/config'
const MAPHUBS_CONFIG = getConfig().publicRuntimeConfig

let MapboxDraw = (config: any) => {}
let MapboxCompare = (beforeMap: any, afterMap: any, config: any) => {}
let mapboxgl = {}
if (typeof window !== 'undefined') {
  MapboxDraw = require('@mapbox/mapbox-gl-draw/dist/mapbox-gl-draw.js')
  mapboxgl = require('mapbox-gl')
  MapboxCompare = require('mapbox-gl-compare')
}

export type AOIDrawingContainerState = {
  showDrawingDrawer: boolean, // drawer for entering drawing name
  showImageryUploadDrawer: boolean, // drawer for uploading imagery
  addImageType?: ['before' | 'after'],
  enableDrawingTool: boolean,
  drawingMessage?: string,
  drawings: Array<Drawing>,
  activeDrawing: Drawing,
  t: Function,
  mapState?: Object, // MapHubs MapState container (will provide a reference to the mapbox-gl instance once loaded)
  draw: Object, // mapbox-gl-draw instance
  drawBar: Object, // mapbox-gl-draw toolbar instance
  aoiDashboard: Object, // AOI Dashboard Container
  swipeEnabled?: boolean, // map swipe enabled when drawing has before and after
  editing?: boolean // editing an existing drawing
}

export default class AOIDrawingContainer extends Container<AOIDrawingContainerState> {
  constructor (t: Function, editAOIReport?: Object) {
    super()
    if (editAOIReport) {
      this.state = {
        enableDrawingTool: false,
        showDrawingDrawer: false,
        drawings: editAOIReport.drawings.drawings,
        drawingMessage: null,
        t
      }
    } else {
      this.state = {
        enableDrawingTool: false,
        showDrawingDrawer: false,
        drawings: [],
        drawingMessage: null,
        t
      }
    }
  }

  registerMapState = async (mapState: Object, aoiDashboard: Object) => {
    await this.setState(state => ({ mapState, aoiDashboard }))
    // if drawings present restore the map
    if (this.state.drawings && this.state.drawings.length > 0) {
      await Promise.all(this.state.drawings.map(async (drawing) => {
        drawing.toggled = false
        drawing.layer.style.layers.forEach(lyr => {
          if (!lyr.layout) lyr.layout = {}
          lyr.layout.visibility = 'visible'
        })
        await aoiDashboard.addLayer(drawing.layer)
        if (drawing.beforeImage) {
          drawing.beforeImage.style.layers.forEach(lyr => {
            if (!lyr.layout) lyr.layout = {}
            lyr.layout.visibility = 'visible'
          })
          await aoiDashboard.addLayer(drawing.beforeImage)
        }
        if (drawing.afterImage) {
          drawing.afterImage.style.layers.forEach(lyr => {
            if (!lyr.layout) lyr.layout = {}
            lyr.layout.visibility = 'visible'
          })
          await aoiDashboard.addLayer(drawing.afterImage)
        }
      }))
    }
  }

  /**
    toggle the drawing on the map along with before/after layers
   */
  toggleDrawing = async (id: string, active: boolean) => {
    const { drawings, aoiDashboard } = this.state
    let selectedDrawing
    drawings.forEach(drawing => {
      if (drawing.id === id) {
        selectedDrawing = drawing
      } else if (active) {
        // toggle off other drawings
        aoiDashboard.toggle(drawing.layer.layer_id, false)
        if (drawing.beforeImage) aoiDashboard.toggle(drawing.beforeImage.layer_id, false)
        if (drawing.afterImage) aoiDashboard.toggle(drawing.afterImage.layer_id, false)
        drawing.toggled = false
      }
    })
    if (selectedDrawing) {
      // now toggle the selected drawing
      aoiDashboard.toggle(selectedDrawing.layer.layer_id, active)
      if (selectedDrawing.beforeImage) await aoiDashboard.toggle(selectedDrawing.beforeImage.layer_id, false)
      if (selectedDrawing.afterImage) await aoiDashboard.toggle(selectedDrawing.afterImage.layer_id, false)
      selectedDrawing.toggled = active
      if (active) {
        if (selectedDrawing.beforeImage && selectedDrawing.afterImage) {
          this.setState({
            swipeEnabled: true,
            drawingMessage: 'Swipe to compare',
            drawings
          })
          this.setupSwipeMap()
        }
      } else {
        this.closeSwipeMap(selectedDrawing)
        this.setState({
          drawings
        })
      }
    } else {
      console.error(`drawing not found: ${id}`)
    }
  }

  startDrawing = async (restoreDrawing?: Drawing) => {
    const { activeDrawing } = this.state
    if (activeDrawing) {
      message.warning('Please save or cancel the active drawing before starting a new drawing')
      return
    }

    if (restoreDrawing) {
      const restoreClone = JSON.parse(JSON.stringify(restoreDrawing))
      await this.setState({
        enableDrawingTool: true,
        showDrawingDrawer: false,
        activeDrawing: restoreClone,
        editing: true
      })
      await this.startDrawingTool()
      // add the drawing to mapbox-gl draw
      this.state.draw.add({type: 'FeatureCollection', features: restoreDrawing.features})
      this.updateDrawing(restoreDrawing.features)
    } else {
      this.setState({
        enableDrawingTool: true,
        showDrawingDrawer: true,
        activeDrawing: {
          id: uuidv4(),
          toggled: false,
          features: []
        }
      })
    }
  }

  /**
    save the initial info for a new drawing and start the drawing tool
  */
  saveInfo = async ({name, type}: {name: LocalizedString, type: string}) => {
    const { activeDrawing } = this.state
    activeDrawing.name = name
    activeDrawing.type = type
    this.setState({ activeDrawing, showDrawingDrawer: false })
    await this.startDrawingTool()
  }

  /**
    add a drawing to the list
  */
  addDrawing = async (drawing: Drawing) => {
    const { activeDrawing, aoiDashboard } = this.state
    if (activeDrawing) {
      message.warning('Please save or cancel the active drawing before starting a new drawing')
      return
    }

    await aoiDashboard.addLayer(drawing.layer)

    return this.setState(state => {
      const drawings = state.drawings
      drawings.push(drawing)
      return { drawings }
    })
  }

  /**
    remove a drawing from the list
   */
  removeDrawing = async (id: string) => {
    return this.setState(state => {
      const { aoiDashboard, drawings } = state
      const newList = []
      drawings.map(async (drawing) => {
        if (drawing.id !== id) {
          newList.push(drawing)
        } else {
          if (drawing.layer) await aoiDashboard.removeLayer(drawing.layer.layer_id)
          if (drawing.beforeImage && drawing.afterImage) {
            this.closeSwipeMap(drawing)
          }
          if (drawing.beforeImage) await aoiDashboard.removeLayer(drawing.beforeImage.layer.layer_id)
          if (drawing.afterImage) await aoiDashboard.removeLayer(drawing.afterImage.layer.layer_id)
        }
      })
      return { drawings: newList }
    })
  }

  /**
    replace a drawing in the list
   */
  replaceDrawing = (drawingUpdate: Object) => {
    const {mapState} = this.state
    const newList = []
    const drawings = this.state.drawings
    drawings.forEach(drawing => {
      if (drawing.id === drawingUpdate.id) {
        newList.push(drawingUpdate)
      } else {
        newList.push(drawing)
      }
    })
    const source = drawingUpdate?.layer?.style?.layers[0]?.source
    const mapboxGL = mapState.state.map.map
    const geoJSONData = mapboxGL.getSource(source)
    geoJSONData.setData({
      type: 'FeatureCollection',
      features: drawingUpdate.features
    })
    this.setState({ drawings: newList, editing: false })
  }

  saveDrawing = async () => {
    const {activeDrawing, editing, drawings, aoiDashboard, t} = this.state
    if (editing) {
      this.replaceDrawing(activeDrawing)
      this.stopDrawingTool()
      message.info(t('Drawing Updated'))
      return
    }
    // toggle off any existing drawings
    if (drawings) {
      drawings.forEach(drawing => {
        this.toggleDrawing(drawing.id, false)
      })
    }
    activeDrawing.toggled = true
    const layer = getDrawingLayer(activeDrawing.id, activeDrawing.name, activeDrawing.features)
    activeDrawing.layer = layer
    await aoiDashboard.addLayer(layer)
    drawings.push(activeDrawing)
    this.setState({drawings})
    this.stopDrawingTool()
    message.info(t('Drawing Saved'))
  }

  cancelDrawing = () => {
    const {t} = this.state
    this.stopDrawingTool()
    message.info(t('Drawing Cancelled'))
  }

  startDrawingTool () {
    const {t, activeDrawing, mapState} = this.state
    const map = mapState.state.map.map
    let config
    const buttons = []
    if (activeDrawing.type === 'area') {
      config = {
        displayControlsDefault: false,
        controls: {
          trash: true
        },
        modes: {
          ...MapboxDraw.modes,
          draw_rectangle: DrawRectangle
        }
        // defaultMode: 'draw_rectangle'
      }
      buttons.push({
        on: 'click',
        action: () => {
          draw.changeMode('draw_rectangle')
        },
        icon: 'edit',
        tooltip: t('Draw Area')
      })
    } else {
      config = {
        displayControlsDefault: false,
        controls: {
          trash: true
        }
        // defaultMode: 'draw_polygon'
      }
      buttons.push({
        on: 'click',
        action: () => {
          draw.changeMode('draw_polygon')
        },
        icon: 'edit',
        tooltip: t('Draw Area')
      })
    }
    buttons.push({
      on: 'click',
      action: this.saveDrawing,
      icon: 'save',
      tooltip: t('Save')
    })
    buttons.push({
      on: 'click',
      action: this.cancelDrawing,
      icon: 'close',
      tooltip: t('Cancel Drawing')
    })
    const draw = new MapboxDraw(config)
    const drawBar = new DrawControlBar({draw, buttons})

    map.addControl(drawBar, 'top-right')

    map.on('draw.create', (e) => {
      const data = this.state.draw.getAll()
      this.updateDrawing(data.features)
    })

    map.on('draw.update', (e) => {
      const data = this.state.draw.getAll()
      this.updateDrawing(data.features)
    })

    map.on('draw.delete', () => {
      this.setState({drawingMessage: t('Draw an area below')})
    })

    message.info(t('Started a new drawing'))

    return this.setState({
      enableDrawingTools: true,
      drawingMessage: t('Draw an area below'),
      draw,
      drawBar
    })
  }

  stopDrawingTool = () => {
    const { mapState } = this.state
    const map = mapState.state.map.map
    map.removeControl(this.state.drawBar)
    this.setState({
      enableDrawingTools: false,
      showDrawingDrawer: false,
      drawingMessage: null,
      activeDrawing: null,
      draw: null,
      drawBar: null
    })
  }

  updateDrawing = (features: Array<Object>) => {
    const {t, activeDrawing} = this.state
    if (features.length > 0) {
      const polygons = {
        type: 'FeatureCollection',
        features: features
      }
      const bbox = turfbbox(polygons)
      const area = turfArea(polygons)
      // restrict to area to 2 decimal points
      const areaM2 = Math.round(area * 100) / 100
      const areaKM2 = area * 0.000001
      const areaHA = areaM2 / 10000

      let areaMessage = t('Total area: ')

      if (areaM2 < 1000) {
        areaMessage = areaMessage + areaM2.toLocaleString() + 'm2 '
      } else {
        areaMessage = areaMessage + areaKM2.toLocaleString() + 'km2 '
      }
      areaMessage = areaMessage + areaHA.toLocaleString() + 'ha'

      activeDrawing.areaHA = areaHA
      activeDrawing.features = features
      activeDrawing.bbox = bbox
      this.setState({
        drawingMessage: areaMessage,
        activeDrawing
      })
    }
  }

  startImageUpload = (id: string, type: string) => {
    this.setState({
      showImageryUploadDrawer: true,
      addImageDrawingID: id,
      addImageType: type
    })
  }

  startImageSelection = (id: string, type: string) => {
    this.setState({
      addImageDrawingID: id,
      addImageType: type
    })
  }

  cancelImageryUpload = () => {
    this.setState({
      showImageryUploadDrawer: false,
      addImageDrawingID: null,
      addImageType: null
    })
  }

  addImage = async (layer: Object) => {
    const { addImageType, addImageDrawingID, drawings, aoiDashboard } = this.state
    let selectedDrawing
    drawings.forEach(drawing => {
      if (drawing.id === addImageDrawingID) {
        selectedDrawing = drawing
      }
    })
    if (selectedDrawing) {
      if (addImageType === 'before') {
        selectedDrawing.beforeImage = layer
      } else if (addImageType === 'after') {
        selectedDrawing.afterImage = layer
      }
      await aoiDashboard.addLayer(layer)
      let swipeMessage
      let swipeEnabled
      if (selectedDrawing.beforeImage && selectedDrawing.afterImage) {
        swipeMessage = 'Swipe to compare'
        swipeEnabled = true
      }
      this.setState({
        drawings,
        showImageryUploadDrawer: false,
        addImageDrawingID: null,
        addImageType: null,
        drawingMessage: swipeMessage,
        swipeEnabled
      })
      if (swipeEnabled) {
        this.setupSwipeMap()
      }
    } else {
      console.error(`drawing not found ${addImageDrawingID}`)
    }
  }

  removeImage = async (id: string, type: string) => {
    this.setState(async state => {
      const { drawings, aoiDashboard } = state
      const updatedDrawings = drawings.map(async drawing => {
        if (drawing.id === id) {
          if (type === 'before') {
            await aoiDashboard.removeLayer(drawing?.beforeImage?.layer_id)
            delete drawing.beforeImage
          } else if (type === 'after') {
            await aoiDashboard.removeLayer(drawing?.afterImage?.layer_id)
            delete drawing.afterImage
          }
          await this.closeSwipeMap(drawing)
        }
        return Object.assign({}, drawing)
      })
      return {drawings: updatedDrawings}
    })
  }

  setupSwipeMap = () => {
    const { drawings, aoiDashboard, mapState } = this.state
    const regularMap = mapState.state.map.map

    let toggledDrawing

    drawings.forEach(drawing => {
      if (drawing.toggled) {
        toggledDrawing = drawing
      }
    })

    const glStyle = aoiDashboard.state.glStyle

    if (toggledDrawing) {
      const styleClone = cloneStyle(glStyle)

      // hide after image in before map
      const afterLayerID = toggledDrawing.afterImage.style.layers[0].id
      const beforeLayerID = toggledDrawing.beforeImage.style.layers[0].id
      glStyle.layers.forEach(lyr => {
        if (lyr.id === afterLayerID) {
          if (!lyr.layout) lyr.layout = {}
          lyr.layout.visibility = 'none'
        }
        if (lyr.id === beforeLayerID) {
          if (!lyr.layout) lyr.layout = {}
          lyr.layout.visibility = 'visible'
        }
      })
      // hide before image in after map
      styleClone.layers.forEach(lyr => {
        if (lyr.id === beforeLayerID) {
          if (!lyr.layout) lyr.layout = {}
          lyr.layout.visibility = 'none'
        }
        if (lyr.id === afterLayerID) {
          if (!lyr.layout) lyr.layout = {}
          lyr.layout.visibility = 'visible'
        }
      })
      console.log('adding swipe map')
      console.log(styleClone)

      const center = regularMap.getCenter()
      const zoom = regularMap.getZoom()
      $('#swipe-map').show()
      mapboxgl.accessToken = MAPHUBS_CONFIG.MAPBOX_ACCESS_TOKEN
      try {
        const afterMap = new mapboxgl.Map({
          container: 'swipe-map', // Container ID
          style: styleClone,
          center,
          zoom
        })
        afterMap.on('error', (error) => {
          console.error(error)
        })

        const compare = new MapboxCompare(regularMap, afterMap, {
          mousemove: false
        })
        this.afterMap = afterMap
        this.compare = compare
      } catch (err) {
        console.error('failed to init swipe map')
        console.error(err)
      }
    }
  }

  closeSwipeMap = (selectedDrawing: Object) => {
    const { aoiDashboard, mapState } = this.state
    const regularMap = mapState.state.map.map
    if (this.compare) {
      // remove from original map
      regularMap.getContainer().removeChild(this.compare._container)
      $('#swipe-map').empty()
      $('#swipe-map').hide()

      if (selectedDrawing && selectedDrawing.afterImage) {
        const glStyle = aoiDashboard.state.glStyle
        const afterLayerID = selectedDrawing.afterImage.style.layers[0].id
        glStyle.layers.forEach(lyr => {
          if (lyr.id === afterLayerID) {
            if (!lyr.layout) lyr.layout = {}
            lyr.layout.visibility = 'visible'
          }
        })
      }

      delete this.compare
      delete this.afterMap
    }
  }
}
