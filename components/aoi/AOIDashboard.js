// @flow
import * as React from 'react'
import { Breadcrumb, Col, Row, Slider } from 'antd'
import Link from 'next/link'
import Cards from '../Cards'
import AOIMap from './AOIMap'
import InfoDrawer from './InfoDrawer'
import AOIReviewPanel from './AOIReviewPanel'
import slugify from 'slugify'
import AOIDashboardContainer from '../../containers/AOIDashboardContainer'
import AOIReportContainer from './AOIReportContainer'
import AOIDrawingContainer from './AOIDrawingContainer'
import AOIFeatureProperties from './AOIFeatureProperties'
import { Provider } from 'unstated'

type Props = {
  aoi: Object,
  list: Object,
  results: Array<Object>,
  reports: Array<Object>,
  currentReports?: Array<Object>,
  editingReportID?: string,
  editAOIReport?: Object,
  t: Function,
  locale: string
}

const cardHeadStyle = {
  fontSize: '14px',
  lineHeight: '8px',
  minHeight: '20px',
  paddingLeft: '5px'
}

class AOIDashboard extends React.Component<Props, void> {
  constructor (props: Props) {
    super(props)
    const list = props.list

    this.AOIDashboardContainer = new AOIDashboardContainer({
      aoi: props.aoi,
      list,
      results: props.results,
      locale: props.locale,
      dashboard: this
    })

    this.AOIReportContainer = new AOIReportContainer(props.aoi, props.results, props.currentReports, props.editingReportID, props.editAOIReport)
    this.AOIDrawingContainer = new AOIDrawingContainer(props.t, props.editAOIReport)
  }

  AOIDashboardContainer: any
  AOIReportContainer: any
  AOIDrawingContainer: any

  shouldComponentUpdate (nextProps: Props) {
    return false
  }

  render () {
    const { t, aoi, list, reports } = this.props
    const { isBuffered } = this.AOIDashboardContainer.state
    const name = t(list.name)
    const crumbs = [
      {
        href: {
          pathname: '/list',
          query: {
            id: list.id,
            name
          }
        },
        as: `/list/${list.id}/${slugify(name)}`,
        label: name
      },
      {
        label: aoi.properties.name
      }
    ]

    let foundFirstGLADCard = false
    let foundFirstFireCard = false

    return (
      <Provider inject={[this.AOIDashboardContainer, this.AOIReportContainer, this.AOIDrawingContainer]}>
        <Row style={{ height: '100%', overflow: 'hidden', backgroundColor: '#F4F5F8', padding: '4px' }}>
          <Col span={10} style={{ height: '100%' }}>
            <div style={{height: 'calc(100% - 200px)', borderRadius: '2px', border: '1px solid #e8e8e8'}}>
              <div style={{height: '25px', backgroundColor: '#FFF', paddingLeft: '10px'}}>
                <Breadcrumb style={{color: '#323333'}} separator='>'>
                  {
                    crumbs.map((crumb) => {
                      if (crumb.href) {
                        return (
                          <Breadcrumb.Item key={crumb.as || crumb.label}>
                            <Link href={crumb.href} as={crumb.as}>
                              <a style={{fontWeight: 'bold', color: '#323333'}}>{crumb.label}</a>
                            </Link>
                          </Breadcrumb.Item>
                        )
                      } else {
                        return (
                          <Breadcrumb.Item key={crumb.label}>
                            <span style={{fontWeight: 'bold', color: '#323333'}}>{crumb.label}</span>
                          </Breadcrumb.Item>
                        )
                      }
                    })
                  }
                </Breadcrumb>
              </div>
              <div style={{height: 'calc(100% - 25px)'}}>
                <AOIMap t={t} />
              </div>
            </div>
            <Row style={{height: '200px'}}>
              <AOIReviewPanel t={t} reports={reports} modules={list.modules} />
            </Row>
          </Col>
          <Col span={14} style={{ height: '100%' }}>
            {isBuffered &&
              <Row style={{ paddingLeft: '20px', paddingRight: '20px', paddingBotton: '10px', height: '50px' }}>
                <Slider
                  min={5} max={50}
                  marks={{
                    5: '5km',
                    10: '10km',
                    25: '25km',
                    50: '50km'
                  }}
                  step={null}
                  onChange={this.AOIDashboardContainer.changeBuffer}
                  defaultValue={this.AOIDashboardContainer.state.buffer}
                />
              </Row>}
            <Row style={{ height: isBuffered ? 'calc(100vh - 50px)' : '100vh', overflow: 'auto' }}>
              {(aoi.feature && aoi.feature.properties) &&
                <div style={{height: '120px', overflow: 'auto'}}>
                  <AOIFeatureProperties properties={aoi.feature.properties} />
                </div>}
              {list.modules.map(mod => {
                // only allow the first GLAD and Fire cards
                if (mod.card.id === 'glad') {
                  if (foundFirstGLADCard) {
                    return ''
                  } else {
                    foundFirstGLADCard = true
                  }
                }
                if (mod.card.id === 'fires') {
                  if (foundFirstFireCard) {
                    return ''
                  } else {
                    foundFirstFireCard = true
                  }
                }

                const Component = Cards[mod.card.id].Component
                return (
                  <Col key={mod.id} span={mod.card.span || 12} style={{padding: '0px 4px 4px 4px'}}>
                    <Component t={t} cardHeadStyle={cardHeadStyle} cardConfig={mod.card} />
                  </Col>
                )
              })}
            </Row>
          </Col>
          <InfoDrawer t={t} />
        </Row>
      </Provider>
    )
  }
}
export default AOIDashboard
