// @flow
import * as React from 'react'
import { Button, Drawer, Radio, Row } from 'antd'
import LocalizedInput from '../form/LocalizedInput'

type Props = {
  t: Function,
  onClose: Function,
  onComplete: Function
}

type State = {
  drawingType?: string,
  name?: Object
}

export default class AOIDrawingDrawer extends React.Component<Props, State> {
  state = {
    drawingType: 'area',
    name: undefined
  }

  nameChange = (name: Object) => {
    this.setState({ name })
  }

  typeChange = (drawingType: any) => {
    this.setState({ drawingType })
  }

  save = () => {
    this.props.onComplete({
      name: this.state.name,
      type: this.state.drawingType
    })
  }

  render () {
    const {nameChange, typeChange, save} = this
    const { t, onClose } = this.props
    const {name, drawingType} = this.state
    return (
      <Drawer
        title={t('Create Drawing')}
        placement='bottom'
        height='400px'
        closable
        destroyOnClose
        bodyStyle={{ height: 'calc(100% - 55px)', padding: 0 }}
        onClose={onClose}
        visible
      >
        <Row style={{padding: '20px 50px'}}>
          <p>Choose if this is a general area, or a tracing where measured hectares will be reported</p>
          <Radio.Group onChange={(e) => typeChange(e.target.value)} value={drawingType}>
            <Radio value='area'>Area</Radio>
            <Radio value='tracing'>Tracing</Radio>
          </Radio.Group>
        </Row>
        <Row style={{padding: '20px 50px'}}>
          <p>Enter a name that will display in the map legend</p>
          <LocalizedInput placeholder={t('Name')} onChange={nameChange} />
        </Row>
        <Row style={{marginTop: '20px', marginRight: '50px', textAlign: 'right'}}>
          <Button type='primary' disabled={!name || !drawingType} onClick={save}>Start Drawing</Button>
        </Row>
      </Drawer>
    )
  }
}
