// @flow
import * as React from 'react'
import { Button } from 'antd'
import AOIDrawingPanel from './AOIDrawingPanel'
import AOISaveReportDrawer from './AOISaveReportDrawer'
import { subscribe } from '../../lib/unstated-props'
import AOIDrawingContainer from './AOIDrawingContainer'

type Props = {
  t: Function,
  reports: Array<Object>,
  modules: Array<Object>,
  containers: any
}

type State = {
  showSavePanel: boolean
}

class AOIReviewPanel extends React.Component<Props, State> {
  state = {
    showSavePanel: false
  }

  save = () => {
    this.setState({ showSavePanel: true })
  }

  onSaveClose = () => {
    this.setState({ showSavePanel: false })
  }

  render () {
    const { t, reports, containers } = this.props
    const { showSavePanel } = this.state
    const { aoiDrawings } = containers
    const { drawings } = aoiDrawings.state

    return (
      <div className='card-container' style={{height: '205px'}}>
        <div className='steps-content'>
          <div style={{height: '160px'}}>
            <AOIDrawingPanel t={t} />
          </div>
        </div>
        <div className='steps-action'>
          <Button type='primary' size='small' disabled={!drawings || drawings.length === 0} onClick={this.save}>{t('Save to Report')}</Button>
        </div>
        {showSavePanel &&
          <AOISaveReportDrawer reports={reports} t={t} onClose={this.onSaveClose} />}
        <style jsx global>{`
          .card-container {
            background-color: #FFF;
            padding: 5px;
          }
          .steps-content {
            
          }
          .steps-action {
            margin-top: 2px;
            margin-right: 5px;
            text-align: right;
          }
        `}
        </style>
      </div>
    )
  }
}
export default subscribe(AOIReviewPanel, {aoiDrawings: AOIDrawingContainer})
