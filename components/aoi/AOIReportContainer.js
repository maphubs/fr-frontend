// @flow
import { Container } from 'unstated'

type AOIReportContainerState = {
  report_id?: string,
  notes?: Object,
  aoi: Object,
  results: Object,
  saved: boolean,
  currentReports?: Array<Object>
}

export default class AOIReportContainer extends Container<AOIReportContainerState> {
  constructor (aoi: Object, results: Object, currentReports: Array<Object>, selectedReportID?: string, editingAOIReport?: Object) {
    super()
    if (selectedReportID && editingAOIReport) {
      let report
      currentReports.forEach(rpt => {
        if (rpt.id === selectedReportID) {
          report = rpt
        }
      })
      this.state = {
        aoi,
        report_id: report?.id,
        results,
        notes: editingAOIReport.notes.notes,
        currentReports,
        saved: true
      }
    } else {
      this.state = {
        aoi,
        results,
        notes: null,
        currentReports,
        saved: false
      }
    }
  }

  setAOI = (aoi: Object, results: Object) => {
    return this.setState(state => ({ aoi, results }))
  }

  setNotes = (notes: Object) => {
    return this.setState(state => ({ notes }))
  }

  selectReport = (report_id: string) => {
    // if selected report is in the current report, also set saved so we update instead
    let alreadySaved
    if (this.state.currentReports) {
      this.state.currentReports.map(currentReport => {
        if (currentReport.id === report_id) {
          alreadySaved = true
        }
      })
    }
    if (alreadySaved) {
      return this.setState(state => ({ report_id, saved: true }))
    } else {
      return this.setState(state => ({ report_id }))
    }
  }

  setSaved = () => {
    return this.setState(state => ({ saved: true }))
  }
}
