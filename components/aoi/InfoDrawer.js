// @flow
import * as React from 'react'
import { Drawer } from 'antd'
import { subscribe } from '../../lib/unstated-props'
import AOIDashboardContainer from '../../containers/AOIDashboardContainer'

type Props = {
  t: Function,
  containers: {aoiDashboard: any}
}

class InfoDrawer extends React.Component<Props, void> {
  render () {
    const { t, containers } = this.props
    const { aoiDashboard } = containers
    const { info } = aoiDashboard.state
    const visible = typeof info !== 'undefined'
    return (
      <Drawer
        title={t('Data Information')}
        placement='bottom'
        height={300}
        closable
        onClose={aoiDashboard.hideInfo}
        visible={visible}
      >
        {info}
      </Drawer>
    )
  }
}
export default subscribe(InfoDrawer, {'aoiDashboard': AOIDashboardContainer})
