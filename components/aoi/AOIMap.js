// @flow
import * as React from 'react'
import { } from 'antd'
import { subscribe } from '../../lib/unstated-props'
import AOIDashboardContainer from '../../containers/AOIDashboardContainer'
import AOIDrawingContainer from './AOIDrawingContainer'
import { Provider } from 'unstated'
import getConfig from 'next/config'
const MAPHUBS_CONFIG = getConfig().publicRuntimeConfig

let Map
let MapContainer
let BaseMapContainer
if (typeof window !== 'undefined') {
  Map = require('@bit/kriscarle.maphubs-components.components.map').InteractiveMap
  MapContainer = require('@bit/kriscarle.maphubs-components.components.map').MapContainer
  BaseMapContainer = require('@bit/kriscarle.maphubs-components.components.map').BaseMapContainer
}

type Props = {
  t: Function,
  containers: {aoiDashboard: any, aoiDrawings: any}
}

class AOIMap extends React.Component<Props, void> {
  constructor (props: Props) {
    super(props)
    if (typeof window !== 'undefined') {
      this.MapState = new MapContainer()
      this.BaseMap = new BaseMapContainer({
        bingKey: MAPHUBS_CONFIG.BING_KEY,
        mapboxAccessToken: MAPHUBS_CONFIG.MAPBOX_ACCESS_TOKEN
      })
    }
  }

  MapState: any
  mapComponent: any
  BaseMap: any

  componentDidMount () {
    const { aoiDashboard, aoiDrawings } = this.props.containers
    console.log('AOIMap registering MapComponent')
    aoiDashboard.registerMapComponent(this.mapComponent, this.MapState)
    aoiDrawings.registerMapState(this.MapState, aoiDashboard)
  }

  render () {
    const { t, containers } = this.props
    const { aoiDashboard, aoiDrawings } = containers
    const { glStyle, mapLayers, bbox } = aoiDashboard.state
    const { drawingMessage, activeDrawing } = aoiDrawings.state

    if (glStyle && mapLayers) {
      return (
        <div style={{ height: '100%' }}>
          <style jsx global>{`
            .collapsible {
              padding-left: 0px;
              list-style-type: none;
            }
            .omh-legend{padding-left:2px;padding-right:2px;padding-top:2px;padding-bottom:4px;min-height:20px;}
            .omh-legend h3{font-size:10px;color:#212121;margin:0px;}
            .base-map-legend *{color:#212121 !important;}
            .omh-legend .block{height:15px;width:20px;float:left;margin-right:5px;border:1px solid #888;}
            .omh-legend .point{height:15px;width:15px;float:left;margin-right:5px;border-radius:50%;border:1px solid #888;}
            .omh-legend .double-stroke{box-shadow:inset 0 0 0 3px rgba(100,100,100,0.2);}
            .word-wrap{overflow-wrap:break-word;-ms-word-break:break-all;word-break:break-word;-ms-hyphens:auto;-moz-hyphens:auto;-webkit-hyphens:auto;-webkit-hyphens:auto;-moz-hyphens:auto;-ms-hyphens:auto;hyphens:auto;}

            .mapboxgl-ctrl-logo {
            position: absolute !important;
            bottom: -5px !important;
            left: 80px !important;
            }

            .maphubs-inset .mapboxgl-ctrl-logo {
              display: none;
            }

            .mapboxgl-canvas{left:0 !important;}
            
            .mapboxgl-popup{z-index:200 !important;height:200px;width:150px;}
            .mapboxgl-popup-content{padding:0 !important;}
            .mapboxgl-popup-close-button{top:-7px !important;right:-7px !important;z-index:201 !important;background-color:rgba(255,255,255,0.75) !important;color:black !important;border-radius:25px !important;border:1px solid black !important;width:14px !important;height:14px !important;line-height:5px !important;padding-bottom:1px !important;padding-top:0px !important;padding-left:0.5px !important;padding-right:0px !important;}
            .maphubs-feature-popup{padding:0;}
            .mapbox-gl-draw_point,.mapbox-gl-draw_line,.mapbox-gl-draw_polygon{border-bottom:none !important;border-right:1px #ddd solid !important;}
            .mapboxgl-ctrl-top-right{top:40px !important;}
            .maphubs-ctrl-scale{border:none !important;padding:0 !important;background-color:inherit !important;position:relative;height:22px;position:absolute;bottom:5px;right:5px;height:34px;margin:0px !important;}
            .map-position{height:12px;width:75px;position:absolute;top:0;right:0;background-color:rgba(255,255,255,0.55);font-size:10px;line-height:10px;text-align:center;box-shadow:none !important;color:#333;}
            .metric-scale{height:12px;font-size:10px;line-height:10px;text-align:center;box-shadow:none !important;background-color:rgba(255,255,255,0.55);border-width:medium 2px 2px;border-style:none solid solid;border-color:#333;padding:0 5px;color:#333;position:absolute;top:12px;right:0;}
            .imperial-scale{height:12px;font-size:10px;line-height:10px;text-align:center;box-shadow:none !important;background-color:rgba(255,255,255,0.55);border-width:medium 2px 2px;border-style:solid solid none;border-color:#333;padding:0 5px;color:#333;position:absolute;bottom:0;right:0;}
            .mapboxgl-compare {
              background-color:#fff;
              position:absolute;
              width:2px;
              height:100%;
              z-index:1;
              }
            .mapboxgl-compare .compare-swiper {
              background-color:#42B532;
              box-shadow:inset 0 0 0 2px #fff;
              display:inline-block;
              border-radius:50%;
              position:absolute;
              width:60px;
              height:60px;
              top:50%;
              left:-30px;
              margin:-30px 1px 0;
              color: #fff;
              cursor:ew-resize;
              background-image:url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+PHN2ZyAgIHhtbG5zOmRjPSJodHRwOi8vcHVybC5vcmcvZGMvZWxlbWVudHMvMS4xLyIgICB4bWxuczpjYz0iaHR0cDovL2NyZWF0aXZlY29tbW9ucy5vcmcvbnMjIiAgIHhtbG5zOnJkZj0iaHR0cDovL3d3dy53My5vcmcvMTk5OS8wMi8yMi1yZGYtc3ludGF4LW5zIyIgICB4bWxuczpzdmc9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiAgIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgICB4bWxuczpzb2RpcG9kaT0iaHR0cDovL3NvZGlwb2RpLnNvdXJjZWZvcmdlLm5ldC9EVEQvc29kaXBvZGktMC5kdGQiICAgeG1sbnM6aW5rc2NhcGU9Imh0dHA6Ly93d3cuaW5rc2NhcGUub3JnL25hbWVzcGFjZXMvaW5rc2NhcGUiICAgd2lkdGg9IjYwIiAgIGhlaWdodD0iNjAiICAgdmVyc2lvbj0iMS4xIiAgIHZpZXdCb3g9IjAgMCA2MCA2MCIgICBpZD0ic3ZnNTQzNCIgICBpbmtzY2FwZTp2ZXJzaW9uPSIwLjkxK2RldmVsK29zeG1lbnUgcjEyOTExIiAgIHNvZGlwb2RpOmRvY25hbWU9Imwtci5zdmciPiAgPG1ldGFkYXRhICAgICBpZD0ibWV0YWRhdGE1NDQ0Ij4gICAgPHJkZjpSREY+ICAgICAgPGNjOldvcmsgICAgICAgICByZGY6YWJvdXQ9IiI+ICAgICAgICA8ZGM6Zm9ybWF0PmltYWdlL3N2Zyt4bWw8L2RjOmZvcm1hdD4gICAgICAgIDxkYzp0eXBlICAgICAgICAgICByZGY6cmVzb3VyY2U9Imh0dHA6Ly9wdXJsLm9yZy9kYy9kY21pdHlwZS9TdGlsbEltYWdlIiAvPiAgICAgICAgPGRjOnRpdGxlPjwvZGM6dGl0bGU+ICAgICAgPC9jYzpXb3JrPiAgICA8L3JkZjpSREY+ICA8L21ldGFkYXRhPiAgPGRlZnMgICAgIGlkPSJkZWZzNTQ0MiIgLz4gIDxzb2RpcG9kaTpuYW1lZHZpZXcgICAgIHBhZ2Vjb2xvcj0iI2ZmZmZmZiIgICAgIGJvcmRlcmNvbG9yPSIjNjY2NjY2IiAgICAgYm9yZGVyb3BhY2l0eT0iMSIgICAgIG9iamVjdHRvbGVyYW5jZT0iMTAiICAgICBncmlkdG9sZXJhbmNlPSIxMCIgICAgIGd1aWRldG9sZXJhbmNlPSIxMCIgICAgIGlua3NjYXBlOnBhZ2VvcGFjaXR5PSIwIiAgICAgaW5rc2NhcGU6cGFnZXNoYWRvdz0iMiIgICAgIGlua3NjYXBlOndpbmRvdy13aWR0aD0iMTI4NiIgICAgIGlua3NjYXBlOndpbmRvdy1oZWlnaHQ9Ijc1MSIgICAgIGlkPSJuYW1lZHZpZXc1NDQwIiAgICAgc2hvd2dyaWQ9InRydWUiICAgICBpbmtzY2FwZTp6b29tPSI0IiAgICAgaW5rc2NhcGU6Y3g9IjI1Ljg4OTgzMSIgICAgIGlua3NjYXBlOmN5PSIzNC4zODE4MzMiICAgICBpbmtzY2FwZTp3aW5kb3cteD0iMCIgICAgIGlua3NjYXBlOndpbmRvdy15PSIyMyIgICAgIGlua3NjYXBlOndpbmRvdy1tYXhpbWl6ZWQ9IjAiICAgICBpbmtzY2FwZTpjdXJyZW50LWxheWVyPSJzdmc1NDM0IiAgICAgaW5rc2NhcGU6b2JqZWN0LW5vZGVzPSJ0cnVlIiAgICAgaW5rc2NhcGU6c25hcC1zbW9vdGgtbm9kZXM9InRydWUiPiAgICA8aW5rc2NhcGU6Z3JpZCAgICAgICB0eXBlPSJ4eWdyaWQiICAgICAgIGlkPSJncmlkNTk4OSIgLz4gIDwvc29kaXBvZGk6bmFtZWR2aWV3PiAgPHBhdGggICAgIHN0eWxlPSJmaWxsOiNmZmZmZmY7ZmlsbC1ydWxlOmV2ZW5vZGQ7c3Ryb2tlOm5vbmU7c3Ryb2tlLXdpZHRoOjFweDtzdHJva2UtbGluZWNhcDpidXR0O3N0cm9rZS1saW5lam9pbjptaXRlcjtzdHJva2Utb3BhY2l0eToxIiAgICAgZD0iTSAyNSAyNCBMIDE2IDMwIEwgMjUgMzYgTCAyNSAyNCB6IE0gMzUgMjQgTCAzNSAzNiBMIDQ0IDMwIEwgMzUgMjQgeiAiICAgICBpZD0icGF0aDU5OTUiIC8+PC9zdmc+);
              }
          `}
          </style>
          <Provider inject={[this.MapState, this.BaseMap]}>
            <Map
              ref={(el) => { this.mapComponent = el }}
              map_id='aoi-dashboard-map'
              height='100%'
              basemap='default'
              fitBounds={bbox}
              style={glStyle}
              layers={mapLayers}
              showTitle={false}
              showLegendLayersButton
              showLayerVisibility={false}
              showLayerInfo={false}
              showShareButtons={false}
              t={t}
              hideInactive
              insetMap={false}
              showLogo
              hash={false}
              primaryColor={MAPHUBS_CONFIG.primaryColor}
              logoSmall={MAPHUBS_CONFIG.logoSmall}
              logoSmallHeight={MAPHUBS_CONFIG.logoSmallHeight}
              logoSmallWidth={MAPHUBS_CONFIG.logoSmallWidth}
              mapboxAccessToken={MAPHUBS_CONFIG.MAPBOX_ACCESS_TOKEN}
              locale='en'
            >
              <div id='swipe-map' style={{position: 'absolute', top: 0, bottom: 0, width: '100%', zIndex: 1, display: 'none', backgroundColor: '#fff'}} />
              {drawingMessage &&
                <div>
                  <div style={{
                    position: 'absolute',
                    top: '10px',
                    right: '100px',
                    backgroundColor: 'rgba(0,0,0,0.6)',
                    color: '#FFF',
                    height: '30px',
                    paddingLeft: '5px',
                    paddingRight: '5px',
                    borderRadius: '4px',
                    zIndex: '100',
                    lineHeight: '30px'
                  }}
                  >
                    <span>{drawingMessage}</span>
                  </div>
                </div>}
            </Map>

          </Provider>
        </div>
      )
    } else {
      return 'Missing Map Layers'
    }
  }
}
export default subscribe(AOIMap, {aoiDashboard: AOIDashboardContainer, aoiDrawings: AOIDrawingContainer})
