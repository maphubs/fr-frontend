// @flow
import * as React from 'react'
import ReactDOM from 'react-dom'
import { Icon, Tooltip } from 'antd'

export default class extendDrawBar {
  constructor (opt: Object) {
    const ctrl = this
    ctrl.draw = opt.draw
    ctrl.buttons = opt.buttons || []
    ctrl.onAddOrig = opt.draw.onAdd
    ctrl.onRemoveOrig = opt.draw.onRemove
  }

  draw: any
  buttons: any
  onAddOrig: any
  onRemoveOrig: any
  elContainer: any
  map: any

  onAdd (map: Object) {
    const ctrl = this
    ctrl.map = map
    ctrl.elContainer = ctrl.onAddOrig(map)
    ctrl.buttons.forEach((b) => {
      ctrl.addButton(b)
    })
    return ctrl.elContainer
  }

  onRemove (map: Object) {
    const ctrl = this
    ctrl.buttons.forEach((b) => {
      ctrl.removeButton(b)
    })
    try {
      ctrl.onRemoveOrig(map)
    } catch (err) {
      // do nothing
    }
  }

  addButton (opt: Object) {
    const ctrl = this
    var elButton = document.createElement('button')
    elButton.className = 'mapbox-gl-draw_ctrl-draw-btn'
    elButton.addEventListener(opt.on, opt.action)

    ReactDOM.render(
      (
        <Tooltip placement='left' title={opt.tooltip} ref={(el => { opt.tooltipRef = el })}>
          <Icon type={opt.icon} />
        </Tooltip>
      ),
      elButton
    )
    ctrl.elContainer.append(elButton)
    opt.elButton = elButton
  }

  removeButton (opt: Object) {
    opt.elButton.removeEventListener(opt.on, opt.action)
    const tooltipPopup = opt.tooltipRef.getPopupDomNode()
    if (tooltipPopup) tooltipPopup.remove()
    opt.elButton.remove()
  }
}
