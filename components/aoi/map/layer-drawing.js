// @flow

export default (id: string, name: Object, features: Array<Object>) => {
  const defaultColor = 'red'
  const layers = [
    {
      'id': `fr-drawing-${id}`,
      'type': 'line',
      'metadata': {
      },
      'source': `fr-drawing-${id}`,
      'paint': {
        'line-color': defaultColor,
        'line-opacity': 0.8,
        'line-width': 3
      }
    }
  ]

  const style = {
    version: 8,
    sources: {},
    layers,
    metadata: {
      'maphubs:active': true,
      'maphubs:priority': 1
    }
  }

  style.sources[`fr-drawing-${id}`] = {
    type: 'geojson',
    data: {
      type: 'FeatureCollection',
      features
    }
  }

  return {
    layer_id: id,
    name,
    source: {en: 'Forest Report'},
    style,
    legend_html: `
      <div class="omh-legend">
      <div class="block" style="border: 1px solid ${defaultColor};">
      </div>
      <h3>{NAME}</h3>
      </div>`
  }
}
