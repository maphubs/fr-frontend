// @flow
import * as React from 'react'
import { Row, Col, Button, Tooltip } from 'antd'
import { subscribe } from '../../lib/unstated-props'
import AOIDrawingContainer from './AOIDrawingContainer'

type Props = {
  id: string,
  bbox: Array<number>,
  beforeImage?: Object,
  afterImage?: Object,
  showS2Selection: Function,
  t: Function,
  containers: any
}

class AOIDrawingRowContent extends React.Component<Props, void> {
  render () {
    const { id, bbox, beforeImage, afterImage, showS2Selection, t } = this.props
    const { aoiDrawings } = this.props.containers
    console.log('drawing row content render')
    console.log(this.props)
    return (
      <Row>
        <Col span={12}>
          <span style={{position: 'absolute', top: '-10px', fontSize: '12px', fontWeight: 'bold'}}>{t('Before Image')}</span>
          {beforeImage &&
            <Row style={{marginTop: '10px', height: '40px'}}>
              <Col span={18}>
                <p>{t(beforeImage.name)}</p>
              </Col>
              <Col span={6}>
                <Button
                  icon='delete' shape='circle' size='small' onClick={() => {
                    aoiDrawings.removeImage(id, 'before')
                  }}
                />
              </Col>
            </Row>}
          {!beforeImage &&
            <Row style={{marginTop: '10px', height: '40px'}}>
              <Col span={6}>
                <Tooltip placement='top' title={t('Upload Imagery')}>
                  <Button
                    icon='upload' shape='circle' size='small' onClick={() => {
                      aoiDrawings.startImageUpload(id, 'before')
                    }}
                  />
                </Tooltip>
              </Col>
              <Col span={6}>
                <Tooltip placement='top' title='Sentinel-2'>
                  <Button
                    icon='cloud' shape='circle' size='small' onClick={() => {
                      showS2Selection(id, bbox, 'before')
                    }}
                  />
                </Tooltip>
              </Col>
            </Row>}
        </Col>
        <Col span={12}>
          <span style={{position: 'absolute', top: '-10px', fontSize: '12px', fontWeight: 'bold'}}>{t('After Image')}</span>
          {afterImage &&
            <Row style={{marginTop: '10px', height: '40px'}}>
              <Col span={18}>
                <p>{t(afterImage.name)}</p>
              </Col>
              <Col span={6}>
                <Button
                  icon='delete' shape='circle' size='small' onClick={() => {
                    aoiDrawings.removeImage(id, 'after')
                  }}
                />
              </Col>
            </Row>}
          {!afterImage &&
            <Row style={{marginTop: '10px', height: '40px'}}>
              <Col span={6}>
                <Tooltip placement='top' title={t('Upload Imagery')}>
                  <Button
                    icon='upload' shape='circle' size='small' onClick={() => {
                      aoiDrawings.startImageUpload(id, 'after')
                    }}
                  />
                </Tooltip>
              </Col>
              <Col span={6}>
                <Tooltip placement='top' title='Sentinel-2'>
                  <Button
                    icon='cloud' shape='circle' size='small' onClick={() => {
                      showS2Selection(id, bbox, 'after')
                    }}
                  />
                </Tooltip>
              </Col>
            </Row>}
        </Col>
      </Row>
    )
  }
}

export default subscribe(AOIDrawingRowContent, {
  aoiDrawings: AOIDrawingContainer
})
