// @flow
import * as React from 'react'
import { Drawer, Row, Button, notification, Col, Steps } from 'antd'
import superagent from 'superagent'
import FileUpload from '../form/FileUpload'
import getConfig from 'next/config'
import Spinner from '../Spinner'
import LocalizedInput from '../form/LocalizedInput'

const MAPHUBS_CONFIG = getConfig().publicRuntimeConfig
const Step = Steps.Step

type Props = {
  t: Function,
  onClose: Function,
  onComplete: Function
}

type State = {
  processing: boolean,
  name?: Object,
  nameSaved: boolean,
  source?: Object,
  sourceSaved: boolean
}

export default class UploadDrawer extends React.Component<Props, State> {
  state = {
    processing: false,
    sourceSaved: false,
    nameSaved: false,
    name: undefined,
    source: undefined
  }

  onUploadError = (error: any) => {
    let messageText = 'Upload Error'
    if (error && error.message) {
      messageText = error.message
    }
    notification.error({
      message: 'Error Uploading Imagery',
      description: messageText,
      duration: 0
    })
  }

  onProcessingStart = () => {
    this.setState({processing: true})
  }

  onUpload = (file: Object) => {
    const { onUploadError } = this
    const { onComplete } = this.props
    const { name, source } = this.state
    this.onProcessingStart()
    superagent.post(`${MAPHUBS_CONFIG.RASTER_UPLOAD_API}/upload/complete`)
      .set({'Content-Type': 'application/json', Authorization: 'Bearer ' + MAPHUBS_CONFIG.RASTER_UPLOAD_API_KEY})
      .accept('json')
      .send({
        uploadUrl: file.uploadURL,
        originalName: file.data.name
      })
      .end((err, res) => {
        if (err) {
          onUploadError(err)
        } else {
          const result = res.body // TileJSON response
          const id = result.name // this is the uuid from the service
          const minzoom = parseInt(result.minzoom, 10)
          const maxzoom = parseInt(result.maxzoom, 10)
          const layer = {
            layer_id: id,
            visible: true,
            name,
            source,
            style: {
              version: 8,
              sources: {},
              layers: [
                {
                  id: `fr-imagery-${id}`,
                  type: 'raster',
                  source: `fr-imagery-${id}`,
                  minzoom: 0,
                  maxzoom: 18,
                  paint: {
                    'raster-opacity': 1
                  },
                  layout: {
                    visibility: 'visible'
                  }
                }
              ],
              metadata: {
                'maphubs:active': true,
                'maphubs:priority': 6
              }
            },
            legend_html: `
            <div class="omh-legend">
            <h3>{NAME}</h3>
            </div>`,
            is_external: true,
            external_layer_type: 'Raster Tile Service',
            external_layer_config: {
              type: 'raster',
              minzoom: parseInt(result.minzoom, 10),
              maxzoom: parseInt(result.maxzoom, 10),
              bounds: result.bounds,
              tiles: result.tiles,
              scheme: result.scheme
            }
          }
          layer.style.sources[`fr-imagery-${id}`] = {
            type: 'raster',
            tiles: result.tiles,
            scheme: result.scheme,
            minzoom,
            maxzoom,
            bounds: result.bounds,
            tileSize: 256
          }
          onComplete(layer)
        }
      })
  }

  nameChange = (name: Object) => {
    this.setState({ name })
  }

  saveName = () => {
    this.setState({ nameSaved: true })
  }

  sourceChange = (source: Object) => {
    this.setState({ source })
  }

  saveSource = () => {
    this.setState({ sourceSaved: true })
  }

  render () {
    const { nameChange, saveName, sourceChange, saveSource, onUpload, onUploadError } = this
    const { t, onClose } = this.props
    const { nameSaved, sourceSaved, processing } = this.state
    let step = 0
    if (processing) {
      step = 3
    } else if (sourceSaved) {
      step = 2
    } else if (nameSaved) {
      step = 1
    }
    return (
      <Drawer
        title={t('Upload Imagery')}
        placement='bottom'
        height='400px'
        closable
        destroyOnClose
        bodyStyle={{ height: 'calc(100% - 55px)', padding: 0 }}
        onClose={onClose}
        visible
      >
        <Row>
          <Col span={8} style={{padding: '20px'}}>
            <Steps direction='vertical' current={step}>
              <Step title={t('Name')} description={t('Provide a Name')} />
              <Step title={t('Source')} description={t('Provide a Source')} />
              <Step title={t('Upload')} description={t('Upload an Imagery File')} />
              <Step title={t('Processing')} description={t('Map Layer Creation')} />
            </Steps>
          </Col>
          <Col span={16}>
            {!nameSaved &&
              <div style={{padding: '20px'}}>
                <Row>
                  <p>{t('Provide a name for this imagery layer')}</p>
                </Row>
                <Row>
                  <LocalizedInput placeholder={t('Name')} onChange={nameChange} />
                </Row>
                <Row style={{marginTop: '20px', textAlign: 'right'}}>
                  <Button type='primary' onClick={saveName}>{t('Save Name')}</Button>
                </Row>
              </div>}
            {(nameSaved && !sourceSaved) &&
              <div style={{padding: '20px'}}>
                <Row>
                  <p>{t('Provide a source for this imagery layer')}</p>
                </Row>
                <Row>
                  <LocalizedInput placeholder={t('Source')} onChange={sourceChange} />
                </Row>
                <Row style={{marginTop: '20px', textAlign: 'right'}}>
                  <Button type='primary' onClick={saveSource}>{t('Save Source')}</Button>
                </Row>
              </div>}
            {processing &&
              <div style={{ height: '300px' }}>
                <Spinner />
              </div>}
            {(nameSaved && sourceSaved && !processing) &&
              <FileUpload
                endpoint={`${MAPHUBS_CONFIG.RASTER_UPLOAD_API}/upload/save`}
                headers={{authorization: `Bearer ${MAPHUBS_CONFIG.RASTER_UPLOAD_API_KEY}`}}
                note={t('Supports: GeoTiffs and MBTiles, GeoTiffs must have RGB visual bands')}
                maxFileSize={52428800}
                allowedFileTypes={['.tif', '.tiff', '.mbtiles']}
                onComplete={onUpload}
                onError={onUploadError}
                t={t}
              />}
          </Col>
        </Row>
      </Drawer>
    )
  }
}
