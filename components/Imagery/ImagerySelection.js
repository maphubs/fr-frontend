// @flow
import * as React from 'react'
import { Button, Row } from 'antd'
import { Provider, Subscribe } from 'unstated'
import ImagerySelectionContainer from './ImagerySelectionContainer'
import SelectionDrawer from './SelectionDrawer'
import ImageryResultCard from './ImageryResultCard'

type Props = {
  bbox?: Array<number>,
  onSelect: Function,
  show?: boolean,
  t: Function
}

/**
  Display the selected image or buttons to select
  Open a drawer with search/selection UI
 */
export default class ImagerySelection extends React.Component<Props, void> {
  ImagerySelectionContainer: any
  constructor (props: Props) {
    super(props)
    this.ImagerySelectionContainer = new ImagerySelectionContainer(props)
  }

  componentDidUpdate (prevProps: Props) {
    if (!prevProps.show && this.props.show) {
      this.ImagerySelectionContainer.showSelection()
    }
  }

  render () {
    const { t, onSelect } = this.props
    return (
      <Provider inject={[this.ImagerySelectionContainer]}>
        <Subscribe to={[ImagerySelectionContainer]}>
          {imagery => {
            return (
              <SelectionDrawer t={t} onSelect={onSelect} />
            )
          }}
        </Subscribe>
      </Provider>
    )
  }
}
