// @flow
import * as React from 'react'
import { Col, DatePicker, Row } from 'antd'

type Props = {
  startChange: Function,
  endChange: Function,
  defaultStartDate?: Object,
  defaultEndDate?: Object,
  t: Function
}

type State = {
  startValue?: string,
  endValue?: string,
  endOpen: boolean
}

export default class DateRange extends React.Component<Props, State> {
  state = {
    startValue: undefined,
    endValue: undefined,
    endOpen: false
  };

  disabledStartDate = (startValue: string) => {
    const endValue = this.state.endValue
    if (!startValue || !endValue) {
      return false
    }
    return startValue.valueOf() > endValue.valueOf()
  }

  disabledEndDate = (endValue: string) => {
    const startValue = this.state.startValue
    if (!endValue || !startValue) {
      return false
    }
    return endValue.valueOf() <= startValue.valueOf()
  }

  onChange = (field: string, value: string) => {
    this.setState({
      [field]: value
    })
  }

  onStartChange = (value: Object) => {
    this.onChange('startValue', value)
    const startDateStr = value.format('YYYY-MM-DD')
    this.props.startChange(startDateStr)
  }

  onEndChange = (value: Object) => {
    this.onChange('endValue', value)
    const endDateStr = value.format('YYYY-MM-DD')
    this.props.endChange(endDateStr)
  }

  handleStartOpenChange = (open: boolean) => {
    if (!open) {
      this.setState({ endOpen: true })
    }
  }

  handleEndOpenChange = (open: boolean) => {
    this.setState({ endOpen: open })
  }

  render () {
    const { startValue, endValue, endOpen } = this.state
    const { defaultStartDate, defaultEndDate, t } = this.props
    return (
      <Row>
        <Col span={12}>
          <DatePicker
            disabledDate={this.disabledStartDate}
            format='YYYY-MM-DD'
            value={startValue || defaultStartDate}
            placeholder={t('Start')}
            onChange={this.onStartChange}
            onOpenChange={this.handleStartOpenChange}
          />
        </Col>
        <Col span={12}>
          <DatePicker
            disabledDate={this.disabledEndDate}
            format='YYYY-MM-DD'
            value={endValue || defaultEndDate}
            placeholder={t('End')}
            onChange={this.onEndChange}
            open={endOpen}
            onOpenChange={this.handleEndOpenChange}
          />
        </Col>
      </Row>
    )
  }
}
