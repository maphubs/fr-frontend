// @flow
import * as React from 'react'
import { Slider, InputNumber, Row, Col, Icon } from 'antd'

type Props = {
  onChange: Function
}

type State = {
  inputValue: number
}

export default class CloudThresholdSlider extends React.Component<Props, State> {
  state = {
    inputValue: 30
  }

  onChange = (value: number) => {
    this.setState({
      inputValue: value
    })
    this.props.onChange(value)
  }

  render () {
    const { inputValue } = this.state
    return (
      <Row>
        <Col span={2}>
          <Icon type='cloud' style={{fontSize: '20px', marginTop: '8px'}} />
        </Col>
        <Col span={16}>
          <Slider
            min={1}
            max={100}
            onChange={this.onChange}
            value={typeof inputValue === 'number' ? inputValue : 0}
          />
        </Col>
        <Col span={4}>
          <InputNumber
            min={1}
            max={100}
            style={{ marginLeft: 16 }}
            value={inputValue}
            onChange={this.onChange}
          />
        </Col>
      </Row>
    )
  }
}
