// @flow
import * as React from 'react'
import { Card, Col, Icon, Progress, Row } from 'antd'
import moment from 'moment'

type Props = {
  result: Object,
  onSelect?: Function
}
export default class ImageryResultCard extends React.Component<Props, void> {
  render () {
    const { result, onSelect } = this.props
    return (
      <div onClick={() => {
        if (onSelect) onSelect(result)
      }}>
        <Card style={{height: '100px', width: '300px'}} bodyStyle={{height: '100px', padding: 0}} bordered>
          <Row>
            <Col span={8}>
              <img style={{height: '99px', width: '99px', border: '1px solid #ddd'}} src={result.assets.thumbnail.href} />
            </Col>
            <Col span={16}>
              <div style={{padding: '10px', position: 'relative'}}>
                <p style={{fontSize: '12px', fontWeight: '700'}}>{moment(result.properties.datetime).format('YYYY-MM-DD')}</p>
                <p style={{fontSize: '10px'}}>{result.properties.collection}</p>
                <div style={{position: 'absolute', right: '5px', bottom: '15px', height: '20px', lineHeight: '20px'}}>
                  <Icon type='cloud' />
                  <Progress type='circle' percent={Math.round(result.properties['eo:cloud_cover'])} width={20} strokeColor={'rgba(0,0,0,0.6)'} strokeWidth={4} />
                </div>
              </div>
            </Col>
          </Row>
        </Card>
      </div>
    )
  }
}
