// @flow
import * as React from 'react'
import { Drawer, Row, Button } from 'antd'
import { subscribe } from '../../lib/unstated-props'
import ImagerySelectionContainer from './ImagerySelectionContainer'
import DateRange from './DateRange'
import CloudThresholdSlider from './CloudThresholdSlider'
import ResultCarousel from './ResultCarousel'
import moment from 'moment'

type Props = {
  t: Function,
  onSelect: Function,
  containers: {imagery: any}
}

class SelectionDrawer extends React.Component<Props, void> {
  componentWillMount () {
    this.props.containers.imagery.init(this.props)
  }

  onSelect = (selectedResult) => {
    this.props.containers.imagery.selectResult(selectedResult)
    this.props.onSelect(selectedResult)
  }

  render () {
    const { t, containers, onSelect } = this.props
    const { imagery } = containers
    const { showSelection, startDate, endDate, searchResults } = imagery.state
    let canSearch
    if (startDate && endDate) {
      canSearch = true
    }
    const results = searchResults ? searchResults.features : []
    return (
      <Drawer
        title={t('Select Imagery')}
        placement='right'
        width='400px'
        closable
        bodyStyle={{ height: 'calc(100% - 55px)', padding: 0 }}
        onClose={imagery.hideSelection}
        visible={showSelection}
      >
        <Row style={{height: '175px', padding: '24px'}}>
          <Row style={{marginBottom: '20px'}}>
            <DateRange startChange={imagery.setStartDate} endChange={imagery.setEndDate} defaultStartDate={moment().subtract(2, 'months')} defaultEndDate={moment()} t={t} />
          </Row>
          <Row style={{marginBottom: '20px'}}>
            <CloudThresholdSlider onChange={imagery.setCloudThreshold} />
          </Row>
          <Row style={{textAlign: 'center', marginBottom: '20px'}}>
            <Button type='primary' disabled={!canSearch} onClick={imagery.search}>Search</Button>
          </Row>
        </Row>
        <Row style={{height: 'calc(100% - 175px)'}}>
          <ResultCarousel results={results} onSelect={onSelect} />
        </Row>
      </Drawer>
    )
  }
}
export default subscribe(SelectionDrawer, {imagery: ImagerySelectionContainer})
