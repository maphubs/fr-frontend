// @flow
import * as React from 'react'
import AutoSizer from 'react-virtualized-auto-sizer'
import { Empty, Card } from 'antd'
import { FixedSizeList as List } from 'react-window'
import { areEqual } from 'react-window'
import ImageryResultCard from './ImageryResultCard'

const AntdCard = Card

type Props = {
  results: Array<Object>,
  onSelect: Function
}

export default class ResultCarousel extends React.Component<Props, void> {
  render () {
    const { results, onSelect } = this.props
    if (!results || results.length === 0) return <Empty description='No Results' />
    const Card = React.memo(
      props => {
        const { index, style } = props
        const result = results[index]
        if (result) {
          return (
            <div style={style}>
              <div style={{position: 'relative'}}>
                <div style={{position: 'absolute', right: '50px'}}>
                  <ImageryResultCard result={results[index]} onSelect={onSelect} />
                </div>
              </div>
            </div>
          )
        } else {
          return (
            <div style={style}>
              <AntdCard
                hoverable
                style={{ width: 200, height: 100 }}
                cover={'Error'}
              />
            </div>
          )
        }
      },
      areEqual
    )

    return (
      <div style={{ height: '100%' }} >
        <AutoSizer disableWidth>
          {({ height }) => (
            <>
              {((120 * results.length) > height) &&
                <style jsx global>{`

                  div.listscarousel::-webkit-scrollbar {
                      width:9px;
                      height: 10px;
                  }

                  div.listscarousel::-webkit-scrollbar-track {
                      -webkit-border-radius:5px;
                      border-radius:5px;
                      background:rgba(0,0,0,0.1);
                  }

                  div.listscarousel::-webkit-scrollbar-thumb {
                      -webkit-border-radius:5px;
                      border-radius:5px;
                      background:rgba(0,0,0,0.2);
                  }

                  div.listscarousel::-webkit-scrollbar-thumb:hover {
                      background:rgba(0,0,0,0.4);
                  }

                  div.listscarousel::-webkit-scrollbar-thumb:window-inactive {
                      background:rgba(0,0,0,0.05);
                  }
                `}</style>
              }
              <List
                className='listscarousel'
                layout='vertical'
                width={400}
                itemCount={results.length}
                itemSize={120}
                height={height}
              >
                {Card}
              </List>
            </>
          )}
        </AutoSizer>
      </div>
    )
  }
}
