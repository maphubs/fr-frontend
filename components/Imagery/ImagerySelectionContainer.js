// @flow
import { Container } from 'unstated'
import { message, notification } from 'antd'
import satAPI from '../../utils/sat-api'
import moment from 'moment'

type ImageryState = {
  bbox?: Array<number>,
  showSelection?: boolean,
  searchResults?: Object, // geojson response from the API
  selectedResult?: Object,
  cloudThreshold: number,
  startDate?: string,
  endDate?: string

}

type Props = {
  bbox: Array<number>
}

export default class ImagerySelectionContainer extends Container<ImageryState> {
  constructor (props: Props) {
    super()
    // this.init(props)
  }

  init (props: Props) {
    this.state = {
      bbox: props.bbox,
      cloudThreshold: 30,
      startDate: moment().subtract(2, 'months').format('YYYY-MM-DD'),
      endDate: moment().format('YYYY-MM-DD')
    }
  }

  setStartDate = (startDate: string) => {
    this.setState({startDate})
  }

  setEndDate = (endDate: string) => {
    this.setState({endDate})
  }

  setCloudThreshold = (cloudThreshold: number) => {
    this.setState({cloudThreshold})
  }

  showSelection = (bbox: Array<number>) => {
    this.setState({showSelection: true, bbox})
  }

  hideSelection = () => {
    this.setState({showSelection: false, searchResults: null})
  }

  search = async () => {
    const { bbox, startDate, endDate, cloudThreshold } = this.state
    try {
      const searchResults = await satAPI.search(bbox, startDate, endDate, {cloudThreshold, collection: 'sentinel-2-l1c'})
      if (!searchResults.features || searchResults.features.length === 0) {
        message.error('No Results Found')
      }
      this.setState({searchResults})
    } catch (err) {
      notification.error({
        message: 'Error',
        description: (err && err.message) ? err.message : 'Server Error',
        duration: 0
      })
    }
  }

  selectResult = (selectedResult: Object) => {
    this.setState({selectedResult, showSelection: false})
  }
}
