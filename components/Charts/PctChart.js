//  @flow
import * as React from 'react'
import numeral from 'numeral'
import { Statistic, Progress, Row, Col } from 'antd'
type Props = {
  percent: number,
  hectares: number,
  color?: string,
  type?: string
}

export default function PctChart ({
  percent, hectares, color = 'green', type = 'small'
}: Props) {
  if (percent) {
    let statSpan = 24
    let chartSpan = 24
    let progressWidth = 100
    const progressStrokeWidth = 12
    let fontSize = '28px'
    let progressType = 'dashboard'
    let chartPadding = '20px'
    if (type === 'small') {
      statSpan = 16
      chartSpan = 8
      progressWidth = 40
      progressType = 'circle'
      fontSize = '24px'
      chartPadding = undefined
    }
    return (
      <Row>
        <Col span={statSpan}>
          <Statistic
            value={hectares}
            precision={2}
            formatter={(val) => numeral(val).format('0,0.00')}
            valueStyle={{ color: '#323333', fontSize }}
            suffix='ha'
          />
        </Col>
        <Col span={chartSpan} style={{padding: chartPadding}}>
          <Progress style={{color: '#323333'}} type={progressType} percent={Math.round(percent)} width={progressWidth} strokeColor='#21C6D8' strokeWidth={progressStrokeWidth} />
        </Col>
      </Row>
    )
  } // else
  return (
    <div>
      <p style={{fontSize: '24px', margin: 0}}>
          No Data
      </p>
    </div>
  )
}
