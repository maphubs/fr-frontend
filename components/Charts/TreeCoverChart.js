//  @flow
import * as React from 'react'
import ChartistGraph from 'react-chartist'

type Props = {
  stats: Object,
  height: string,
  t: Function
}

export default class TreeCoverChart extends React.Component<Props> {
  componentDidMount () {
    if (window && this.chart && this.chart.chartist) {
      const { stats } = this.props
      const maxRadius = 25
      const minRadius = 2
      let min = 0
      let max = 0
      Object.keys(stats).forEach((key) => {
        const val = stats[key]
        if (val < min) {
          min = val
        } else if (val > max) {
          max = val
        }
      })

      const ratio = (maxRadius - minRadius) / (max - min)

      // eslint-disable-next-line
      const Chartist = require('chartist')
      this.chart.chartist.on('draw', (data) => {
        if (data.type === 'bar') {
          const value = Math.abs(Chartist.getMultiValue(data.value))
          if (value > 0) {
            const radius = value * ratio // 0.001
            data.group.append(new Chartist.Svg('circle', {
              cx: data.x2,
              cy: data.y2 + radius,
              r: radius
            }, 'cover-crown'))
          }
        }
      })
    }
  }

  render () {
    const { stats, t, height } = this.props

    let foundValue
    const series = Object.keys(stats).map((key) => {
      const val = stats[key]
      if (val) foundValue = val
      return val
    })

    if (!foundValue) {
      return (
        <div style={{ height: '100px' }}>
          <p style={{ fontSize: 28 }}>
            {t('No Data')}
          </p>
        </div>
      )
    }

    const lineChartData = {
      labels: ['2001', '2002', '2003', '2004', '2005', '2006', '2007', '2008', '2009', '2010', '2011', '2012', '2013', '2014', '2015', '2016', '2017', '2018'],
      series: [
        {
          name: 'cover',
          className: 'cover-bar',
          data: series
        }
      ]
    }

    const chartOptions = {
      low: 0,
      showArea: true,
      showPoint: true,
      height: '100%',
      axisX: {
        labelInterpolationFnc (value, index) {
          return index % 2 === 1 ? value : null
        }
      },
      axisY: {

      },
      chartPadding: {
        top: 10,
        right: 10,
        bottom: 10,
        left: 10
      }
    }

    return (
      <div style={{ height, marginTop: '20px' }}>
        <ChartistGraph ref={(ref) => { this.chart = ref }} data={lineChartData} options={chartOptions} type={'Bar'} />
        <style jsx global>{`
            .cover-bar .ct-bar {
              stroke: #8F6241;
              stroke-width: 5px;
            }
            .ct-chart {
              height: 100%;
            }
            .cover-bar .cover-crown {
              stroke-width: 1px;
              stroke: #ddd;
              fill: #6AC164;
            }
          `}
        </style>
      </div>
    )
  }
}
