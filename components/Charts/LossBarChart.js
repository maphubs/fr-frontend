//  @flow
import * as React from 'react'
import AutoSizer from 'react-virtualized-auto-sizer'
import { BarChart, CartesianGrid, XAxis, YAxis, Bar } from 'recharts'
import numeral from 'numeral'

type Props = {
  stats: Object,
  height: string,
  color: string,
  barLabelColor: string,
  t: Function
}

const years = [2001, 2002, 2003, 2004, 2005, 2006, 2007, 2008, 2009, 2010, 2011, 2012, 2013, 2014, 2015, 2016, 2017, 2018]

export default class LossBarChart extends React.Component<Props> {
  static defaultProps = {
    color: '#EF7E62',
    barLabelColor: 'black'
  }
  chart: Object

  render () {
    // console.log(this.props.stats)
    const { stats, t, height, color, barLabelColor } = this.props

    let foundValue
    const data = {}
    let series
    if (stats) {
      Object.keys(stats).forEach((key) => {
        const val = stats[key]
        if (val) foundValue = val
        const keyNum = parseInt(key, 10)
        data[keyNum + 2000] = val
      })

      series = years.map((year) => {
        return {
          name: year,
          loss: data[year] ? Math.round(data[year]) : null
        }
      })
      // console.log(series)
    }

    if (!foundValue) {
      return (
        <div style={{ height }}>
          <p style={{ fontSize: 28 }}>
            {t('No Data')}
          </p>
        </div>
      )
    }

    return (
      <AutoSizer>
        {({ height, width }) => (
          <BarChart
            width={width}
            height={height}
            data={series}
            margin={{
              top: 5, right: 30, left: 20, bottom: 5
            }}
          >
            <XAxis dataKey='name' />
            <YAxis
              label={{ value: 'hectares', angle: -90, position: 'insideLeft', offset: -10, fill: '#323333' }}
              tickFormatter={(val) => { return numeral(val).format('0,0') }}
            />
            <Bar dataKey='loss' fill={color} isAnimationActive={false}
              label={{
                position: 'top',
                angle: 0,
                offset: 5,
                fill: '#323333',
                stroke: '#FFF',
                style: {fontWeight: '400', fontSize: '12px', strokeWidth: '0px'},
                formatter: (val) => { return numeral(val).format('0,0') }
              }} />
          </BarChart>
        )
        }
      </AutoSizer>
    )
  }
}
