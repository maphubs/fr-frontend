//  @flow
import * as React from 'react'
import { Card, Col, Row } from 'antd'
import numeral from 'numeral'

type Props = {
  stats: Object,
  height: string,
  t: Function
}

const renderCard = (year, value) => (
  <Col span={4} sm={4} xs={4}>
    <Card headStyle={{ display: 'none' }} bordered bodyStyle={{ padding: '12px' }}>
      <h5>{year}</h5>
      {numeral(value).format('0,0.00')} ha
    </Card>
  </Col>
)

export default function LossTable ({ stats, t, height }: Props) {
  if (!stats) return ''

  return (
    <div style={{ height, marginTop: '20px' }}>
      <Row gutter={0}>
        {renderCard('2001', stats[1])}
        {renderCard('2002', stats[2])}
        {renderCard('2003', stats[3])}
        {renderCard('2004', stats[4])}
        {renderCard('2005', stats[5])}
        {renderCard('2006', stats[6])}
      </Row>
      <Row gutter={0}>
        {renderCard('2007', stats[7])}
        {renderCard('2008', stats[8])}
        {renderCard('2009', stats[9])}
        {renderCard('2010', stats[10])}
        {renderCard('2011', stats[11])}
        {renderCard('2012', stats[12])}
      </Row>
      <Row gutter={0}>
        {renderCard('2013', stats[13])}
        {renderCard('2014', stats[14])}
        {renderCard('2015', stats[15])}
        {renderCard('2016', stats[16])}
        {renderCard('2017', stats[17])}
        {renderCard('2018', stats[18])}
      </Row>
    </div>
  )
}
