//  @flow
import * as React from 'react'
import CalendarHeatmap from 'react-calendar-heatmap'
import { Button } from 'antd'
import numeral from 'numeral'

type Props = {
  t: Function,
  startDate: string,
  endDate: string,
  stats?: Object,
  onClick?: Function,
  onClearSelected?: Function
}

type State = {
  selectedValue?: Object
}

class FiresChart extends React.Component<Props, State> {
  constructor (props: Props) {
    super(props)
    this.state = {}
  }

  componentWillReceiveProps (nextProps: Props) {
    // if dates have changed clear the selected value
    if (nextProps.startDate !== this.props.startDate) {
      this.setState({ selectedValue: undefined })
    }
  }

  clearSelected = () => {
    this.setState({ selectedValue: undefined })
    if (this.props.onClearSelected) this.props.onClearSelected()
  }

  render () {
    const {
      stats, onClick, startDate, endDate, t, report
    } = this.props
    const { selectedValue } = this.state

    if (!stats) {
      return ''
    }

    let selected
    if (selectedValue) {
      selected = (
        <div>
          <p style={{ fontSize: '16px', float: 'left', color: '#323333' }}>{`${numeral(selectedValue.count).format('0')} ${t('fires on')} ${selectedValue.date}`}</p>
          <Button
            style={{
              border: 'none'
            }}
            onClick={this.clearSelected}
            shape='circle'
            icon='close-circle-o'
            type='primary'
            ghost
          />
        </div>
      )
    }
    // numDays={365}
    const months = [t('Jan'), t('Feb'), t('Mar'), t('Apr'), t('May'), t('Jun'), t('Jul'), t('Aug'), t('Sep'), t('Oct'), t('Nov'), t('Dec')]
    return (
      <div>
        <CalendarHeatmap
          values={stats && stats.values}
          startDate={new Date(startDate)}
          endDate={new Date(endDate)}
          monthLabels={months}
          showWeekdayLabels={false}
          onClick={(value) => {
            this.setState({ selectedValue: value })
            if (onClick) {
              onClick(value)
            }
          }}
          classForValue={(value) => {
            if (!value) {
              return 'color-empty'
            }
            if (stats && stats.values && stats.values.length === 1) {
              return 'color-scale-fires-5'
            }

            // const classSize = Math.floor((stats.max / 4))
            const classSize = (stats.max / 4)
            const classID = Math.ceil((value.count / classSize))
            return `color-scale-fires-${classID}`
          }}
        />
        {selected}
        <style jsx global>{`

        .react-calendar-heatmap text {
          font-size: 10px;
          fill: #aaa;
        }

        .react-calendar-heatmap .react-calendar-heatmap-small-text {
          font-size: 5px;
        }

        .react-calendar-heatmap rect:hover {
          stroke: #555;
          stroke-width: 1px;
        }

        .react-calendar-heatmap .color-empty {
          fill: #eeeeee;
        }

        .react-calendar-heatmap .color-filled {
          fill: #F96034;
        }

        .react-calendar-heatmap .color-scale-fires-0 {
          fill: black;
        }
        .react-calendar-heatmap .color-scale-fires-1 {
          fill: #fee5d9;
        }
        .react-calendar-heatmap .color-scale-fires-2 {
          fill: #fcae91;
        }
        .react-calendar-heatmap .color-scale-fires-3 {
          fill: #fb6a4a;
        }
        .react-calendar-heatmap .color-scale-fires-4 {
          fill: #F96034;
        }

        .react-calendar-heatmap .color-scale-fires-5 {
          fill: #F96034;
        }
          `}
        </style>
      </div>
    )
  }
}

export default FiresChart
