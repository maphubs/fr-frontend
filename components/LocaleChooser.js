// @flow
import React from 'react'
import localeUtil from '../locales/util'
import { Menu, Dropdown, Icon } from 'antd'
import { subscribe } from '../lib/unstated-props'
import UserContainer from '../containers/UserContainer'
import getConfig from 'next/config'
const MAPHUBS_CONFIG = getConfig().publicRuntimeConfig

const supportedLangs = localeUtil.getSupported()
let languagesFromConfig
const langs = []
if (MAPHUBS_CONFIG.LANGUAGES) {
  languagesFromConfig = MAPHUBS_CONFIG.LANGUAGES.split(',')
  languagesFromConfig = languagesFromConfig.map(lang => lang.trim())
  supportedLangs.map(lang => {
    if (languagesFromConfig.includes(lang.value)) {
      langs.push(lang)
    }
  })
}

type Props = {
  containers: any
};

class LocaleChooser extends React.Component<Props, void> {
  /*
  shouldComponentUpdate (nextProps: Props) {
    if (this.props.containers.user.state.locale !== nextProps.containers.user.state.locale) {
      return true
    }
    return false
  }
  */

  onChange = (e: Object) => {
    const locale = e.target.id
    console.log(`LOCALE CHANGE: ${locale}`)
    this.props.containers.user.changeLocale(locale)
  }

  render () {
    const locale = this.props.containers.user.state.locale
    const label = localeUtil.getConfig(locale).label
    const menu = (
      <Menu>
        {langs.map(l => {
          return (
            <Menu.Item key={`locale-${l.value}`}>
              <a href='#!' id={l.value} onClick={this.onChange}>{`${l.name} (${l.label})`}</a>
            </Menu.Item>
          )
        })}
      </Menu>
    )
    return (
      <Dropdown overlay={menu} trigger={['click']}>
        <a href='#'>
          {label} <Icon type='down' />
        </a>
      </Dropdown>
    )
  }
}
export default subscribe(LocaleChooser, {user: UserContainer})
