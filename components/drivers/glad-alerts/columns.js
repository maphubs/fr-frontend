// @flow
import * as React from 'react'
import {
  BarChart, Bar, Tooltip, XAxis
} from 'recharts'
import numeral from 'numeral'

type ModuleConfig = {
  columns: Array<Object>,
  year: string
}

type Module = {
  id: string,
  name: Object,
  config: ModuleConfig
}

const getColVal = (record, dataIndex) => {
  let val = Object.assign({}, record)
  if (!val) return undefined
  const dataIndexParts = dataIndex.split('.')
  if (dataIndexParts.length > 1) {
    dataIndexParts.forEach((part) => {
      if (val) val = val[part]
    })
  } else {
    val = val[dataIndex]
  }

  return val
}

const calcTotal = (mod, record) => {
  let total = 0
  if (!record[mod.id]) return
  const months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
    .map(y => `${y}-${mod.config.year}`)
  months.map(month => {
    const val = record[mod.id].months[month]
    if (val) total += val
  })
  return total
}

const getDefaultColConfig = (modConfig: ModuleConfig) => {
  const year = modConfig.year
  return [
    {
      id: `glad-alerts-${year}-all`,
      enabled: true,
      width: 150,
      dataIndex: 'total',
      unit: 'ha',
      format: '0,0.00',
      name: {
        en: `Forest Alerts ${year}`,
        es: `Alertas forestales ${year}`
      }
    },
    {
      id: `glad-alerts-${year}-all-count`,
      enabled: true,
      width: 150,
      dataIndex: 'total',
      unit: 'ha',
      format: '0,0.00',
      name: {
        en: `Forest Alerts Total ${year}`,
        es: `Total de alertas forestales ${year}`
      }
    },
    {
      id: `glad-alerts-${year}-last-month`,
      enabled: true,
      width: 150,
      dataIndex: 'last_month',
      unit: 'ha',
      format: '0,0.00',
      name: {
        en: 'Forest Alerts - Last Month',
        es: 'Alertas forestales - Último mes'
      }
    }
  ]
}

export default (mod: Module, t: Function): Array<Object> => {
  let colConfigs
  if (!mod.config.columns) {
    colConfigs = getDefaultColConfig(mod.config)
  } else {
    colConfigs = mod.config.columns
  }
  // create antd table column from config
  return colConfigs.map(col => {
    const unit = col.unit || ''
    if (col.id.endsWith('-all')) {
      return {
        title: t(col.name),
        dataIndex: col.dataIndex,
        width: 220,
        type: 'chart',
        key: col.id,
        module_id: mod.id,
        unit,
        render: (text, record) => {
          if (record[mod.id] &&
            record[mod.id].months &&
            Object.keys(record[mod.id].months).length > 0
          ) {
            const months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
            const data = months.map(month => {
              const val = record[mod.id].months[`${month}-${mod.config.year}`]
              return {
                name: month,
                alerts: val
              }
            })

            return (
              <BarChart
                width={200}
                height={40}
                data={data}
              >
                <XAxis
                  dataKey='name' height={10}
                  tick={{fontSize: '6px'}}
                  tickFormatter={val => t(val).charAt(0)}
                />
                <Tooltip formatter={(val) => numeral(val * 0.0001).format('0,0')} />
                <Bar dataKey='alerts' fill='#42B532' unit='ha' />
              </BarChart>
            )
          } else {
            return (<span>NA</span>)
          }
        }
      }
    } else if (col.id.endsWith('-all-count')) {
      const width = col.width || 150
      const format = col.format || '0,0.00'
      const unit = col.unit || ''
      return {
        title: t(col.name),
        dataIndex: col.dataIndex,
        width,
        key: col.id,
        module_id: mod.id,
        unit,
        render: (text, record) => {
          if (record[mod.id] && record[mod.id].months) {
            const total = calcTotal(mod, record)
            const totalHa = total * 0.0001
            return (
              <span>{numeral(totalHa).format(format)}{unit}</span>
            )
          } else {
            return (<span>NA</span>)
          }
        }
      }
    } else if (col.id.endsWith('-last-month')) {
      const width = col.width || 150
      const format = col.format || '0,0.00'
      const unit = col.unit || ''
      const module_id = mod.id
      const dataIndex = col.dataIndex
      return {
        title: t(col.name),
        dataIndex,
        width,
        key: col.id,
        module_id,
        unit,
        render: (text, record) => {
          const val = getColVal(record, `${module_id}.${dataIndex}`)
          if (val) {
            return (
              <span>{numeral(val).format(format)}{unit}</span>
            )
          } else {
            return (<span>NA</span>)
          }
        }
      }
    } else {
      console.error(`unknown column config ${col.id}`)
    }
  })
}
