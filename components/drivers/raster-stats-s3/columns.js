// @flow
import * as React from 'react'
import numeral from 'numeral'
import getColVal from '../../../utils/get-table-col-value'

type ModuleConfig = {
  columns: Array<Object>,
  density: number
}

type Module = {
  id: string,
  name: Object,
  config: ModuleConfig
}

const getDefaultColConfig = (modConfig: ModuleConfig) => {
  return [
    {
      id: 'raster-stats-default',
      enabled: true,
      width: 150,
      dataIndex: 'stats.1',
      unit: 'ha',
      format: '0,0.00',
      name: {
        en: 'Raster Stats'
      }
    }
  ]
}

export default (mod: Module, t: Function): Array<Object> => {
  let colConfigs
  if (!mod.config.columns) {
    colConfigs = getDefaultColConfig(mod.config)
  } else {
    colConfigs = mod.config.columns
  }
  // create antd table column from config
  return colConfigs.map(col => {
    if (col) {
      const width = col.width || 150
      const format = col.format || '0,0.00'
      const unit = col.unit || ''
      return {
        title: t(col.name),
        dataIndex: col.dataIndex,
        width,
        key: col.id,
        module_id: mod.id,
        render: (text, record) => {
          const val = getColVal(record, `${mod.id}.${col.dataIndex}`)
          return (
            <span>{numeral(val).format(format)}{unit}</span>
          )
        }
      }
    }
  })
}
