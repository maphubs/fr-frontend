import GLADAlertColumns from './glad-alerts/columns'
import TreeCoverDensityColumns from './tree-cover-density/columns'
import TreeCoverLossColumns from './tree-cover-loss/columns'
import VectorTileColumns from './vector-tiles/columns'
import RasterStatsColumns from './raster-stats-s3/columns'
import FiresColumns from './fires/columns'

export default {
  'glad-alerts': {
    Columns: GLADAlertColumns
  },
  fires: {
    Columns: FiresColumns
  },
  'tree-cover-density': {
    Columns: TreeCoverDensityColumns
  },
  'tree-cover-loss': {
    Columns: TreeCoverLossColumns
  },
  'vector-tiles': {
    Columns: VectorTileColumns
  },
  'raster-stats-s3': {
    Columns: RasterStatsColumns
  }
}
