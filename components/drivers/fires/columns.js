// @flow
import * as React from 'react'
import {
  BarChart, Bar, Tooltip, XAxis
} from 'recharts'
import numeral from 'numeral'
import getColVal from '../../../utils/get-table-col-value'

type ModuleConfig = {
  columns: Array<Object>,
  year: string,
  type: string
}

type Module = {
  id: string,
  name: Object,
  config: ModuleConfig
}

const calcTotal = (mod, record) => {
  let total = 0
  if (!record[mod.id]) return
  const months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
    .map(y => `${y}-${mod.config.year}`)
  months.map(month => {
    const val = record[mod.id].months[month]
    if (val) total += val
  })
  return total
}

const getDefaultColConfig = (modConfig: ModuleConfig) => {
  const year = modConfig.year
  return [
    {
      id: `fire-alerts-${year}-last-month`,
      enabled: true,
      width: 150,
      dataIndex: 'last_30',
      unit: 'ha',
      format: '0,0.00',
      name: {
        en: 'Fires - Last Month',
        es: 'Incendios - Último mes'
      }
    },
    {
      id: `fire-alerts-${year}-chart`,
      enabled: true,
      width: 150,
      dataIndex: 'count',
      unit: 'count',
      format: '0,0',
      name: {
        en: `Fires ${year}`,
        es: `Incendios ${year}`
      }
    },
    {
      id: `fire-alerts-${year}-count`,
      enabled: true,
      width: 150,
      dataIndex: 'count',
      unit: 'count',
      format: '0,0',
      name: {
        en: `Fires ${year}`,
        es: `Incendios ${year}`
      }
    }
  ]
}

export default (mod: Module, t: Function): Array<Object> => {
  let colConfigs
  if (!mod.config.columns) {
    colConfigs = getDefaultColConfig(mod.config)
  } else {
    colConfigs = mod.config.columns
  }
  // create antd table column from config
  return colConfigs.map(col => {
    if (col.id.endsWith('-chart')) {
      return {
        title: t(col.name),
        dataIndex: col.dataIndex,
        width: 220,
        key: col.id,
        type: 'chart',
        module_id: mod.id,
        render: (text, record) => {
          if (record[mod.id] &&
            record[mod.id].months &&
            Object.keys(record[mod.id].months).length > 0
          ) {
            const months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
            const data = months.map(month => {
              const val = record[mod.id].months[`${month}-${mod.config.year}`]
              return {
                name: month,
                alerts: val
              }
            })

            return (
              <BarChart
                width={200}
                height={40}
                data={data}
              >
                <XAxis
                  dataKey='name' height={10}
                  tick={{fontSize: '6px'}}
                  tickFormatter={val => t(val).charAt(0)}
                />
                <Tooltip formatter={(val) => numeral(val).format('0,0')} />
                <Bar dataKey='alerts' fill='orange' unit='' />
              </BarChart>
            )
          } else {
            return (<span>NA</span>)
          }
        }
      }
    } else if (col.id.endsWith('-count')) {
      const width = col.width || 150
      const dataIndex = col.dataIndex
      const format = col.format || '0,0'

      return {
        title: t(col.name),
        dataIndex,
        width,
        key: col.id,
        module_id: mod.id,
        render: (text, record) => {
          if (record[mod.id] && record[mod.id].months) {
            const total = calcTotal(mod, record)
            return (
              <span>{numeral(total).format(format)}</span>
            )
          } else {
            return (<span>NA</span>)
          }
        }
      }
    } else if (col.id.endsWith('-last-month')) {
      const width = col.width || 150
      const format = col.format || '0,0'
      return {
        title: t(col.name),
        dataIndex: col.dataIndex,
        width,
        key: col.id,
        module_id: mod.id,
        render: (text, record) => {
          const val = getColVal(record, `${mod.id}.${col.dataIndex}`)
          if (val) {
            return (
              <span>{numeral(val).format(format)}</span>
            )
          } else {
            return (<span>NA</span>)
          }
        }
      }
    } else {
      console.error(`unknown column config ${col.id}`)
    }
  })
}
