// @flow
import * as React from 'react'
import numeral from 'numeral'
import {
  BarChart, Bar, Tooltip, XAxis
} from 'recharts'
import getColVal from '../../../utils/get-table-col-value'
type ModuleConfig = {
  columns: Array<Object>
}

type Module = {
  id: string,
  name: Object,
  config: ModuleConfig
}

const getDefaultColConfig = (modConfig: ModuleConfig) => {
  return [
    {
      id: 'tree-cover-loss-chart',
      enabled: true,
      width: 150,
      name: {
        en: 'Tree Cover Loss 2001-2018',
        es: 'Pérdida cubierta arbórea 2001-2018'
      }
    },
    {
      id: 'tree-cover-loss-all',
      enabled: true,
      width: 150,
      dataIndex: 'total',
      unit: 'ha',
      format: '0,0.00',
      name: {
        en: 'Tree Cover Loss 2001-2018',
        es: 'Pérdida cubierta arbórea 2001-2018'
      }
    }
  ]
}

export default (mod: Module, t: Function): Array<Object> => {
  let colConfigs
  if (!mod.config.columns) {
    colConfigs = getDefaultColConfig(mod.config)
  } else {
    colConfigs = mod.config.columns
  }
  // create antd table column from config
  return colConfigs.map(col => {
    if (col.id.endsWith('-all')) {
      const width = col.width || 100
      const format = col.format || '0,0.00'
      const unit = col.unit || ''
      return {
        title: t(col.name),
        dataIndex: col.dataIndex,
        width,
        key: col.id,
        unit,
        module_id: mod.id,
        sorter: (a, b) => {
          let aVal = getColVal(a, `${mod.id}.${col.dataIndex}`)
          let bVal = getColVal(b, `${mod.id}.${col.dataIndex}`)
          if (typeof aVal !== 'number') aVal = 0
          if (typeof bVal !== 'number') bVal = 0
          return aVal - bVal
        },
        render: (text, record) => {
          return (
            <span>{numeral(text).format(format)}{unit}</span>
          )
        }
      }
    } else if (col.id.endsWith('-chart')) {
      return {
        title: t(col.name),
        dataIndex: col.dataIndex,
        width: 220,
        type: 'chart',
        module_id: mod.id,
        sorter: (a, b) => {
          let aVal = getColVal(a, `${mod.id}.${col.dataIndex}`)
          let bVal = getColVal(b, `${mod.id}.${col.dataIndex}`)
          if (typeof aVal !== 'number') aVal = 0
          if (typeof bVal !== 'number') bVal = 0
          return aVal - bVal
        },
        render: (text, record) => {
          if (record['tree-cover-loss'] && record['tree-cover-loss'].stats) {
            const stats = record['tree-cover-loss'].stats
            const statsWithEmptyVals = {}
            const maxYear = 18 // 2018
            for (var i = 1; i <= maxYear; i++) {
              statsWithEmptyVals[i] = stats[i] || 0
            }
            const data = Object.keys(statsWithEmptyVals).map(key => {
              const value = statsWithEmptyVals[key]
              return {
                name: key,
                loss: value
              }
            })
            return (

              <BarChart
                width={200}
                height={40}
                data={data}
              >
                <XAxis
                  dataKey='name' height={10}
                  tick={{fontSize: '6px'}}
                />
                <Tooltip formatter={(val) => numeral(val).format('0,0')} />
                <Bar dataKey='loss' fill='#42B532' unit='ha' />
              </BarChart>
            )
          } else {
            return (<span>NA</span>)
          }
        }
      }
    } else {
      const width = col.width || 100
      const format = col.format || '0,0.00'
      const unit = col.unit || ''
      return {
        title: t(col.name),
        dataIndex: col.dataIndex,
        width,
        key: col.id,
        unit,
        render: (text, record) => {
          return (
            <span>{numeral(text).format(format)}{unit}</span>
          )
        }
      }
    }
  })
}
