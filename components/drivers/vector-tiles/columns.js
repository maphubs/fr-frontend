// @flow
import * as React from 'react'
import numeral from 'numeral'
import getColVal from '../../../utils/get-table-col-value'
type ModuleConfig = {
  columns: Array<Object>
}

type Module = {
  id: string,
  name: Object,
  config: ModuleConfig
}

export default (mod: Module, t: Function): Array<Object> => {
  const colConfigs = mod.config.columns
  if (!colConfigs) {
    return []
  }
  // create antd table column from config
  return colConfigs.map(col => {
    if (col) {
      const width = col.width || 100
      const format = col.format || '0,0.00'
      const unit = col.unit || ''
      return {
        title: t(col.name),
        dataIndex: col.dataIndex,
        width,
        key: col.id,
        module_id: mod.id,
        unit,
        sorter: (a, b) => {
          let aVal = getColVal(a, `${mod.id}.${col.dataIndex}`)
          let bVal = getColVal(b, `${mod.id}.${col.dataIndex}`)
          if (typeof aVal !== 'number') aVal = 0
          if (typeof bVal !== 'number') bVal = 0
          return aVal - bVal
        },
        render: (text, record) => {
          return (
            <span>{numeral(text).format(format)}{unit}</span>
          )
        }
      }
    }
  })
}
