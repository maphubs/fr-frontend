// @flow
import * as React from 'react'
import _isequal from 'lodash.isequal'
import { Button } from 'antd'

let AceEditor = ''

type Props = {|
  id: string,
  onSave?: Function,
  onChange?: Function,
  title?: string,
  code: string,
  mode: string,
  theme: string,
  showOnMount: boolean,
  t: Function
|}

type State = {
  code: string,
  canSave: boolean,
  show: boolean
}

export default class CodeEditor extends React.Component<Props, State> {
  props: Props

  static defaultProps = {
    id: 'code-editor',
    mode: 'json',
    theme: 'monokai',
    showOnMount: true
  }

  editor: any

  constructor (props: Props) {
    super(props)
    this.state = {
      code: props.code,
      canSave: true,
      show: false
    }
  }

  componentDidMount () {
    require('brace')
    AceEditor = require('react-ace').default
    require('brace/mode/json')
    require('brace/mode/html')
    require('brace/theme/monokai')
    if (this.props.showOnMount) {
      this.show()
    }
  }

  componentWillReceiveProps (nextProps: Props) {
    this.setState({code: nextProps.code})
  }

  shouldComponentUpdate (nextProps: Props, nextState: State) {
    // only update if something changes
    if (!_isequal(this.props, nextProps)) {
      return true
    }
    if (!_isequal(this.state, nextState)) {
      return true
    }
    return false
  }

  componentDidUpdate () {
    const _this = this
    if (this.refs.ace) {
      this.editor = this.refs.ace.editor
      this.editor.getSession().on('changeAnnotation', () => {
        const annotations = _this.editor.getSession().getAnnotations()
        let canSave = true
        if (annotations && annotations.length > 0) {
          annotations.forEach((anno) => {
            if (anno.type === 'error') {
              canSave = false
            }
          })
        }
        _this.setState({canSave})
      })
    }
  }

  show = () => {
    if (!this.state.show) {
      this.setState({show: true})
    }
  }

  hide = () => {
    this.setState({show: false})
  }

  onChange = (code: any) => {
    this.setState({code})
    if (this.props.onChange) {
      this.props.onChange(code)
    }
  }

  onCancel = () => {
    this.hide()
  }

  onSave = () => {
    if (this.state.canSave && this.props.onSave) {
      this.props.onSave(this.state.code)
    }
  }

  render () {
    const {show, code, canSave} = this.state
    const {id, title, mode, theme, t} = this.props
    let editor = ''
    if (show) {
      let enableBasicAutocompletion
      if (this.props.mode !== 'json') {
        enableBasicAutocompletion = true
      }

      editor = (
        <AceEditor
          ref='ace'
          mode={mode}
          theme={theme}
          onChange={this.onChange}
          name={id}
          width='100%'
          height='100%'
          highlightActiveLine
          enableBasicAutocompletion={enableBasicAutocompletion}
          value={code}
          editorProps={{$blockScrolling: true}}
        />
      )
    }

    return (
      <div style={{height: 'calc(100% - 100px)', width: '100%'}}>
        {
          title &&
            <h3 className='left no-padding'>{title}</h3>
        }
        {editor}
        <div style={{marginTop: '20px'}}>
          <Button type='primary' disabled={!canSave} onClick={this.onSave}>{t('Save')}</Button>
        </div>
      </div>
    )
  }
}
