// @flow
import * as React from 'react'
import { Form, Button, Input, Icon } from 'antd'
import Mutation from '../Mutation'
import gql from 'graphql-tag'
const FormItem = Form.Item

type Props = {
  form: any,
  onSubmit?: Function,
  t: Function
}

function hasErrors (fieldsError) {
  return Object.keys(fieldsError).some(field => fieldsError[field])
}

class InviteUserForm extends React.Component<Props, void> {
  componentDidMount () {
    // To disabled submit button at the beginning.
    this.props.form.validateFields()
  }

  render () {
    const {props} = this
    const { t } = this.props
    const { getFieldDecorator, getFieldsError, getFieldError, isFieldTouched } = this.props.form
    const emailError = isFieldTouched('email') && getFieldError('email')
    return (
      <Mutation
        query={gql`
          mutation inviteUser($email: String) {
            inviteUser(email: $email) {
              email,
              key,
              used
            }
          }
        `}
        successMessage={t('Invite Sent')}
        onComplete={() => {
          if (props.onSubmit) {
            props.onSubmit()
          }
        }}
      >
        {(inviteUser) => (
          <Form onSubmit={(e) => {
            e.preventDefault()
            props.form.validateFields(async (err, values) => {
              if (!err) {
                await inviteUser({ email: values.email })
              }
              props.form.resetFields()
            })
          }}
          >
            <FormItem
              validateStatus={emailError ? 'error' : ''}
              help={emailError || ''}
            >
              {getFieldDecorator('email', {
                initialValue: '',
                rules: [
                  {
                    type: 'email',
                    message: t('Please enter a valid email address')
                  },
                  { required: true, message: t('Please enter an email') }]
              })(
                <Input
                  prefix={<Icon type='user' style={{ color: 'rgba(0,0,0,.25)' }} />}
                  placeholder={t('Email')}
                />
              )}
            </FormItem>
            <FormItem>
              <Button
                type='primary'
                htmlType='submit'
                disabled={hasErrors(getFieldsError())}
              >
                {t('Invite')}
              </Button>
            </FormItem>
          </Form>
        )}
      </Mutation>
    )
  }
}

export default Form.create()(InviteUserForm)
