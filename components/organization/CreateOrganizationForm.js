// @flow
import * as React from 'react'
import { Form, Button } from 'antd'
import Mutation from '../Mutation'
import gql from 'graphql-tag'
import LocalizedInput from '../form/LocalizedInput'
const FormItem = Form.Item

type Props = {
  form: any,
  onSubmit?: Function
}

function hasErrors (fieldsError) {
  return Object.keys(fieldsError).some(field => fieldsError[field])
}

class CreateOrganizationForm extends React.Component<Props, void> {
  componentDidMount () {
    // To disabled submit button at the beginning.
    this.props.form.validateFields()
  }

  render () {
    const {props} = this
    const { getFieldDecorator, getFieldsError, getFieldError, isFieldTouched } = this.props.form
    const nameError = isFieldTouched('name') && getFieldError('name')
    return (
      <Mutation
        query={gql`
          mutation createOrganization($name: JSON) {
            createOrganization(name: $name) {
              id
            }
          }
        `}
        successMessage='Created'
        onComplete={() => {
          if (props.onSubmit) {
            props.onSubmit()
          }
        }}
      >
        {(createOrganization) => (
          <Form onSubmit={(e) => {
            e.preventDefault()
            props.form.validateFields((err, values) => {
              if (!err) {
                createOrganization({ name: values.name })
              }
            })
          }}
          >
            <FormItem
              validateStatus={nameError ? 'error' : ''}
              help={nameError || ''}
            >
              {getFieldDecorator('name', {
                initialValue: {},
                rules: [{ required: true, message: 'Please input a name' }]
              })(
                <LocalizedInput />
              )}
            </FormItem>
            <FormItem>
              <Button
                type='primary'
                htmlType='submit'
                disabled={hasErrors(getFieldsError())}
              >
                {'Save'}
              </Button>
            </FormItem>
          </Form>
        )}
      </Mutation>
    )
  }
}

export default Form.create()(CreateOrganizationForm)
