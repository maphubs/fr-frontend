//  @flow
import * as React from 'react'
import { Button } from 'antd'

const ButtonGroup = Button.Group

type Props = {
  defaultIcon: string,
  toggleIcon: string,
  style: Object,
  children: Array<any>
}

type State = {
  selected?: boolean
}

export default class ButtonSelect extends React.Component<Props, State> {
  static defaultProps = {
    defaultIcon: 'cloud',
    toggleIcon: 'cloud-download'
  }

  constructor (props: Props) {
    super(props)
    this.state = {}
  }

  select = () => {
    this.setState({ selected: true })
  }

  deselect = () => {
    this.setState({ selected: false })
  }

  render () {
    const { selected } = this.state
    const { defaultIcon, toggleIcon, children, style } = this.props

    if (!children || children.length < 2) {
      return ''
    }

    let child = children[0]
    if (selected) {
      child = children[1]
    }

    return (
      <div style={style}>
        <ButtonGroup>
          <Button type='primary' ghost onClick={this.deselect} icon={defaultIcon} />
          <Button type='primary' ghost onClick={this.select} icon={toggleIcon} />
        </ButtonGroup>
        {child}
      </div>
    )
  }
}
