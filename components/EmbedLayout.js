import * as React from 'react'
import Head from 'next/head'
import enUS from 'antd/lib/locale-provider/en_US'
import enGB from 'antd/lib/locale-provider/en_GB'
import frFR from 'antd/lib/locale-provider/fr_FR'
import esES from 'antd/lib/locale-provider/es_ES'
import deDE from 'antd/lib/locale-provider/de_DE'
import itIT from 'antd/lib/locale-provider/it_IT'
import ptBR from 'antd/lib/locale-provider/pt_BR'
import ptPT from 'antd/lib/locale-provider/pt_PT'
import NProgress from 'nprogress'
import Router from 'next/router'
import {Layout, LocaleProvider} from 'antd'

Router.onRouteChangeStart = (url) => {
  // console.log(`Loading: ${url}`)
  NProgress.start()
}
Router.onRouteChangeComplete = () => NProgress.done()
Router.onRouteChangeError = () => NProgress.done()

const getAntLocale = (lang) => {
  console.log(lang)
  let antLocale
  if (lang === 'en' || lang === 'en-US') {
    antLocale = enUS
  } else if (lang === 'de' || lang === 'de-DE') {
    antLocale = deDE
  } else if (lang === 'es' || lang === 'es-ES') {
    antLocale = esES
  } else if (lang === 'fr' || lang === 'fr-FR') {
    antLocale = frFR
  } else if (lang === 'it' || lang === 'it-IT') {
    antLocale = itIT
  } else if (lang === 'pt' || lang === 'pt-BR') {
    antLocale = ptBR
  } else if (lang === 'pt-PT') {
    antLocale = ptPT
  } else if (lang === 'en-GB') {
    antLocale = enGB
  } else {
    antLocale = enUS
  }
  return antLocale
}

type Props = {
  t: any,
  language: string,
  title: string,
  children: any
}

type State = {
  collapsed: boolean
}

class AppLayout extends React.Component<Props, State> {
  state = {

  };

  render () {
    const {
      language, title, children
    } = this.props
    return (
      <div>
        <Head>
          <meta name='viewport' content='width=device-width, initial-scale=1' />
          <meta charSet='utf-8' />
          <title>{title}</title>
          <link rel='stylesheet' href='//cdnjs.cloudflare.com/ajax/libs/antd/2.9.3/antd.min.css' />
          <link rel='stylesheet' type="text/css' href='/static/nprogress.css" />
        </Head>
        <LocaleProvider locale={getAntLocale(language)}>
          <Layout style={{height: '100vh', minHeight: '100vh', background: 'none'}}>
            {children}
          </Layout>
        </LocaleProvider>
      </div>
    )
  }
}

export default AppLayout
