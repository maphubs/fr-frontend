// @flow
import * as React from 'react'
import { Col, Row, Slider } from 'antd'

type Props = {
  defaultValue: number,
  onSelect: Function
}

export default class RadiusSlider extends React.Component<Props, void> {
  shouldComponentUpdate () { return false }
  static defaultProps = {
    defaultValue: 25
  }

  render () {
    const {defaultValue, onSelect} = this.props
    return (
      <Row>
        <style jsx global>{`
          .ant-slider-mark-text {
            font-size: 10px;
          }
          .ant-slider-with-marks {
            margin-top: 5px;
            margin-bottom: 18px;
          }
        `}
        </style>
        <Col span={4}>
          <span style={{fontSize: '12px', textAlign: 'right', lineHeight: '12px'}} />
        </Col>
        <Col span={20}>
          <Slider
            min={5} max={50}
            marks={{
              5: '5km',
              10: '10km',
              25: '25km',
              50: '50km'
            }}
            step={null}
            onChange={onSelect}
            defaultValue={defaultValue}
          />
        </Col>
      </Row>
    )
  }
}
