//  @flow
import * as React from 'react'
import { Tabs } from 'antd'
import dynamic from 'next/dynamic'
const CKEditorNote = dynamic(() => import('./CKEditorNote.js'), {
  ssr: false
})

const TabPane = Tabs.TabPane

const langs = ['en', 'fr', 'es', 'id', 'pt', 'it', 'de']

type Props = {
  value?: Object,
  onChange?: Function
}

type State = {
  value: Object
}

export default class LocalizedEditor extends React.Component<Props, State> {
  constructor (props: Props) {
    super(props)
    let value = Object.assign({}, props.value)
    langs.map((lang) => {
      if (!value[lang]) {
        value[lang] = ''
      }
    })
    this.state = { value }
  }

  componentWillReceiveProps (nextProps: Props) {
    if ('value' in nextProps) {
      const value = nextProps.value
      this.setState(value)
    }
  }

  getValue = () => {
    return this.state.value
  }

  handleChange = (lang: string, val: string) => {
    let changedValue = {}
    changedValue[lang] = val

    this.setState({
      value: Object.assign({}, this.state.value, changedValue)
    })

    const onChange = this.props.onChange
    if (onChange) {
      onChange(Object.assign({}, this.state.value, changedValue))
    }
  }

  render () {
    const {value} = this.state
    const {handleChange} = this
    return (
      <>
        <style jsx>{`

          .localized-input {
            padding-bottom: 0px;
            height: 100%;
          }
        `}
        </style>
        <div className='localized-input'>
          <Tabs animated={false} size='small' type='card' style={{height: '100%'}} >
            {langs.map(lang => {
              console.log(value)
              return (
                <TabPane tab={lang} key={lang}>
                  <div style={{height: 'calc(100% - 75px'}}>
                    <CKEditorNote
                      initialData={value[lang]}
                      onChange={(data) => {
                        console.log(data)
                        handleChange(lang, data)
                      }}
                    />
                  </div>
                </TabPane>
              )
            })
            }
          </Tabs>
        </div>
      </>
    )
  }
}
