// @flow
import * as React from 'react'
import Uppy from '@uppy/core'
import Tus from '@uppy/tus'
import { Dashboard } from '@uppy/react'
import '../../node_modules/@uppy/dashboard/dist/style.min.css'
// import '../../node_modules/@uppy/core/dist/style.min.css'
// import GoogleDrive from '@uppy/google-drive';
// import Dropbox from '@uppy/dropbox';
import UserContainer from '../../containers/UserContainer'
import { subscribe } from '../../lib/unstated-props'

type Props = {
  endpoint: string,
  note: string,
  onComplete: Function,
  onError: Function,
  height: number,
  maxFileSize?: number,
  allowedFileTypes?: Array<string>,
  meta?: Object,
  headers?: Object,
  t: Function,
  containers: {user: UserContainer}
}

class UppyFileUpload extends React.Component<Props, void> {
  uppy: any

  static defaultProps = {
    height: 400
  }

  componentWillMount () {
    const {maxFileSize, allowedFileTypes, meta, headers, endpoint, containers} = this.props
    this.uppy = Uppy({
      id: 'uppy',
      autoProceed: false,
      debug: false,
      restrictions: {
        maxFileSize,
        maxNumberOfFiles: 1,
        minNumberOfFiles: false,
        allowedFileTypes
      },
      thumbnailGeneration: false,
      meta
    })
    // this.uppy.use(GoogleDrive, { host: 'http://localhost:3020' });
    // this.uppy.use(Dropbox, { host: 'http://localhost:3020' });
    const config = {
      endpoint,
      headers: headers || {}
    }
    if (!headers || !headers.authorization) {
      // if this wasn't customized for an external service, assume we authenticating normally
      let token
      if (
        containers.user &&
        containers.user.state.profile &&
        containers.user.state.profile.accessToken
      ) {
        token = containers.user.state.profile.accessToken
        config.headers.authorization = `Bearer ${token}`
      }
    }

    this.uppy.use(Tus, config)

    this.uppy.on('complete', (result) => {
      if (result.successful && result.successful.length === 1) {
        const file = result.successful[0]
        console.log(file)
        if (this.props.onComplete) {
          this.props.onComplete(file)
        }
      }
    })

    this.uppy.run()
  }

  render () {
    const {note, height, t} = this.props
    if (this.uppy) {
      return (
        <>
          <style jsx global>{`
            .uppy-DashboardAddFiles-info {
              display: block !important;
            }
          `}
          </style>
          <Dashboard
            uppy={this.uppy}
            // plugins={['GoogleDrive', 'Dropbox']}
            inline
            height={height}
            note={note}
            showLinkToFileUploadResult={false}
            proudlyDisplayPoweredByUppy={false}
            showProgressDetails
            locale={{
              strings: {
                selectToUpload: t('Select files to upload'),
                closeModal: t('Close Modal'),
                upload: t('Upload'),
                importFrom: t('Import files from'),
                dashboardTitle: t('Upload'),
                copyLinkToClipboardSuccess: t('Link copied to clipboard.'),
                copyLinkToClipboardFallback: t('Copy the URL below'),
                fileSource: t('File source'),
                done: t('Done'),
                localDisk: t('Local Disk'),
                myDevice: t('My Device'),
                dropPasteImport: t('Drop files here, paste, %{browse} or import from'),
                dropPaste: t('Drop files here, paste or %{browse}'),
                browse: t('browse'),
                fileProgress: t('File progress: upload speed and ETA'),
                numberOfSelectedFiles: t('Number of selected files'),
                uploadAllNewFiles: t('Upload all new files'),
                emptyFolderAdded: t('No files were added from empty folder'),
                folderAdded: {
                  0: t('Added %{smart_count} file from %{folder}'),
                  1: t('Added %{smart_count} files from %{folder}')
                }
              }
            }}
          />
        </>
      )
    } else {
      return 'Failed to intialize Uppy uploader'
    }
  }
}
export default subscribe(UppyFileUpload, {user: UserContainer})
