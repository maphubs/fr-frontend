// @flow

import * as React from 'react'
import { Tabs } from 'antd'
import AutoSizer from 'react-virtualized-auto-sizer'
import CKEditor from '@ckeditor/ckeditor5-react'
import MapHubsEditor from '@maphubs/maphubs-story-editor'
import getConfig from 'next/config'
const MAPHUBS_CONFIG = getConfig().publicRuntimeConfig

const langs = ['en', 'fr', 'es', 'id', 'pt', 'it', 'de']

const TabPane = Tabs.TabPane

type Props = {
  initialData?: Object,
  onChange?: Function,
  getMap?: Function,
  cropImage?: Function,
  onImageUpload?: Function,
  language?: string,
  reportID: string
}

export default class CKEditorReport extends React.Component<Props, void> {
  editorInstance: any

  domContainer: any

  constructor (props: Props) {
    super(props)
    this.editorInstance = null
  }

  static defaultProps = {
    initialData: '',
    language: 'en'
  }

  shouldComponentUpdate () {
    return false
  }

  render () {
    const { initialData, language, getMap, cropImage, onImageUpload, reportID } = this.props
    const host = MAPHUBS_CONFIG.host ? MAPHUBS_CONFIG.host.replace('.', '') : 'unknownhost'
    const editorConfiguration = {
      language,
      maphubsMap: {
        getMap
      },
      maphubsUpload: {
        assetUploadAPI: `${MAPHUBS_CONFIG.ASSET_UPLOAD_API}/image/upload`, // maphubs asset upload service
        assetUploadAPIKey: MAPHUBS_CONFIG.ASSET_UPLOAD_API_KEY, //
        subfolder: `${host}-reports`, // can be used to group content by host and or type
        subfolderID: reportID, // an id for example a story id that can be used to bulk delete content later
        onUpload: onImageUpload,
        cropImage
      },
      mediaEmbed: {
        previewsInData: true
      }
    }

    return (
      <AutoSizer disableWidth>
        {({ height }) => (
          <div>
            <style jsx global>{`
              .ck.ck-editor__main>.ck-editor__editable {
                height: ${height - 80}px;
                overflow-y: scroll;
              }
              .ck-content p {
                font-size: 20px;
              }
              .ck-content ul {
                list-style: initial;
                font-size: 20px;
                padding-left: 40px;
              }
              .ck-content ul li {
                list-style-type: inherit;
              }
              .ck-content ol {
                font-size: 20px;
              }
              .ck-content a {
                color: ${MAPHUBS_CONFIG.primaryColor};
                text-decoration: underline;
              }
            `}</style>
            <Tabs size='small'
              tabBarStyle={{marginBottom: 0}}
              animated={false}
              style={{height: '100%'}}
            >
              {langs.map(lang => {
                const data = initialData ? initialData[lang] : ''
                return (
                  <TabPane
                    tab={<span>{lang}</span>}
                    key={lang}
                  >
                    <div style={{padding: '0px', height: 'calc(100% - 75px'}}>
                      <CKEditor
                        editor={MapHubsEditor}
                        config={editorConfiguration}
                        data={data}
                        onInit={editor => {
                          this.editorInstance = editor
                          console.log('Init.', editor)
                        }}
                        onChange={(event, editor) => {
                          const data = editor.getData()
                          if (this.props.onChange) this.props.onChange(lang, data)
                        }}
                        onBlur={editor => {
                          // console.log('Blur.', editor)
                        }}
                        onFocus={editor => {
                          // console.log('Focus.', editor)
                        }}
                      />
                    </div>
                  </TabPane>
                )
              })
              }
            </Tabs>
          </div>
        )}
      </AutoSizer>
    )
  }
}
