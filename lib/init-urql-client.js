// @flow
import {
  createClient,
  dedupExchange,
  cacheExchange,
  fetchExchange,
  ssrExchange
} from 'urql'

import 'isomorphic-unfetch'

import getConfig from 'next/config'
const MAPHUBS_CONFIG = getConfig().publicRuntimeConfig

let urqlClient = null
let ssrCache = null

const apiURL = MAPHUBS_CONFIG.API_URL
const apiURLInternal = MAPHUBS_CONFIG.API_URL_INTERNAL

export default function initUrqlClient (initialState: any, token?: Object) {
  let headers

  if (typeof location !== 'undefined') {
    const accessTokenParam = location.hash.split('access_token=')[1]
    if (accessTokenParam) {
      token = accessTokenParam
    }
  }

  if (token) {
    headers = {
      authorization: token ? `Bearer ${token}` : null
    }
  } else {
    console.error('URQL Missing Token')
  }

  // Create a new client for every server-side rendered request to reset its state
  // for each rendered page
  // Reuse the client on the client-side however
  const isServer = typeof window === 'undefined'
  if (isServer || !urqlClient) {
    ssrCache = ssrExchange({ initialState })
    const baseURL = isServer ? apiURLInternal : apiURL
    urqlClient = createClient({
      url: `${baseURL}/graphql`,
      fetchOptions: {
        headers
      },
      // Active suspense mode on the server-side
      suspense: isServer,
      exchanges: [dedupExchange, cacheExchange, ssrCache, fetchExchange]
    })
  }

  // Return both the cache and the client
  return [urqlClient, ssrCache]
}
