// @flow
import * as React from 'react'

import ssrPrepass from 'react-ssr-prepass'
import initUrqlClient from './init-urql-client'

type Props = {
  urqlClient: any,
  urqlState: any,
  pageProps: any
}

const withUrqlClient = (App: any) => {
  return class WithUrql extends React.Component<Props, void> {
    static async getInitialProps (ctx: any) {
      const { AppTree } = ctx

      // Run the wrapped component's getInitialProps function
      let appProps = {}
      if (App.getInitialProps) {
        appProps = await App.getInitialProps(ctx)
      }
      let token
      if (appProps.pageProps && appProps.pageProps.user && appProps.pageProps.user.accessToken) {
        token = appProps.pageProps.user.accessToken
      }

      // getInitialProps is universal, but we only want
      // to run server-side rendered suspense on the server
      const isBrowser = typeof window !== 'undefined'
      if (isBrowser) {
        return appProps
      }

      const [urqlClient, ssrCache] = initUrqlClient({}, token)

      // Run suspense and hence all urql queries
      await ssrPrepass(
        <AppTree
          {...appProps}
          urqlClient={urqlClient}
        />
      )

      // Extract query data from the Apollo store
      // Extract the SSR query data from urql's SSR cache
      let urqlState
      if (ssrCache) {
        urqlState = ssrCache.extractData()
      }

      return {
        ...appProps,
        urqlState
      }
    }

    urqlClient: any

    constructor (props: Props) {
      super(props)

      if (props.urqlClient) {
        this.urqlClient = props.urqlClient
      } else {
        let token
        if (props.pageProps && props.pageProps.user && props.pageProps.user.accessToken) {
          token = props.pageProps.user.accessToken
        }
        // Create the urql client and rehydrate the prefetched data
        const [urqlClient] = initUrqlClient(props.urqlState, token)
        this.urqlClient = urqlClient
      }
    }

    render () {
      if (!this.urqlClient) return 'failed to init urql client'
      return <App {...this.props} urqlClient={this.urqlClient} />
    }
  }
}

export default withUrqlClient
