const i18n = require('i18n')
i18n.configure({
  locales: ['en', 'fr', 'es', 'pt', 'id'],
  directory: './locales',
  defaultLocale: 'en',
  extension: '.json',
  logErrorFn (msg) {
    console.error(msg)
  }
})

module.exports = i18n
