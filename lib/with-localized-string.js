// @flow
import * as React from 'react'
import hoistStatics from 'hoist-non-react-statics'

type Props = {
  t: any
}

export default function withLocalizedString (Component: React.Component<any>, language: string) {
  class WithLocalizedstring extends React.Component<Props> {
    static displayName = 'withLocalizedString'

    render () {
      const { t } = this.props
      const originalProps = this.props

      const props = {}
      Object.keys(originalProps).forEach(key => {
        if (key !== 't') {
          props[key] = originalProps[key]
        }
      })

      props.t = (val) => {
        if (typeof val === 'string') {
          return t(val)
        } else if (typeof val === 'object') {
          if (language) {
            return val[language]
          } else {
            console.error('missing language')
            return val.en
          }
        }
      }

      return <Component {...props} />
    }
  }
  return hoistStatics(WithLocalizedstring, Component)
}
