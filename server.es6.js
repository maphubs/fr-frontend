const express = require('express')
const next = require('next')
const session = require('express-session')
var RedisStore = require('connect-redis')(session)
const passport = require('passport')
const Auth0Strategy = require('passport-auth0')
const authRoutes = require('./routes/auth-routes')
const config = require('./config')
const shrinkRay = require('shrink-ray-current')
const bodyParser = require('body-parser')
const i18n = require('./lib/i18n')

const port = parseInt(process.env.MAPHUBS_FORESTS_PORT, 10) || 4004
const app = next({ dev: process.env.NODE_ENV !== 'production' })
const handle = app.getRequestHandler();

(async () => {
  try {
    await app.prepare()
    const server = express()
    server.disable('x-powered-by')
    server.use(shrinkRay())
    server.use(i18n.init)

    const sessionConfig = {
      secret: config.SESSION_SECRET,
      cookie: {
        maxAge: 86400 * 1000 // 24 hours in milliseconds
      },
      resave: false,
      saveUninitialized: true,
      store: new RedisStore({
        host: config.REDIS_HOST,
        port: config.REDIS_PORT
      })
    }
    server.use(session(sessionConfig))

    const auth0Strategy = new Auth0Strategy(
      {
        domain: config.AUTH0_DOMAIN,
        clientID: config.AUTH0_CLIENT_ID,
        clientSecret: config.AUTH0_CLIENT_SECRET,
        callbackURL: config.AUTH0_CALLBACK_URL
      },
      function (accessToken, refreshToken, extraParams, profile, done) {
        profile.accessToken = accessToken

        // check if user has a local user object
        let hosts = []

        if (profile._json['https://maphubs.com/hosts']) {
          hosts = profile._json['https://maphubs.com/hosts']
        }
        console.log(hosts)
        const host = hosts.find(({ host }) => host === config.host)

        if (host && host.role) {
          profile.role = host.role
        }

        return done(null, profile)
      }
    )

    passport.use(auth0Strategy)
    passport.serializeUser((user, done) => done(null, user))
    passport.deserializeUser((user, done) => done(null, user))
    server.use(passport.initialize())
    server.use(passport.session())

    authRoutes(app, server)

    server.get('/favicon.ico', (req, res) => {
      res.status(204).send()
    })

    server.post('/setlocale', bodyParser.json({limit: '1kb'}), (req, res) => {
      const locale = req?.body?.locale
      if (locale) {
        req.session.locale = locale
        req.setLocale(locale)
        console.log(`setting locale in server sesssion ${locale}`)
        res.status(200).send({success: true})
      } else {
        console.log(req?.body)
        res.status(200).send({success: false, error: 'missing locale'})
      }
    })

    // screenshots don't require normal session auth
    // but won't load anything meaningful unless an access token is available to send to the API
    require('./routes/screenshots')(app, server)

    // everything else below this point requires auth

    server.use((req, res, next) => {
      if (req.path.startsWith('/_next')) {
        next()
        return
      }
      if (!req.isAuthenticated()) return res.redirect('/login')

      if (req.user.role && req.user.role > 0) {
        next()
      } else {
        app.render(req, res, '/user/inviteRequired', {})
      }

      // override browser language if user has chosen a language
      let locale = 'en'
      if (req.session && req.session.locale) {
        // the user has specified a language from the options on the website
        locale = req.session.locale
        req.setLocale(locale)
      } else {
        // use local from i18n parsing of http accept-language
        locale = req.locale
      }
    })

    require('./routes/list')(app, server)

    require('./routes/dashboard')(app, server)

    require('./routes/admin')(app, server)

    require('./routes/report')(app, server)

    // use next.js routing for everything else
    server.get('*', handle)

    await server.listen(port)
    console.log(`> Ready on http://localhost:${port}`)
  } catch (err) {
    console.error(err)
  }
})()
