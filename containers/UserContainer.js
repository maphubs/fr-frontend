// @flow
import { Container } from 'unstated'

type UserContainerState = {
  profile: Object,
  locale: string
}

export default class UserContainer extends Container<UserContainerState> {
  constructor (profile: Object, locale:string) {
    super()
    this.state = { profile, locale }
  }

  setProfile = (profile: Object) => {
    return this.setState(state => ({ profile }))
  }

  changeLocale = async (locale: string) => {
    console.log(`changing locale to ${locale}`)
    const response = await fetch('/setlocale', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({ locale })
    })
    const result = await response.json()
    if (!result?.success) console.error('failed to change locale')
    return this.setState(state => ({ locale }))
  }
}
