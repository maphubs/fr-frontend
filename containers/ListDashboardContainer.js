// @flow
import { Container } from 'unstated'
import RatingColumn from '../components/list/ListTable/Columns/Rating'
import NameColumn from '../components/list/ListTable/Columns/Name'
import Drivers from '../components/drivers'
import {min, max, mean, median, sum, equalIntervalBreaks, ckmeans} from 'simple-statistics'

let workerInstance
let WebWorkerEnabler
if (typeof window !== 'undefined') {
  WebWorkerEnabler = require('../workers/WebWorkerEnabler').default
}

type ListDashboardState = {
  config: Object,
  result_id: string,
  modules: Array<Object>,
  listResults?: Array<Object>,
  listResultsAgg?: Array<Object>,
  aggField?: string,
  tableData: Array<Object>,
  tableTotalWidth: number,
  resultColumns: Array<Object>,
  selectionResultColumns: Array<Object>,
  selectedColumn: Object,
  selectedColumnValues: Array<Object>,
  selectedColumnStats: {
    min: number,
    max: number,
    mean: number,
    sum: number,
    median: number,
    ckmeans: number,
    equalIntervalBreaks: number
  },
  selectedAOI?: string,
  selectedRowKeys: Array<string>,
  searchText: string,
  radiusSelection: string,
  isBuffered: boolean,
  mapComponent: Object,
  sorting?: boolean,
  sortDirectionAscending?: boolean
}

type Props = {
  config: Object,
  modules: Array<Object>,
  listResults?: Array<Object>,
  listResultsAgg?: Array<Object>, // used by summary agg page
  aggField?: string, // used by summary agg page
  radiusSelection?: string,
  t: Function
}

const clone = (obj) => {
  if (obj == null || typeof obj !== 'object') return obj
  var copy = obj.constructor()
  for (var attr in obj) {
    if (obj.hasOwnProperty(attr)) copy[attr] = obj[attr]
  }
  return copy
}

const getColVal = (record, dataIndex) => {
  let val = clone(record)
  if (!val) return undefined
  const dataIndexParts = dataIndex.split('.')
  if (dataIndexParts.length > 1) {
    dataIndexParts.forEach((part) => {
      if (val) val = val[part]
    })
  } else {
    val = val[dataIndex]
  }

  return val
}

export default class ListDashboardContainer extends Container<ListDashboardState> {
  constructor (props: Props) {
    super()
    this.state = {}
    this.init(props)
  }

  nameSearchInput: any

  init (props: Props): ListDashboardState {
    const { config, modules, listResults, listResultsAgg, aggField, t } = props
    RatingColumn.title = t('Rating')
    const resultColumns = [
      RatingColumn,
      NameColumn({
        title: t('Name'),
        state: this.state,
        handleSearch: this.handleNameSearch,
        handleReset: this.handleNameReset,
        searchInput: this.nameSearchInput
      })
    ]

    let tableTotalWidth = 67 + 85 + 250 // selection + rating + name

    // add properties (if configured)
    if (config.listProperties) {
      resultColumns.push({
        title: t('Properties'),
        children: config.listProperties.map(prop => {
          tableTotalWidth += 100
          return {
            title: t(prop.label),
            dataIndex: `properties[${prop.key}]`,
            width: 100,
            key: prop.key,
            sorter: (a: any, b: any) => {
              const aVal = a.properties[prop.key]
              const bVal = b.properties[prop.key]
              if (typeof aVal === 'string') {
                const aValLower = aVal.toLowerCase()
                const bValLower = bVal.toLowerCase()
                return aValLower.localeCompare(bValLower)
              } else {
                return aVal > bVal
              }
            }
          }
        })
      })
    }
    // add results
    let selectedColumn
    if (modules.length > 0) {
      const firstModCols = Drivers[modules[0].driver.id].Columns(modules[0], t)
      if (firstModCols && firstModCols.length > 0) {
        selectedColumn = firstModCols[0]
      }
    }
    modules.forEach((mod) => {
      const cols = Drivers[mod.driver.id].Columns(mod, t)
      if (cols) {
        cols.forEach(col => {
          if (col && col.width) {
            if (config.sortByModuleID && config.sortByModuleColKey &&
              mod.id === config.sortByModuleID &&
              col.key === config.sortByModuleColKey) {
              selectedColumn = col
            }
            tableTotalWidth += col.width
          } else {
            tableTotalWidth += 100
            console.error('col width not found')
          }
        })
        resultColumns.push({
          title: t(mod.name),
          children: cols
        })
      } else {
        resultColumns.push({
          title: t(mod.name),
          children: [
            {
              title: t(mod.name),
              dataIndex: `${mod.id}.${'val'}`,
              width: 100,
              key: mod.id
            }
          ]
        })
        tableTotalWidth += 100
      }
    })

    // get full children col list
    const selectionResultColumns = []
    resultColumns.map(col => {
      if (col.children) {
        col.children.map(childCol => {
          if (childCol?.type !== 'chart') {
            selectionResultColumns.push(childCol)
          }
        })
      } else {
        selectionResultColumns.push(col)
      }
    })

    let isBuffered = false
    const radiusSelection = props.radiusSelection || '25'
    let result_id = 'default'

    let tableData = []
    const selectedColumnValues = []

    if (listResults) {
      const firstAOI = listResults[0].aoi
      if (firstAOI.properties && firstAOI.properties.buffers) {
        isBuffered = true
        result_id = `buffer-${radiusSelection}`
      }

      const filteredResults = listResults.filter(row => row.result_id === result_id)
      tableData = filteredResults.map((row) => {
        const clonedRow = {
          key: row.aoi.id,
          aoi: row.aoi,
          properties: row.aoi.properties
        }
        if (row.aoi.rating && row.aoi.rating.rank) {
          row.aoi.properties.rating = row.aoi.rating.rank
        }
        row.aoi.properties.id = row.aoi.id

        row.results.forEach((result) => {
          if (result.result_id === result_id) {
            clonedRow[result.module.id] = result.summary
          }
        })
        selectedColumnValues.push(getColVal(clonedRow, `${selectedColumn.module_id}.${selectedColumn.dataIndex}`))
        return clonedRow
      })
    } else if (listResultsAgg && aggField) {
      // process the summary page aggregate results instead
      const firstAOI = listResultsAgg[0]?.children[0]?.aoi
      if (firstAOI.properties && firstAOI.properties.buffers) {
        isBuffered = true
        result_id = `buffer-${radiusSelection}`
      }

      const filteredResults = listResultsAgg.filter(row => row.result_id === result_id)

      tableData = filteredResults.map((row) => {
        const properties = {
          name: row.agg_id
        }
        properties[aggField] = row.agg_id

        if (!row.children) row.children = []

        const clonedRow = {
          key: row.agg_id,
          properties,
          children: row.children.map(child => {
            const clonedChildRow = {
              key: child.aoi.id,
              properties: Object.assign({}, child.aoi.properties, child.aoi.centroid.properties),
              aoi: child.aoi
            }
            child.aoi.centroid.properties.rating = child?.aoi?.rating?.rank
            child.aoi.centroid.properties.id = child.aoi.id

            child.results.forEach((result) => {
              if (result.result_id === result_id) {
                clonedChildRow[result.module.id] = result.summary
              }
            })
            return clonedChildRow
          })
        }

        row.results.forEach((result) => {
          if (result.result_id === result_id) {
            clonedRow[result.module.id] = result.summary
          }
        })
        selectedColumnValues.push(getColVal(clonedRow, `${selectedColumn.module_id}.${selectedColumn.dataIndex}`))
        return clonedRow
      })
    }

    this.sortTableData(selectedColumn, tableData, false) // TODO: allow module config to specfic default sort direction?

    const selectedColumnStats = this.getSelectedColumnStats(selectedColumnValues)

    if (typeof window !== 'undefined') {
      workerInstance = new WebWorkerEnabler(() => {
        self.addEventListener('message', e => { // eslint-disable-line no-restricted-globals
          if (!e) return
          const {key, dataIndex, module_id, tableData, sortDirection, ascending} = e.data
          if (key) {
            console.log(`sorting table data using ${key} (Web Worker)`)
            // console.log(tableData)

            const clone = (obj) => {
              if (obj == null || typeof obj !== 'object') return obj
              var copy = obj.constructor()
              for (var attr in obj) {
                if (obj.hasOwnProperty(attr)) copy[attr] = obj[attr]
              }
              return copy
            }

            const getColVal = (record, dataIndex) => {
              let val = clone(record)
              if (!val) return undefined
              const dataIndexParts = dataIndex.split('.')
              if (dataIndexParts.length > 1) {
                dataIndexParts.forEach((part) => {
                  if (val) val = val[part]
                })
              } else {
                val = val[dataIndex]
              }

              return val
            }

            const sorter = (a, b) => {
              if (module_id === 'name') {
                let aVal = a.properties.name
                let bVal = b.properties.name
                if (typeof aVal !== 'string') aVal = ''
                if (typeof bVal !== 'string') bVal = ''
                const aValLower = aVal.toLowerCase()
                const bValLower = bVal.toLowerCase()
                // return collator.compare(aValLower, bValLower)
                if (ascending) {
                  return (aValLower > bValLower ? -1 : (aValLower < bValLower ? 1 : 0))
                } else {
                  return (aValLower < bValLower ? -1 : (aValLower > bValLower ? 1 : 0))
                }
                // return aValLower.localeCompare(bValLower)
              } else if (module_id === 'rating') {
                let aVal = a?.aoi?.rating?.rank ?? 99
                let bVal = b?.aoi?.rating?.rank ?? 99
                if (typeof aVal !== 'number') aVal = 99
                if (typeof bVal !== 'number') bVal = 99
                if (ascending) return aVal - bVal
                return bVal - aVal
              } else {
                let aVal = getColVal(a, `${module_id}.${dataIndex}`)
                let bVal = getColVal(b, `${module_id}.${dataIndex}`)
                if (typeof aVal !== 'number') aVal = 0
                if (typeof bVal !== 'number') bVal = 0
                if (ascending) return bVal - aVal
                return aVal - bVal
              }
            }
            tableData.sort((a, b) => {
              if (sortDirection === 'asc') {
                return sorter(a, b)
              } else {
                return sorter(b, a)
              }
            })
          }
          postMessage(tableData)
        })
      })
    }

    const state = {
      config,
      modules,
      result_id,
      listResults,
      listResultsAgg,
      aggField,
      tableData,
      resultColumns,
      selectedColumn,
      selectedColumnValues,
      selectedColumnStats,
      tableTotalWidth,
      selectionResultColumns,
      searchText: '',
      radiusSelection,
      isBuffered,
      selectedRowKeys: [],
      mapComponent: this?.state?.mapComponent
    }

    this.state = state
    return state
  }

  registerMapComponent = (mapComponent: Object) => {
    this.setState({mapComponent})
  }

  setSortDirection = async (sortDirectionAscending: boolean) => {
    const tableData = await this.sortTableDataAsync(this.state.selectedColumn, this.state.tableData, sortDirectionAscending)
    return this.setState({sortDirectionAscending, tableData})
  }

  selectAOI = async (id: string) => {
    // always return to default zoom so map queries all data
    await this.state.mapComponent.zoomMapToDefault(false)
    return this.selectAOIHelper(id)
  }

  /**
    Called when an AOI is selected either from the table or the map
   */
  selectAOIHelper = async (id: string) => {
    const { listResults } = this.state
    if (!listResults) console.error('missing list results')

    let selectedAOI

    for (const listResult of listResults) {
      if (listResult.aoi.id === id) {
        selectedAOI = listResult.aoi
        break
      }
    }

    await this.setState({selectedRowKeys: [id], selectedAOI})
    return this.zoomMapToAOI(selectedAOI)
  }

  deselectAOI = async (doNotAnimate: boolean) => {
    await this.setState({selectedRowKeys: [], selectedAOI: null})
    return this.state.mapComponent.zoomMapToDefault(!doNotAnimate)
  }

  selectRadius = (radiusSelection: number) => {
    console.log(`selecting radius: ${radiusSelection}`)

    const { config, modules, listResults, listResultsAgg, aggField } = this.state
    const state = this.init({config, modules, listResults, listResultsAgg, aggField, radiusSelection})
    this.setState(state)
    this.state.mapComponent.selectSource({
      module_id: state.selectedColumn.module_id,
      result_id: state.result_id,
      dataIndex: state.selectedColumn.dataIndex,
      min: state.selectedColumnStats.min,
      max: state.selectedColumnStats.max,
      ckmeans: state.selectedColumnStats.ckmeans
    })
  }

  handleNameSearch = (selectedKeys: Array<string>, confirm: Function) => {
    confirm()
    this.setState({ searchText: selectedKeys[0] })
  }

  handleNameReset = (clearFilters: Function) => {
    clearFilters()
    this.setState({ searchText: '' })
  }

  zoomMapToAOI = (aoi: Object) => {
    return this.state.mapComponent.zoomToAOI(aoi.id, this.state.result_id)
  }

  selectColumn = async (selectedColumn: Object) => {
    console.log(`Selected Column changing to ${selectedColumn.key}`)
    this.setState({sorting: true})
    const tableData = await this.sortTableDataAsync(selectedColumn, this.state.tableData, this.state.sortDirectionAscending)
    const selectedColumnValues = tableData.map(row => getColVal(row, `${selectedColumn.module_id}.${selectedColumn.dataIndex}`))
    const selectedColumnStats = this.getSelectedColumnStats(selectedColumnValues)
    await this.setState({selectedColumn, tableData, selectedColumnValues, selectedColumnStats, sorting: false})
    // now update the map
    this.state.mapComponent.selectSource({
      module_id: selectedColumn.module_id,
      result_id: this.state.result_id,
      dataIndex: selectedColumn.dataIndex,
      min: selectedColumnStats.min,
      max: selectedColumnStats.max,
      ckmeans: selectedColumnStats.ckmeans
    })
  }

  /// helpers (do not modify state themselves)

  sortTableData = (selectedColumn: Object, tableData: Array<Object>, ascending: boolean) => {
    if (selectedColumn) {
      console.log(`sorting table data using ${selectedColumn.key}`)

      const { module_id, dataIndex, sortDirection } = selectedColumn

      const sorter = (a, b) => {
        let aVal = getColVal(a, `${module_id}.${dataIndex}`)
        let bVal = getColVal(b, `${module_id}.${dataIndex}`)
        if (typeof aVal !== 'number') aVal = 0
        if (typeof bVal !== 'number') bVal = 0
        if (ascending) return bVal - aVal
        return aVal - bVal
      }
      tableData.sort((a, b) => {
        if (sortDirection === 'asc') {
          return sorter(a, b)
        } else {
          return sorter(b, a)
        }
      })
    }
  }

  sortTableDataAsync = async (selectedColumn: Object, tableData: Array<Object>, ascending: boolean) => {
    return new Promise((resolve, reject) => {
      try {
        workerInstance.addEventListener('message', e => {
          console.log('Received response')
          resolve(e.data)
        }, false)
        workerInstance.postMessage({key: selectedColumn.key, module_id: selectedColumn.module_id, dataIndex: selectedColumn.dataIndex, tableData, ascending})
      } catch (err) {
        reject(err)
      }
    })
  }

  getSelectedColumnStats = (selectedColumnValues: Array<number>) => {
    const cleanData = selectedColumnValues.filter(val => val)
    if (cleanData?.length > 0) {
      let numBuckets = 5
      if (cleanData.length < numBuckets) {
        numBuckets = cleanData.length
      }
      const ckmeansResult = ckmeans(cleanData, numBuckets).map(cluster => {
        return {
          min: min(cluster), // for stops in the map we just need to know where cluster begins
          max: max(cluster),
          count: cluster.length,
          cluster
        }
      })
      return {
        min: min(cleanData),
        max: max(cleanData),
        mean: mean(cleanData),
        median: median(cleanData),
        sum: sum(cleanData),
        ckmeans: ckmeansResult,
        equalIntervalBreaks: equalIntervalBreaks(cleanData, numBuckets)
      }
    } else {
      return {}
    }
  }
}
