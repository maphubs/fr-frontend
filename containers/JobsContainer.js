import { Container } from 'unstated'

type JobsContainerState = {
  jobCounts: {
    waiting: number,
    active: number,
    completed: number,
    failed: number,
    delayed: number,
    paused: number
  },
  jobsForList: {
    id: string,
    timestamp: string,
    processedOn: string,
    finishedOn: string,
    progress: number,
    data: Object,
    attemptsMade: number
  }

}

export default class JobsContainer extends Container<JobsContainerState> {
  state = {
    jobCounts: undefined,
    jobsForList: undefined
  }

  setJobs = (jobCounts, jobsForList) => {
    return this.setState(state => ({ jobCounts, jobsForList }))
  }

  deleteJob = (job_id) => {
    // TODO: remove the job from jobsForList
  }
}
