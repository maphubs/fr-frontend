// @flow
import { Container } from 'unstated'
import turfBuffer from '../utils/patched-turf-buffer'
import turfArea from '@turf/area'
import Cards from '../components/Cards/init'
import {getLayerFRActive} from '../components/aoi/map/layer-feature'
import turfbbox from '@turf/bbox'
import uuidv4 from 'uuid/v4'

import buildMapStyle from '../utils/build-map-style'

import type { MapHubsLayer } from '../types/maphubsLayer'

export type AOIDashboardState = {
  list: Object,
  results: Object,
  toggles: Object,
  area: number,
  geom: Object, // GeoJSON geometry
  bbox: Array<number>,
  feature: Object, // original GeoJSON feature
  showToggle: boolean,
  locale: string,
  info?: any,
  buffer: number,
  isBuffered?: boolean,
  bufferFeature?: Object,
  mapLayers?: Array<MapHubsLayer>,
  glStyle?: Object,
  mapState: Object, // copy of MapContainer state from MapHubs map
  mapComponent: Object, // copy of InteractiveMap component
  gladResult: Object,
  fireResult: Object,
  dashboard: Object,
  resultIDFilter: string
}

type Props = {aoi: any, results: Array<Object>, list: any}

export default class AOIDashboardContainer extends Container<AOIDashboardState> {
  constructor (props: Props) {
    super()
    this.init(props)
  }

  containers: Array<Object> // stores card container classes

  init (props: Props) {
    let geom
    let area
    let bbox
    let isBuffered
    let bufferFeature
    let buffer
    let resultIDFilter = 'default'
    let results

    const { aoi, list } = props
    if (aoi) {
      geom = aoi.feature.geometry
      if (geom.type === 'Point' || geom.type === 'LineString' || geom.type === 'MultiLineString') {
        buffer = 25
        bufferFeature = turfBuffer(aoi.feature, buffer, {steps: 128})
        if (bufferFeature) {
          geom = bufferFeature.geometry
          isBuffered = true
          resultIDFilter = `buffer-${25}`
        }
      } else {
        geom = aoi.feature.geometry
      }
      area = (turfArea(geom) / 10000)
      bbox = turfbbox(geom)
    }

    const toggles = {}

    const feature = bufferFeature || geom
    if (props.results) {
      results = {}
      props.results.forEach(r => {
        if (!results[r.module.id]) results[r.module.id] = {}
        results[r.module.id][r.result_id] = Object.assign(r.summary, r.details)
      })

      const mapLayers = []
      this.containers = []

      const modules = list.modules

      let foundFirstGLADCard = false
      let foundFirstFireCard = false

      modules.map(async (mod) => {
        let card = mod.card
        // only allow the first GLAD and Fire cards
        if (card.id === 'glad') {
          if (foundFirstGLADCard) {
            return
          } else {
            foundFirstGLADCard = true
          }
        }
        if (card.id === 'fires') {
          if (foundFirstFireCard) {
            return
          } else {
            foundFirstFireCard = true
          }
        }
        const layerConfig = mod.layer
        if (!card) {
          // assume a single-stat module by default
          card = {
            id: 'single-stat',
            type: 'config',
            toggled: true,
            value: 'area'
          }
        }
        if (!layerConfig) {
          console.error(`Missing a layer config for module: ${module.id}`)
        }
        toggles[mod.id] = card.toggled
        const CardContainer = Cards[card.id]
        let container
        if (CardContainer) {
          container = new CardContainer()
          this.containers.push(container)
        }
        if (container) {
          container.init(card, layerConfig, {results, resultIDFilter, moduleID: card.moduleID})
          const layer = container.getLayer(card.toggled)
          if (layer) mapLayers.push(layer)
        } else {
          console.error(`Card ${card.id} missing container`)
        }
      })
      const id = uuidv4()
      const featureLayer = getLayerFRActive({id, short_id: id }, feature)
      mapLayers.push(featureLayer)

      const { mapLayersSorted, glStyle } = this.updateStyleFromLayers(mapLayers)
      this.state = {
        aoi,
        list,
        resultIDFilter,
        feature: aoi.feature,
        results,
        mapLayers: mapLayersSorted,
        glStyle,
        geom,
        bbox,
        area,
        isBuffered,
        bufferFeature,
        buffer,
        showToggle: true,
        locale: props.locale,
        toggles
      }
    }
  }

  updateStyleFromLayers = (mapLayers: Array<Object>) => {
    // reorder layers based on priority to improve overlay on raster
    const layersGroupedByPriority = [[], [], [], [], [], [], [], [], [], [], []]
    let mapLayersSorted

    mapLayers.forEach(lyr => {
      if (!lyr.style) {
        console.error('map layer missing style')
      } else {
        const style = lyr.style
        if (style.metadata) {
          const priority = style.metadata['maphubs:priority']
          layersGroupedByPriority[priority - 1].push(lyr)
        } else {
          console.error(`style missing metadata ${lyr.short_id}`)
          layersGroupedByPriority[2].push(lyr)
        }
      }
    })
    mapLayersSorted = []

    layersGroupedByPriority.forEach(lyrGrp => {
      mapLayersSorted = mapLayersSorted.concat(lyrGrp)
    })

    const glStyle = buildMapStyle(mapLayersSorted)
    return {
      mapLayersSorted,
      glStyle
    }
  }

  registerMapComponent = (mapComponent: Object, mapState: Object) => {
    return this.setState(state => ({ mapComponent, mapState }))
  }

  toggle = (id: string, checked: boolean) => {
    // updateMap
    const { mapComponent } = this.state

    if (mapComponent) {
      mapComponent.toggleVisibility(id)
    } else {
      console.error('Map component not found')
    }

    // save state
    const toggles = this.state.toggles
    toggles[id] = checked
    this.setState({
      toggles
    })
  }

  showInfo = (info: any) => {
    this.setState({ info })
  }

  hideInfo = () => {
    this.setState({ info: undefined })
  }

  onGLADClick = (alert: Object) => {
    this.setGLADGeoJSON(alert.features, true)
  }

   onFireClick = (alert: Object) => {
     this.setFireGeoJSON(alert.features, true)
   }

  setGLADGeoJSON = (features: Array<Object>, zoomToAlerts?: boolean = false) => {
    if (this.state.mapState && this.state.mapState.state.map) {
      const mapboxGL = this.state.mapState.state.map.map
      const geoJSONData = mapboxGL.getSource('fr-glad-geojson')
      const data = {
        type: 'FeatureCollection',
        features: features
      }
      if (geoJSONData) {
        geoJSONData.setData(data)
        if (zoomToAlerts) {
          const bbox = turfbbox(data)
          const bounds = [[bbox[0], bbox[1]], [bbox[2], bbox[3]]]
          mapboxGL.fitBounds(bounds, {padding: 25, curve: 3, speed: 0.6, maxZoom: 13})
        }
      }
    }
  }

  setFireGeoJSON = (features: Array<Object>, zoomToAlerts?: boolean = false) => {
    if (this.state.mapState && this.state.mapState.state.map) {
      const mapboxGL = this.state.mapState.state.map.map
      const geoJSONData = mapboxGL.getSource('fr-fires-geojson')
      const data = {
        type: 'FeatureCollection',
        features: features
      }
      geoJSONData.setData(data)
      if (zoomToAlerts) {
        const bbox = turfbbox(data)
        const bounds = [[bbox[0], bbox[1]], [bbox[2], bbox[3]]]
        mapboxGL.fitBounds(bounds, {padding: 25, curve: 3, speed: 0.6, maxZoom: 13})
      }
    }
  }

  onGLADClearSelected = () => {
    // return map data to full year (and density if selected)
    console.log('GLAD clear selected')
    this.changeGLAD(this.state.gladResult, true)
  }

  onFireClearSelected = () => {
    // return map data to full year (and density if selected)
    console.log('GLAD clear selected')
    this.changeFires(this.state.fireResult, true)
  }

  changeGLAD = (gladResult: Object, zoomToAlerts?: boolean = false) => {
    console.log('change GLAD')
    this.setGLADGeoJSON(gladResult.geoms, zoomToAlerts)
    this.setState({gladResult})
  }

  changeFires = (fireResult: Object, zoomToAlerts?: boolean = false) => {
    console.log('change Fires')
    this.setFireGeoJSON(fireResult.features, zoomToAlerts)
    this.setState({fireResult})
  }

  addLayer = async (layer: Object) => {
    await this.setState(state => {
      const { mapLayers } = state
      const newLayers = mapLayers.concat([layer])
      const { mapLayersSorted, glStyle } = this.updateStyleFromLayers(newLayers)
      return {
        mapLayers: mapLayersSorted,
        glStyle
      }
    })
  }

  removeLayer = async (layer_id: string) => {
    await this.setState(state => {
      const newLayers = []
      const { mapLayers } = state
      mapLayers.forEach(layer => {
        if (layer.layer_id !== layer_id) {
          newLayers.push(layer)
        }
      })
      const { mapLayersSorted, glStyle } = this.updateStyleFromLayers(newLayers)
      return {
        mapLayers: mapLayersSorted,
        glStyle
      }
    })
  }

  updateLayer = (layer: Object) => {
    const { mapLayers, mapComponent } = this.state
    let index
    mapLayers.forEach((lyr, i) => {
      if (lyr.id === layer.id) {
        index = i
      }
    })
    mapLayers.splice(index, 1, layer)
    this.setState({
      mapLayers
    })
    mapComponent.updateLayers(mapLayers, true)
  }

  changeBuffer = async (buffer: number) => {
    const { feature } = this.state
    let isBuffered
    let bufferFeature
    if (feature) {
      const geom = feature.geometry
      if (geom.type === 'Point' || geom.type === 'LineString' || geom.type === 'MultiLineString') {
        isBuffered = true
        bufferFeature = turfBuffer(geom, buffer, {steps: 128})
        bufferFeature.properties.buffer = buffer
        const area = (turfArea(bufferFeature) / 10000)
        const bbox = turfbbox(bufferFeature)
        const resultIDFilter = `buffer-${buffer}`
        this.setState({bufferFeature, isBuffered, buffer, area, bbox, resultIDFilter})

        // update the map feature
        const mapboxGL = this.state.mapState.state.map.map
        const geoJSONData = mapboxGL.getSource('omh-feature-geojson')
        const data = {
          type: 'FeatureCollection',
          features: [bufferFeature]
        }
        geoJSONData.setData(data)
        const bounds = [[bbox[0], bbox[1]], [bbox[2], bbox[3]]]
        mapboxGL.fitBounds(bounds, {padding: 25, curve: 3, speed: 0.6})

        // call update function for each card
        await Promise.all(this.containers.map(async (container) => {
          if (container) {
            await container.update(this.state)
          }
        }))
      }
    }
  }
}
