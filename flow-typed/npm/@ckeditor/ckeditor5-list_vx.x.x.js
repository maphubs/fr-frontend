// flow-typed signature: 162d39d000c336499e8625614a94bb7c
// flow-typed version: <<STUB>>/@ckeditor/ckeditor5-list_v^11.0.1/flow_v0.77.0

/**
 * This is an autogenerated libdef stub for:
 *
 *   '@ckeditor/ckeditor5-list'
 *
 * Fill this stub out by replacing all the `any` types.
 *
 * Once filled out, we encourage you to share your work with the
 * community by sending a pull request to:
 * https://github.com/flowtype/flow-typed
 */

declare module '@ckeditor/ckeditor5-list' {
  declare module.exports: any;
}

/**
 * We include stubs for each file inside this npm package in case you need to
 * require those files directly. Feel free to delete any files that aren't
 * needed.
 */
declare module '@ckeditor/ckeditor5-list/src/converters' {
  declare module.exports: any;
}

declare module '@ckeditor/ckeditor5-list/src/indentcommand' {
  declare module.exports: any;
}

declare module '@ckeditor/ckeditor5-list/src/list' {
  declare module.exports: any;
}

declare module '@ckeditor/ckeditor5-list/src/listcommand' {
  declare module.exports: any;
}

declare module '@ckeditor/ckeditor5-list/src/listediting' {
  declare module.exports: any;
}

declare module '@ckeditor/ckeditor5-list/src/listui' {
  declare module.exports: any;
}

declare module '@ckeditor/ckeditor5-list/src/utils' {
  declare module.exports: any;
}

// Filename aliases
declare module '@ckeditor/ckeditor5-list/src/converters.js' {
  declare module.exports: $Exports<'@ckeditor/ckeditor5-list/src/converters'>;
}
declare module '@ckeditor/ckeditor5-list/src/indentcommand.js' {
  declare module.exports: $Exports<'@ckeditor/ckeditor5-list/src/indentcommand'>;
}
declare module '@ckeditor/ckeditor5-list/src/list.js' {
  declare module.exports: $Exports<'@ckeditor/ckeditor5-list/src/list'>;
}
declare module '@ckeditor/ckeditor5-list/src/listcommand.js' {
  declare module.exports: $Exports<'@ckeditor/ckeditor5-list/src/listcommand'>;
}
declare module '@ckeditor/ckeditor5-list/src/listediting.js' {
  declare module.exports: $Exports<'@ckeditor/ckeditor5-list/src/listediting'>;
}
declare module '@ckeditor/ckeditor5-list/src/listui.js' {
  declare module.exports: $Exports<'@ckeditor/ckeditor5-list/src/listui'>;
}
declare module '@ckeditor/ckeditor5-list/src/utils.js' {
  declare module.exports: $Exports<'@ckeditor/ckeditor5-list/src/utils'>;
}
