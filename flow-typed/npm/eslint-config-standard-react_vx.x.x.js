// flow-typed signature: b59d8d8a74349e9e2bfeb2fc5d9f8e3a
// flow-typed version: <<STUB>>/eslint-config-standard-react_v^9.2.0/flow_v0.116.1

/**
 * This is an autogenerated libdef stub for:
 *
 *   'eslint-config-standard-react'
 *
 * Fill this stub out by replacing all the `any` types.
 *
 * Once filled out, we encourage you to share your work with the
 * community by sending a pull request to:
 * https://github.com/flowtype/flow-typed
 */

declare module 'eslint-config-standard-react' {
  declare module.exports: any;
}

/**
 * We include stubs for each file inside this npm package in case you need to
 * require those files directly. Feel free to delete any files that aren't
 * needed.
 */


// Filename aliases
declare module 'eslint-config-standard-react/index' {
  declare module.exports: $Exports<'eslint-config-standard-react'>;
}
declare module 'eslint-config-standard-react/index.js' {
  declare module.exports: $Exports<'eslint-config-standard-react'>;
}
