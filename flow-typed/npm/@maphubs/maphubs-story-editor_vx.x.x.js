// flow-typed signature: 90b95332cea25f58f947d2498410de70
// flow-typed version: <<STUB>>/@maphubs/maphubs-story-editor_v^0.1.4/flow_v0.116.1

/**
 * This is an autogenerated libdef stub for:
 *
 *   '@maphubs/maphubs-story-editor'
 *
 * Fill this stub out by replacing all the `any` types.
 *
 * Once filled out, we encourage you to share your work with the
 * community by sending a pull request to:
 * https://github.com/flowtype/flow-typed
 */

declare module '@maphubs/maphubs-story-editor' {
  declare module.exports: any;
}

/**
 * We include stubs for each file inside this npm package in case you need to
 * require those files directly. Feel free to delete any files that aren't
 * needed.
 */
declare module '@maphubs/maphubs-story-editor/build/ckeditor' {
  declare module.exports: any;
}

declare module '@maphubs/maphubs-story-editor/build/translations/de' {
  declare module.exports: any;
}

declare module '@maphubs/maphubs-story-editor/build/translations/es' {
  declare module.exports: any;
}

declare module '@maphubs/maphubs-story-editor/build/translations/fr' {
  declare module.exports: any;
}

declare module '@maphubs/maphubs-story-editor/build/translations/id' {
  declare module.exports: any;
}

declare module '@maphubs/maphubs-story-editor/build/translations/it' {
  declare module.exports: any;
}

declare module '@maphubs/maphubs-story-editor/build/translations/pt' {
  declare module.exports: any;
}

// Filename aliases
declare module '@maphubs/maphubs-story-editor/build/ckeditor.js' {
  declare module.exports: $Exports<'@maphubs/maphubs-story-editor/build/ckeditor'>;
}
declare module '@maphubs/maphubs-story-editor/build/translations/de.js' {
  declare module.exports: $Exports<'@maphubs/maphubs-story-editor/build/translations/de'>;
}
declare module '@maphubs/maphubs-story-editor/build/translations/es.js' {
  declare module.exports: $Exports<'@maphubs/maphubs-story-editor/build/translations/es'>;
}
declare module '@maphubs/maphubs-story-editor/build/translations/fr.js' {
  declare module.exports: $Exports<'@maphubs/maphubs-story-editor/build/translations/fr'>;
}
declare module '@maphubs/maphubs-story-editor/build/translations/id.js' {
  declare module.exports: $Exports<'@maphubs/maphubs-story-editor/build/translations/id'>;
}
declare module '@maphubs/maphubs-story-editor/build/translations/it.js' {
  declare module.exports: $Exports<'@maphubs/maphubs-story-editor/build/translations/it'>;
}
declare module '@maphubs/maphubs-story-editor/build/translations/pt.js' {
  declare module.exports: $Exports<'@maphubs/maphubs-story-editor/build/translations/pt'>;
}
