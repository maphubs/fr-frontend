// flow-typed signature: ea245679aefa63cbfdd5041c1a47d6ae
// flow-typed version: <<STUB>>/@uppy/react_v^1.4.2/flow_v0.116.1

/**
 * This is an autogenerated libdef stub for:
 *
 *   '@uppy/react'
 *
 * Fill this stub out by replacing all the `any` types.
 *
 * Once filled out, we encourage you to share your work with the
 * community by sending a pull request to:
 * https://github.com/flowtype/flow-typed
 */

declare module '@uppy/react' {
  declare module.exports: any;
}

/**
 * We include stubs for each file inside this npm package in case you need to
 * require those files directly. Feel free to delete any files that aren't
 * needed.
 */
declare module '@uppy/react/lib/Dashboard' {
  declare module.exports: any;
}

declare module '@uppy/react/lib/DashboardModal' {
  declare module.exports: any;
}

declare module '@uppy/react/lib/DragDrop' {
  declare module.exports: any;
}

declare module '@uppy/react/lib/ProgressBar' {
  declare module.exports: any;
}

declare module '@uppy/react/lib/propTypes' {
  declare module.exports: any;
}

declare module '@uppy/react/lib/StatusBar' {
  declare module.exports: any;
}

declare module '@uppy/react/lib/Wrapper' {
  declare module.exports: any;
}

declare module '@uppy/react/src/__mocks__/DashboardPlugin' {
  declare module.exports: any;
}

declare module '@uppy/react/src/__mocks__/DragDropPlugin' {
  declare module.exports: any;
}

declare module '@uppy/react/src/__mocks__/ProgressBarPlugin' {
  declare module.exports: any;
}

declare module '@uppy/react/src/__mocks__/StatusBarPlugin' {
  declare module.exports: any;
}

declare module '@uppy/react/src/Dashboard' {
  declare module.exports: any;
}

declare module '@uppy/react/src/Dashboard.test' {
  declare module.exports: any;
}

declare module '@uppy/react/src/DashboardModal' {
  declare module.exports: any;
}

declare module '@uppy/react/src/DashboardModal.test' {
  declare module.exports: any;
}

declare module '@uppy/react/src/DragDrop' {
  declare module.exports: any;
}

declare module '@uppy/react/src/DragDrop.test' {
  declare module.exports: any;
}

declare module '@uppy/react/src/ProgressBar' {
  declare module.exports: any;
}

declare module '@uppy/react/src/ProgressBar.test' {
  declare module.exports: any;
}

declare module '@uppy/react/src/propTypes' {
  declare module.exports: any;
}

declare module '@uppy/react/src/StatusBar' {
  declare module.exports: any;
}

declare module '@uppy/react/src/StatusBar.test' {
  declare module.exports: any;
}

declare module '@uppy/react/src/Wrapper' {
  declare module.exports: any;
}

// Filename aliases
declare module '@uppy/react/index' {
  declare module.exports: $Exports<'@uppy/react'>;
}
declare module '@uppy/react/index.js' {
  declare module.exports: $Exports<'@uppy/react'>;
}
declare module '@uppy/react/lib/Dashboard.js' {
  declare module.exports: $Exports<'@uppy/react/lib/Dashboard'>;
}
declare module '@uppy/react/lib/DashboardModal.js' {
  declare module.exports: $Exports<'@uppy/react/lib/DashboardModal'>;
}
declare module '@uppy/react/lib/DragDrop.js' {
  declare module.exports: $Exports<'@uppy/react/lib/DragDrop'>;
}
declare module '@uppy/react/lib/ProgressBar.js' {
  declare module.exports: $Exports<'@uppy/react/lib/ProgressBar'>;
}
declare module '@uppy/react/lib/propTypes.js' {
  declare module.exports: $Exports<'@uppy/react/lib/propTypes'>;
}
declare module '@uppy/react/lib/StatusBar.js' {
  declare module.exports: $Exports<'@uppy/react/lib/StatusBar'>;
}
declare module '@uppy/react/lib/Wrapper.js' {
  declare module.exports: $Exports<'@uppy/react/lib/Wrapper'>;
}
declare module '@uppy/react/src/__mocks__/DashboardPlugin.js' {
  declare module.exports: $Exports<'@uppy/react/src/__mocks__/DashboardPlugin'>;
}
declare module '@uppy/react/src/__mocks__/DragDropPlugin.js' {
  declare module.exports: $Exports<'@uppy/react/src/__mocks__/DragDropPlugin'>;
}
declare module '@uppy/react/src/__mocks__/ProgressBarPlugin.js' {
  declare module.exports: $Exports<'@uppy/react/src/__mocks__/ProgressBarPlugin'>;
}
declare module '@uppy/react/src/__mocks__/StatusBarPlugin.js' {
  declare module.exports: $Exports<'@uppy/react/src/__mocks__/StatusBarPlugin'>;
}
declare module '@uppy/react/src/Dashboard.js' {
  declare module.exports: $Exports<'@uppy/react/src/Dashboard'>;
}
declare module '@uppy/react/src/Dashboard.test.js' {
  declare module.exports: $Exports<'@uppy/react/src/Dashboard.test'>;
}
declare module '@uppy/react/src/DashboardModal.js' {
  declare module.exports: $Exports<'@uppy/react/src/DashboardModal'>;
}
declare module '@uppy/react/src/DashboardModal.test.js' {
  declare module.exports: $Exports<'@uppy/react/src/DashboardModal.test'>;
}
declare module '@uppy/react/src/DragDrop.js' {
  declare module.exports: $Exports<'@uppy/react/src/DragDrop'>;
}
declare module '@uppy/react/src/DragDrop.test.js' {
  declare module.exports: $Exports<'@uppy/react/src/DragDrop.test'>;
}
declare module '@uppy/react/src/ProgressBar.js' {
  declare module.exports: $Exports<'@uppy/react/src/ProgressBar'>;
}
declare module '@uppy/react/src/ProgressBar.test.js' {
  declare module.exports: $Exports<'@uppy/react/src/ProgressBar.test'>;
}
declare module '@uppy/react/src/propTypes.js' {
  declare module.exports: $Exports<'@uppy/react/src/propTypes'>;
}
declare module '@uppy/react/src/StatusBar.js' {
  declare module.exports: $Exports<'@uppy/react/src/StatusBar'>;
}
declare module '@uppy/react/src/StatusBar.test.js' {
  declare module.exports: $Exports<'@uppy/react/src/StatusBar.test'>;
}
declare module '@uppy/react/src/Wrapper.js' {
  declare module.exports: $Exports<'@uppy/react/src/Wrapper'>;
}
