// flow-typed signature: cf476bc9cbfd6e9f92d8e3b3f1f5e203
// flow-typed version: <<STUB>>/@uppy/tus_v^1.5.2/flow_v0.116.1

/**
 * This is an autogenerated libdef stub for:
 *
 *   '@uppy/tus'
 *
 * Fill this stub out by replacing all the `any` types.
 *
 * Once filled out, we encourage you to share your work with the
 * community by sending a pull request to:
 * https://github.com/flowtype/flow-typed
 */

declare module '@uppy/tus' {
  declare module.exports: any;
}

/**
 * We include stubs for each file inside this npm package in case you need to
 * require those files directly. Feel free to delete any files that aren't
 * needed.
 */
declare module '@uppy/tus/lib/getFingerprint' {
  declare module.exports: any;
}

declare module '@uppy/tus/lib' {
  declare module.exports: any;
}

declare module '@uppy/tus/src/getFingerprint' {
  declare module.exports: any;
}

declare module '@uppy/tus/src' {
  declare module.exports: any;
}

// Filename aliases
declare module '@uppy/tus/lib/getFingerprint.js' {
  declare module.exports: $Exports<'@uppy/tus/lib/getFingerprint'>;
}
declare module '@uppy/tus/lib/index' {
  declare module.exports: $Exports<'@uppy/tus/lib'>;
}
declare module '@uppy/tus/lib/index.js' {
  declare module.exports: $Exports<'@uppy/tus/lib'>;
}
declare module '@uppy/tus/src/getFingerprint.js' {
  declare module.exports: $Exports<'@uppy/tus/src/getFingerprint'>;
}
declare module '@uppy/tus/src/index' {
  declare module.exports: $Exports<'@uppy/tus/src'>;
}
declare module '@uppy/tus/src/index.js' {
  declare module.exports: $Exports<'@uppy/tus/src'>;
}
