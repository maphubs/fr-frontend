// flow-typed signature: ba99e3ae99c6b88f58370f5190199e9d
// flow-typed version: <<STUB>>/unstated_v2.1.1/flow_v0.105.2

/**
 * This is an autogenerated libdef stub for:
 *
 *   'unstated'
 *
 * Fill this stub out by replacing all the `any` types.
 *
 * Once filled out, we encourage you to share your work with the
 * community by sending a pull request to:
 * https://github.com/flowtype/flow-typed
 */

declare module 'unstated' {
  declare module.exports: any;
}

/**
 * We include stubs for each file inside this npm package in case you need to
 * require those files directly. Feel free to delete any files that aren't
 * needed.
 */
declare module 'unstated/lib/unstated.es' {
  declare module.exports: any;
}

declare module 'unstated/lib/unstated' {
  declare module.exports: any;
}

// Filename aliases
declare module 'unstated/lib/unstated.es.js' {
  declare module.exports: $Exports<'unstated/lib/unstated.es'>;
}
declare module 'unstated/lib/unstated.js' {
  declare module.exports: $Exports<'unstated/lib/unstated'>;
}
