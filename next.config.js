
const withLess = require('@zeit/next-less')
const withCSS = require('@zeit/next-css')
const lessToJS = require('less-vars-to-js')
const withTM = require('next-transpile-modules')
const fs = require('fs')
const path = require('path')
const mapHubsConfig = require('./config')
const FilterWarningsPlugin = require('webpack-filter-warnings-plugin')

// fix: prevents error when .less files are required by node
if (typeof require !== 'undefined') {
  // eslint-disable-next-line node/no-deprecated-api
  require.extensions['.less'] = (file) => {}
}

const {ANALYZE, ASSET_CDN_PREFIX} = process.env
const useCDN = (ASSET_CDN_PREFIX && process.env.NODE_ENV === 'production')
const pathToMapboxGL = path.resolve(__dirname, './node_modules/mapbox-gl/dist/mapbox-gl.js')

const themeVariables = lessToJS(
  fs.readFileSync(path.resolve(__dirname, './theme.less'), 'utf8')
)

let WBSAP
if (process.env.NODE_ENV !== 'production') {
  const {WebpackBundleSizeAnalyzerPlugin} = require('webpack-bundle-size-analyzer')
  WBSAP = WebpackBundleSizeAnalyzerPlugin
}

module.exports = withCSS(withLess(withTM({
  publicRuntimeConfig: {
    API_URL: mapHubsConfig.API_URL,
    API_URL_INTERNAL: mapHubsConfig.API_URL_INTERNAL,
    MAPBOX_ACCESS_TOKEN: mapHubsConfig.MAPBOX_ACCESS_TOKEN,
    BING_KEY: mapHubsConfig.BING_KEY,
    OPENROUTESERVICE_API_KEY: mapHubsConfig.OPENROUTESERVICE_API_KEY,
    EARTHENGINE_CLIENTID: mapHubsConfig.EARTHENGINE_CLIENTID,
    logoSmall: mapHubsConfig.logoSmall,
    logoSmallWidth: mapHubsConfig.logoSmallWidth,
    logoSmallHeight: mapHubsConfig.logoSmallHeight,
    primaryColor: mapHubsConfig.primaryColor,
    RASTER_UPLOAD_API: mapHubsConfig.RASTER_UPLOAD_API,
    RASTER_UPLOAD_API_KEY: mapHubsConfig.RASTER_UPLOAD_API_KEY,
    host: mapHubsConfig.host,
    ASSET_UPLOAD_API: mapHubsConfig.ASSET_UPLOAD_API,
    ASSET_UPLOAD_API_KEY: mapHubsConfig.ASSET_UPLOAD_API_KEY,
    AUTH0_DOMAIN: mapHubsConfig.AUTH0_DOMAIN,
    AUTH0_CLIENT_ID: mapHubsConfig.AUTH0_CLIENT_ID,
    DEV: process.env.NODE_ENV !== 'production',
    APP_ENABLED: mapHubsConfig.APP_ENABLED,
    APP_DOWNLOAD_URL: mapHubsConfig.APP_DOWNLOAD_URL,
    APP_VERSION: mapHubsConfig.APP_VERSION,
    LANGUAGES: mapHubsConfig.LANGUAGES
  },
  transpileModules: ['react-dnd', 'react-dnd-html5-backend'],
  lessLoaderOptions: {
    modifyVars: themeVariables,
    javascriptEnabled: true
  },
  exportPathMap: () => {
    return {}
  },
  assetPrefix: useCDN ? ASSET_CDN_PREFIX : '',
  poweredByHeader: false,
  webpack (config, { dev, isServer }) {
    // console.log(JSON.stringify(config.module.rules, null, 2))
    config.plugins.push(
      new FilterWarningsPlugin({
        exclude: /mini-css-extract-plugin[^]*Conflicting order between:/
      })
    )
    if (dev) {
      config.devtool = 'cheap-eval-source-map'
    } else {
      config.devtool = 'source-map'
      if (config.optimization && config.optimization.minimizer) {
        for (const plugin of config.optimization.minimizer) {
          if (plugin.constructor.name === 'TerserPlugin') {
            plugin.options.sourceMap = true
            break
          }
        }
      }
    }
    if (isServer) {
      const antStyles = /antd\/.*?\/style.*?/
      const origExternals = [...config.externals]
      config.externals = [
        (context, request, callback) => {
          if (request.match(antStyles)) return callback()
          if (typeof origExternals[0] === 'function') {
            origExternals[0](context, request, callback)
          } else {
            callback()
          }
        },
        ...(typeof origExternals[0] === 'function' ? [] : origExternals)
      ]

      config.module.rules.unshift({
        test: antStyles,
        use: 'null-loader'
      })
    }

    if (!config.node) config.node = {}
    config.node.fs = 'empty'

    config.module.rules.push({
      test: /ckeditor5-[^/]+\/theme\/icons\/[^/]+\.svg$/,
      use: ['raw-loader']
    })

    config.module.rules.push({
      test: /\.(glsl|vert|frag)(\??.*)$/,
      use: [{ loader: 'raw-loader' }]
    })

    config.module.rules.push({
      test: /\.js.map$/,
      use: [{ loader: 'ignore-loader' }]
    })

    if (!config.module.noParse) {
      config.module.noParse = []
    }
    config.module.noParse.push(pathToMapboxGL)

    if (ANALYZE) {
      config.plugins.push(
        new WBSAP('stats.txt')
      )
    }

    return config
  }
})))
