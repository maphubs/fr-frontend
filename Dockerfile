FROM node:12-alpine as builder
ENV NODE_ENV=production
WORKDIR /app
RUN apk add --no-cache --upgrade git python make gcc g++
COPY package.json package-lock.json /app/
RUN npm config set '@bit:registry' https://node.bitsrc.io && \
    npm install --production

# Bundle app source
FROM node:12-alpine
ENV NODE_ENV=production
WORKDIR /app
COPY --from=builder /app .
COPY . .
EXPOSE 4004
CMD ["node", "/app/server.js"]