// @flow
module.exports = function (app: any, server: any) {
  server.get('/list/:id/:name', (req, res) => {
    return app.render(req, res, '/list/view', { id: req.params.id, name: req.params.name })
  })

  server.get('/list/:id', (req, res) => {
    return app.render(req, res, '/list/view', { id: req.params.id })
  })

  server.get('/listsummary/:id/:aggField/:name', (req, res) => {
    return app.render(req, res, '/list/summary', { id: req.params.id, aggField: req.params.aggField, name: req.params.name })
  })

  server.get('/listsummary/:id/:aggField', (req, res) => {
    return app.render(req, res, '/list/summary', { id: req.params.id, aggField: req.params.aggField })
  })
}
