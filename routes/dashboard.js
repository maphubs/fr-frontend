// @flow
module.exports = function (app: any, server: any) {
  server.get('/dashboard/:listid/:aoiid/:listname/:name', (req, res) => {
    return app.render(req, res, '/dashboard', {
      aoiID: req.params.aoiid,
      listID: req.params.listid,
      listName: req.params.listname,
      name: req.params.name
    })
  })

  server.get('/dashboard/:listid/:aoiid', (req, res) => {
    return app.render(req, res, '/dashboard', {
      aoiID: req.params.aoiid,
      listID: req.params.listid,
      name: req.params.name
    })
  })
}
