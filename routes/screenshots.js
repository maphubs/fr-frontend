// @flow
module.exports = function (app: any, server: any) {
  server.get('/screenshots/module/card/:report_id/:aoi_id/:module_id/:result_id', (req, res) => {
    return app.render(req, res, '/screenshots/module/card', {
      report_id: req.params.report_id,
      aoi_id: req.params.aoi_id,
      module_id: req.params.module_id,
      result_id: req.params.result_id
    })
  })

  server.get('/screenshots/module/map/:report_id/:aoi_id/:module_id/:result_id', (req, res) => {
    return app.render(req, res, '/screenshots/module/map', {
      report_id: req.params.report_id,
      aoi_id: req.params.aoi_id,
      module_id: req.params.module_id,
      result_id: req.params.result_id
    })
  })

  server.get('/screenshots/overview/:id', (req, res) => {
    return app.render(req, res, '/screenshots/overview', {id: req.params.id})
  })

  server.get('/screenshots/report/drawing/:type/:report_id/:aoi_id/:drawing_id/:result_id', (req, res) => {
    return app.render(req, res, '/screenshots/report/drawing',
      {
        report_id: req.params.report_id,
        aoi_id: req.params.aoi_id,
        type: req.params.type,
        drawing_id: req.params.drawing_id,
        result_id: req.params.result_id
      })
  })
}
