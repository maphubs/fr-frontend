// @flow
module.exports = function (app: any, server: any) {
  server.get('/report/edit/:id/:title', (req, res) => {
    return app.render(req, res, '/report/edit', {id: req.params.id, title: req.params.title})
  })

  server.get('/report/edit/:id', (req, res) => {
    return app.render(req, res, '/report/edit', { id: req.params.id })
  })

  server.get('/report/view/:id/:title', (req, res) => {
    return app.render(req, res, '/report/view', {id: req.params.id, title: req.params.title})
  })

  server.get('/report/fullscreen/:id/:title', (req, res) => {
    return app.render(req, res, '/report/view', {id: req.params.id, title: req.params.title})
  })
}
