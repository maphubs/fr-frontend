const passport = require('passport')
const config = require('../config')
require('isomorphic-unfetch')

module.exports = function (app: any, server: any) {
  server.get('/login', passport.authenticate('auth0', {
    scope: 'openid email profile',
    audience: 'https://users.maphubs.com'
  }), (req, res) => res.redirect('/'))

  server.get('/callback', (req, res, next) => {
    passport.authenticate('auth0', (err, user) => {
      if (err) return next(err)
      if (!user) return res.redirect('/login')
      req.logIn(user, (err) => {
        if (err) return next(err)
        res.redirect('/')
      })
    })(req, res, next)
  })

  server.get('/signup', passport.authenticate('auth0', {
    audience: 'https://users.maphubs.com',
    responseType: 'code',
    scope: 'openid profile email'
  })
  , (req, res) => res.redirect('/'))

  server.get('/signup/invite/:key', async (req, res, next) => {
    const inviteKey = req.params.key
    if (inviteKey) {
      // check key using the API
      let body
      try {
        const result = await fetch(`${config.API_URL_INTERNAL}/api/useinvite/${inviteKey}`)
        body = await result.json()
      } catch (err) {
        console.error(err.message)
      }
      console.log(body)
      if (body && body.success) {
        // check if auth0 already
        const middleware = passport.authenticate('auth0', {
          audience: 'https://users.maphubs.com',
          responseType: 'code',
          scope: 'openid profile email',
          callbackURL: `${config.host}/signupcomplete?key=${inviteKey}`
        })
        return middleware(req, res, next)
      } else {
        console.error('failed to lookup invite')
        app.render(req, res, '/user/inviteRequired', {})
      }
    } else {
      app.render(req, res, '/user/inviteRequired', {})
    }
  })

  server.get('/signupcomplete', (req, res, next) => {
    passport.authenticate('auth0', async (err, user) => {
      if (err) return next(err)
      if (!user) return res.redirect('/login')
      user.role = 2 // assign the role manually since the Auth0 metadata won't pickup the role until next login
      const inviteKey = req.query.key
      console.log('SIGNUP COMPLETE')
      if (inviteKey) {
        const url = `${config.API_URL_INTERNAL}/api/completeinvite/${inviteKey}`
        let body
        try {
          const result = await fetch(url, {})
          body = await result.json()
        } catch (err_) {
          console.error(err_.message)
        }
        if (body && body.success) {
          app.render(req, res, '/user/signupComplete', {})
        } else {
          throw new Error(`failed to complete signup ${body.error}`)
        }
      } else {
        app.render(req, res, '/user/inviteRequired', {})
      }
    })(req, res, next)
  })

  server.get('/logout', (req, res) => {
    req.logout()

    const {AUTH0_DOMAIN, AUTH0_CLIENT_ID} = config
    res.redirect(`https://${AUTH0_DOMAIN}/logout?client_id=${AUTH0_CLIENT_ID}&returnTo=https://forest.report`)
  })
}
