// @flow
module.exports = function (app: any, server: any) {
  server.get('/admin/driver/:id/:name', (req, res) => {
    return app.render(req, res, '/admin/driver', { id: req.params.id, name: req.params.name })
  })

  server.get('/admin/driver/:id', (req, res) => {
    return app.render(req, res, '/admin/driver', { id: req.params.id })
  })

  server.get('/admin/module/view/:id/:name', (req, res) => {
    return app.render(req, res, '/admin/module/view', { id: req.params.id, name: req.params.name })
  })

  server.get('/admin/module/view/:id', (req, res) => {
    return app.render(req, res, '/admin/module/view', { id: req.params.id })
  })

  server.get('/admin/list/view/:id/:name', (req, res) => {
    return app.render(req, res, '/admin/list/view', { id: req.params.id, name: req.params.name })
  })

  server.get('/admin/list/view/:id', (req, res) => {
    return app.render(req, res, '/admin/list/view', { id: req.params.id })
  })

  server.get('/admin/aoi/view/:id/:name', (req, res) => {
    return app.render(req, res, '/admin/aoi/view', { id: req.params.id, name: req.params.name })
  })

  server.get('/admin/aoi/view/:id', (req, res) => {
    return app.render(req, res, '/admin/aoi/view', { id: req.params.id })
  })

  server.get('/admin/list/addaois/:id', (req, res) => {
    return app.render(req, res, '/admin/list/addaois', { id: req.params.id })
  })

  server.get('/admin/list/addmodules/:id', (req, res) => {
    return app.render(req, res, '/admin/list/addmodules', { id: req.params.id })
  })

  server.get('/admin/list/addlinkedlists/:id', (req, res) => {
    return app.render(req, res, '/admin/list/addlinkedlists', { id: req.params.id })
  })

  server.get('/admin/list/job/:list_id/:job_id', (req, res) => {
    return app.render(req, res, '/admin/list/job', { list_id: req.params.list_id, job_id: req.params.job_id })
  })

  server.get('/admin/organization/:id/:name', (req, res) => {
    return app.render(req, res, '/admin/organization/edit', { id: req.params.id, name: req.params.name })
  })

  server.get('/admin/organization/:id', (req, res) => {
    return app.render(req, res, '/admin/organization/edit', { id: req.params.id })
  })

  server.get('/admin/user/:id', (req, res) => {
    return app.render(req, res, '/admin/user', { id: req.params.id })
  })
}
