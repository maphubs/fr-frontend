// @flow

export type ReportFooter = {
  pageNumber: number,
  logo: string,
  accentColor: string
}
