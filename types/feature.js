// @flow

export type Feature = {
  type: string,
  properties?: Object,
  geometry: Object
}
