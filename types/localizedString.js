// @flow

export type LocalizedString = {
  en: string,
  es?: string,
  fr?: string,
  pt?: string,
  id?: string
}
