// @flow
export type Photo = {
  base64: string,
  exif: Object,
  width: number,
  height: number
}

export type Location = {
  coords: {
    latitude: number,
    longitude: number,
    altitude: number,
    accuracy: number,
    altitudeAccuracy: number,
    heading: number,
    speed: number
  },
  timestamp: number
}

export type Observation = {
  id: string, // uuid as the id for the observation
  aoi_id: string,
  report_id: string,
  photo: Photo,
  location: Location, // high accuracy GPS location
  notes: string,
  modified?: number
}
