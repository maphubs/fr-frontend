// @flow

import type { LocalizedString } from './localizedString'
import type { MapHubsLayer } from './maphubsLayer'
import type { Feature } from './feature'

export type Drawing = {
  id: string,
  type?: string,
  toggled: boolean,
  name: LocalizedString,
  layer: MapHubsLayer,
  features: Array<Feature>,
  beforeImage?: MapHubsLayer,
  afterImage?: MapHubsLayer,
  areaHA?: Object,
  usingAOIBoundary?: boolean
}
