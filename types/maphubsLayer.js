// @flow
import type { LocalizedString } from './localizedString'

export type MapHubsLayer = {
  layer_id: string,
  short_id: string,
  style: Object,
  name: LocalizedString,
  description: LocalizedString,
  source: LocalizedString,
  external_layer_type?: string,
  external_layer_config?: {
    url?: string,
    tiles?: Array<string>
  },
  legend_html: string,
  bbox: Array<number>
}
