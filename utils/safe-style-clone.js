
export function cloneStyle (style: Object) {
  if (style.version !== 8 || !style.sources || !style.layers) {
    console.error('not a mapbox style')
    return null
  }
  const clonedStyle = {
    version: 8,
    sources: {},
    layers: []
  }

  // clone the sources
  Object.keys(style.sources).map(key => {
    const currentSource = style.sources[key]
    const clonedSource = Object.assign({}, currentSource)
    clonedStyle.sources[key] = clonedSource
  })

  // clone the layers
  clonedStyle.layers = style.layers.map(lyr => Object.assign({}, lyr))
  // console.log(style.layers.map(lyr => lyr.id))
  return clonedStyle
}
