import turfCentroid from '@turf/centroid'
/**
 * Identify GLAD region from given geometry
 * @param {*} geom
 */
export default (geom) => {
  let centroid
  let region
  const feature = {type: 'Feature', geometry: geom}
  if (geom.type === 'Point') {
    centroid = feature
  } else {
    centroid = turfCentroid(feature)
  }

  const lon = centroid.geometry.coordinates[0]
  const lat = centroid.geometry.coordinates[1]

  if (lon <= -29 && lat <= 15) {
    region = 'southamerica'
  } else if (lon >= -29 && lon <= 54 && lat <= 38) {
    region = 'africa'
  } else if (lon >= 90 && lat <= 20) {
    region = 'asia'
  }

  if (!region) region = 'global'

  return region
}
