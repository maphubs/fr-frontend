
type SatAPIOptions = {
  cloudThreshold: number,
  collection: ['sentinel-2-l1c' | 'landsat-8-l1'],
}

const defaultOptions = {
  cloudThreshold: 50,
  collection: 'landsat-8-l1'
}

export default {
  async search (bbox: Array<number>, startDate: string, endDate: string, options?: SatAPIOptions = {}) {
    const selectedOptions = Object.assign(defaultOptions, options)
    // "time": "2018-02-12T00:00:00Z/2018-03-18T12:31:12Z",
    const time = `${startDate}T00:00:00Z/${endDate}T00:00:00Z`

    const response = await fetch('https://sat-api.developmentseed.org/stac/search', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        Accept: 'application/geo+json'
      },
      body: JSON.stringify({
        bbox,
        time,
        intersects: null,
        query: {
          'eo:cloud_cover': {
            lt: selectedOptions.cloudThreshold
          },
          collection: {
            eq: selectedOptions.collection
          }
        },
        sort: [
          {
            field: 'eo:cloud_cover',
            direction: 'desc'
          }
        ]
      })
    })
    return response.json() // geojson
  },

  // Just need to call he next page URL?
  async nextPage (url) {
    const response = await fetch(url)
    return response.json()
  },

  /**
    Given a selected result from the API, build a raster MapHubs layer that can be added to MapHubs
   */

  // https://m422dt0618.execute-api.eu-central-1.amazonaws.com/production/sentinel/tiles/S2A_tile_20180217_54MVT_0/{z}/{x}/{y}.png?rgb=04,03,02&tile=256&histo=393,2991-638,3000-856,3147
  // example id "S2B_13SDD_20180218_0"
  sentinelIDToTileID (id) {
    const parts = id.split('_')
    return `${parts[0]}_tile_${parts[2]}_${parts[1]}_${parts[3]}`
  },

  getMapHubsLayer (selectedResult) {
    const tileId = this.sentinelIDToTileID(selectedResult.id)
    const tileSize = 256
    const bandR = '04'
    const bandG = '03'
    const bandB = '02'
    // https://m422dt0618.execute-api.eu-central-1.amazonaws.com
    const url = `https://homgm56739dwedh.belugacdn.link/production/sentinel/tiles/${tileId}/{z}/{x}/{y}.png?rgb=${bandR},${bandG},${bandB}&tile=${tileSize}&histo=50,1500-50,1500-50,1500`

    const idParts = selectedResult.id.split('_')
    const date = idParts[2]

    const layer = {
      layer_id: `${selectedResult.id}`,
      short_id: `${selectedResult.id}`,
      visible: true,
      name: {
        en: date
      },
      source: {
        en: `${selectedResult.properties.collection}`
      },
      style: {
        version: 8,
        sources: {},
        layers: [
          {
            id: `fr-imagery-${selectedResult.id}`,
            type: 'raster',
            source: `fr-imagery-${selectedResult.id}`,
            minzoom: 0,
            maxzoom: 18,
            paint: {
              'raster-opacity': 1
            },
            layout: {
              visibility: 'visible'
            }
          }
        ],
        metadata: {
          'maphubs:active': true,
          'maphubs:priority': 6
        }
      },
      legend_html: `
      <div class="omh-legend">
      <h3>{NAME}</h3>
      </div>`,
      is_external: true,
      external_layer_type: 'raster',
      external_layer_config: {
        type: 'raster',
        url: url
      }
    }
    layer.style.sources[`fr-imagery-${selectedResult.id}`] = {
      type: 'raster',
      tiles: [
        url
      ],
      tileSize
    }
    return layer
  }
}
