// @flow

import moment from 'moment'
import turfarea from '@turf/area'

type GLADResult = {
  date: string,
  count: number,
  features: Array<Object>
}

type GLADStats = {
  values: Array<GLADResult>,
  max: number,
  lastMonth?: number
}

export default {
  processGLADResult (result: Object, year: string = '2020'): GLADStats {
    console.log(result)
    const data = {}
    if (result.geoms) {
      // loop through the geoms
      result.geoms.forEach((feature) => {
        const day = feature.properties.value
        if (!data[day]) {
          data[day] = []
        }
        data[day].push(feature)
      })
    } else {
      console.error('GLAD features not found')
    }
    let max = 0
    const values = Object.keys(data).map((key): GLADResult => {
      const features = data[key]
      const count = turfarea({ type: 'FeatureCollection', features }) / 10000
      if (count > max) {
        max = count
      }
      const dayOfYear = parseInt(key, 10)
      const date = moment(`${year}-01-01`).dayOfYear(dayOfYear).format('YYYY-MM-DD')
      return {
        date,
        count,
        features
      }
    })
    return {
      values,
      max
    }
  },

  async getLoss (geom: Object) {
    try {
      // eslint-disable-next-line no-undef
      const response = await fetch('https://stats-api.forest.report/loss', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({
          geom
        })
      })

      const result = await response.json()
      // console.log(result)
      if (result && result.stats) {
        if (result.stats['0']) {
          delete result.stats['0']
        }
        return result.stats
      }
      return null
    } catch (err) {
      console.error(err.message)
    }
  },

  async getDensity (geom: Object, density: number, stats: Object) {
    try {
      // eslint-disable-next-line no-undef
      const response = await fetch('https://stats-api.forest.report/remaining', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({
          geom,
          density
        })
      })
      const result = await response.json()
      if (result) {
        stats[density] = result.area
      }
    } catch (err) { console.error(err.message) }
  }
}
