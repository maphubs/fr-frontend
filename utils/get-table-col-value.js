// @flow

export default (record: Object, dataIndex: string) => {
  let val = Object.assign({}, record)
  if (!val) return undefined
  const dataIndexParts = dataIndex.split('.')
  if (dataIndexParts.length > 1) {
    dataIndexParts.forEach((part) => {
      if (val) val = val[part]
    })
  } else {
    val = val[dataIndex]
  }

  return val
}
