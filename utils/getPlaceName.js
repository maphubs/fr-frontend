// @flow
import request from 'superagent'
import getConfig from 'next/config'
const MAPHUBS_CONFIG = getConfig().publicRuntimeConfig

export default async (lon: number, lat: number) => {
  try {
    const res = await request.get(`https://api.mapbox.com/geocoding/v5/mapbox.places/${lon},${lat}.json?access_token=${MAPHUBS_CONFIG.MAPBOX_ACCESS_TOKEN}`)
    if (res && res.body && res.body.features  && res.body.features.length > 0) {
      const feature = res.body.features[0]
      return feature.place_name
    }
  } catch (err) {
    console.error(err)
    return 'Unknown'
  }
}
