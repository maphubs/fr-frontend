// @flow

export function addRecentlyViewed (list_id: string) {
  if (typeof localStorage !== 'undefined') {
    const recentlyViewed = JSON.parse(localStorage.getItem('recently-viewed-lists')) || []
    if (!recentlyViewed.includes(list_id)) recentlyViewed.push(list_id)
    localStorage.setItem('recently-viewed-lists', JSON.stringify(recentlyViewed))
  }
}

export function getRecentlyViewed () {
  if (typeof localStorage !== 'undefined') {
    return JSON.parse(localStorage.getItem('recently-viewed-lists')) || []
  }
}
