// eslint-disable-next-line no-var
var getenv = require('getenv')
require('dotenv').config()

module.exports = {
  AUTH0_DOMAIN: getenv('AUTH0_DOMAIN'),
  AUTH0_CLIENT_ID: getenv('AUTH0_CLIENT_ID'),
  AUTH0_CLIENT_SECRET: getenv('AUTH0_CLIENT_SECRET'),
  AUTH0_CALLBACK_URL: getenv('AUTH0_CALLBACK_URL'),
  API_URL: getenv('API_URL'),
  API_URL_INTERNAL: getenv('API_URL_INTERNAL'),
  MAPBOX_ACCESS_TOKEN: getenv('MAPBOX_ACCESS_TOKEN'),
  BING_KEY: getenv('BING_KEY', ''),
  OPENROUTESERVICE_API_KEY: getenv('OPENROUTESERVICE_API_KEY'),
  EARTHENGINE_CLIENTID: getenv('EARTHENGINE_CLIENTID', ''),
  logoSmall: getenv('LOGO_SMALL'),
  logoSmallWidth: getenv.int('LOGO_WIDTH_SMALL'),
  logoSmallHeight: getenv.int('LOGO_HEIGHT_SMALL'),
  primaryColor: getenv('PRIMARY_COLOR'),
  RASTER_UPLOAD_API: getenv('RASTER_UPLOAD_API'),
  RASTER_UPLOAD_API_KEY: getenv('RASTER_UPLOAD_API_KEY'),
  host: getenv('EXTERNAL_HOST'),
  ASSET_UPLOAD_API: getenv('ASSET_UPLOAD_API'),
  ASSET_UPLOAD_API_KEY: getenv('ASSET_UPLOAD_API_KEY'),
  SESSION_SECRET: getenv('SESSION_SECRET'),
  REDIS_HOST: getenv('REDIS_HOST'),
  REDIS_PORT: getenv('REDIS_PORT'),
  APP_ENABLED: getenv.boolish('APP_ENABLED', false),
  APP_DOWNLOAD_URL: getenv('APP_DOWNLOAD_URL', 'https://forest.report'),
  APP_VERSION: getenv('APP_VERSION', 'unknown'),
  LANGUAGES: getenv('LANGUAGES', 'en,fr,es,pt,id')
}
