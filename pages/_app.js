import App from 'next/app'
import * as React from 'react'
import numeral from 'numeral'
import 'numeral/locales/es'
import 'numeral/locales/pt-br'
import 'numeral/locales/fr'
import withUrqlClient from '../lib/with-urql-client'
import { Provider as URQLProvider } from 'urql'
import ErrorBoundary from 'react-error-boundaries'
import UserContainer from '../containers/UserContainer'
import NProgress from 'nprogress'
import Router from 'next/router'
import { Provider as UnStatedProvider } from 'unstated'
import '../static/nprogress.css'
import Locales from '../utils/locales'

Router.events.on('routeChangeStart', url => {
  console.log(`Loading: ${url}`)
  NProgress.start()
})
Router.events.on('routeChangeComplete', () => NProgress.done())
Router.events.on('routeChangeError', () => NProgress.done())

class MyApp extends App {
  static async getInitialProps ({ Component, ctx, router }) {
    let pageProps = {}

    if (Component.getInitialProps) {
      pageProps = await Component.getInitialProps(ctx)
    }

    pageProps.user = ctx?.req?.session?.passport?.user
    pageProps.locale = ctx?.req?.locale
    // console.log(ctx?.req?.locale)

    return { pageProps, pathname: ctx.pathname }
  }

  UserState: any

  constructor (props: Props) {
    super(props)
    const { user, locale } = props.pageProps
    this.UserState = new UserContainer(user, locale)

    // these are the only locale where we translate numbers so fr
    if (locale === 'pt') numeral.locale('pt-br')
    if (locale === 'es') numeral.locale('es')
    if (locale === 'fr') numeral.locale('fr')
  }

  translate = (val: any) => {
    const locale = this.UserState.state.locale
    if (typeof val === 'undefined' || !val) return val
    if (typeof val === 'string') {
      if (locale) {
        return Locales.getLocaleString(locale, val)
      } else {
        return val
      }
    } else {
      return Locales.getLocaleStringObject(locale, val)
    }
  }

  render () {
    const { Component, pageProps, urqlClient } = this.props

    return (
      <ErrorBoundary>
        <UnStatedProvider inject={[this.UserState]}>
          <URQLProvider value={urqlClient}>
            <Component {...pageProps} t={this.translate} />
          </URQLProvider>
        </UnStatedProvider>
      </ErrorBoundary>
    )
  }
}

export default withUrqlClient(MyApp)
