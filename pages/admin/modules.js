//  @flow
import * as React from 'react'
import { Button, Col, notification, Row, Table } from 'antd'
import { Query } from 'urql'
import gql from 'graphql-tag'
import Link from 'next/link'
import Layout from '../../components/Layout'
import AutoSizer from 'react-virtualized-auto-sizer'
import Spinner from '../../components/Spinner'
import DeleteModuleButton from '../../components/module/DeleteModuleButton'

type Props = {
  t: any
}

export default class Modules extends React.Component<Props, void> {
  static async getInitialProps ({req, query}: {req: any, query: any}) {
    return {}
  }

  render () {
    const {t} = this.props
    return (
      <Layout
        title='Admin - Modules - Forest Report'
        crumbs={[
          {href: '/admin', label: 'Admin'},
          {label: 'Modules'}
        ]}
        t={t} initialLanguage='en'
      >
        <Query
          query={gql`
            {
              modules {
                id
                name
                config
                driver {
                  id
                  version
                }
              }
            }
          `}
        >
          {({ fetching, error, data }) => {
            if (fetching) return <Spinner />
            if (error) {
              notification.error({
                message: 'Error',
                description: error.message,
                duration: 0
              })
              return ''
            }
            if (!data.modules) return <p>{t('No Data')}</p>

            const columns = [{
              title: t('ID'),
              dataIndex: 'id',
              width: 250,
              key: 'id',
              sorter: (a, b) => {
                let aVal = a.id
                let bVal = b.id
                if (typeof aVal !== 'string') aVal = ''
                if (typeof bVal !== 'string') bVal = ''
                const aValLower = aVal.toLowerCase()
                const bValLower = bVal.toLowerCase()
                return aValLower.localeCompare(bValLower)
              }
            }, {
              title: t('Name'),
              dataIndex: 'name',
              key: 'name',
              sorter: (a, b) => {
                let aVal = t(a?.name)
                let bVal = t(b?.name)
                if (typeof aVal !== 'string') aVal = ''
                if (typeof bVal !== 'string') bVal = ''
                const aValLower = aVal.toLowerCase()
                const bValLower = bVal.toLowerCase()
                return aValLower.localeCompare(bValLower)
              },
              render: (text, record) => (
                <span>
                  {t(record?.name)}
                </span>
              )
            }, {
              title: t('Driver'),
              dataIndex: 'driver',
              width: 150,
              key: 'driver',
              render: (text, record) => (
                <Link
                  href={{
                    pathname: '/admin/driver',
                    query: {id: record.driver.id}
                  }}
                  as={`/admin/driver/${record.driver.id}`}
                >
                  <a>{record.driver.id}</a>
                </Link>
              )
            }, {
              title: t('Action'),
              key: 'action',
              width: 150,
              render: (text, record) => (
                <span>
                  <Row>
                    <Col span={8}>
                      <Link
                        href={{
                          pathname: '/admin/module/view',
                          query: {id: record.id}
                        }}
                        as={`/admin/module/view/${record.id}`}
                      >
                        <Button icon='edit' shape='circle' size='small' />
                      </Link>
                    </Col>
                    <Col span={8}>
                      <DeleteModuleButton module_id={record.id} t={t} />
                    </Col>
                  </Row>
                </span>
              )
            }]

            return (
              <>
                <AutoSizer disableWidth>
                  {({ height }) => (
                    <>
                      <Table
                        dataSource={data.modules}
                        columns={columns}
                        size='small'
                        scroll={{ y: height - 100 }}
                        pagination={{
                          pageSize: 25,
                          showSizeChanger: true,
                          pageSizeOptions: ['10', '25', '50']
                        }}
                      />

                    </>
                  )}
                </AutoSizer>
                <div style={{position: 'absolute', top: '-35px', right: '10px'}}>
                  <Link
                    href={{
                      pathname: '/admin/module/create'
                    }}
                    as='/admin/module/create'
                  >
                    <a><Button type='primary'>{t('New Module')}</Button></a>
                  </Link>
                </div>
              </>
            )
          }}
        </Query>
      </Layout>
    )
  }
}
