//  @flow
import * as React from 'react'
import { Button, Row, Table } from 'antd'
import Mutation from '../../components/Mutation'
import Query from '../../components/Query'
import gql from 'graphql-tag'
import Router from 'next/router'
import Link from 'next/link'
import Layout from '../../components/Layout'
import Spinner from '../../components/Spinner'
import AutoSizer from 'react-virtualized-auto-sizer'

type Props = {
  t: any
}

export default class Lists extends React.Component<Props, void> {
  static async getInitialProps ({req, query}: {req: any, query: any}) {
    return {}
  }

  render () {
    const {t} = this.props
    return (
      <Layout
        title='Admin - Lists - Forest Report'
        crumbs={[
          {href: '/admin', label: 'Admin'},
          {label: 'Lists'}
        ]}
        t={t} initialLanguage='en'
      >
        <Query
          query={gql`
            {
              lists {
                id
                name
                status
                aoiCount
                modules {
                  id
                }
              }
            }
          `}
        >
          {({ fetching, data }) => {
            if (fetching) return <Spinner />

            const columns = [{
              title: t('ID'),
              dataIndex: 'id',
              key: 'id',
              width: 150
            }, {
              title: t('Name'),
              dataIndex: 'name',
              key: 'name',
              sorter: (a, b) => {
                let aVal = t(a.name)
                let bVal = t(b.name)
                if (typeof aVal !== 'string') aVal = ''
                if (typeof bVal !== 'string') bVal = ''
                const aValLower = aVal.toLowerCase()
                const bValLower = bVal.toLowerCase()
                return aValLower.localeCompare(bValLower)
              },
              render: (text, record) => (
                <span style={{fontSize: '12px'}}>
                  {t(record.name)}
                </span>
              )
            }, {
              title: t('Status'),
              width: 100,
              dataIndex: 'status',
              key: 'status',
              render: (text, record) => (
                <span style={{fontSize: '12px'}}>
                  {record.status}
                </span>
              ),
              sorter: (a, b) => {
                let aVal = a.status
                let bVal = b.status
                if (typeof aVal !== 'string') aVal = ''
                if (typeof bVal !== 'string') bVal = ''
                const aValLower = aVal.toLowerCase()
                const bValLower = bVal.toLowerCase()
                return aValLower.localeCompare(bValLower)
              }
            }, {
              title: t('Areas'),
              width: 100,
              dataIndex: 'aoiCount',
              key: 'aois',
              render: (text, record) => (
                <span style={{fontSize: '12px'}}>
                  {record?.aoiCount}
                </span>
              ),
              sorter: (a, b) => {
                return parseInt(a?.aoiCount) - parseInt(b?.aoiCount)
              }
            }, {
              title: t('Modules'),
              width: 100,
              dataIndex: 'modules.length',
              key: 'modules',
              render: (text, record) => (
                <span style={{fontSize: '12px'}}>
                  {record?.modules?.length}
                </span>
              ),
              sorter: (a, b) => {
                return parseInt(a?.modules?.length) - parseInt(b?.modules?.length)
              }
            }, {
              title: t('Action'),
              key: 'action',
              width: 200,
              render: (text, record) => (
                <span>
                  <Link
                    href={{
                      pathname: '/admin/list/view',
                      query: {
                        id: record.id,
                        name: t(record.name)
                      }
                    }}
                    as={`/admin/list/view/${record.id}`}
                  >
                    <Button icon='edit' shape='circle' size='small' />
                  </Link>
                </span>
              )
            }]

            return (
              <div style={{ height: '100% ' }}>
                <Row style={{ height: 'calc(100% - 50px)' }}>
                  <AutoSizer disableWidth>
                    {({ height }) => (
                      <>
                        <Table dataSource={data.lists} columns={columns} size='small' scroll={{ y: height - 200 }} />
                      </>
                    )}
                  </AutoSizer>
                </Row>
                <Row>
                  <Mutation
                    query={gql`
                      mutation createList($organizationID: String!) {
                        createList(organizationID: $organizationID) {
                          id
                        }
                      }
                    `}
                    onComplete={(data) => {
                      Router.push(`/admin/list/view/${data.createList.id}`)
                    }}
                  >
                    {(createList) => (
                      <Button
                        type='primary'
                        onClick={() => {
                          createList({organizationID: 'DEFAULT'})
                        }}
                      >{t('New List')}
                      </Button>
                    )}
                  </Mutation>
                </Row>
              </div>
            )
          }}
        </Query>
      </Layout>
    )
  }
}
