//  @flow
import * as React from 'react'
import { notification, Table, Button } from 'antd'
import Query from '../../components/Query'
import gql from 'graphql-tag'
import Link from 'next/link'
import AutoSizer from 'react-virtualized-auto-sizer'
import Layout from '../../components/Layout'
import Spinner from '../../components/Spinner'
type Props = {
  t: any
}

export default class Organizations extends React.Component<Props, void> {
  static async getInitialProps ({req, query}: {req: any, query: any}) {
    return {}
  }

  render () {
    const {t} = this.props

    return (
      <Layout
        title='Admin - Organizations - Forest Report'
        crumbs={[
          {href: '/admin', label: 'Admin'},
          {label: 'Organizations'}
        ]}
        t={t} initialLanguage='en'
      >
        <Query
          query={gql`
            {
              organizations {
                id
                name
                config
                members {
                  id
                }
              }
            }
          `}
        >
          {({ fetching, data }) => {
            if (fetching) return <Spinner />
            const organizations = data.organizations || []

            const columns = [{
              title: t('ID'),
              dataIndex: 'id',
              key: 'id',
              sorter: (a, b) => {
                let aVal = a.id
                let bVal = b.id
                if (typeof aVal !== 'string') aVal = ''
                if (typeof bVal !== 'string') bVal = ''
                const aValLower = aVal.toLowerCase()
                const bValLower = bVal.toLowerCase()
                return aValLower.localeCompare(bValLower)
              }
            }, {
              title: t('Name'),
              dataIndex: 'name',
              key: 'name',
              sorter: (a, b) => {
                let aVal = t(a.name)
                let bVal = t(b.name)
                if (typeof aVal !== 'string') aVal = ''
                if (typeof bVal !== 'string') bVal = ''
                const aValLower = aVal.toLowerCase()
                const bValLower = bVal.toLowerCase()
                return aValLower.localeCompare(bValLower)
              },
              render: (text, record) => (
                <span>
                  {t(record.name)}
                </span>
              )
            }, {
              title: t('Members'),
              dataIndex: 'members',
              key: 'members',
              render: (text, record) => (
                <span>{record.members ? record.members.length : 'NONE'}</span>
              )
            }, {
              title: t('Action'),
              key: 'action',
              render: (text, record) => (
                <span>
                  <Link
                    href={{
                      pathname: '/admin/organization/edit',
                      query: {id: record.id}
                    }}
                    as={`/admin/organization/edit/${record.id}`}
                  >
                    <a>{t('Edit')}</a>
                  </Link>
                </span>
              )
            }]

            return (
              <>
                <AutoSizer disableWidth>
                  {({ height }) => (
                    <>
                      <Table
                        dataSource={organizations}
                        columns={columns}
                        size='small'
                        scroll={{ y: height - 100 }}
                        pagination={{
                          pageSize: 25,
                          showSizeChanger: true,
                          pageSizeOptions: ['10', '25', '50']
                        }}
                      />
                    </>
                  )}
                </AutoSizer>
                <div style={{position: 'absolute', top: '-35px', right: '10px'}}>
                  <Link
                    href={{
                      pathname: '/admin/organization/create'
                    }}
                    as='/admin/organization/create'
                  >
                    <a><Button type='primary' disabled>{t('New Organization')}</Button></a>
                  </Link>
                </div>
              </>
            )
          }}
        </Query>
      </Layout>
    )
  }
}
