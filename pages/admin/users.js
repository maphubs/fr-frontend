//  @flow
import * as React from 'react'
import { Button, Row, Table, Tabs } from 'antd'
import Query from '../../components/Query'
import gql from 'graphql-tag'
import Link from 'next/link'
import Layout from '../../components/Layout'
import Spinner from '../../components/Spinner'
import InviteUserForm from '../../components/organization/InviteUserForm'

const TabPane = Tabs.TabPane

type Props = {
  t: any
}

export default class Users extends React.Component<Props, void> {
  static async getInitialProps ({req, query}: {req: any, query: any}) {
    return {}
  }

  render () {
    const {t} = this.props
    return (
      <Layout
        title='Admin - Users - Forest Report'
        crumbs={[
          {href: '/admin', label: 'Admin'},
          {label: 'Users'}
        ]}
        t={t}
      >
        <Query
          query={gql`
            {
              users {
                id
                email
              }
              userInvites {
                email
                key
                used
              }
            }
          `}
        >
          {({ fetching, data }) => {
            if (fetching) return <Spinner />
            const users = data.users || []
            const invites = data.userInvites || []

            const userColumns = [{
              title: t('ID'),
              dataIndex: 'id',
              key: 'id',
              sorter: (a, b) => {
                let aVal = a.id
                let bVal = b.id
                if (typeof aVal !== 'string') aVal = ''
                if (typeof bVal !== 'string') bVal = ''
                const aValLower = aVal.toLowerCase()
                const bValLower = bVal.toLowerCase()
                return aValLower.localeCompare(bValLower)
              }
            }, {
              title: t('Email'),
              dataIndex: 'email',
              key: 'email',
              width: 400,
              sorter: (a, b) => {
                let aVal = a.email
                let bVal = b.email
                if (typeof aVal !== 'string') aVal = ''
                if (typeof bVal !== 'string') bVal = ''
                const aValLower = aVal.toLowerCase()
                const bValLower = bVal.toLowerCase()
                return aValLower.localeCompare(bValLower)
              },
              render: (text, record) => (
                <span>
                  {record.email}
                </span>
              )
            }, {
              title: t('Action'),
              key: 'action',
              width: 100,
              render: (text, record) => (
                <span>
                  <Link
                    href={{
                      pathname: '/admin/user',
                      query: {id: record.id}
                    }}
                    as={`/admin/user/edit/${record.id}`}
                  >
                    <a>{t('Edit')}</a>
                  </Link>
                </span>
              )
            }]

            const inviteColumns = [
              {
                title: t('Email'),
                dataIndex: 'email',
                key: 'email',
                width: 250,
                sorter: (a, b) => {
                  let aVal = a.email
                  let bVal = b.email
                  if (typeof aVal !== 'string') aVal = ''
                  if (typeof bVal !== 'string') bVal = ''
                  const aValLower = aVal.toLowerCase()
                  const bValLower = bVal.toLowerCase()
                  return aValLower.localeCompare(bValLower)
                },
                render: (text, record) => (
                  <span>
                    {record.email}
                  </span>
                )
              }, {
                title: t('Key'),
                dataIndex: 'key',
                key: 'key'
              }, {
                title: t('Action'),
                key: 'action',
                width: 100,
                render: (text, record) => (
                  <span>
                    <Button>{t('Resend')}</Button>
                    <Button>{t('Copy Link')}</Button>
                  </span>
                )
              }
            ]

            return (
              <>
                <Row>
                  <InviteUserForm
                    t={t} onSubmit={(val) => {
                      // TODO: update the invite list
                    }}
                  />
                </Row>
                <Row>
                  <Tabs type='card' style={{height: 'calc(100% - 22px)'}} animated={false} tabBarStyle={{margin: 0}}>
                    <TabPane tab={t('Users')} key='users'>
                      <Table dataSource={users} columns={userColumns} size='small' />
                    </TabPane>
                    <TabPane tab={t('Invites')} key='invites'>
                      <Table dataSource={invites} columns={inviteColumns} size='small' />
                    </TabPane>
                  </Tabs>
                </Row>
              </>
            )
          }}
        </Query>
      </Layout>
    )
  }
}
