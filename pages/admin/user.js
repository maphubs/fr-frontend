//  @flow
import * as React from 'react'
import { Empty, notification, Select, Tabs } from 'antd'
import {Query} from 'urql'
import Mutation from '../../components/Mutation'
import gql from 'graphql-tag'

import CodeEditor from '../../components/CodeEditor'
import Layout from '../../components/Layout'
import Spinner from '../../components/Spinner'

const TabPane = Tabs.TabPane
const Option = Select.Option

type Props = {
  t: any,
  id: string
}

export default class Driver extends React.Component<Props, void> {
  static async getInitialProps ({req, query}: {req: any, query: any}) {
    return {
      id: query.id
    }
  }

  render () {
    const {id, t} = this.props
    return (
      <Layout
        title={`Admin - User - ${id} - Forest Report`}
        crumbs={[
          {href: '/admin', label: 'Admin'},
          {href: '/admin/users', label: 'Users'},
          {label: id}
        ]}
        t={t}
      >
        <Query
          query={gql`
            {
              user(id: "${id}") {
                id
                email
                role
                config
              }
            }
          `}
        >
          {({ fetching, error, data }) => {
            if (fetching) return <Spinner />
            if (error) {
              notification.error({
                message: 'Error',
                description: error.message,
                duration: 0
              })
              return ''
            }
            if (!data.user) return <Empty description='User Not Found' />
            const { id, email, role, config } = data.user
            return (
              <Mutation
                query={gql`
                  mutation updateUser($id: String!, $email: String, $role: Int, $config: JSON) {
                    updateUser(id: $id, email: $email, role: $role, config: $config) {
                      id
                      email
                      role
                      config
                    }
                  }
                `}
                successMessage='User Updated'
              >
                {(updateUser, { fetching }) => (
                  <Tabs type='card'>
                    <TabPane tab={t('Info')} key='info'>
                      <p><b>{t('ID')}:</b> {id}</p>
                      <p><b>{t('Email')}:</b> {email}</p>
                      <p><b>{t('Role')}:</b></p>
                      <Select
                        value={role} style={{ width: 120 }}
                        onSelect={(val) => {
                          updateUser({
                            id,
                            role: val,
                            config,
                            email
                          }
                          )
                        }}
                      >
                        <Option value={0}>{t('Unauthorized')}</Option>
                        <Option value={2}>{t('Member')}</Option>
                        <Option value={1}>{t('Admin')}</Option>
                      </Select>
                    </TabPane>
                    <TabPane tab={t('Config')} key='config'>
                      <div>
                        <div style={{height: '400px', width: '600px'}}>
                          <CodeEditor
                            title='User Config'
                            onSave={code => {
                              updateUser({ id, config: code, role, email })
                            }}
                            code={JSON.stringify(config, undefined, 2)}
                            t={t}
                          />
                        </div>
                        {fetching && <Spinner />}
                      </div>
                    </TabPane>
                  </Tabs>
                )}
              </Mutation>
            )
          }}
        </Query>
      </Layout>
    )
  }
}
