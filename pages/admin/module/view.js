//  @flow
import * as React from 'react'
import { notification, Tabs } from 'antd'
import {Query} from 'urql'
import gql from 'graphql-tag'
import Mutation from '../../../components/Mutation'

import CodeEditor from '../../../components/CodeEditor'
import Layout from '../../../components/Layout'
import Spinner from '../../../components/Spinner'
import AttributionForm from '../../../components/module/AttributionForm'
import ModuleInfoForm from '../../../components/module/ModuleInfoForm'

const TabPane = Tabs.TabPane
type Props = {
  id: string,
  t: any,
  initialLanguage: string
}

export default class Module extends React.Component<Props, void> {
  static async getInitialProps ({req, query}: {req: any, query: any}) {
    return {
      id: query.id,
      name: query.name
    }
  }

  render () {
    const {id, t, initialLanguage} = this.props
    return (
      <Layout
        title={`Admin - Module - ${id} - Forest Report`}
        crumbs={[
          {href: '/admin', label: 'Admin'},
          {href: '/admin/modules', label: 'Modules'},
          {label: id}
        ]}
        t={t} initialLanguage={initialLanguage}
      >
        <Query
          query={gql`
            {
              module(id: "${id}") {
                id
                name
                config
                driver {
                  id
                }
                layer
                card
                attribution
              }
            }
          `}
        >
          {({ fetching, error, data }) => {
            if (fetching) return <Spinner />
            if (error) {
              notification.error({
                message: 'Error',
                description: error.message,
                duration: 0
              })
              return ''
            }
            if (!data.module) return <p>No Data</p>
            const { id, name, config, driver, layer, card, attribution } = data.module
            return (
              <div>
                <Tabs type='card'>
                  <TabPane tab={t('Info')} key='info'>
                    <p><b>{t('ID')}:</b> {id}</p>
                    <p><b>{t('Driver')}:</b> {driver.id}</p>
                    <ModuleInfoForm id={id} name={name} />
                  </TabPane>
                  <TabPane tab={t('Config')} key='config'>
                    <Mutation
                      query={gql`
                        mutation updateModuleConfig($id: String!, $config: JSON!) {
                          updateModuleConfig(id: $id, config: $config) {
                            id
                            config
                          }
                        }
                      `}
                    >
                      {(updateModuleConfig, { fetching }) => (
                        <div>
                          <div style={{height: '400px', width: '600px'}}>
                            <CodeEditor
                              title='Config'
                              onSave={code => {
                                updateModuleConfig({ id, config: code })
                              }}
                              code={JSON.stringify(config, undefined, 2)}
                              t={t}
                            />
                          </div>
                          {fetching && <Spinner />}
                        </div>
                      )}
                    </Mutation>
                  </TabPane>
                  <TabPane tab={t('Layer')} key='layer'>
                    <Mutation
                      query={gql`
                        mutation updateModuleLayer($id: String!, $layer: JSON!) {
                          updateModuleLayer(id: $id, layer: $layer) {
                            id
                            layer
                          }
                        }
                      `}
                    >
                      {(updateModuleLayer, { fetching }) => (
                        <div>
                          <div style={{height: '400px', width: '600px'}}>
                            <CodeEditor
                              title='Layer'
                              onSave={code => {
                                updateModuleLayer({ id, layer: code })
                              }}
                              code={JSON.stringify(layer, undefined, 2)}
                              t={t}
                            />
                          </div>
                          {fetching && <Spinner />}
                        </div>
                      )}
                    </Mutation>
                  </TabPane>
                  <TabPane tab={t('Card')} key='card'>
                    <Mutation
                      query={gql`
                        mutation updateModuleCard($id: String!, $card: JSON!) {
                          updateModuleCard(id: $id, card: $card) {
                            id
                            card
                          }
                        }
                      `}
                    >
                      {(updateModuleCard, { fetching }) => (
                        <div>
                          <div style={{height: '400px', width: '600px'}}>
                            <CodeEditor
                              title='Card'
                              onSave={code => {
                                updateModuleCard({ id, card: code })
                              }}
                              code={JSON.stringify(card, undefined, 2)}
                              t={t}
                            />
                          </div>
                          {fetching && <Spinner />}
                        </div>
                      )}
                    </Mutation>
                  </TabPane>
                  <TabPane tab={t('Attribution')} key='attribution'>
                    <AttributionForm module_id={id} attribution={attribution} />
                  </TabPane>
                </Tabs>
              </div>
            )
          }}
        </Query>
      </Layout>
    )
  }
}
