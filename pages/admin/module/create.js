//  @flow
import * as React from 'react'
import CreateModuleForm from '../../../components/module/CreateModuleForm'
import Layout from '../../../components/Layout'

type Props = {
  t: any,
  initialLanguage: string
}

type State = {
  name?: Object,
  config?: Object,
  saving?: boolean
}

export default class CreateModule extends React.Component<Props, State> {
  static async getInitialProps ({req, query}: {req: any, query: any}) {
    return {
      id: query.id
    }
  }

  render () {
    const {t, initialLanguage} = this.props

    return (
      <Layout
        title={`${t('Admin')} - ${t('Create Module')} - Forest Report`}
        crumbs={[
          {href: '/admin', label: t('Admin')},
          {href: '/admin/modules', label: t('Modules')},
          {label: t('Create')}
        ]}
        t={t} initialLanguage={initialLanguage}
      >
        <div>
          <h3>{t('Create Module')}</h3>
          <CreateModuleForm t={t} />
        </div>
      </Layout>
    )
  }
}
