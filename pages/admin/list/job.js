//  @flow
import * as React from 'react'
import { Query } from 'urql'
import gql from 'graphql-tag'
import { notification, Row, Tabs } from 'antd'
import Layout from '../../../components/Layout'
import Spinner from '../../../components/Spinner'
import CodeEditor from '../../../components/CodeEditor'

const TabPane = Tabs.TabPane

const getQuery = (list_id: string, job_id: string) => {
  return gql`
    {
      job(list_id: "${list_id}", id: "${job_id}"){
        id
        timestamp
        processedOn
        finishedOn
        progress
        data
        attemptsMade
        failedReason
        stacktrace
        returnvalue
      }
    }
  `
}

type Props = {
  list_id: string,
  job_id: string,
  t: any,
  initialLanguage: any
}

export default class Job extends React.Component<Props, void> {
  static async getInitialProps ({req, query}: {req: any, query: any}) {
    return {
      list_id: query.list_id,
      job_id: query.job_id
    }
  }

  render () {
    const { list_id, job_id, t, initialLanguage } = this.props
    return (
      <Layout
        title={`Admin - Job - ${job_id} - Forest Report`}
        crumbs={[
          {href: '/admin', label: 'Admin'},
          {href: '/admin/lists', label: 'Lists'},
          {
            href: {
              pathname: '/admin/list/view',
              query: { id: list_id }
            },
            as: `/admin/list/view/${list_id}/`,
            label: list_id
          },
          {label: job_id}
        ]}
        t={t} initialLanguage={initialLanguage}
      >
        <Query query={getQuery(list_id, job_id)}>
          {({ fetching, error, data }) => {
            if (fetching) return <Spinner />
            if (error) {
              notification.error({
                message: 'Error',
                description: error.message,
                duration: 0
              })
              return ''
            }
            const { job } = data
            if (!job) {
              notification.error({
                message: 'Error',
                description: t('Job Not Found'),
                duration: 0
              })
              return ''
            }
            return (
              <>
                <Row>
                  <p><b>Job ID:</b>{job_id}</p>
                </Row>
                <Row>
                  <p><b>Status:</b> {job.failedReason ? `Failed: ${job.failedReason}` : 'Pending'}</p>
                </Row>
                <Row>
                  <Tabs animated={false} size='small' type='card'>
                    <TabPane tab={t('Data')} key='data'>
                      <div style={{height: '400px', width: '600px'}}>
                        <CodeEditor
                          code={JSON.stringify(job.data, undefined, 2)}
                          t={t}
                          showOnMount
                        />
                      </div>
                    </TabPane>
                    <TabPane tab={t('Stacktrace')} key='stacktrace'>
                      {job.stacktrace.map((trace, i) => {
                        return (<p key={i}>{trace}</p>)
                      })}
                    </TabPane>
                    <TabPane tab={t('Return Value')} key='returnvalue'>
                      <div style={{height: '400px', width: '600px'}}>
                        <CodeEditor
                          code={JSON.stringify(job.returnvalue, undefined, 2)}
                          t={t}
                          showOnMount
                        />
                      </div>
                    </TabPane>
                  </Tabs>
                </Row>
              </>
            )
          }}
        </Query>
      </Layout>
    )
  }
}
