//  @flow
import * as React from 'react'
import { Button, Col, message, notification, Row, Select, Spin, Table } from 'antd'
import {Query} from 'urql'
import Mutation from '../../../components/Mutation'
import gql from 'graphql-tag'
import Layout from '../../../components/Layout'
import _find from 'lodash.find'
import Spinner from '../../../components/Spinner'

const Option = Select.Option

type Props = {
  id: string,
  t: any,
  initialLanguage: string
}

type State = {
  saving: boolean,
  selectedLists: Array<string>,
  relationship: string
}

export default class AddLinkedLists extends React.Component<Props, State> {
  static async getInitialProps ({req, query}: {req: any, query: any}) {
    return {
      id: query.id
    }
  }

  constructor (props: Props) {
    super(props)
    this.state = {
      saving: false,
      selectedLists: [],
      relationship: 'buyer'
    }
  }

  saveSuccess = () => {
    this.setState({
      selectedLists: [],
      saving: false
    })
    message.success('Linked Lists')
  }

  saveError = () => {
    this.setState({
      saving: false
    })
  }

  handleRelationshipChange = (relationship: string) => {
    this.setState({relationship})
  }

  getQuery = (id: string) => {
    return gql`
    {
      list(id: "${id}") {
        id
        name
        linkedLists {
          id
          name
        }
      }
      lists {
        id
        name
      }
    }
    `
  }

  render () {
    const _this = this
    const {saveSuccess, saveError, getQuery, handleRelationshipChange} = this
    const {id, t, initialLanguage} = this.props
    const {saving, selectedLists, relationship} = this.state

    const query = getQuery(id)
    return (
      <Query
        query={query}
      >
        {// eslint-disable-next-line react/no-unused-prop-types
          ({ fetching, error, data }: {fetching: boolean, error: any, data: any}) => {
            if (fetching) return <Spinner />
            if (error) {
              notification.error({
                message: 'Error',
                description: error.message,
                duration: 0
              })
              return ''
            }
            if (!data.list || !data.lists) return <p>{t('No Data')}</p>

            const {list, lists} = data
            const { id, name } = list
            const title = name ? t(name) : id

            // delete the current list from the list
            const filteredLists = []
            lists.forEach((c) => {
              if (c.id !== id) filteredLists.push(c)
            })

            const listColumns = [{
              title: t('ID'),
              dataIndex: 'id',
              key: 'id'
            }, {
              title: t('Name'),
              dataIndex: 'name',
              key: 'name',
              render: (text, record) => (
                <span>
                  {t(record.name)}
                </span>
              )
            }]

            const rowSelection = {
              onChange: (selectedRowKeys, selectedRows) => {
                console.log(`selectedRowKeys: ${selectedRowKeys} selectedRows: ${selectedRows}`)
                _this.setState({selectedLists: selectedRowKeys})
              },
              getCheckboxProps: record => {
                const alreadyInList = _find(list.linkedLists, {id: record.id})
                return {
                  disabled: alreadyInList,
                  name: record.id
                }
              }
            }

            return (
              <Layout
                title={`${t('Admin')} - ${t('List')} - ${title} - Forest Report`}
                crumbs={[
                  {href: '/admin', label: t('Admin')},
                  {href: '/admin/lists', label: t('Lists')},
                  {
                    href: {
                      pathname: '/admin/list/view',
                      query: {
                        id,
                        name: t(name)
                      }
                    },
                    as: `/admin/list/view/${id}`,
                    label: title
                  },
                  {label: t('Add Linked Lists')}
                ]}
                t={t} initialLanguage={initialLanguage}
              >
                <Spin spinning={saving} delay={10}>
                  <div>
                    <p><b>{t('Select Lists to link to list:')}</b> {t(name)}</p>
                    <Table rowKey='id' dataSource={filteredLists} columns={listColumns} rowSelection={rowSelection} size='small' />
                    <Row style={{marginTop: '10px'}}>
                      <Mutation
                        query={gql`
                          mutation addLinkedLists($id: String!, $childIDs: [String]!, $relationship: JSON) {
                            addLinkedLists(id: $id, childIDs: $childIDs, relationship: $relationship) {
                              id
                              linkedLists {
                                id
                                name
                              }
                            }
                          }
                        `}
                        onComplete={saveSuccess}
                        onError={saveError}
                      >
                        {(addLinkedLists, { fetching }) => {
                          return (
                            <Row type='flex' justify='end'>
                              <Col span={10}>
                                <span style={{marginRight: '10px'}}>{t('Relationship')}</span>
                                <Select defaultValue='buyer' style={{ width: 300 }} onChange={handleRelationshipChange}>
                                  <Option value='buyer'>{t('Buyer (in supply chain)')}</Option>
                                  <Option value='owner'>{t('Owner (direct ownership)')}</Option>
                                </Select>
                              </Col>
                              <Col span={4}>
                                <Button
                                  disabled={fetching}
                                  onClick={() => {
                                    console.log(selectedLists)
                                    _this.setState({saving: true})
                                    addLinkedLists({
                                      id,
                                      childIDs: selectedLists,
                                      relationship: {value: relationship}
                                    })
                                  }} type='primary'
                                >Link Lists
                                </Button>
                              </Col>
                            </Row>
                          )
                        }}
                      </Mutation>
                    </Row>
                  </div>
                </Spin>
              </Layout>
            )
          }
        }
      </Query>
    )
  }
}
