//  @flow
import * as React from 'react'
import { Button, message, Row, Spin, Table } from 'antd'
import Query from '../../../components/Query'
import AutoSizer from 'react-virtualized-auto-sizer'
import Mutation from '../../../components/Mutation'
import gql from 'graphql-tag'
import Link from 'next/link'
import Layout from '../../../components/Layout'
import _find from 'lodash.find'
import Spinner from '../../../components/Spinner'
type Props = {
  id: string,
  t: any
}

type State = {
  saving: boolean,
  selectedModules: Array<string>
}

export default class ListAddModules extends React.Component<Props, State> {
  static async getInitialProps ({req, query}: {req: any, query: any}) {
    return {
      id: query.id
    }
  }

  constructor (props: Props) {
    super(props)
    this.state = {
      saving: false,
      selectedModules: []
    }
  }

  saveSuccess = () => {
    this.setState({
      selectedModules: [],
      saving: false
    })
    message.success('Added Modules')
  }

  saveError = () => {
    this.setState({
      saving: false
    })
  }

  getQuery = (id: string) => {
    return gql`
    {
      list(id: "${id}") {
        id
        name
        modules {
            id
          name
          config
          driver {
            id
            version
          }
        }
      }
      modules {
        id
        name
        config
        driver {
          id
          version
        }
      }
    }
    `
  }

  render () {
    const _this = this
    const {saveSuccess, saveError, getQuery} = this
    const {id, t} = this.props
    const {saving, selectedModules} = this.state

    const query = getQuery(id)
    return (
      <Query
        query={query}
      >
        {// eslint-disable-next-line react/no-unused-prop-types
          ({ fetching, data }: {fetching: boolean, error: any, data: any}) => {
            if (fetching) return <Spinner />
            if (!data.list) return <p>No Data</p>

            const {list, modules} = data
            const { id, name } = list
            const title = name ? t(name) : id

            const moduleColumns = [{
              title: t('ID'),
              dataIndex: 'id',
              key: 'id',
              width: 150
            }, {
              title: t('Name'),
              dataIndex: 'name',
              key: 'name',
              sorter: (a, b) => {
                let aVal = t(a?.name)
                let bVal = t(b?.name)
                if (typeof aVal !== 'string') aVal = ''
                if (typeof bVal !== 'string') bVal = ''
                const aValLower = aVal.toLowerCase()
                const bValLower = bVal.toLowerCase()
                return aValLower.localeCompare(bValLower)
              },
              render: (text, record) => (
                <span>
                  {t(record.name)}
                </span>
              )
            }, {
              title: t('Driver'),
              dataIndex: 'driver',
              key: 'driver',
              width: 150,
              render: (text, record) => (
                <Link
                  href={{
                    pathname: '/admin/driver',
                    query: { id: record.driver.id }
                  }}
                  as={`/admin/driver/${record.driver.id}`}
                >
                  <a>{record.driver.id}</a>
                </Link>
              )
            }]

            const rowSelection = {
              onChange: (selectedRowKeys, selectedRows) => {
                _this.setState({selectedModules: selectedRowKeys})
              },
              getCheckboxProps: record => {
                const alreadyInList = _find(list.modules, {id: record.id})
                return {
                  disabled: alreadyInList,
                  name: record.id
                }
              }
            }

            return (
              <Layout
                title={`${t('Admin')} - ${t('List')} - ${title} - Forest Report`}
                crumbs={[
                  {href: '/admin', label: t('Admin')},
                  {href: '/admin/lists', label: t('Lists')},
                  {
                    href: {
                      pathname: '/admin/list/view',
                      query: {
                        id,
                        name: t(name)
                      }
                    },
                    as: `/admin/list/view/${id}`,
                    label: title
                  },
                  {label: 'Add Modules'}
                ]}
                t={t}
              >
                <div style={{position: 'absolute', top: '-35px', right: '10px'}}>
                  <p><b>{t('Select Modules to add to list:')}</b> {t(name)}</p>
                </div>
                <AutoSizer disableWidth>
                  {({ height }) => (
                    <Spin spinning={saving} delay={10}>
                      <div>
                        <Table
                          rowKey='id'
                          dataSource={modules}
                          columns={moduleColumns}
                          rowSelection={rowSelection}
                          size='small'
                          scroll={{ y: height - 150 }}
                          pagination={{
                            pageSize: 25,
                            showSizeChanger: true,
                            pageSizeOptions: ['10', '25', '50']
                          }}
                        />
                        <Row style={{marginTop: '10px'}}>
                          <Mutation
                            query={gql`
                              mutation addListModules($id: String!, $moduleIDs: [String]!) {
                                addListModules(id: $id, moduleIDs: $moduleIDs) {
                                  id
                                  modules {
                                    id
                                    name
                                    config
                                    driver {
                                      id
                                      version
                                    }
                                  }
                                }
                              }
                            `}
                            onComplete={saveSuccess}
                            onError={saveError}
                          >
                            {(addListMembers) => (
                              <Button
                                disabled={addListMembers.fetching}
                                onClick={() => {
                                  console.log(selectedModules)
                                  _this.setState({saving: true})
                                  addListMembers({ id, moduleIDs: selectedModules })
                                }} type='primary'
                              >Add Modules
                              </Button>
                            )}
                          </Mutation>
                        </Row>
                      </div>
                    </Spin>
                  )}
                </AutoSizer>
              </Layout>
            )
          }
        }
      </Query>
    )
  }
}
