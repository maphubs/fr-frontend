//  @flow
import * as React from 'react'
import { Input, message, notification, Row, Spin } from 'antd'
import Query from '../../../components/Query'
import gql from 'graphql-tag'
import Layout from '../../../components/Layout'
import slugify from 'slugify'
import Router from 'next/router'
import FileUpload from '../../../components/form/FileUpload'
import Spinner from '../../../components/Spinner'
import getConfig from 'next/config'
import UserContainer from '../../../containers/UserContainer'
import { subscribe } from '../../../lib/unstated-props'

const MAPHUBS_CONFIG = getConfig().publicRuntimeConfig
type Props = {
  id: string,
  t: any,
  initialLanguage: string,
  containers: {user: UserContainer}
}

type State = {
  processing: boolean,
  nameField?: string
}

class ListAddAOIs extends React.Component<Props, State> {
  static async getInitialProps ({req, query}: {req: any, query: any}) {
    return {
      id: query.id
    }
  }

  constructor (props: Props) {
    super(props)
    this.state = {
      processing: false
    }
  }

  onUpload = async (list_id: string, name: Object, file: Object) => {
    const {t, containers} = this.props
    const {nameField} = this.state
    let token = ''
    if (
      containers.user &&
      containers.user.state.profile &&
      containers.user.state.profile.accessToken
    ) {
      token = containers.user.state.profile.accessToken
    } else {
      notification.error({
        message: 'Error',
        description: 'Missing Authenticaion Token',
        duration: 0
      })
    }
    this.onProcessingStart()
    try {
      const response = await fetch(`${MAPHUBS_CONFIG.API_URL}/api/upload/complete`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          authorization: `Bearer ${token}`
        },
        body: JSON.stringify({
          uploadUrl: file.uploadURL,
          list_id,
          originalName: file.data.name,
          nameField
        })
      })
      const result = await response.json()
      if (result.success) {
        this.setState({processing: false})
        message.success(t('Added Areas'))
        Router.push(`/admin/list/view/${list_id}/${slugify(t(name))}`)
      } else {
        if (result.code === 'MULTIPLESHP') {
          this.onUploadError(new Error(t('Zip file contained multiple shapefiles')))
        } else {
          this.onUploadError(new Error(t('Invalid File Upload')))
        }
      }
    } catch (err) {
      this.onUploadError(err)
    }
  }

  onUploadError = (err: Error) => {
    const {t} = this.props
    this.setState({processing: false})
    notification.error({
      message: t('Error Adding Areas'),
      description: err.message || err.toString(),
      duration: 0
    })
  }

  getQuery = (id: string) => {
    return gql`
    {
      list(id: "${id}") {
        id
        name
      }
    }
    `
  }

  onProcessingStart = () => {
    this.setState({processing: true})
  }

  onChangeNameField = (e: any) => {
    this.setState({nameField: e.target.value})
  }

  render () {
    const {getQuery} = this
    const {id, t, initialLanguage} = this.props
    const {processing} = this.state
    const query = getQuery(id)

    return (
      <Query
        query={query}
      >
        {// eslint-disable-next-line react/no-unused-prop-types
          ({ fetching, data }: {fetching: boolean, data: any}) => {
            if (fetching) return <Spinner />
            if (!data.list) return <p>{t('No Data')}</p>

            const {list} = data
            const { id, name } = list
            const title = name ? t(name) : id

            return (
              <Layout
                title={`${t('Admin')} - ${t('List')} - ${title} - Forest Report`}
                crumbs={[
                  {href: '/admin', label: t('Admin')},
                  {href: '/admin/lists', label: t('Lists')},
                  {
                    href: {
                      pathname: '/admin/list/view/',
                      query: {
                        id,
                        name: t(name)
                      }
                    },
                    as: `/admin/list/view/${id}/${t(name)}`,
                    label: title
                  },
                  {label: t('Add Areas')}
                ]}
                t={t} initialLanguage={initialLanguage}
              >
                <Spin spinning={processing} delay={10}>
                  <div>
                    <Row justify='center' style={{maxWidth: '300px', margin: '20px auto'}}>
                      <Input placeholder={t('Name Field')} allowClear onChange={this.onChangeNameField} />
                    </Row>
                    <Row style={{maxWidth: '800px', margin: 'auto'}}>
                      <FileUpload
                        t={t}
                        endpoint={`${MAPHUBS_CONFIG.API_URL}/api/upload/save`}
                        note={t('Supported files: Shapefile (Zip), GeoJSON, KML,  GPX (tracks or waypoints), or CSV (with Lat/Lon fields), and MapHubs format')}
                        onProcessingStart={this.onProcessingStart}
                        onComplete={(file: Object) => {
                          this.onUpload(id, name, file)
                        }}
                        onError={this.onUploadError}
                      />
                    </Row>
                  </div>
                </Spin>
              </Layout>
            )
          }
        }
      </Query>
    )
  }
}
export default subscribe(ListAddAOIs, {user: UserContainer})
