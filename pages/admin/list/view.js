//  @flow
import * as React from 'react'
import { Button, Divider, message, Row, Table, Tabs } from 'antd'
import Mutation from '../../../components/Mutation'
import Query from '../../../components/Query'
import gql from 'graphql-tag'
import Link from 'next/link'
import Layout from '../../../components/Layout'
import ListInfoForm from '../../../components/list/ListInfoForm'
import CodeEditor from '../../../components/CodeEditor'
import LocalizedEditor from '../../../components/form/LocalizedEditor'
import AutoSizer from 'react-virtualized-auto-sizer'
import Spinner from '../../../components/Spinner'
import Jobs from '../../../components/list/jobs/Jobs'
import Analysis from '../../../components/list/analysis/Analysis'
import ListModules from '../../../components/list/modules'
import ListAOIsAdmin from '../../../components/list/ListAOIsAdmin'

const TabPane = Tabs.TabPane

type Props = {
  id: string,
  name: string,
  t: any
}

type State = {
  descriptionModified?: boolean,
  configModified?: boolean
}

export default class ListView extends React.Component<Props, State> {
  static async getInitialProps ({req, query}: {req: any, query: any}) {
    return {
      id: query.id,
      name: query.name
    }
  }

  descriptionEditor: any

  constructor (props: Props) {
    super(props)
    this.state = {}
  }

  setDescriptionModified = (descriptionModified: boolean) => {
    this.setState({descriptionModified})
  }

  setConfigModified = (configModified: boolean) => {
    this.setState({configModified})
  }

  getQuery = (id: string) => {
    return gql`
    {
      list(id: "${id}") {
        id
        name
        logo
        description
        config
        status
        modules {
            id
            priority
          name
          config
          driver {
            id
            version
          }
        }
        linkedLists {
          id
          name
        }
      }
    }
    `
  }

  render () {
    const _this = this
    const {setDescriptionModified, setConfigModified, getQuery} = this
    const {id, name, t} = this.props
    const {descriptionModified, configModified} = this.state

    const query = getQuery(id)

    return (
      <Layout
        title={`Admin - List - ${name} - Forest Report`}
        crumbs={[
          {href: '/admin', label: 'Admin'},
          {href: '/admin/lists', label: 'Lists'},
          {label: name}
        ]}
        t={t}
      >
        <Query query={query}>
          {({ fetching, data }) => {
            if (fetching) return <Spinner />
            if (!data.list) return <p>No Data</p>

            const { id, name, logo, description, config, modules, linkedLists, status } = data.list

            const linkedListColumns = [{
              title: t('ID'),
              dataIndex: 'id',
              key: 'id',
              sorter: (a, b) => {
                let aVal = a.id
                let bVal = b.id
                if (typeof aVal !== 'string') aVal = ''
                if (typeof bVal !== 'string') bVal = ''
                const aValLower = aVal.toLowerCase()
                const bValLower = bVal.toLowerCase()
                return aValLower.localeCompare(bValLower)
              }
            }, {
              title: t('Name'),
              dataIndex: 'name',
              key: 'name',
              sorter: (a, b) => {
                let aVal = t(a.name)
                let bVal = t(b.name)
                if (typeof aVal !== 'string') aVal = ''
                if (typeof bVal !== 'string') bVal = ''
                const aValLower = aVal.toLowerCase()
                const bValLower = bVal.toLowerCase()
                return aValLower.localeCompare(bValLower)
              },
              render: (text, record) => (
                <span>
                  {t(record.name)}
                </span>
              )
            }, {
              title: t('Action'),
              key: 'action',
              render: (text, record) => (
                <span>
                  <Link
                    href={{
                      pathname: '/admin/list/view',
                      query: {
                        id: record.id,
                        name: t(record.name)
                      }
                    }}
                    as={`/admin/list/view/${record.id}`}
                  >
                    <a>View</a>
                  </Link>
                  <Divider type='vertical' />
                  <a href='#'>{t('Remove')}</a>
                </span>
              )
            }]

            return (
              <div style={{height: '100%'}}>
                <p><b>{t('Name')}:</b> {t(name)}</p>
                <Mutation
                  query={gql`
                      mutation updateListConfig($id: String!, $description: JSON, $config: JSON) {
                        updateListConfig(id: $id, description: $description, config: $config) {
                          id
                          description
                          config
                        }
                      }
                    `}
                  successMessage='Saved'
                  onComplete={() => {
                    this.setState({descriptionModified: false, configModified: false})
                  }}
                >
                  {(updateListConfig) => (
                    <>
                      <style jsx global>{`
                        .ant-tabs-content {
                          height: 100%;
                        }
                        .ant-tabs-tabpane {
                          height: 100%;
                        }
                      `}
                      </style>
                      <Tabs type='card' style={{height: 'calc(100% - 22px)'}} animated={false} tabBarStyle={{margin: 0}}>
                        <TabPane tab={t('Info')} key='info'>
                          <ListInfoForm id={id} name={name} logo={logo} status={status} t={t} />
                        </TabPane>
                        <TabPane tab={t('Description')} key='description'>
                          <div style={{height: '400px'}}>
                            <LocalizedEditor
                              ref={(el) => { _this.descriptionEditor = el }}
                              value={description}
                              onChange={(val) => {
                                if (!descriptionModified) {
                                  setDescriptionModified(true)
                                }
                              }}
                            />
                          </div>
                          <Button
                            type='primary' disabled={!descriptionModified}
                            onClick={() => {
                              if (!_this.descriptionEditor) {
                                message.error(t('Failed to save, please try again.'))
                                return
                              }
                              const description = _this.descriptionEditor.getValue()
                              updateListConfig({ id, description })
                            }}
                          >{t('Save')}
                          </Button>
                          {
                            descriptionModified &&
                              <span style={{marginLeft: '10px'}}>{t('Not Saved')}</span>
                          }
                        </TabPane>
                        <TabPane tab={t('Areas')} key='aois'>
                          <ListAOIsAdmin list_id={id} t={t} />
                        </TabPane>
                        <TabPane tab={t('Linked Lists')} key='linked'>
                          <AutoSizer disableWidth>
                            {({ height }) => (
                              <>
                                <Table dataSource={linkedLists} columns={linkedListColumns} size='small' scroll={{ y: height - 200 }} />
                                <Row style={{marginTop: '10px'}}>
                                  <Link
                                    href={{
                                      pathname: '/admin/list/addlinkedlists',
                                      query: {id}
                                    }}
                                    as={`/admin/list/addlinkedlists/${id}`}
                                  >
                                    <Button type='primary'>Add Linked Lists</Button>
                                  </Link>
                                </Row>
                              </>
                            )}
                          </AutoSizer>
                        </TabPane>
                        <TabPane tab={t('Modules')} key='modules'>
                          <ListModules list_id={id} modules={modules} query={query} t={t} />
                        </TabPane>
                        <TabPane tab={t('Jobs')} key='jobs'>
                          <Jobs list_id={id} t={t} />
                        </TabPane>
                        <TabPane tab={t('Config')} key='config'>
                          <div>
                            <div style={{height: '400px', width: '600px'}}>
                              <CodeEditor
                                title='Config'
                                onSave={code => {
                                  updateListConfig({ id, config: code })
                                }}
                                onChange={code => {
                                  if (!configModified) {
                                    setConfigModified(true)
                                  }
                                }}
                                code={JSON.stringify(config, undefined, 2)}
                                t={t}
                                showOnMount
                              />
                              {
                                configModified &&
                                  <span style={{marginLeft: '10px'}}>{t('Not Saved')}</span>
                              }
                            </div>
                          </div>
                        </TabPane>
                        <TabPane tab={t('Analysis')} key='analysis'>
                          <div style={{padding: '25px'}}>
                            <Analysis list_id={id} t={t} />
                          </div>
                        </TabPane>
                      </Tabs>
                    </>
                  )}
                </Mutation>
              </div>
            )
          }}
        </Query>
      </Layout>
    )
  }
}
