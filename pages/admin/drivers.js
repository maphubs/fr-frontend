//  @flow
import * as React from 'react'
import { Divider, notification, Table } from 'antd'
import {Query} from 'urql'
import gql from 'graphql-tag'
import Link from 'next/link'
import Layout from '../../components/Layout'
import Spinner from '../../components/Spinner'
type Props = {
  t: any
}

export default class Drivers extends React.Component<Props, void> {
  static async getInitialProps ({req, query}: {req: any, query: any}) {
    return {}
  }

  render () {
    const {t} = this.props
    return (
      <Layout
        title={`${t('Admin')} - ${t('Drivers')} - Forest Report`}
        crumbs={[
          {href: '/admin', label: t('Admin')},
          {href: '/drivers', label: t('Drivers')}
        ]}
        t={t}
      >
        <Query
          query={gql`
            {
              drivers {
                id
                version
              }
            }
          `}
        >
          {({ fetching, error, data }) => {
            if (fetching) return <Spinner />
            if (error) {
              notification.error({
                message: 'Error',
                description: error.message,
                duration: 0
              })
              return ''
            }
            const columns = [{
              title: t('ID'),
              dataIndex: 'id',
              key: 'id',
              sorter: (a, b) => {
                let aVal = a.id
                let bVal = b.id
                if (typeof aVal !== 'string') aVal = ''
                if (typeof bVal !== 'string') bVal = ''
                const aValLower = aVal.toLowerCase()
                const bValLower = bVal.toLowerCase()
                return aValLower.localeCompare(bValLower)
              }
            }, {
              title: t('Version'),
              dataIndex: 'version',
              key: 'version'
            }, {
              title: t('Action'),
              key: 'action',
              render: (text, record) => (
                <span>
                  <Link
                    href={{
                      pathname: '/admin/driver',
                      query: { id: record.id }
                    }}
                    as={`/admin/driver/${record.id}`}
                  >
                    <a>{t('Edit')}</a>
                  </Link>
                  <Divider type='vertical' />
                  <a href='#'>{t('Delete')}</a>
                </span>
              )
            }]

            return (
              <Table dataSource={data.drivers} columns={columns} size='small' />
            )
          }}
        </Query>
      </Layout>
    )
  }
}
