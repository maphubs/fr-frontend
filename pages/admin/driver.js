//  @flow
import * as React from 'react'
import { notification } from 'antd'
import {Query} from 'urql'
import Mutation from '../../components/Mutation'
import gql from 'graphql-tag'

import CodeEditor from '../../components/CodeEditor'
import Layout from '../../components/Layout'
import Spinner from '../../components/Spinner'

type Props = {
  id: string,
  t: any,
  initialLanguage: string
}

export default class Driver extends React.Component<Props, void> {
  static async getInitialProps ({req, query}: {req: any, query: any}) {
    return {
      id: query.id,
      name: query.name
    }
  }

  render () {
    const {id, t, initialLanguage} = this.props
    return (
      <Layout
        title={`Admin - Driver - ${id} - Forest Report`}
        crumbs={[
          {href: '/admin', label: 'Admin'},
          {href: '/admin/drivers', label: 'Drivers'},
          {label: id}
        ]}
        t={t} initialLanguage={initialLanguage}
      >
        <Query
          query={gql`
            {
              driver(id: "${id}") {
                id
                version
                default_config
              }
            }
          `}
        >
          {({ fetching, error, data }) => {
            if (fetching) return <Spinner />
            if (error) {
              notification.error({
                message: 'Error',
                description: error.message,
                duration: 0
              })
              return ''
            }
            if (!data.driver) return <p>No Data</p>
            const { id, version, default_config } = data.driver
            return (
              <div>
                <p><b>ID:</b> {id}</p>
                <p><b>Version:</b> {version}</p>
                <Mutation
                  query={gql`
                    mutation updateDriverConfig($id: String!, $config: JSON!) {
                      updateDriverConfig(id: $id, default_config: $config) {
                        id
                        default_config
                      }
                    }
                  `}
                >
                  {(updateDriverConfig, { fetching }) => (
                    <div>
                      <div style={{height: '400px', width: '600px'}}>
                        <CodeEditor
                          title='Default Config'
                          onSave={code => {
                            updateDriverConfig({ id, config: code })
                          }}
                          code={JSON.stringify(default_config, undefined, 2)}
                          t={t}
                        />
                      </div>
                      {fetching && <Spinner />}
                    </div>
                  )}
                </Mutation>
              </div>
            )
          }}
        </Query>
      </Layout>
    )
  }
}
