//  @flow
import * as React from 'react'
import Router from 'next/router'
import CreateOrganizationForm from '../../../components/organization/CreateOrganizationForm'
import Layout from '../../../components/Layout'

type Props = {
  t: any,
  initialLanguage: string
}

type State = {
  name?: Object,
  config?: Object,
  saving?: boolean
}

export default class CreateOrganization extends React.Component<Props, State> {
  static async getInitialProps ({req, query}: {req: any, query: any}) {
    return {
      id: query.id
    }
  }

  onSubmit = () => {
    Router.push('/admin/organizations')
  }
  render () {
    const {onSubmit} = this
    const {t, initialLanguage} = this.props

    return (
      <Layout
        title={`Admin - Create Organization - Forest Report`}
        crumbs={[
          {href: '/admin', label: 'Admin'},
          {href: '/admin/organizations', label: 'Organizations'},
          {label: 'Create'}
        ]}
        t={t} initialLanguage={initialLanguage}
      >
        <div>
          <h3>Create Organization</h3>
          <CreateOrganizationForm onSubmit={onSubmit} />
        </div>
      </Layout>
    )
  }
}
