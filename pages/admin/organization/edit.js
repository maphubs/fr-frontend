//  @flow
import * as React from 'react'
import { Button, Divider, notification, Row, Table, Tabs } from 'antd'
import {Query} from 'urql'
import Mutation from '../../../components/Mutation'
import AutoSizer from 'react-virtualized-auto-sizer'
import gql from 'graphql-tag'
import Link from 'next/link'
import Layout from '../../../components/Layout'
import OrganizationInfoForm from '../../../components/organization/OrganizationInfoForm'
import CodeEditor from '../../../components/CodeEditor'
import Spinner from '../../../components/Spinner'

const TabPane = Tabs.TabPane

type Props = {
  id: string,
  name: string,
  t: any
}

type State = {
  name: Object
}

export default class EditOrganization extends React.Component<Props, State> {
  static async getInitialProps ({req, query}: {req: any, query: any}) {
    return {
      id: query.id,
      name: query.name
    }
  }

  render () {
    const {id, name, t} = this.props
    return (
      <Layout
        title={`Admin - Organization - ${id} - Forest Report`}
        crumbs={[
          {href: '/admin', label: 'Admin'},
          {href: '/admin/organization', label: 'Organizations'},
          {label: id}
        ]}
        t={t}
      >
        <Query
          query={gql`
            {
              organization(id: "${id}") {
                id
                name
                config
                members {
                  id
                  email
                  role
                }
              }
            }
          `}
        >
          {({ fetching, error, data }) => {
            if (fetching) return <Spinner />
            if (error) {
              notification.error({
                message: 'Error',
                description: error.message,
                duration: 0
              })
              return ''
            }
            if (!data.organization) return <p>No Data</p>

            const { id, name, config, members } = data.organization

            const memberColumns = [{
              title: t('ID'),
              width: 300,
              dataIndex: 'id',
              key: 'id'
            }, {
              title: t('Email'),
              dataIndex: 'email',
              key: 'email',
              render: (text, record) => (
                <span>
                  {record?.email}
                </span>
              )
            }, {
              title: t('Role'),
              width: 150,
              dataIndex: 'role',
              key: 'role',
              render: (text, record) => (
                <span>
                  {record?.role === 1 && <b>{t('Admin')}</b>}
                  {record?.role === 2 && <span>{t('Member')}</span>}
                </span>
              )
            }, {
              title: t('Action'),
              width: 150,
              key: 'action',
              render: (text, record) => (
                <span>
                  <Link
                    href={{
                      pathname: '/admin/user',
                      query: {id: record.id}
                    }}
                    as={`/admin/user/${record.id}`}
                  >
                    <a>View</a>
                  </Link>
                  <Divider type='vertical' />
                  <a href='#'>Remove</a>
                </span>
              )
            }]

            return (
              <div style={{height: '100%'}}>
                <style jsx global>{`
                  .ant-tabs-content {
                    height: 100%;
                  }
                  .ant-tabs-tabpane {
                    height: 100%;
                  }
                `}
                </style>
                <p><b>Name:</b> {t(name)}</p>
                <Tabs type='card' style={{height: 'calc(100% - 22px)'}} animated={false} tabBarStyle={{margin: 0}}>
                  <TabPane tab={t('Name')} key='info'>
                    <OrganizationInfoForm id={id} name={name} />
                  </TabPane>
                  <TabPane tab={t('Members')} key='members'>
                    <AutoSizer disableWidth>
                      {({ height }) => (
                        <Table
                          dataSource={members}
                          columns={memberColumns}
                          size='small'
                          scroll={{ y: height - 100 }}
                          pagination={{
                            pageSize: 25,
                            showSizeChanger: true,
                            pageSizeOptions: ['10', '25', '50']
                          }}
                        />
                      )}
                    </AutoSizer>
                    <div style={{position: 'absolute', top: '-35px', right: '10px'}}>
                      <Link
                        href={{
                          pathname: '/admin/organization/addmembers',
                          query: {id}
                        }}
                        as={`/admin/organization/addmembers/${id}`}
                      >
                        <Button type='primary'>Add Members</Button>
                      </Link>
                    </div>
                  </TabPane>
                  <TabPane tab={t('Config')} key='config'>
                    <Mutation
                      query={gql`
                        mutation updateOrganization($id: String!, $name: JSON, $config: JSON) {
                          updateOrganization(id: $id, name: $name, config: $config) {
                            id
                            name
                            config
                          }
                        }
                      `}
                    >
                      {(updateOrganization, { fetching }) => (
                        <div>
                          <div style={{height: '400px', width: '600px'}}>
                            <CodeEditor
                              title='Config'
                              onSave={code => {
                                updateOrganization({ id, config: code })
                              }}
                              code={JSON.stringify(config, undefined, 2)}
                              t={t}
                            />
                          </div>
                          {fetching && <Spinner />}
                        </div>
                      )}
                    </Mutation>
                  </TabPane>
                </Tabs>
              </div>
            )
          }}
        </Query>
      </Layout>
    )
  }
}
