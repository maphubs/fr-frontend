//  @flow
import * as React from 'react'
import { Table, Divider, Row, Button, notification, Modal, Steps } from 'antd'
import { Query } from 'urql'
import Mutation from '../components/Mutation'
import gql from 'graphql-tag'
import Router from 'next/router'
import AutoSizer from 'react-virtualized-auto-sizer'
import Link from 'next/link'
import Layout from '../components/Layout'
import Spinner from '../components/Spinner'
import moment from 'moment'
import slugify from 'slugify'
import { QRCode } from 'react-qr-svg'
import DeleteReportButton from '../components/report/DeleteReportButton'
import { Subscribe } from 'unstated'
import UserContainer from '../containers/UserContainer'
import getConfig from 'next/config'
const MAPHUBS_CONFIG = getConfig().publicRuntimeConfig

const { Step } = Steps

type Props = {
  t: any,
  initialLanguage: string
}

type State = {
  showMobile?: boolean,
  selectedReport?: Object,
  currentMobileStep?: number
}

const getQuery = (userID: string) => {
  return gql`
  {
    reports {
      id
      title
      subtitle
      author
      monitoring_period
      status
      published_at
      aoi_count
    }
    user (id: "${userID}") {
      id
      apiKey
    }
  }
  `
}

export default class Reports extends React.Component<Props, State> {
  static async getInitialProps ({req, query}: {req: any, query: any}) {
    return {}
  }

  constructor (props: Props) {
    super()
    this.state = {}
  }

  handleOpenMobileModal = (selectedReport: Object) => {
    this.setState({ showMobile: true, selectedReport, currentMobileStep: 0 })
  }

  handleCloseMobileModal = () => {
    this.setState({ showMobile: false, selectedReport: undefined, currentMobileStep: undefined })
  }

  render () {
    const {t, initialLanguage} = this.props
    const { showMobile, selectedReport, currentMobileStep } = this.state
    return (
      <Layout
        title={`${t('Reports')} - Forest Report`}
        crumbs={[
          {href: '/admin', label: 'Admin'},
          {label: t('Reports')}
        ]}
        t={t} initialLanguage={initialLanguage}
      >
        <Subscribe to={[UserContainer]}>
          {user => {
            return (
              <Query
                query={getQuery(user.state.profile.id)}
              >
                {({ fetching, error, data }) => {
                  if (fetching) return <Spinner />
                  if (error) {
                    notification.error({
                      message: 'Error',
                      description: error.message,
                      duration: 0
                    })
                    return ''
                  }
                  const reports = data.reports || []

                  const user = data.user

                  const columns = [{
                    title: t('ID'),
                    dataIndex: 'id',
                    width: 100,
                    key: 'id',
                    sorter: (a, b) => {
                      let aVal = a.id
                      let bVal = b.id
                      if (typeof aVal !== 'string') aVal = ''
                      if (typeof bVal !== 'string') bVal = ''
                      const aValLower = aVal.toLowerCase()
                      const bValLower = bVal.toLowerCase()
                      return aValLower.localeCompare(bValLower)
                    }
                  }, {
                    title: t('Title'),
                    dataIndex: 'title',
                    key: 'title',
                    sorter: (a, b) => {
                      let aVal = t(a.title)
                      let bVal = t(b.title)
                      if (typeof aVal !== 'string') aVal = ''
                      if (typeof bVal !== 'string') bVal = ''
                      const aValLower = aVal.toLowerCase()
                      const bValLower = bVal.toLowerCase()
                      return aValLower.localeCompare(bValLower)
                    },
                    render: (text, record) => (
                      <span>
                        {t(record.title)}
                      </span>
                    )
                  }, {
                    title: t('Subtitle'),
                    dataIndex: 'subtitle',
                    key: 'subtitle',
                    width: 200,
                    sorter: (a, b) => {
                      let aVal = t(a?.subtitle)
                      let bVal = t(b?.subtitle)
                      if (typeof aVal !== 'string') aVal = ''
                      if (typeof bVal !== 'string') bVal = ''
                      const aValLower = aVal.toLowerCase()
                      const bValLower = bVal.toLowerCase()
                      return aValLower.localeCompare(bValLower)
                    },
                    render: (text, record) => (
                      <span>
                        {t(record?.subtitle)}
                      </span>
                    )
                  }, {
                    title: t('Monitoring Period'),
                    dataIndex: 'monitoring_period',
                    key: 'monitoring_period',
                    width: 200,
                    sorter: (a, b) => {
                      let aVal = t(a?.monitoring_period)
                      let bVal = t(b?.monitoring_period)
                      if (typeof aVal !== 'string') aVal = ''
                      if (typeof bVal !== 'string') bVal = ''
                      const aValLower = aVal.toLowerCase()
                      const bValLower = bVal.toLowerCase()
                      return aValLower.localeCompare(bValLower)
                    },
                    render: (text, record) => (
                      <span>
                        {t(record?.monitoring_period)}
                      </span>
                    )
                  }, {
                    title: t('Status'),
                    width: 150,
                    dataIndex: 'status',
                    key: 'status',
                    sorter: (a, b) => {
                      let aVal = a.status
                      let bVal = b.status
                      if (typeof aVal !== 'string') aVal = ''
                      if (typeof bVal !== 'string') bVal = ''
                      const aValLower = aVal.toLowerCase()
                      const bValLower = bVal.toLowerCase()
                      return aValLower.localeCompare(bValLower)
                    },
                    render: (text, record) => (
                      <span>
                        {record.status}
                      </span>
                    )
                  }, {
                    title: t('Published'),
                    width: 200,
                    dataIndex: 'published_at',
                    key: 'published_at',
                    render: (text, record) => (
                      <span style={{fontSize: '12px'}}>
                        {record.published_at && moment(record.published_at).format('YYYY-MM-DD HH:MM:SS')}
                      </span>
                    )
                  }, {
                    title: t('Action'),
                    width: 150,
                    key: 'action',
                    render: (text, record) => (
                      <span>
                        <Link
                          href={{
                            pathname: '/report/view',
                            query: {
                              id: record.id,
                              title: t(record.title)
                            }
                          }}
                          as={`/report/view/${record.id}/${t(record.title)}`}

                        >
                          <Button icon='file-pdf' shape='circle' size='small' style={{marginRight: '5px'}} />
                        </Link>
                        <Link
                          href={{
                            pathname: '/report/edit',
                            query: {
                              id: record.id,
                              title: t(record.title)
                            }
                          }}

                          as={`/report/edit/${record.id}/${slugify(t(record.title))}`}
                        >
                          <Button icon='edit' shape='circle' size='small' style={{marginRight: '5px'}} />
                        </Link>
                        {MAPHUBS_CONFIG.APP_ENABLED &&
                          <Button
                            icon='mobile' shape='circle' size='small'
                            style={{marginRight: '5px'}}
                            onClick={() => {
                              this.handleOpenMobileModal(record)
                            }}
                          />}
                        <Divider type='vertical' />
                        <DeleteReportButton id={record.id} t={t} />
                      </span>
                    )
                  }]

                  return (
                    <>
                      <Row style={{height: 'calc(100% - 50px)'}}>
                        <AutoSizer disableWidth>
                          {({ height }) => (
                            <Table dataSource={reports} columns={columns} size='small' scroll={{ y: height - 100 }} />
                          )}
                        </AutoSizer>
                      </Row>
                      <div style={{position: 'absolute', top: '10px', right: '10px'}}>
                        <Mutation
                          query={gql`
                            mutation createReport($organization_id: String!) {
                              createReport(organization_id: $organization_id) {
                                id
                              }
                            }
                          `}
                          onComplete={(data) => {
                            console.log(data)
                            Router.push(`/report/edit/${data.createReport.id}`)
                          }}
                        >
                          {(createReport) => (
                            <Button
                              type='primary'
                              onClick={() => {
                                createReport({organization_id: 'DEFAULT'})
                              }}
                            >{t('New Report')}
                            </Button>
                          )}
                        </Mutation>
                      </div>
                      <Modal
                        title='Forest Report App'
                        visible={showMobile}
                        onCancel={this.handleCloseMobileModal}
                        footer={false}
                        style={{ top: 20 }}
                      >
                        <Steps current={currentMobileStep} onChange={(current) => this.setState({currentMobileStep: current})}>
                          <Step key='download' title={t('Get the App')} />
                          <Step key='sync' title={t('Download the Report')} />
                        </Steps>
                        <div>
                          {
                            currentMobileStep === 0 &&
                              <div style={{textAlign: 'center'}}>
                                <p style={{marginTop: '20px'}}>{t('Scan from phone to install the app')}</p>
                                <Row style={{marginTop: '20px'}}>
                                  <QRCode
                                    level='Q'
                                    style={{ width: 256 }}
                                    value={MAPHUBS_CONFIG.APP_DOWNLOAD_URL}
                                  />
                                  <p>{t('Latest version:')} {MAPHUBS_CONFIG.APP_VERSION}</p>
                                  <p>{t('or')} <a href={MAPHUBS_CONFIG.APP_DOWNLOAD_URL}>{t('click here')}</a> {t('to download the APK')}</p>
                                </Row>
                                <Row style={{marginTop: '20px'}}>
                                  <Button type='primary' onClick={() => this.setState({currentMobileStep: 1})}>
                                    {t('I have the app')}
                                  </Button>
                                </Row>
                              </div>
                          }
                          {
                            currentMobileStep === 1 &&
                              <div style={{textAlign: 'center'}}>
                                <p style={{marginTop: '20px'}}>{t('Click Add Report in the app and scan the QR code below')}</p>
                                <Row style={{marginTop: '20px'}}>
                                  <QRCode
                                    level='Q'
                                    style={{ width: 256 }}
                                    value={JSON.stringify({
                                      apiURL: MAPHUBS_CONFIG.API_URL,
                                      id: selectedReport?.id,
                                      userID: user?.id,
                                      apiKey: user?.apiKey
                                    })}
                                  />
                                </Row>
                                <Row style={{marginTop: '20px'}}>
                                  <Button style={{ marginLeft: 8 }} onClick={() => this.setState({currentMobileStep: 0})}>
                                    {t('Back')}
                                  </Button>
                                </Row>
                              </div>
                          }
                        </div>
                      </Modal>
                    </>
                  )
                }}
              </Query>
            )
          }}
        </Subscribe>
      </Layout>
    )
  }
}
