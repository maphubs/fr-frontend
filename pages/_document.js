import * as React from 'react'
import Document, { Head, Main, NextScript } from 'next/document'

import config from '../config'

export default class MyDocument extends Document {
  render () {
    return (
      <html lang='en'>
        <Head>
          <meta name='viewport' content='width=device-width, initial-scale=1' />
          <meta charSet='utf-8' />
          <link rel='apple-touch-icon-precomposed' sizes='57x57' href='/static/icons/apple-touch-icon-57x57.png' />
          <link rel='apple-touch-icon-precomposed' sizes='114x114' href='/static/icons/apple-touch-icon-114x114.png' />
          <link rel='apple-touch-icon-precomposed' sizes='72x72' href='/static/icons/apple-touch-icon-72x72.png' />
          <link rel='apple-touch-icon-precomposed' sizes='144x144' href='/static/icons/apple-touch-icon-144x144.png' />
          <link rel='apple-touch-icon-precomposed' sizes='60x60' href='/static/icons/apple-touch-icon-60x60.png' />
          <link rel='apple-touch-icon-precomposed' sizes='120x120' href='/static/icons/apple-touch-icon-120x120.png' />
          <link rel='apple-touch-icon-precomposed' sizes='76x76' href='/static/icons/apple-touch-icon-76x76.png' />
          <link rel='apple-touch-icon-precomposed' sizes='152x152' href='/static/icons/apple-touch-icon-152x152.png' />
          <link rel='icon' type='image/png' href='/static/icons/favicon-196x196.png' sizes='196x196' />
          <link rel='icon' type='image/png' href='/static/icons/favicon-96x96.png' sizes='96x96' />
          <link rel='icon' type='image/png' href='/static/icons/favicon-32x32.png' sizes='32x32' />
          <link rel='icon' type='image/png' href='/static/icons/favicon-16x16.png' sizes='16x16' />
          <link rel='icon' type='image/png' href='/static/icons/favicon-128.png' sizes='128x128' />
          <meta name='application-name' content='&nbsp;' />
          <meta name='msapplication-TileColor' content='#FFFFFF' />
          <meta name='msapplication-TileImage' content='/static/icons/mstile-144x144.png' />
          <meta name='msapplication-square70x70logo' content='/static/icons/mstile-70x70.png' />
          <meta name='msapplication-square150x150logo' content='/static/icons/mstile-150x150.png' />
          <meta name='msapplication-wide310x150logo' content='/static/icons/mstile-310x150.png' />
          <meta name='msapplication-square310x310logo' content='/static/icons/mstile-310x310.png' />

          <script
            type='text/javascript' dangerouslySetInnerHTML={{
              __html: `
              var MAPHUBS_CONFIG = {
                AUTH0_DOMAIN: "${config.AUTH0_DOMAIN}",
                AUTH0_CLIENT_ID: "${config.AUTH0_CLIENT_ID}",
                AUTH0_CALLBACK_URL: "${config.AUTH0_CALLBACK_URL}",
                API_URL: "${config.API_URL}",
                MAPBOX_ACCESS_TOKEN: "${config.MAPBOX_ACCESS_TOKEN}",
                OPENROUTESERVICE_API_KEY: "${config.OPENROUTESERVICE_API_KEY}",
                EARTHENGINE_CLIENTID: "${config.EARTHENGINE_CLIENTID}",
                logoSmall: "${config.logoSmall}",
                logoSmallWidth: "${config.logoSmallWidth}",
                logoSmallHeight: "${config.logoSmallHeight}",
                primaryColor: "${config.primaryColor}"
              }
              `
            }}
          />
          <link href='https://fonts.googleapis.com/css?family=Roboto:300,400,500,700,900&display=swap' rel='stylesheet' />
        </Head>
        <body>
          <Main />
          <NextScript />
          <script
            type='text/javascript' dangerouslySetInnerHTML={{
              __html: `
              Userback = window.Userback || {};
              Userback.access_token = '4787|6506|RgiVFuqtlpoFGgSIaXOnXXCwp21uxY9nDFXK8dnA6eNxb4jfph';
              (function(id) {
                  var s = document.createElement('script');
                  s.async = 1;s.src = 'https://static.userback.io/widget/v1.js';
                  var parent_node = document.head || document.body;parent_node.appendChild(s);
              })('userback-sdk');
          `
            }}
          />
        </body>
      </html>
    )
  }
}
