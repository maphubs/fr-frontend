//  @flow
import * as React from 'react'
import Head from 'next/head'
import { Button } from 'antd'
import getConfig from 'next/config'
const MAPHUBS_CONFIG = getConfig().publicRuntimeConfig

type Props = {
  t: Function
}

export default function SignupComplete ({t}: Props) {
  return (
    <div>
      <Head>
        <title>Signup Complete</title>
      </Head>
      <div>
        <style jsx>{`
          .wrapper {
            display: flex;
            align-items: center;
            justify-content: center;
            height: 100vh;
          }
        `}
        </style>
        <div className='wrapper'>
          <img src='https://hpvhe47439ygwrt.belugacdn.link/forestreport/FR_LOGO_JUNE2019.png' style={{position: 'absolute', top: 'calc(50% - 200px)'}} height={94} width={128} alt='Forest Report Logo' />
          <div style={{textAlign: 'center'}}>
            <h3>Signup Complete!</h3>
            <br />
            <Button type='primary' href={`https://${MAPHUBS_CONFIG.AUTH0_DOMAIN}/logout?client_id=${MAPHUBS_CONFIG.AUTH0_CLIENT_ID}&returnTo=${MAPHUBS_CONFIG.host}/`}>Login To Get Started</Button>
            <br />
            <br />
            <p>For help with your account please contact <a href='mailto:support@maphubs.com'>support@maphubs.com</a></p>
          </div>
        </div>
      </div>
    </div>
  )
}
