//  @flow
import * as React from 'react'
import Head from 'next/head'

type Props = {
  t: Function
}

export default function InviteRequired ({t}: Props) {
  return (
    <div>
      <Head>
        <title>Invite Required</title>
      </Head>
      <div>
        <style jsx>{`
          .wrapper {
            display: flex;
            align-items: center;
            justify-content: center;
            height: 100vh;
          }
        `}
        </style>
        <div className='wrapper'>
          <img src='/static/FR_LOGO_JUNE2019.png' style={{position: 'absolute', top: 'calc(50% - 200px)'}} height={94} width={128} alt='Forest Report Logo' />
          <div style={{textAlign: 'center'}}>
            <h3>An invitation is required to access this site.</h3>
            <br />
            <p>For help with your account please contact <a href='mailto:support@maphubs.com'>support@maphubs.com</a></p>
          </div>
        </div>
      </div>
    </div>
  )
}
