//  @flow
import * as React from 'react'
import { notification } from 'antd'
import {Query} from 'urql'
import gql from 'graphql-tag'
import Layout from '../components/Layout'
import Spinner from '../components/Spinner'

import dynamic from 'next/dynamic'
const AOIDashboard = dynamic(() => import('../components/aoi/AOIDashboard'), {
  ssr: false
})

type Props = {
  aoiID: string,
  listID: string,
  listName: string,
  name: string,
  t: any
}

type State = {
  name: Object
}

type QueryResult = {
  loading: boolean,
  error?: {
    message: string
  },
  data: {
    aoi: Object,
    list: Object,
    aoiResults: Array<Object>
  }
}

const getQuery = (aoiID: string, listID: string, reportID: string) => {
  let reportQuery = ''
  if (reportID) {
    reportQuery = `
      aoiReport(report_id: "${reportID}", aoi_id: "${aoiID}") {
        drawings
        notes
        results
        config
      }
    `
  }
  return gql`
    {
      aoi(id: "${aoiID}") {
        id
        properties
        feature
      }
      list(id: "${listID}") {
        id
        name
        modules {
          id
          name
          config
          layer
          card
        }
        config
      }
      aoiResults(id: "${aoiID}") {
        module {
          id
        }
        result_id
        summary
        details
      }
      reportsByAOI (aoi_id: "${aoiID}") {
        id
        title
      }
      reports (status: "draft") {
        id
        title
        status
      }
      ${reportQuery}
    }
  `
}

export default class AOI extends React.Component<Props, State> {
  static async getInitialProps ({req, query}: {req: any, query: any}) {
    return {
      aoiID: query.aoiID,
      listID: query.listID,
      listName: query.listName,
      name: query.name
    }
  }

  render () {
    const {aoiID, listID, name, t} = this.props
    let reportID
    if (typeof location !== 'undefined' && location.hash.split('reportID=').length > 1) {
      reportID = location.hash.split('reportID=')[1].split('&')[0]
    }
    return (
      <Layout
        title={`${name} - Forest Report`}
        t={t} initialLanguage="en" changeLanguage={() => {}}
        padding={0}
      >
        <Query
          query={getQuery(aoiID, listID, reportID)}
        >
          {({fetching, error, data}) => {
            if (fetching) return <Spinner />
            if (error) {
              notification.error({
                message: 'Error',
                description: error.message,
                duration: 0
              })
              return ''
            }
            if (!data.aoi) return <p>No Data</p>

            const {aoi, list, aoiResults, reports, reportsByAOI} = data

            return (
              <AOIDashboard aoi={aoi} results={aoiResults} list={list} reports={reports} currentReports={reportsByAOI} editingReportID={reportID} editAOIReport={data.aoiReport} t={t} locale="en" />
            )
          }}
        </Query>
      </Layout>
    )
  }
}
