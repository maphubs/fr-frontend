//  @flow
import * as React from 'react'
import { Row, Divider } from 'antd'
import Layout from '../components/Layout'
import Query from '../components/Query'
import gql from 'graphql-tag'
import Spinner from '../components/Spinner'
import ListsCarousel from '../components/list/ListsCarousel'
import { getRecentlyViewed } from '../utils/recently-viewed-lists'
type Props = {
  t: any,
  initialLanguage: string
}

export default class Home extends React.Component<Props, void> {
  static async getInitialProps ({req, query}: {req: any, query: any}) {
    return {}
  }

  render () {
    const {t, initialLanguage} = this.props

    return (
      <Layout
        title='Forest Report'
        t={t} initialLanguage={initialLanguage}
        collapsed={false} padding={0}
      >
        <Query
          query={gql`
            {
              lists (published: true) {
                id
                name
                description
                logo
                aoiCount
              }
            }
          `}
        >
          {({ fetching, error, data }) => {
            if (fetching) return <Spinner />
            if (error) {
              return <p>{error}</p>
            }
            const { lists } = data
            const recentlyViewedIDs = getRecentlyViewed()
            const recentlyViewedLists = []
            if (recentlyViewedIDs && recentlyViewedIDs.length > 0) {
              lists.forEach(list => {
                if (recentlyViewedIDs.includes(list.id)) {
                  recentlyViewedLists.push(list)
                }
              })
            }

            return (
              <div style={{padding: '20px', height: '100%', overflow: 'auto'}}>
                <Row style={{marginBottom: '20px'}}>
                  <Divider orientation='left'>{t('Lists')}</Divider>
                  <ListsCarousel lists={lists} t={t} />
                </Row>
                {(recentlyViewedLists && recentlyViewedLists.length > 0) &&
                  <Row style={{marginBottom: '20px'}}>
                    <Divider orientation='left'>{t('Recently Viewed')}</Divider>
                    <ListsCarousel lists={recentlyViewedLists} t={t} />
                  </Row>}
              </div>
            )
          }}
        </Query>
      </Layout>
    )
  }
}
