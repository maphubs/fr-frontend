// @flow
import * as React from 'react'
import Error from 'next/error'

type Props = {
  statusCode: any
}

export default class CustomError extends React.Component<Props, void> {
  static getInitialProps ({ res, err }: {res: any, err: any}) {
    const statusCode = res ? res.statusCode : (err ? err.statusCode : null)
    return {
      statusCode
    }
  }

  render () {
    const { statusCode } = this.props
    return (
      <Error statusCode={statusCode} />
    )
  }
}
