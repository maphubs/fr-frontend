//  @flow
import * as React from 'react'
import { Row, Col, PageHeader } from 'antd'
import Query from '../../components/Query'
import gql from 'graphql-tag'
import Router from 'next/router'
import turfbbox from '@turf/bbox'
import AutoSizer from 'react-virtualized-auto-sizer'
import Layout from '../../components/Layout'
import Spinner from '../../components/Spinner'
import { addRecentlyViewed } from '../../utils/recently-viewed-lists'
import ListSummaryStats from '../../components/list/ListSummaryStats'
import ListRadiusSlider from '../../components/list/ListRadiusSlider'
import ListMap from '../../components/list/map/ListMap'
import ListDownload from '../../components/list/ListDownload'
import ListSummaryButton from '../../components/list/ListSummaryButton'
import ListTable from '../../components/list/ListTable'
import ListDashboardContainer from '../../containers/ListDashboardContainer'
import ListDataFieldSelection from '../../components/list/ListDataFieldSelection'
import { Provider } from 'unstated'
// import getConfig from 'next/config'
// const MAPHUBS_CONFIG = getConfig().publicRuntimeConfig

type Props = {
  id: string,
  name: string,
  t: any,
  initialLanguage: string,
}

export default class List extends React.Component<Props, void> {
  static async getInitialProps ({req, query}: {req: any, query: any}) {
    return {
      id: query.id,
      name: query.name
    }
  }

  listDashboardContainer: any
  mapComponent: any

  componentDidMount () {
    addRecentlyViewed(this.props.id)
  }

  render () {
    const { id, name, t, initialLanguage } = this.props

    const title = name || id

    return (
      <Layout
        title={`List - ${title} - Forest Report`}
        padding={0}
        t={t} initialLanguage={initialLanguage}
      >
        <Query
          query={gql`
            {
              list(id: "${id}") {
                id
                name
                config
                bbox
                modules {
                  id
                  name
                  config
                  driver {
                    id
                    version
                  }
                  card
                  attribution
                  summary_stats
                }
              }
              listResults(list_id: "${id}") {
                result_id
                aoi {
                  id
                  properties
                  rating
                }
                results {
                  result_id
                  module {
                    id
                    name
                    config
                  }
                  summary
                }
              }
            }
          `}
        >
          {({ fetching, data }) => {
            if (fetching) return <div style={{height: '100vh'}}><Spinner /></div>
            if (!data?.list || !data?.listResults) return <p>No Data</p>

            const { config, bbox, modules } = data.list
            // convert from geom to bbox
            let bboxCoords
            if (bbox) {
              bboxCoords = turfbbox({
                type: 'FeatureCollection',
                features: [
                  {
                    type: 'Feature',
                    geometry: bbox
                  }
                ]
              })
            }

            this.listDashboardContainer = new ListDashboardContainer({
              config,
              modules,
              listResults: data.listResults,
              t
            })

            return (
              <Provider inject={[this.listDashboardContainer]}>
                <Row style={{height: '43px'}}>
                  <PageHeader
                    style={{padding: '5px'}}
                    onBack={() => {
                      Router.push({
                        pathname: '/',
                        query: { }
                      })
                    }}
                    title={title}
                    subTitle=''
                  />
                  <div style={{position: 'absolute', top: 0, right: '10px', width: '400px'}}>
                    <ListRadiusSlider />
                  </div>
                </Row>
                <Row style={{ backgroundColor: '#F4F5F8', height: 'calc(100% - 43px)' }}>
                  <Col span={10} style={{height: '100%'}}>
                    <Row style={{height: '60%'}}>
                      <AutoSizer>
                        {({ height, width }) => (
                          <ListMap
                            ref={el => { this.mapComponent = el }}
                            t={t}
                            width={width} height={height}
                            bbox={bboxCoords}
                            mapStyle='mapbox://styles/mapbox/streets-v11'
                            list_id={id}
                          />
                        )}
                      </AutoSizer>
                    </Row>
                    <Row style={{height: '40%'}}>
                      <ListSummaryStats t={t} list_id={id} list_name={name} />
                    </Row>
                  </Col>
                  <Col span={14} style={{height: '100%'}}>
                    <Row style={{height: '50px', backgroundColor: '#FFF', paddingLeft: '20px', paddingRight: '20px', textAlign: 'right'}}>
                      <Col span={16} style={{ textAlign: 'left' }}>
                        <ListDataFieldSelection />
                      </Col>
                      <Col span={8} style={{ textAlign: 'right' }}>
                        {
                          config.aggFields &&
                            <span style={{marginRight: '10px'}}>
                              <ListSummaryButton t={t} id={id} name={name} aggFields={config.aggFields} />
                            </span>
                        }
                        <ListDownload name={name} />
                      </Col>
                    </Row>
                    <Row style={{ height: 'calc(100% - 50px)', backgroundColor: '#F4F5F8' }}>
                      <ListTable />
                    </Row>
                  </Col>
                </Row>
              </Provider>
            )
          }}
        </Query>
      </Layout>
    )
  }
}
