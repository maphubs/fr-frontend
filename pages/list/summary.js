//  @flow
import * as React from 'react'
import { Row, Col, PageHeader } from 'antd'
import Query from '../../components/Query'
import gql from 'graphql-tag'
import Router from 'next/router'
import { Provider } from 'unstated'
import AutoSizer from 'react-virtualized-auto-sizer'
import turfbbox from '@turf/bbox'
import Layout from '../../components/Layout'
import Spinner from '../../components/Spinner'
import ListRadiusSlider from '../../components/list/ListRadiusSlider'
import ListDashboardContainer from '../../containers/ListDashboardContainer'
import ListDownload from '../../components/list/ListDownload'
import ListMap from '../../components/list/map/ListMap'
import ListTable from '../../components/list/ListTable'
import ListSummaryStats from '../../components/list/ListSummaryStats'
import ListDataFieldSelection from '../../components/list/ListDataFieldSelection'
// import getConfig from 'next/config'
// const MAPHUBS_CONFIG = getConfig().publicRuntimeConfig

type Props = {
  id: string,
  aggField: string,
  name: string,
  t: any,
  initialLanguage: string
}

export default class ListSummary extends React.Component<Props, void> {
  static async getInitialProps ({req, query}: {req: any, query: any}) {
    return {
      id: query.id,
      aggField: query.aggField,
      name: query.name
    }
  }

  listDashboardContainer: any
  nameSearchInput: any
  mapComponent: any

  render () {
    const { id, name, aggField, t, initialLanguage } = this.props
    const title = name || id

    return (
      <Layout
        title={`List - ${title} - Forest Report`}
        padding={0}
        t={t} initialLanguage={initialLanguage}
      >
        <Query
          query={gql`
            {
              list(id: "${id}") {
                id
                name
                config
                bbox
                modules {
                  id
                  name
                  config
                  driver {
                    id
                    version
                  }
                  card
                  attribution
                  summary_stats
                }
              }
              listResultsAgg(list_id: "${id}", agg_field: "${aggField}") {
                agg_id
                result_id
                children {
                  result_id
                  aoi {
                    id
                    properties
                    rating
                    centroid
                  }
                  results {
                    result_id
                    module {
                      id
                    }
                    summary
                  }
                }
                results {
                  result_id
                  module {
                    id
                  }
                  summary
                }
              }
            }
          `}
        >
          {({ fetching, data }) => {
            if (fetching) return <div style={{height: '100vh'}}><Spinner /></div>

            if (!data?.list) return <p>No Data</p>

            const { config, bbox, modules } = data.list
            // convert from geom to bbox
            let bboxCoords
            if (bbox) {
              bboxCoords = turfbbox({
                type: 'FeatureCollection',
                features: [
                  {
                    type: 'Feature',
                    geometry: bbox
                  }
                ]
              })
            }

            this.listDashboardContainer = new ListDashboardContainer({
              config,
              modules,
              listResultsAgg: data.listResultsAgg,
              aggField,
              t
            })

            return (
              <Provider inject={[this.listDashboardContainer]}>
                <Row>
                  <PageHeader
                    style={{padding: '5px'}}
                    onBack={() => {
                      Router.push({
                        pathname: `/list/${id}/${name}`,
                        query: { id, name }
                      })
                    }}
                    title={title}
                    subTitle={aggField}
                  />
                  <div style={{position: 'absolute', top: 0, right: '10px', width: '400px'}}>
                    <ListRadiusSlider />
                  </div>
                </Row>
                <Row style={{ height: 'calc(100% - 32px)', backgroundColor: '#F4F5F8' }}>
                  <Col span={10} style={{height: '100%'}}>
                    <Row style={{height: '60%'}}>
                      <AutoSizer>
                        {({ height, width }) => (
                          <ListMap
                            ref={el => { this.mapComponent = el }}
                            t={t}
                            width={width} height={height}
                            bbox={bboxCoords}
                            mapStyle='mapbox://styles/mapbox/streets-v11'
                            list_id={id}
                          />
                        )}
                      </AutoSizer>
                    </Row>
                    <Row style={{height: '40%'}}>
                      <ListSummaryStats t={t} list_id={id} list_name={name} />
                    </Row>
                  </Col>
                  <Col span={14} style={{height: '100%'}}>
                    <Row style={{height: '50px', backgroundColor: '#FFF', paddingLeft: '20px', paddingRight: '20px', textAlign: 'right'}}>
                      <Col span={16} style={{ textAlign: 'left' }}>
                        <ListDataFieldSelection />
                      </Col>
                      <Col span={8} style={{ textAlign: 'right' }}>
                        <ListDownload name={name} />
                      </Col>
                    </Row>
                    <Row style={{ height: 'calc(100% - 50px)', backgroundColor: '#F4F5F8' }}>
                      <ListTable />
                    </Row>
                  </Col>
                </Row>
              </Provider>
            )
          }}
        </Query>
      </Layout>
    )
  }
}
