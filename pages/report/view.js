//  @flow
import * as React from 'react'
import gql from 'graphql-tag'
import Query from '../../components/Query'
import Spinner from '../../components/Spinner'
import CoverPage from '../../components/report/pages/CoverPage'
import NotesPage from '../../components/report/pages/NotesPage'
import TOCPage from '../../components/report/pages/TOCPage'
import AOIOverviewPage from '../../components/report/pages/AOIOverview'
import AOIOverviewAdditionalPage from '../../components/report/pages/AOIOverviewAdditional'
import AOIImageryPage from '../../components/report/pages/AOIImagery'
import AOIObservationPage from '../../components/report/pages/AOIObservationPage'
import AOINotesPage from '../../components/report/pages/AOINotes'
import AttributionsPage from '../../components/report/pages/AttributionsPage'
import LicensePage from '../../components/report/pages/LicensePage'

type Props = {
  id: string,
  title: string,
  t: any,
  initialLanguage: string
}

export default class Report extends React.Component<Props, void> {
  static async getInitialProps ({req, query}: {req: any, query: any}) {
    return {
      id: query.id,
      title: query.title
    }
  }

  render () {
    const {id, t} = this.props
    return (
      <Query
        query={gql`
          {
            report(id: "${id}") {
              id
              title
              subtitle
              author
              monitoring_period
              body
              published_at
              config
              cover_image
              disclaimer
              copyright
              logo
              accent_color
              observations {
                id
                aoi_id
                photo {
                  base64
                  exif
                  width
                  height
                }
                location
                notes
              }
              aois {
                aoi {
                  id
                  properties
                  feature
                }
                modules
                imagery
                drawings
                notes
                config
                overview_screenshot
                module_screenshots
                drawing_screenshots
                priority
              }
            }
          }
        `}
      >
        {({ fetching, data }) => {
          if (fetching) return <Spinner />

          const {title, subtitle, author, monitoring_period, body, aois, published_at, config, cover_image, disclaimer, logo} = data.report
          const templateConfig = {
            accentColor: data.report.accent_color,
            logo,
            copyright: data.report.copyright
          }

          // link observations to aoi
          const observationDict = {}
          if (data.report.observations) {
            data.report.observations.map(obs => {
              if (!observationDict[obs.aoi_id]) observationDict[obs.aoi_id] = []
              observationDict[obs.aoi_id].push(obs)
            })
          }

          let currentPage = 4
          const combinedModules = {}
          let notesPages = []
          let modulesList
          if (body && t(body)) {
            notesPages = t(body).split('{PAGEBREAK}')
          }
          currentPage += notesPages.length
          return (
            <div>
              <CoverPage
                title={title} subtitle={subtitle}
                published_at={published_at}
                author={author}
                monitoring_period={monitoring_period}
                image={cover_image}
                logo={logo}
                t={t}
              />
              <LicensePage pageNumber={2} body={disclaimer} {...templateConfig} t={t} />
              <TOCPage aois={aois} notesPages={notesPages.length} pageNumber={3} {...templateConfig} t={t} />
              {notesPages.map((notes, i) => (<NotesPage key={`notespage-${i}`} notes={notes} pageNumber={4 + i} {...templateConfig} t={t} />))}
              {aois.map((aoi, aoiCount) => {
                const aoiModules = aoi.modules
                const observations = observationDict[aoi.aoi.id] || []
                aoiModules.map(mod => {
                  if (!combinedModules[mod.id]) {
                    combinedModules[mod.id] = mod
                  }
                  { aoi.module_screenshots &&
                    aoi.module_screenshots.forEach(modScreenshot => {
                      if (modScreenshot.module_id === mod.id) {
                        if (modScreenshot.type === 'map') mod.mapScreenshot = modScreenshot.image
                        if (modScreenshot.type === 'card') mod.cardScreenshot = modScreenshot.image
                      }
                    })
                  }
                })

                modulesList = [{
                  id: 'mapbox-basemaps',
                  name: {
                    en: 'Mapbox Base Maps'
                  },
                  attribution: `
                    <p>Thanks to support from the <a href='https://www.mapbox.com/community/'>Mapbox Community</a> team https://www.mapbox.com/community/</p>
                    <p>
                      <img src='/static/mapbox-logo-black.png' style='width: 200px' alt='Mapbox Logo' />
                    </p>
                    <p>
                      <a href="https://www.mapbox.com/about/maps/" target="_blank">© Mapbox</a>&nbsp;
                      <a href="http://www.openstreetmap.org/about/" target="_blank">© OpenStreetMap</a>
                    </p>
                    
                    `
                }]

                modulesList = modulesList.concat(Object.values(combinedModules))

                return (
                  <>
                    <AOIOverviewPage aoi={aoi} modules={[aoiModules[0], aoiModules[1]]} config={config} pageNumber={currentPage++} {...templateConfig} t={t} />
                    {aoiModules.length > 2 &&
                      <AOIOverviewAdditionalPage
                        aoi={aoi}
                        mod1={aoiModules[2]}
                        mod2={aoiModules.length > 3 && aoiModules[3]}
                        mod3={aoiModules.length > 4 && aoiModules[4]}
                        pageNumber={currentPage++}
                        {...templateConfig}
                        t={t}
                      />}
                    {aoiModules.length > 5 &&
                      <AOIOverviewAdditionalPage
                        aoi={aoi}
                        mod1={aoiModules[5]}
                        mod2={aoiModules.length > 6 && aoiModules[6]}
                        mod3={aoiModules.length > 7 && aoiModules[7]}
                        pageNumber={currentPage++}
                        {...templateConfig}
                        t={t}
                      />}
                    {aoiModules.length > 8 &&
                      <AOIOverviewAdditionalPage
                        aoi={aoi}
                        mod1={aoiModules[8]}
                        mod2={aoiModules.length > 9 && aoiModules[9]}
                        mod3={aoiModules.length > 10 && aoiModules[10]}
                        pageNumber={currentPage++}
                        {...templateConfig}
                        t={t}
                      />}
                    {aoi.drawings.drawings &&
                      <AOIImageryPage
                        aoi={aoi}
                        drawing1={aoi.drawings.drawings[0]}
                        drawing2={aoi.drawings.drawings.length > 1 && aoi.drawings.drawings[1]}
                        pageNumber={currentPage++} {...templateConfig}
                        t={t}
                      />}
                    {(aoi.drawings.drawings && aoi.drawings.drawings.length > 2) &&
                      <AOIImageryPage
                        aoi={aoi}
                        drawing1={aoi.drawings.drawings[2]}
                        drawing2={aoi.drawings.drawings.length > 3 && aoi.drawings.drawings[3]}
                        pageNumber={currentPage++} {...templateConfig}
                        t={t}
                      />}
                    {observations.length > 0 &&
                      <AOIObservationPage
                        aoi={aoi}
                        observation1={observations[0]}
                        observation2={observations.length > 1 && observations[1]}
                        observation3={observations.length > 2 && observations[2]}
                        pageNumber={currentPage++} {...templateConfig}
                        t={t}
                      />}
                    {observations.length > 3 &&
                      <AOIObservationPage
                        aoi={aoi}
                        observation1={observations[3]}
                        observation2={observations.length > 4 ? observations[4] : undefined}
                        observation3={observations.length > 5 && observations[5]}
                        pageNumber={currentPage++} {...templateConfig}
                        t={t}
                      />}
                    {observations.length > 6 &&
                      <AOIObservationPage
                        aoi={aoi}
                        observation1={observations[6]}
                        observation2={observations.length > 7 && observations[7]}
                        observation3={observations.length > 8 && observations[8]}
                        pageNumber={currentPage++} {...templateConfig}
                        t={t}
                      />}
                    {observations?.length > 9 &&
                      <AOIObservationPage
                        aoi={aoi}
                        observation1={observations[9]}
                        observation2={observations.length > 10 && observations[10]}
                        observation3={observations.length > 11 && observations[11]}
                        pageNumber={currentPage++} {...templateConfig}
                        t={t}
                      />}
                    {aoi.notes.notes &&
                      <AOINotesPage aoi={aoi} notes={aoi.notes.notes} pageNumber={currentPage++} {...templateConfig} t={t} />}
                  </>
                )
              })}
              {(modulesList && modulesList.length > 0) &&
                <AttributionsPage modules={[modulesList[0], modulesList[1], modulesList[2]]} pageNumber={currentPage++} {...templateConfig} t={t} />}
              {(modulesList && modulesList.length > 3) &&
                <AttributionsPage modules={[modulesList[3], modulesList[4], modulesList[5]]} pageNumber={currentPage++} {...templateConfig} t={t} />}
              {(modulesList && modulesList.length > 6) &&
                <AttributionsPage modules={[modulesList[6], modulesList[7], modulesList[8]]} pageNumber={currentPage++} {...templateConfig} t={t} />}
              {(modulesList && modulesList.length > 9) &&
                <AttributionsPage modules={[modulesList[9], modulesList[10], modulesList[11]]} pageNumber={currentPage++} {...templateConfig} t={t} />}
              <style jsx global>{`
                body {
                  font-family: 'Roboto', sans-serif !important;
                  color: #323333;
                }
                
                .report-notes-wrapper table {
                  width: 100%;
                  font-size: 12px;
                }
                .report-notes-wrapper table th{
                  border:1px solid #323333;
                  padding-left: 5px;
                  background-color: #d9d9d9;
                }
                .report-notes-wrapper table td{
                  border:1px solid #d9d9d9;
                  padding-left:5px;
                }

                .report-notes-wrapper img {
                  width: 100%;
                }
                
              `}
              </style>
            </div>
          )
        }}
      </Query>
    )
  }
}
