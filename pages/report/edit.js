//  @flow
import * as React from 'react'
import { Button, notification, Tabs } from 'antd'
import { Query } from 'urql'
import gql from 'graphql-tag'
import Router from 'next/router'
import slugify from 'slugify'
import Layout from '../../components/Layout'
import Spinner from '../../components/Spinner'
import ReportAOIs from '../../components/report/ReportAOIs'
import ReportObservations from '../../components/report/ReportObservations'
import ReportNotesForm from '../../components/report/ReportNotesForm'
import ReportDisclaimerForm from '../../components/report/ReportDisclaimerForm'
import ReportInfoForm from '../../components/report/ReportInfoForm'
import ReportConfigForm from '../../components/report/ReportConfigForm'

const TabPane = Tabs.TabPane
type Props = {
  id: string,
  title: string,
  t: any,
  initialLanguage: string
}

export default class EditReport extends React.Component<Props, void> {
  static async getInitialProps ({req, query}: {req: any, query: any}) {
    return {
      id: query.id,
      title: query.title
    }
  }

  getQuery = (id: string) => {
    return gql`
    {
      report(id: "${id}") {
        id
        title
        subtitle
        author
        monitoring_period
        body
        config
        status
        published_at
        cover_image
        logo
        accent_color
        disclaimer
        copyright
        observations {
          id
          aoi_id
          photo {
            base64
            exif
            width
            height
          }
          location
          notes
        }
        aois {
          aoi {
            id
            properties
            list {
              id
              name
            }
          }
          priority
        }
      }
    }
  `
  }

  render () {
    const {id, title, t, initialLanguage} = this.props
    const query = this.getQuery(id)
    return (
      <Layout
        title={`Report - ${title || 'New Report'} - Forest Report`}
        crumbs={[
          {href: '/reports', label: 'Reports'},
          {label: title || 'New Report'}
        ]}
        t={t} initialLanguage={initialLanguage}
      >
        <Query
          query={query}
        >
          {({ fetching, error, data }) => {
            if (fetching) return <Spinner />
            if (error) {
              notification.error({
                message: 'Error',
                description: error.message,
                duration: 0
              })
              return ''
            }
            if (!data.report) return <p>{t('No Data')}</p>

            const { id, title, subtitle, author, monitoring_period, config, body, status, published_at, cover_image, logo, accent_color, disclaimer, copyright } = data.report

            return (
              <div style={{height: '100%', width: '100%'}}>
                <style jsx global>{`
                  .ant-tabs-content {
                    height: 100%;
                  }
                  .ant-tabs-tabpane {
                    height: 100%;
                  }
                `}
                </style>
                <div style={{position: 'absolute', top: '10px', right: '10px'}}>
                  <Button
                    type='primary' ghost onClick={() => {
                      Router.push(`/report/view/${id}/${slugify(t(title))}`)
                    }}
                  >{t('View Report')}
                  </Button>
                </div>
                <Tabs animated={false} size='small' type='card' style={{height: '100%'}}>
                  <TabPane tab={t('Info')} key='info'>
                    <ReportInfoForm
                      id={id}
                      status={status}
                      published_at={published_at}
                      title={title}
                      subtitle={subtitle}
                      author={author}
                      monitoring_period={monitoring_period}
                      accent_color={accent_color}
                      logo={logo}
                      cover_image={cover_image}
                      query={query}
                      t={t}
                    />
                  </TabPane>
                  <TabPane tab={t('Areas')} key='aois'>
                    <ReportAOIs report={data.report} aois={data.report.aois} query={query} t={t} />
                  </TabPane>
                  <TabPane tab={t('Observations')} key='observations'>
                    <ReportObservations report={data.report} observations={data.report.observations} query={query} t={t} />
                  </TabPane>
                  <TabPane tab={t('Notes')} key='notes'>
                    <ReportNotesForm notes={body} id={id} t={t} query={query} />
                  </TabPane>
                  <TabPane tab={t('Disclaimer')} key='disclaimer'>
                    <ReportDisclaimerForm disclaimer={disclaimer} copyright={copyright} id={id} t={t} query={query} />
                  </TabPane>
                  <TabPane tab={t('Config')} key='config'>
                    <ReportConfigForm id={id} config={config} t={t} />
                  </TabPane>
                </Tabs>
              </div>
            )
          }}
        </Query>
      </Layout>
    )
  }
}
