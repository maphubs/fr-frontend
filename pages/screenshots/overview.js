//  @flow
import * as React from 'react'
import { notification } from 'antd'
import {Query} from 'urql'
import gql from 'graphql-tag'
import Spinner from '../../components/Spinner'
import getConfig from 'next/config'
import turfbbox from '@turf/bbox'
import { Provider } from 'unstated'
const MAPHUBS_CONFIG = getConfig().publicRuntimeConfig

let Map
let MapContainer
let BaseMapContainer
if (typeof window !== 'undefined') {
  Map = require('@bit/kriscarle.maphubs-components.components.map').default
  MapContainer = require('@bit/kriscarle.maphubs-components.components.map').MapContainer
  BaseMapContainer = require('@bit/kriscarle.maphubs-components.components.map').BaseMapContainer
}

type Props = {
  id: string,
  t: any
}

export default class AOIOverviewMap extends React.Component<Props, void> {
  static async getInitialProps ({req, query}: {req: any, query: any}) {
    return {
      id: query.id
    }
  }

  constructor (props: Props) {
    super(props)
    if (typeof window !== 'undefined') {
      this.MapState = new MapContainer()
      this.BaseMap = new BaseMapContainer({
        bingKey: MAPHUBS_CONFIG.BING_KEY,
        mapboxAccessToken: MAPHUBS_CONFIG.MAPBOX_ACCESS_TOKEN
      })
    }
  }

  MapState: any
  BaseMap: any
  render () {
    const { id, t } = this.props
    // force map page to only render on client
    if (typeof window === 'undefined') return ''
    return (
      <Query
        query={gql`
          {
            aoi(id: "${id}") {
              id
              properties
              feature
            }
          }
        `}
      >
        {({fetching, error, data}) => {
          if (fetching) return <Spinner />
          if (error) {
            notification.error({
              message: 'Error',
              description: error.message,
              duration: 0
            })
            return ''
          }
          if (!data.aoi) {
            notification.error({
              message: 'Error',
              description: 'AOI Not Found',
              duration: 0
            })
            return ''
          }

          const {aoi} = data
          const bbox = turfbbox(aoi.feature)

          return (
            <div style={{width: '640px', height: '480px'}}>
              <Provider inject={[this.MapState, this.BaseMap]}>
                <Map
                  id={id}
                  style={{width: '100%', height: '100%'}}
                  disableScrollZoom
                  fitBounds={bbox}
                  zoom={3}
                  maxZoom={3}
                  glStyle={{
                    version: 8,
                    sources: {
                      aoi: {
                        type: 'geojson',
                        data: aoi.feature
                      }
                    },
                    layers: [
                      {
                        id: 'location',
                        type: 'circle',
                        source: 'aoi',
                        paint: {
                          'circle-color': 'red'
                        }
                      }
                    ]
                  }}
                  t={t}
                  insetMap={false}
                  interactive={false}
                  showScale
                  hash={false}
                  showLogo={false}
                  showPlayButton={false}
                  primaryColor={MAPHUBS_CONFIG.primaryColor}
                  preserveDrawingBuffer
                  mapboxAccessToken={MAPHUBS_CONFIG.MAPBOX_ACCESS_TOKEN}
                  locale="en"
                />
              </Provider>
              <style jsx global>{`
                .map-position {
                  display: none;
                }
                .mapboxgl-ctrl-logo{display:none !important;}
              `}
              </style>
            </div>
          )
        }}
      </Query>
    )
  }
}
