//  @flow
import * as React from 'react'
import { notification } from 'antd'
import {Query} from 'urql'
import gql from 'graphql-tag'
import getConfig from 'next/config'
import { Provider, Subscribe } from 'unstated'
import Spinner from '../../../components/Spinner'
import AOIDashboardContainer from '../../../containers/AOIDashboardContainer'

const MAPHUBS_CONFIG = getConfig().publicRuntimeConfig

let Map
let MapContainer
let BaseMapContainer
if (typeof window !== 'undefined') {
  Map = require('@bit/kriscarle.maphubs-components.components.map').InteractiveMap
  MapContainer = require('@bit/kriscarle.maphubs-components.components.map').MapContainer
  BaseMapContainer = require('@bit/kriscarle.maphubs-components.components.map').BaseMapContainer
}

type Props = {
  report_id: string,
  aoi_id: string,
  module_id: string,
  result_id: string,
  t: any
}

export default class AOIModuleMap extends React.Component<Props, void> {
  static async getInitialProps ({req, query}: {req: any, query: any}) {
    return {
      report_id: query.report_id,
      aoi_id: query.aoi_id,
      module_id: query.module_id,
      result_id: query.result_id
    }
  }

  constructor (props: Props) {
    super(props)
    if (typeof window !== 'undefined') {
      this.MapState = new MapContainer()
      this.BaseMap = new BaseMapContainer({
        bingKey: MAPHUBS_CONFIG.BING_KEY,
        mapboxAccessToken: MAPHUBS_CONFIG.MAPBOX_ACCESS_TOKEN
      })
    }
  }

  MapState: any
  BaseMap: any
  AOIDashboardContainer: any
  mapComponent: any

  render () {
    const { report_id, aoi_id, module_id, result_id, t, initialLanguage } = this.props
    // force map page to only render on client
    if (typeof window === 'undefined') return ''
    return (
      <Query
        query={gql`
          {
            aoi(id: "${aoi_id}") {
              id
              properties
              feature
            }
            aoiResults(id: "${aoi_id}", module_id: "${module_id}", result_id: "${result_id}") {
              module {
                id
                config
                layer
                card
              }
              result_id
              summary
              details
            }
            aoiReport(report_id: "${report_id}", aoi_id: "${aoi_id}") {
              drawings
              results
              config
            }
          }
        `}
      >
        {({fetching, error, data}) => {
          if (fetching) return <Spinner />
          if (error) {
            notification.error({
              message: 'Error',
              description: error.message,
              duration: 0
            })
            return ''
          }
          if (!data.aoi || !data.aoiResults || data.aoiResults.length === 0 || !data.aoiReport || !data.aoiReport.drawings) {
            notification.error({
              message: 'Error',
              description: 'AOI Module Not Found',
              duration: 0
            })
            return ''
          }

          const {aoi, aoiResults, aoiReport} = data
          const result = aoiResults[0]
          const drawings = aoiReport.drawings.drawings

          // enable the module layer
          if (result.module.card) {
            result.module.card.toggled = true
          }

          this.AOIDashboardContainer = new AOIDashboardContainer({
            aoi,
            list: {modules: [result.module]},
            results: [result],
            resultIDFilter: result_id,
            locale: initialLanguage,
            dashboard: this
          })

          drawings.forEach(drawing => {
            drawing.layer.visible = true
            drawing.layer.toggled = true
            if (!drawing.layer.style.metadata) {
              drawing.layer.style.metadata = {}
            }
            drawing.layer.style.metadata['maphubs:priority'] = 2
            drawing.layer.style.metadata['maphubs:active'] = true
            this.AOIDashboardContainer.addLayer(drawing.layer)
          })

          return (
            <Provider inject={[this.AOIDashboardContainer, this.MapState, this.BaseMap]}>
              <Subscribe to={[AOIDashboardContainer]}>
                {aoiDashboard => {
                  const { glStyle, mapLayers, bbox } = aoiDashboard.state

                  // add drawings layers to style
                  drawings.forEach(drawing => {
                    // enable all layers
                    drawing.layer.style.layers.forEach(drawingLayer => {
                      if (!drawingLayer.layout) drawingLayer.layout = {}
                      drawingLayer.layout.visibility = 'visible'
                    })
                    const drawingSourceID = Object.keys(drawing.layer.style.sources)[0]
                    glStyle.sources[drawingSourceID] = drawing.layer.style.sources[drawingSourceID]
                    glStyle.layers = glStyle.layers.concat(drawing.layer.style.layers)
                  })

                  return (
                    <div style={{width: '640px', height: '480px'}}>
                      <Map
                        ref={(el) => { this.mapComponent = el }}
                        map_id='aoi-report-module-map'
                        height='100%'
                        basemap='default'
                        fitBounds={bbox}
                        style={glStyle}
                        layers={mapLayers}
                        showLegendLayersButton={false}
                        showShareButtons={false}
                        interactive={false}
                        t={t}
                        hideInactive={false}
                        insetMap={false}
                        showLogo={false}
                        primaryColor={MAPHUBS_CONFIG.primaryColor}
                        logoSmall={MAPHUBS_CONFIG.logoSmall}
                        logoSmallHeight={MAPHUBS_CONFIG.logoSmallHeight}
                        logoSmallWidth={MAPHUBS_CONFIG.logoSmallWidth}
                        preserveDrawingBuffer
                        mapboxAccessToken={MAPHUBS_CONFIG.MAPBOX_ACCESS_TOKEN}
                        locale='en'
                      />
                      <style jsx global>{`
                        .map-position {
                          display: none;
                        }
                        .btn-floating {
                          display: none;
                        }
                        .collapsible {
                          padding-left: 0px;
                          list-style-type: none;
                        }
                        .omh-legend{padding-left:2px;padding-right:2px;padding-top:2px;padding-bottom:4px;min-height:20px;}
                        .omh-legend h3{font-size:10px;color:#212121;margin:0px;}
                        .base-map-legend *{color:#212121 !important;}
                        .omh-legend .block{height:15px;width:20px;float:left;margin-right:5px;border:1px solid #888;}
                        .omh-legend .point{height:15px;width:15px;float:left;margin-right:5px;border-radius:50%;border:1px solid #888;}
                        .omh-legend .double-stroke{box-shadow:inset 0 0 0 3px rgba(100,100,100,0.2);}
                        .word-wrap{overflow-wrap:break-word;-ms-word-break:break-all;word-break:break-word;-ms-hyphens:auto;-moz-hyphens:auto;-webkit-hyphens:auto;-webkit-hyphens:auto;-moz-hyphens:auto;-ms-hyphens:auto;hyphens:auto;}
            
            
                        .mapboxgl-canvas{left:0 !important;}
                        .mapboxgl-ctrl{-moz-box-shadow:0 2px 5px 0 rgba(0,0,0,0.16),0 2px 10px 0 rgba(0,0,0,0.12) !important;-webkit-box-shadow:0 2px 5px 0 rgba(0,0,0,0.16),0 2px 10px 0 rgba(0,0,0,0.12) !important;box-shadow:0 2px 5px 0 rgba(0,0,0,0.16),0 2px 10px 0 rgba(0,0,0,0.12) !important;}
                        .mapboxgl-ctrl-bottom-left .mapboxgl-ctrl{margin:0 !important;}
                        .mapboxgl-popup{z-index:200 !important;height:200px;width:150px;}
                        .mapboxgl-popup-content{padding:0 !important;}
                        .mapboxgl-popup-close-button{top:-7px !important;right:-7px !important;z-index:201 !important;background-color:rgba(255,255,255,0.75) !important;color:black !important;border-radius:25px !important;border:1px solid black !important;width:14px !important;height:14px !important;line-height:5px !important;padding-bottom:1px !important;padding-top:0px !important;padding-left:0.5px !important;padding-right:0px !important;}
                        .maphubs-feature-popup{padding:0;}
                        .mapbox-gl-draw_point,.mapbox-gl-draw_line,.mapbox-gl-draw_polygon{border-bottom:none !important;border-right:1px #ddd solid !important;}
                        .mapboxgl-ctrl-logo{display:none !important;}
                        .mapboxgl-ctrl-top-right{top:40px !important;}
                        .maphubs-ctrl-scale{border:none !important;padding:0 !important;background-color:inherit !important;position:relative;height:22px;position:absolute;bottom:5px;right:5px;height:34px;margin:0px !important;}
                        .map-position{height:12px;width:75px;position:absolute;top:0;right:0;background-color:rgba(255,255,255,0.55);font-size:10px;line-height:10px;text-align:center;box-shadow:none !important;color:#333;}
                        .metric-scale{height:12px;font-size:10px;line-height:10px;text-align:center;box-shadow:none !important;background-color:rgba(255,255,255,0.55);border-width:medium 2px 2px;border-style:none solid solid;border-color:#333;padding:0 5px;color:#333;position:absolute;top:12px;right:0;}
                        .imperial-scale{height:12px;font-size:10px;line-height:10px;text-align:center;box-shadow:none !important;background-color:rgba(255,255,255,0.55);border-width:medium 2px 2px;border-style:solid solid none;border-color:#333;padding:0 5px;color:#333;position:absolute;bottom:0;right:0;}
                      `}
                      </style>
                    </div>
                  )
                }}
              </Subscribe>
            </Provider>
          )
        }}
      </Query>
    )
  }
}
