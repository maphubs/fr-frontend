//  @flow
import * as React from 'react'
import { notification } from 'antd'
import {Query} from 'urql'
import gql from 'graphql-tag'
import { Provider } from 'unstated'
import Spinner from '../../../components/Spinner'
import AOIDashboardContainer from '../../../containers/AOIDashboardContainer'
import Cards from '../../../components/Cards'

type Props = {
  report_id: string,
  aoi_id: string,
  module_id: string,
  result_id: string,
  t: any,
  initialLanguage: string
}

type State = {
  ready?: boolean
}

export default class AOIModuleCard extends React.Component<Props, State> {
  static async getInitialProps ({req, query}: {req: any, query: any}) {
    return {
      report_id: query.report_id,
      aoi_id: query.aoi_id,
      module_id: query.module_id,
      result_id: query.result_id
    }
  }

  state = {
    ready: false
  }

  AOIDashboardContainer: any

  onReady = () => {
    if (!this.state.ready) this.setState({ ready: true })
  }

  render () {
    const { aoi_id, module_id, result_id, initialLanguage, t } = this.props
    const { ready } = this.state
    // force  page to only render on client
    if (typeof window === 'undefined') return ''
    return (
      <Query
        query={gql`
          {
            aoi(id: "${aoi_id}") {
              id
              properties
              feature
            }
            aoiResults(id: "${aoi_id}", module_id: "${module_id}", result_id: "${result_id}") {
              module {
                id
                config
                layer
                card
              }
              result_id
              summary
              details
            }
          }
        `}
      >
        {({fetching, error, data}) => {
          if (fetching) return <Spinner />
          if (error) {
            notification.error({
              message: 'Error',
              description: error.message,
              duration: 0
            })
            return ''
          }
          if (!data.aoi || !data.aoiResults) {
            notification.error({
              message: 'Error',
              description: 'AOI Module Not Found',
              duration: 0
            })
            return ''
          }

          const {aoi, aoiResults} = data
          const result = aoiResults[0]

          this.AOIDashboardContainer = new AOIDashboardContainer({
            aoi,
            list: {
              modules: [result.module]
            },
            results: [result],
            resultIDFilter: result_id,
            locale: initialLanguage,
            dashboard: this
          })

          const card = result.module.card
          if (!card.module_id) card.module_id = result.module.id
          const CardComponent = Cards[card.id].Component
          return (
            <Provider inject={[this.AOIDashboardContainer]}>
              <div style={{width: '640px', height: '480px'}}>
                <CardComponent
t={t} report showToggle={false}
                  cardHeadStyle={{
                    display: 'none'
                  }}
                  onReady={this.onReady}
                  cardConfig={card}
                />
                {ready &&
                  <div id='screenshot-ready' style={{display: 'none'}} />}
              </div>
            </Provider>
          )
        }}
      </Query>
    )
  }
}
